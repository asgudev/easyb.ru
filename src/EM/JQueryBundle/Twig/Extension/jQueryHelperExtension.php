<?php

namespace EM\JQueryBundle\Twig\Extension;

use Symfony\Bridge\Twig\Extension;
use Symfony\Component\Form\FormView;

/**
 * HelperExtension extends Twig with form capabilities.
 *
 * @author Shustrik
 */
class jQueryHelperExtension extends \Twig_Extension
{

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            'jq_add_plugins_by_name'      => new \Twig_Function_Method($this, 'jqAddPluginsByName', array(
                'is_safe' => array('html')
            )),
            'jq_add_plugin'               => new \Twig_Function_Method($this, 'jqAddPlugin', array(
                'is_safe' => array('html')
            )),
            'jq_periodically_call_remote' => new \Twig_Function_Method($this, 'jqPeriodicallyCallRemote', array(
                'is_safe' => array('html')
            )),
            'jq_button_to_remote'         => new \Twig_Function_Method($this, 'jqButtonToRemote', array(
                'is_safe' => array('html')
            )),
            'jq_link_to_remote'           => new \Twig_Function_Method($this, 'jqLinkToRemote', array(
                'is_safe' => array('html')
            )),
            'jq_update_element_function'  => new \Twig_Function_Method($this, 'jqUpdateElementFunction', array(
                'is_safe' => array('html')
            )),
            'jq_remote_function'          => new \Twig_Function_Method($this, 'jqRemoteFunction', array(
                'is_safe' => array('html')
            )),
            'jq_form_remote_tag'          => new \Twig_Function_Method($this, 'jqFormRemoteTag', array(
                'is_safe' => array('html')
            )),
            'jq_visual_effect'            => new \Twig_Function_Method($this, 'jqVisualEffect', array(
                'is_safe' => array('html')
            )),
            'jq_submit_to_remote'         => new \Twig_Function_Method($this, 'jqSubmitToRemote', array(
                'is_safe' => array('html')
            )),
            'jq_sortable_element'         => new \Twig_Function_Method($this, 'jqSortableElement', array(
                'is_safe' => array('html')
            )),
            'jq_input_auto_complete_tag'  => new \Twig_Function_Method($this, 'jqInputAutoCompleteTag', array(
                'is_safe' => array('html')
            )),
            'jq_draggable_element'        => new \Twig_Function_Method($this, 'jqDraggableElement', array(
                'is_safe' => array('html')
            )),
            'jq_link_to_function'         => new \Twig_Function_Method($this, 'jqLinkToFunction', array(
                'is_safe' => array('html')
            )),
            'jq_button_to_function'       => new \Twig_Function_Method($this, 'jqButtonToFunction', array(
                'is_safe' => array('html')
            )),
            'jq_javascript_tag'           => new \Twig_Function_Method($this, 'jqJavascriptTag', array(
                'is_safe' => array('html')
            )),
            'jq_javascript_cdata_section' => new \Twig_Function_Method($this, 'jqJavascriptCdataSection', array(
                'is_safe' => array('html')
            )),
            'jq_end_javascript_tag'       => new \Twig_Function_Method($this, 'jqEndJavascriptTag', array(
                'is_safe' => array('html')
            )),
            'javascript_tag'              => new \Twig_Function_Method($this, 'javascriptTag', array(
                'is_safe' => array('html')
            )),
            'end_javascript_tag'          => new \Twig_Function_Method($this, 'endJavascriptTag', array(
                'is_safe' => array('html')
            )),
            'javascript_cdata_section'    => new \Twig_Function_Method($this, 'javascriptCdataSection', array(
                'is_safe' => array('html')
            )),
            'content_tag'                 => new \Twig_Function_Method($this, 'contentTag', array(
                'is_safe' => array('html')
            )),
            'cdata_section'               => new \Twig_Function_Method($this, 'cdataSection', array(
                'is_safe' => array('html')
            )),
        );
    }

    /**
     * Render Function
     *
     * @param array $options
     *
     * @return string
     */
    public function jqPeriodicallyCallRemote($options)
    {
        $frequency = isset($options['frequency']) ? $options['frequency'] : 10; // every ten seconds by default
        $code = 'setInterval(function() {' . $this->jqRemoteFunction($options) . '}, ' . ($frequency * 1000) . ')';

        return $this->javascriptTag($code);
    }

    /**
     * Render Function
     *
     * @param array $options
     *
     * @return string
     */
    public function jqButtonToRemote($name, $options = array(), $html_options = array())
    {
        return $this->jqButtonToFunction($name, $this->jqRemoteFunction($options), $html_options);
    }

    /**
     * Render Function
     *
     * @param array $options
     *
     * @return string
     */
    public function jqLinkToRemote($name, $options = array(), $html_options = array())
    {
        return $this->jqLinkToFunction($name, $this->jqRemoteFunction($options), $html_options);
    }

    /**
     * Render Function
     *
     * @param array $options
     *
     * @return string
     */
    public function jqUpdateElementFunction($element_id, $options = array())
    {
        $content = $this->escapeJavascript(isset($options['content']) ? $options['content'] : '');

        $value = isset($options['action']) ? $options['action'] : 'update';
        switch ($value)
        {
            case 'update':
                $updateMethod = $this->updateMethod(isset($options['position']) ? $options['position'] : '');
                $javascript_function = "jQuery('#$element_id').$updateMethod('$content')";
                break;

            case 'empty':
                $javascript_function = "jQuery('#$element_id').empty()";
                break;

            case 'remove':
                $javascript_function = "jQuery('#$element_id').remove()";
                break;

            default:
                throw new sfException('Invalid action, choose one of update, remove, empty');
        }

        $javascript_function .= ";\n";

        return (isset($options['binding']) ? $javascript_function . $options['binding'] : $javascript_function);
    }

    /**
     * Render Function
     *
     * @param array $options
     *
     * @return string
     */
    public function jqRemoteFunction($options)
    {
        // Defining elements to update
        if (isset($options['update']) && is_array($options['update']))
        {
            // On success, update the element with returned data
            if (isset($options['update']['success']))
            {
                $update_success = "#" . $options['update']['success'];
            }

            // On failure, execute a client-side function
            if (isset($options['update']['failure']))
            {
                $update_failure = $options['update']['failure'];
            }
        }
        else if (isset($options['update']))
        {
            $update_success = "#" . $options['update'];
        }

        // Update method
        $updateMethod = $this->updateMethod(isset($options['position']) ? $options['position'] : '');

        // Callbacks
        if (isset($options['loading']))
        {
            $callback_loading = $options['loading'];
        }
        if (isset($options['complete']))
        {
            $callback_complete = $options['complete'];
        }
        if (isset($options['success']))
        {
            $callback_success = $options['success'];
        }
        if (isset($options['error']))
        {
            $update_failure = $options['error'];
        }

        $execute = 'false';
        if ((isset($options['script'])) && ($options['script'] == '1'))
        {
            $execute = 'true';
        }

        // Data Type
        if (isset($options['dataType']))
        {
            $dataType = $options['dataType'];
        }
        elseif ($execute)
        {
            $dataType = 'html';
        }
        else
        {
            $dataType = 'text';
        }

        // POST or GET ?
        $method = 'POST';
        if ((isset($options['method'])) && (strtoupper($options['method']) == 'GET'))
        {
            $method = $options['method'];
        }

        // async or sync, async is default
        if ((isset($options['type'])) && ($options['type'] == 'synchronous'))
        {
            $type = 'false';
        }

        // Is it a form submitting
        if (isset($options['form']))
        {
            $formData = 'jQuery(this).serialize()';
        }
        elseif (isset($options['submit']))
        {
            $formData = 'jQuery(\'#' . $options['submit'] . '\').serialize()';
        }
        // boutell and JoeZ99: 'with' should not be quoted, it's not useful
        // that way, see the Symfony documentation for the original remote_function
        elseif (isset($options['with']))
        {
            $formData = $options['with'];
        }
        // Is it a link with csrf protection
        // build the function
        $function = "jQuery.ajax({";
        $function .= 'type:\'' . $method . '\'';
        $function .= ',dataType:\'' . $dataType . '\'';
        if (isset($type))
        {
            $function .= ',async:' . $type;
        }
        if (isset($formData))
        {
            $function .= ',data:' . $formData;
        }
        if (isset($update_success) and !isset($callback_success))
        {
            $function .= ',success:function(data, textStatus){jQuery(\'' . $update_success . '\').' . $updateMethod . '(data);}';
        }
        if (isset($update_failure))
        {
            $function .= ',error:function(XMLHttpRequest, textStatus, errorThrown){' . $update_failure . '}';
        }
        if (isset($callback_loading))
        {
            $function .= ',beforeSend:function(XMLHttpRequest){' . $callback_loading . '}';
        }
        if (isset($callback_complete))
        {
            $function .= ',complete:function(XMLHttpRequest, textStatus){' . $callback_complete . '}';
        }
        if (isset($callback_success))
        {
            $function .= ',success:function(data, textStatus){' . $callback_success . '}';
        }
        $function .= ',url:\'' . $options['url'] . '\'';
        $function .= '})';

        if (isset($options['before']))
        {
            $function = $options['before'] . '; ' . $function;
        }
        if (isset($options['after']))
        {
            $function = $function . '; ' . $options['after'];
        }
        if (isset($options['condition']))
        {
            $function = 'if (' . $options['condition'] . ') { ' . $function . '; }';
        }
        if (isset($options['confirm']))
        {
            $function = "if (confirm('" . $this->escapeJavascript($options['confirm']) . "')) { $function; }";
            if (isset($options['cancel']))
            {
                $function = $function . ' else { ' . $options['cancel'] . ' }';
            }
        }

        return $function;
    }

    /**
     * Render Function
     *
     * @param array $options
     *
     * @return string
     */
    public function jqFormRemoteTag($options = array(), $options_html = array())
    {
        $options = $options;
        $options_html = $options_html;

        $options['form'] = true;

        $options_html['onsubmit'] = $this->jqRemoteFunction($options) . '; return false;';
        $options_html['action'] = isset($options_html['action']) ? $options_html['action'] : $options['url'];
        $options_html['method'] = isset($options_html['method']) ? $options_html['method'] : 'post';

        return $this->tag('form', $options_html);
    }

    /**
     * Render Function
     *
     * @param array $options
     *
     * @return string
     */
    public function jqVisualEffect($effect, $element_id = false, $js_options = array())
    {

        //format slide /fade effect name correctly.
        if (preg_match("/^(slide|fade)/i", $effect, $matches))
        {
            $count = strtolower($matches[1]) == 'fade' ? 3 : 4;
            $effect = preg_replace("/(^|_|-)+(.)/e", '', $effect); //remove non alpha char
            $effect = preg_replace('/\ +/', '', $effect); //remove space


            $effect = trim(strtolower($matches[1]) . ucfirst(strtolower(substr($effect, $count))));
        }
        else
        {
            $effect = trim(strtolower($effect));
        }

        $element = $element_id ? "'$element_id'" : 'this';

        //Building speed
        $speed = isset($js_options['speed']) ? is_numeric($js_options['speed']) ? $js_options['speed'] : "'" . $js_options['speed'] . "'" : "'normal'";

        //Building opacty
        $opacity = isset($js_options['opacity']) && is_numeric($js_options['opacity']) ? $js_options['opacity'] >= 0 && $js_options['opacity'] <= 1 ? $js_options['opacity'] : 0.5 : 0.5;

        //Building callback
        $callback = isset($js_options['callback']) ? ", " . $js_options['callback'] : NULL;


        if (in_array($effect, array('hide', 'show', 'slideDown', 'slideUp', 'slideToggle', 'fadeIn', 'fadeOut')))
        {
            return sprintf("jQuery(%s).%s(%s %s );", $element, $effect, $speed, $callback);
        }
        elseif ($effect == "fadeTo")
        {
            return sprintf("jQuery(%s).%s(%s, %s %s);", $element, $effect, $speed, $opacity, $callback);
        }
        else
        {
            return sprintf("jQuery(%s).%s();", $element, $effect);
        }
    }

    /**
     * Render Function
     *
     * @param array $options
     *
     * @return string
     */
    public function jqSubmitToRemote($name, $value, $options = array(), $options_html = array())
    {
        $options = $options;
        $options_html = $options_html;

        if (!isset($options['with']))
        {
            $options['with'] = 'jQuery(this.form.elements).serialize()';
        }

        $options_html['type'] = 'button';
        $options_html['onclick'] = $this->jqRemoteFunction($options) . '; return false;';
        $options_html['name'] = $name;
        $options_html['value'] = $value;

        return $this->tag('input', $options_html);
    }

    /**
     * Render Function
     *
     * @param array $options
     *
     * @return string
     */
    public function jqSortableElement($selector, $options = array())
    {
        jq_add_plugins_by_name(array("ui"));
        $options = $options;
        $options['url'] = url_for($options['url']);
        $options['type'] = 'POST';
        $selector = json_encode($selector);
        $options = json_encode($options);

        $result = <<<EOM
        $(document).ready(
        function()
        {
            $($selector).sortable(
            {
            update: function(e, ui)
            {
                var serial = jQuery($selector).sortable('serialize', {});
                var options = $options;
                options['data'] = serial;
                $.ajax(options);
            }
            } );
        });
EOM;
        return $this->javascriptTag($result);
    }

    /**
     * Render Function
     *
     * @param array $options
     *
     * @return string
     */
    public function jqInputAutoCompleteTag($name, $value, $url, $tag_options = array(), $completion_options = array())
    {
        $tag_options = $tag_options;
        $comp_options = $completion_options;

        // Convert to JSON parameters
        $jsonOptions = '';
        foreach ($comp_options as $key => $val)
        {
            if ($jsonOptions != '')
            {
                $jsonOptions .= ', ';
            }
            switch ($key)
            {
                case 'formatItem':
                case 'formatResult':
                    $jsonOptions .= "$key: " . $val;
                    break;
                default:

                    $jsonOptions .= "$key: $val";
                    break;
            }
        }

        // Get Id from name attribute
        $tag_options['id'] = $tag_options['id'];

        // Add input form
        $javascript = $this->tag('input', array_merge(array('type'  => 'text',
                                                            'class' => 'text',
                                                            'name'  => $name,
                                                            'value' => $value), $tag_options));
        // Calc JQuery Javascript code

        $autocomplete_script = sprintf('jQuery("#%s").autocomplete("%s",{ %s });', $tag_options['id'], $url, $jsonOptions);
        $javascript .= $this->javascriptTag($autocomplete_script);
        echo $javascript;
    }

    /**
     * Render Function
     *
     * @param array $options
     *
     * @return string
     */
    public function jqDraggableElement($selector, $options = array())
    {
        $options = json_encode($options);
        $selector = json_encode($selector);
        return $this->javascriptTag("jQuery($selector).draggable($options)");
    }

    /**
     * Render Function
     *
     * @param array $options
     *
     * @return string
     */
    public function jqLinkToFunction($name, $function, $html_options = array())
    {
        $html_options = $html_options;

        $html_options['href'] = isset($html_options['href']) ? $html_options['href'] : '#';
        if (isset($html_options['confirm']))
        {
            $confirm = $this->escapeJavascript($html_options['confirm']);
            $html_options['onclick'] = "if(confirm('$confirm')){ $function;}; return false;";
            // tom@punkave.com: without this we get a confirm attribute, which breaks confirm() in IE
            // (we could call window.confirm, but there is no reason to have the
            // nonstandard confirm attribute)
            unset($html_options['confirm']);
        }
        else
        {
            $html_options['onclick'] = $function . '; return false;';
        }

        return $this->contentTag('a', $name, $html_options);
    }

    /**
     * Render Function
     *
     * @param array $options
     *
     * @return string
     */
    public function jqButtonToFunction($name, $function, $html_options = array())
    {
        return $this->buttonToFunction($name, $function, $html_options);
    }

    /**
     * Render Function
     *
     * @param array $options
     *
     * @return string
     */
    public function jqJavascriptTag($content)
    {
        return $this->javascriptTag($content);
    }

    /**
     * Render Function
     *
     * @param array $options
     *
     * @return string
     */
    public function jqJavascriptCdataSection($content)
    {
        return $this->javascriptCdataSection($content);
    }


    /**
     * Render Function
     *
     * @param array $options
     *
     * @return string
     */
    public function jqEndJavascriptTag($options)
    {
        return $this->endJavascriptTag();
    }

    public function javascriptTag($content)
    {
        if (NULL !== $content)
        {
            return $this->contentTag('script', $this->javascriptCdataSection($content), array('type' => 'text/javascript'));
        }
        else
        {
            ob_start();
        }
    }

    public function endJavascriptTag()
    {
        echo $this->javascriptTag(ob_get_clean());
    }

    public function javascriptCdataSection($content)
    {
        return "\n//" . $this->cdataSection("\n$content\n//") . "\n";
    }

    public function contentTag($name, $content = '', $options = array())
    {
        if (!$name)
        {
            return '';
        }

        return '<' . $name . $this->tagOptions($options) . '>' . $content . '</' . $name . '>';
    }

    public function tag($name, $options = array())
    {
        if (!$name)
        {
            return '';
        }

        return '<' . $name . $this->tagOptions($options) . '>';
    }

    public function cdataSection($content)
    {
        return "<![CDATA[$content]]>";
    }

    private function tagOptions($options = array())
    {
        $html = '';
        foreach ($options as $key => $value)
        {
            $html .= ' ' . $key . '="' . $value . '"';
        }
        return $html;
    }

    private function updateMethod($position)
    {
        // Updating method
        $updateMethod = 'html';
        switch ($position)
        {
            case 'before':
                $updateMethod = 'before';
                break;
            case 'after':
                $updateMethod = 'after';
                break;
            case 'top':
                $updateMethod = 'prepend';
                break;
            case 'bottom':
                $updateMethod = 'append';
                break;
        }

        return $updateMethod;
    }

    private function escapeJavascript($javascript = '')
    {
        $javascript = preg_replace('/\r\n|\n|\r/', "\\n", $javascript);
        $javascript = preg_replace('/(["\'])/', '\\\\\1', $javascript);

        return $javascript;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'jquery.twig.extension';
    }

}
