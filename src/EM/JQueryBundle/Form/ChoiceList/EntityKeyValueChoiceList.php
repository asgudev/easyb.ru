<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Olivier Chauvel <olivier@generation-multiple.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace EM\JQueryBundle\Form\ChoiceList;

use Symfony\Component\Form\Util\PropertyPath;
use Symfony\Component\Form\Exception\FormException;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Bridge\Doctrine\Form\ChoiceList\EntityChoiceList;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use \Symfony\Bridge\Doctrine\Form\ChoiceList\ORMQueryBuilderLoader;

/**
 * AjaxArrayChoiceList
 *
 * @author Olivier Chauvel <olivier@generation-multiple.com>
 */
class EntityKeyValueChoiceList extends EntityChoiceList
{
    private $ajax;
    private $propertyPath;
    private $classMetadata;

    /**
     * Constructs
     *
     * @param EntityManager  $em
     * @param string         $class
     * @param string         $property
     * @param QueryBuilder   $qb
     * @param array|\Closure $choices
     * @param string         $groupBy
     * @param boolean        $ajax
     */
    public function __construct(\Doctrine\Common\Persistence\ObjectManager $em, $class, $property = null, $qb = null, $choices = null, $ajax = false)
    {
        $this->ajax = $ajax;
        $this->classMetadata = $em->getClassMetadata($class);
        if ($property) {
            $this->propertyPath = new PropertyPath($property);
        }

        $loader = $qb ? new ORMQueryBuilderLoader($qb, $em, $class) : null;

        parent::__construct($em, $class, $property, $loader, $choices, array());
    }

    /**
     * {@inheritdoc}
     */
    protected function load()
    {
        if (!$this->ajax) {
            parent::load();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getChoices()
    {
        $choices = $this->getRemainingViews();

        if (empty($choices)) {
            $choices = array();
        }

        $array = array();
        foreach ($choices as $choice) {
            if ($choice->label) {
                $array[] = trim($choice->label);
            }
        }

        return $array;
    }

    /**
     * {@inheritdoc}
     */
    public function getRemainingViews()
    {
        if ($this->ajax) {
            return array();
        }

        return parent::getRemainingViews();
    }

    /**
     * {@inheritdoc}
     */
    public function getPreferredViews()
    {
        if ($this->ajax) {
            return array();
        }

        return parent::getPreferredViews();
    }

    /**
     * Get intersaction $choices to $ids
     *
     * @param array $ids
     *
     * @return array $intersect
     */
    public function getIntersect(array $ids)
    {
        $intersect = array();

        if ($this->ajax) {
            foreach ($this->getChoicesForValues($ids) as $entity) {

                if ($this->propertyPath) {
                    $label = trim($this->propertyPath->getValue($entity));
                } else {
                    $label =  trim((string)$entity);
                }
                if ($label) {
                    $intersect[] = trim($label);
                }
            }
        } else {
            foreach ($this->getChoices() as $choice) {
                if (in_array($choice['value'], $ids)) {
                    $intersect[] = trim($choice);
                }
            }
        }

        return $intersect;
    }

    public function getChoicesForValues(array $values)
    {
        $choices = $this->getRemainingViews();

        if (empty($choices)) {
            $choices = array();
        }
        $array = array();
        foreach ($choices as $choice) {
            if (in_array(trim($choice->label), $values)) {
                $array[] = trim($choice->value);
            }
        }

        return parent::getChoicesForValues($array);
    }
}
