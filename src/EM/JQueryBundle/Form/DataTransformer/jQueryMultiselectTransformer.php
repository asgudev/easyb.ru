<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Olivier Chauvel <olivier@generation-multiple.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace EM\JQueryBundle\Form\DataTransformer;

use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceListInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\DataTransformerInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Util\PropertyPath;

/**
 * ChoiceToJsonTransformer
 *
 * @author Olivier Chauvel <olivier@generation-multiple.com>
 */
class jQueryMultiselectTransformer implements DataTransformerInterface
{
    private $choiceList;
    private $class;
    private $property;
    private $withCreate;

    public function __construct(ChoiceListInterface $choiceList, $class, $property, $withCreate)
    {
        $this->choiceList = $choiceList;
        $this->class = $class;
        $this->property = $property;
        $this->withCreate = $withCreate;
    }

    /**
     * Transforms entities into choice keys.
     *
     * @param  Collection|object $collection A collection of entities, a single entity or NULL
     *
     * @return mixed An array of choice keys, a single key or NULL
     */
    public function transform($collection)
    {
        if (null === $collection) {
            return array();
        }

        if (!($collection instanceof Collection)) {
            throw new UnexpectedTypeException($collection, 'Doctrine\Common\Collections\Collection');
        }
        $tags = array();
        foreach ($collection->getValues() as $tag) {
            $property = new PropertyPath($this->property);
            $tags[] = $property->getValue($tag);
        }

        return implode(',', $tags);
    }

    /**
     * Transforms choice keys into entities.
     *
     * @param  mixed $keys        An array of keys, a single key or NULL
     *
     * @return Collection|object  A collection of entities, a single entity or NULL
     */
    public function reverseTransform($keys)
    {
        $keys = explode(',', $keys);
        $collection = new ArrayCollection();
        if ('' === $keys || null === $keys) {
            return $collection;
        }
        if (!is_array($keys)) {
            throw new UnexpectedTypeException($keys, 'array');
        }
        $notFound = array();
        $property = new PropertyPath($this->property);
        $collection = $this->choiceList->getChoicesForValues($keys);
        $choices = $this->choiceList->getChoices();
        foreach ($keys as $key) {
            if (!$key) {
                continue;
            }
            if ($this->withCreate && !in_array($key, $choices)) {
                $item = new $this->class();
                $property->setValue($item, $key);
                $collection[] = $item;
            } elseif (!in_array($key, $choices)) {
                $notFound[] = $key;
            }
        }
        if (count($notFound) > 0) {
            throw new TransformationFailedException(sprintf('The entities with keys "%s" could not be found. Please choice from autocomplete', implode('", "', $notFound)));
        }

        return new ArrayCollection($collection);
    }
}