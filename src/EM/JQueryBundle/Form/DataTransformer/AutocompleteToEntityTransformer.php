<?php
/*
* This file is part of the Symfony package.
*
* (c) Fabien Potencier <fabien@symfony.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace EM\JQueryBundle\Form\DataTransformer;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyPath;

/**
 * AutocompleteToEntityTransformer
 *
 * @author shustrik <vadim.godko@gmail.com>
 */
class AutocompleteToEntityTransformer implements DataTransformerInterface
{

    private $withCreate = false;
    private $entityManager = null;
    private $class = null;
    private $propertyPath = false;
    private $findByTextQuery = false;
    private $accessor;

    public function __construct($withCreate, ObjectManager $entityManager, $class, $propertyPath = false, $findByTextQuery = false)
    {
        $this->withCreate = $withCreate;
        $this->entityManager = $entityManager;
        $this->findByTextQuery = $findByTextQuery;
        $this->class = $class;
        $this->accessor = PropertyAccess::createPropertyAccessor();
        $this->propertyPath = $propertyPath ? $propertyPath : 'name';

    }

    /**
     * Transforms a value from the original representation to a transformed representation.
     *
     * This method is called on two occasions inside a form field:
     *
     * 1. When the form field is initialized with the data attached from the datasource (object or array).
     * 2. When data from a request is bound using {@link Field::bind()} to transform the new input data
     *    back into the renderable format. For example if you have a date field and bind '2009-10-10' onto
     *    it you might accept this value because its easily parsed, but the transformer still writes back
     *    "2009/10/10" onto the form field (for further displaying or other purposes).
     *
     * This method must be able to deal with empty values. Usually this will
     * be NULL, but depending on your implementation other empty values are
     * possible as well (such as empty strings). The reasoning behind this is
     * that value transformers must be chainable. If the transform() method
     * of the first value transformer outputs NULL, the second value transformer
     * must be able to process that value.
     *
     * By convention, transform() should return an empty string if NULL is
     * passed.
     *
     * @param  mixed $value              The value in the original representation
     *
     * @return mixed                     The value in the transformed representation
     *
     * @throws UnexpectedTypeException   when the argument is not a string
     * @throws TransformationFailedException  when the transformation fails
     */
    function transform($value)
    {
        if ($value && is_object($value)) {
            $identifier = $this->entityManager
                ->getClassMetadata($this->class)
                ->getSingleIdentifierFieldName();
            $method = 'get' . (ucfirst($identifier));
            $entity = $this->entityManager
                ->getRepository($this->class)
                ->findOneBy(array ($identifier => $value->$method()));
            if ($entity) {
                return array (
                    'autocompleter_value' => $entity->$method(),
                    'autocompleter_name'  => $this->accessor->getValue($entity, $this->propertyPath)
                );
            }
        }

        return array (
            'autocompleter_value' => null,
            'autocompleter_name'  => null
        );
    }

    /**
     * Transforms a value from the transformed representation to its original
     * representation.
     *
     * This method is called when {@link Field::bind()} is called to transform the requests tainted data
     * into an acceptable format for your data processing/model layer.
     *
     * This method must be able to deal with empty values. Usually this will
     * be an empty string, but depending on your implementation other empty
     * values are possible as well (such as empty strings). The reasoning behind
     * this is that value transformers must be chainable. If the
     * reverseTransform() method of the first value transformer outputs an
     * empty string, the second value transformer must be able to process that
     * value.
     *
     * By convention, reverseTransform() should return NULL if an empty string
     * is passed.
     *
     * @param  mixed $value              The value in the transformed representation
     *
     * @return mixed                     The value in the original representation
     *
     * @throws UnexpectedTypeException   when the argument is not of the expected type
     * @throws TransformationFailedException  when the transformation fails
     */
    function reverseTransform($value)
    {
        if ($value['autocompleter_value']) {
            $identifier = $this->entityManager
                ->getClassMetadata($this->class)
                ->getSingleIdentifierFieldName();
            $entity = $this->entityManager
                ->getRepository($this->class)
                ->findOneBy(array ($identifier => $value['autocompleter_value']));

            return $entity;
        } elseif ($value['autocompleter_name']) {
            $valueText = mb_strtolower($value['autocompleter_name'], mb_detect_encoding($value['autocompleter_name']));
            $er = $this->entityManager->getRepository($this->class);
            $findByTextQuery = $this->findByTextQuery;
            $entity = ($findByTextQuery instanceof \Closure) ? $findByTextQuery($er, $valueText) : $this->getDefaultQuery($er, $valueText);
            if ($entity) {
                return $entity;
            } elseif ($value['autocompleter_name'] && $this->withCreate) {
                $entity = new $this->class;
                $entity->setName($value['autocompleter_name']);
                $this->entityManager->persist($entity);

                return $entity;
            }
        }

        return null;
    }

    private function getDefaultQuery(EntityRepository $er, $value)
    {
        $er->createQueryBuilder('q')
            ->where(sprintf('lower(q.%s) = :value', $this->propertyPath))
            ->setParameter('value', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }
}