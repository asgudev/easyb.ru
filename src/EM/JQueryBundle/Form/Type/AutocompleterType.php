<?php
/*
 * This file is part of the Symfony package.
 *
 * (c) Olivier Chauvel <olivier@generation-multiple.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace EM\JQueryBundle\Form\Type;

use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use EM\JQueryBundle\Form\DataTransformer\AutocompleteToEntityTransformer;
use Symfony\Component\OptionsResolver\Options;

/**
 * @author Olivier Chauvel <olivier@generation-multiple.com>
 */
class AutocompleterType extends AbstractType
{

    protected $registry;

    public function __construct(RegistryInterface $registry)
    {
        $this->registry = $registry;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addViewTransformer(new AutocompleteToEntityTransformer($options['with_create'], $this->registry->getManager($options['em']), $options['class'], $options['property'], $options['find_text_query']))
            ->add('autocompleter_value', 'hidden', array ('attr' => $options['attr']))->add('autocompleter_name', 'text', array ('attr' => $options['attr']));
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['ajax'] = $options['ajax'];
        $view->vars['success'] = $options['success'];
        $view->vars['route_name'] = $options['route_name'];
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $defaultOptions = array (
            'widget'          => 'choice',
            'route_name'      => null,
            'ajax'            => function (Options $options) {
                return $options['route_name'] ? true : false;
            },
            'success'         => '',
            'find_text_query' => false,
            'with_create'     => false,
            'em'              => null,
            'error_bubbling'  => false,
            'property'        => false,
            'default_value'   => '',
            'default_name'    => '',
        );

        $resolver->setDefaults($defaultOptions);
        $resolver->setRequired(array ('class'));
    }

    public function getParent()
    {
        return 'form';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'em_autocompleter';
    }
}