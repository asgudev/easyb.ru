<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Olivier Chauvel <olivier@generation-multiple.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace EM\JQueryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use EM\JQueryBundle\Form\ChoiceList\EntityKeyValueChoiceList;
use EM\JQueryBundle\Form\DataTransformer\jQueryMultiselectTransformer;


use Symfony\Bridge\Doctrine\Form\EventListener\MergeDoctrineCollectionListener;
use \Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * @author Vadim Golodko <vadim.goldoko@gmail.com>
 */
class MultiselectType extends AbstractType
{

    protected $registry;

    public function __construct(RegistryInterface $registry)
    {
        $this->registry = $registry;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventSubscriber(new MergeDoctrineCollectionListener())->addViewTransformer(new jQueryMultiselectTransformer($options['choice_list'], $options['class'], $options['property'], $options['enable_new_options']));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $registry = $this->registry;
        $defaultOptions = array('em'                        => NULL,
                                'class'                     => NULL,
                                'property'                  => NULL,
                                'query_builder'             => NULL,
                                'choices'                   => NULL,
                                'choice_list'               => function (\Symfony\Component\OptionsResolver\Options $options, $previousValue) use ($registry)
                                {
                                    return new EntityKeyValueChoiceList($registry->getManager($options['em']), $options['class'], $options['property'], $options['query_builder'], $options['choices']);
                                },
                                'enable_new_options'        => true,);

        $resolver->setDefaults($defaultOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['choices'] = $options['choice_list']->getChoices();
        $view->vars['enable_new_options'] = $options['enable_new_options'];
    }


    public function getParent()
    {
        return 'text';
    }

    public function getName()
    {
        return 'em_jquery_multiselect';
    }
}
