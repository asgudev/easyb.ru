<?php
/**
 * This file is part of easyb package.
 *
 * (c) Evgeniy Marchenkov <evgeniy.public@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Easyb\AdvertBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class AbstractTextLengthCheck extends Constraint
{
    /**
     * @var string
     */
    public $offerMessage = 'Правило № 2. Запрещено размещать текст менее чем на 500 знаков и более чем на 2000 знаков. Причина: Объем текста прямо пропорционально влияет на поисковую выдачу. Чем информативнее текст, тем он полезнее, тем выше он в выдаче поисковиков.';

    /**
     * @var string
     */
    public $demandMessage = 'Правило № 2. Запрещено размещать текст менее чем на 500 знаков. Причина: Объем текста прямо пропорционально влияет на поисковую выдачу. Чем информативнее текст, тем он полезнее, тем выше он в выдаче поисковиков.';

    /**
     * @var string
     */
    public $charset = 'UTF-8';

    /**
     * @return string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}