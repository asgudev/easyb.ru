<?php
/**
 * This file is part of easyb package.
 *
 * (c) Evgeniy Marchenkov <evgeniy.public@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Easyb\AdvertBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Easyb\AdvertBundle\Entity\Demand;
use Symfony\Component\Validator\ConstraintValidator;

class DemandExpireAtCheckValidator extends ConstraintValidator
{
    /**
     * @param Demand     $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        $now = new \DateTime();
        $expireAt = $value->getExpireAt();
        $endExpireAt = $now->modify('+30 days');
/*
        If ($expireAt > $endExpireAt) {
            $this->context->addViolationAt(
                'expireAt',
                $constraint->message,
                array(),
                null
            );
        }*/
    }
}