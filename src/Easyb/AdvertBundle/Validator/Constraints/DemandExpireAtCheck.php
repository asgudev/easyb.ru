<?php
/**
 * This file is part of easyb package.
 *
 * (c) Evgeniy Marchenkov <evgeniy.public@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Easyb\AdvertBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class DemandExpireAtCheck extends Constraint
{
    /**
     * @var string
     */
    public $message = 'Процедура проведения тендера не может превышать 30 календарных дней от даты его создания.';

    /**
     * @var string
     */
    public $charset = 'UTF-8';

    /**
     * @return string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}