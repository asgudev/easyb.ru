<?php
/**
 * This file is part of easyb package.
 *
 * (c) Evgeniy Marchenkov <evgeniy.public@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Easyb\AdvertBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Easyb\AdvertBundle\Entity\AbstractAdvert;
use Symfony\Component\Validator\ConstraintValidator;

class AbstractTextLengthCheckValidator extends ConstraintValidator
{
    /**
     * @param AbstractAdvert $value
     * @param Constraint     $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        $abstractText = $value->getText();
        $abstractText = str_replace('&nbsp;', '', $abstractText);
        $abstractText = html_entity_decode($abstractText);
        $abstractText = preg_replace('/\s+/', '', $abstractText);
        $abstractText = strip_tags($abstractText);
        $abstractTextLength = iconv_strlen($abstractText, $constraint->charset);

        if ($value->isOffer()) {
            If ($abstractTextLength < 500 || $abstractTextLength > 2000) {
                $this->context->addViolationAt(
                    'text',
                    $constraint->offerMessage,
                    array(),
                    null
                );
            }
        } else {
            If ($abstractTextLength < 500) {
                $this->context->addViolationAt(
                    'text',
                    $constraint->demandMessage,
                    array(),
                    null
                );
            }
        }
    }
}
