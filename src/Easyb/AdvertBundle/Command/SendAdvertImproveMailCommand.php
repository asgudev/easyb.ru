<?php
/**
 * This file is part of easyb package.
 *
 * (c) Evgeniy Marchenkov <evgeniy.public@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Easyb\AdvertBundle\Command;

use Doctrine\ORM\EntityManager;
use Easyb\AdvertBundle\Entity\AbstractAdvert;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Easyb\UserBundle\Entity\Mail;

class SendAdvertImproveMailCommand extends ContainerAwareCommand
{
    /**
     * @see Command
     */
    protected function configure()
    {
        $this
            ->setName('easyb:mail:advert_improve')
            ->setDescription('Sends a mail if there are offer / demand improve notification');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $adverts = $this->getEm()->getRepository('EasybAdvertBundle:AbstractAdvert')->findAdvertsNoImprove15Days();

            /** @var AbstractAdvert $advert */
            foreach ($adverts as $advert) {
                $mail = $advert->isOffer() ? $this->getMail(Mail::TYPE_ADMIN_OFFER_IMPROVE) : $this->getMail(Mail::TYPE_ADMIN_DEMAND_IMPROVE);

                $this->sendEmailData($advert, $mail);

                // Persist offer object for the updating submitted emails
                $this->getEm()->persist($advert);
            }

            $this->getEm()->flush();

            $output->writeln(sprintf('<info>Sends a mail with offer / demand improve notification was successful done!</info>'));
        } catch (\Exception $e) {
            $output->writeln(sprintf('<error>Sends a mail with offer / demand improve notification was unsuccessful done!</error>'));
            $output->writeln(sprintf('<error>%s</error>', $e->getMessage()));
        }
    }

    /**
     * @return EntityManager
     */
    private function getEm()
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @param int $mailType
     *
     * @return Mail
     */
    private function getMail($mailType)
    {
        return $this->getEm()->getRepository('EasybUserBundle:Mail')->findOneByType($mailType);
    }

    /**
     * @param AbstractAdvert $advert
     * @param Mail $mail
     */
    private function sendEmailData(AbstractAdvert $advert, Mail $mail)
    {
        $email = $advert->getUser()->getEmail();
        $template = 'EasybMainBundle:Mail:mail.html.twig';
        $parameters = [
            'subject' => $this->getSubject($mail, $advert),
            'text'    => $this->getText($mail, $advert)
        ];

        $this->sendEmail($template, $parameters, $email);

        // Add submitted email for advert by it's identifier
        $advert->addSubmittedEmail($advert->getId());
    }

    /**
     * @param Mail $mail
     * @param AbstractAdvert $advert
     *
     * @return string
     */
    private function getSubject(Mail $mail, AbstractAdvert $advert)
    {
        $subject = $mail->getSubject();
        return $subject;
    }

    /**
     * @param Mail $mail
     * @param AbstractAdvert $advert
     *
     * @return mixed|string
     */
    private function getText(Mail $mail, AbstractAdvert $advert)
    {
        $router = $this->getContainer()->get('router');

        $text = $mail->getMessage();

        $text = str_replace("[username]", $advert->getUser()->getFio(), $text);

        $text = str_replace("[offer_link]", $router->generate('show_offer', [
            'slug'     => $advert->getSlug(),
            'company'  => $advert->getJob()->getSlug(),
            'city'     => $advert->getJob()->getCity()->getSlug(),
            'category' => $advert->getCategory()->getSlug(),
        ], true), $text);

        $text = str_replace("[demand_link]", $router->generate('show_demand', [
            'slug'     => $advert->getSlug(),
            'company'  => $advert->getJob()->getSlug(),
            'city'     => $advert->getJob()->getCity()->getSlug(),
            'category' => $advert->getCategory()->getSlug(),
        ], true), $text);

        return $text;
    }

    /**
     * @param string $template
     * @param array  $parameters
     * @param string $email
     */
    private function sendEmail($template, $parameters, $email)
    {
        $this->getMailing()->sendEmail($template, $parameters, $email);
    }

    /**
     * @return \Easyb\MainBundle\Mailing\MailingInterface
     */
    private function getMailing()
    {
        return $this->getContainer()->get('easyb.mailer');
    }
}