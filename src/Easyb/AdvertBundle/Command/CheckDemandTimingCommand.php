<?php

namespace Easyb\AdvertBundle\Command;

use Doctrine\Common\Persistence\ObjectManager;
use Easyb\AdvertBundle\Entity\Demand;
use Easyb\AdvertBundle\Entity\Proposal;
use Easyb\AdvertBundle\Entity\Reaction;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class CheckDemandTimingCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            ->setName('easyb:check:demand:timing')
            ->setDescription('Update demand status on non approved when the time has expired');
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $demands = $this->getEm()->getRepository('EasybAdvertBundle:Demand')->findActive();

            $this->checkDemandsTiming($demands, $output);

            $output->writeln(sprintf('<info>Update demand status on non approved when the time has expired was successful done!</info>'));
        } catch (\Exception $e) {
            $output->writeln(sprintf('<error>Update demand status on non approved when the time has expired was unsuccessful done!</error>'));
            $output->writeln(sprintf('<error>%s</error>', $e->getMessage()));
        }
    }

    /**
     * @return ObjectManager
     */
    private function getEm()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }

    /**
     * @return \Easyb\AdvertBundle\Manager\AllAdvertManager
     */
    private function getAllAdvertManager()
    {
        return $this->getContainer()->get('easyb.all_advert.manager');
    }

    /**
     * @param array $demands
     */
    private function checkDemandsTiming(array $demands, $console)
    {
        $allAdvertManager = $this->getAllAdvertManager();
        $dispatcher = $this->getContainer()->get('event_dispatcher');


        //TODO: massive optimizations

        /** @var Demand[] $demands */
        //TODO: move logic to DQL / massive optimizations
        foreach ($demands as $demand) {
            $now = new \DateTime();
            //check expire offer
            if (!$demand->isFinished()) {
                //первый этап
                if (($demand->getExpireAt() < $now) && ($demand->getIsPublic())) {
                    $reactions = $demand->getReactions()->toArray();
                    $approved = [];
                    $not_approved = [];
                    foreach ($reactions as $reaction) {
                        if ($reaction->getStatus() == Reaction::STATUS_APPROVE) {
                            $approved[] = $reaction;
                            //$this->declineReaction($reaction);
                        } elseif ($reaction->getStatus() == Reaction::STATUS_START) {
                            $not_approved[] = $reaction;
                        }
                    }
                    if (count($approved) < 2) {
                        foreach ($not_approved as $reaction) {

                            if ($reaction->getStatus() == Reaction::STATUS_START) {
                                $this->declineReaction($reaction, false);
                                $console->writeln(sprintf('disqual ' . $reaction->getId()));
                            }
                        }
                        $this->demandFailed($demand);
                        $console->writeln(sprintf('demand failed ' . $demand->getId()));
                    } else {
                        foreach ($not_approved as $reaction) {
                            $this->declineReaction($reaction);
                        }
                    }
                }

                //второй этап
                if ($demand->getExpireProposalAt() < $now) {
                    $console->writeln(sprintf('step 2 ' . $demand->getId()));

                    $reactions = $demand->getReactions()->toArray();
                    $proposals = [];
                    foreach ($reactions as $reaction) {
                        $proposals = array_merge($proposals, $reaction->getProposal()->toArray());
                    }

                    if ($demand->isExtendedOffer()) {
                        if (count($proposals) == 0) {
                            $this->demandFailed($demand);
                            $console->writeln(sprintf('demand failed ' . $demand->getId()));
                        }

                        if (count($proposals) == 1) {
                            $this->demandWinner($proposals[0]);
                            $console->writeln(sprintf('demand fast winner ' . $demand->getId() . ' ' . $proposals[0]->getId()));
                        }

                        /** @var Reaction $reaction */
                        foreach ($reactions as $reaction) {
                            if (($reaction->getStatus() == Reaction::STATUS_APPROVE) || ($reaction->getStatus() == Reaction::STATUS_DIRECT)) {
                                if (count($reaction->getProposal()->toArray()) == 0) {
                                    $this->disqualReaction($reaction);
                                    $console->writeln(sprintf('disqual ' . $reaction->getId()));
                                }
                            }
                        }
                    } else {
                        if ((count($proposals) == 0) || (count($proposals) == 1)) {
                            $this->extendDemandOffer($demand);
                            $console->writeln(sprintf('extend offer step ' . $demand->getId()));
                        }
                    }
                }

                //второй этап (переторг)
                if (($demand->getExpireSubProposalAt() != null) && ($demand->getExpireSubProposalAt() < $now)) {
                    $console->writeln(sprintf('step 2-2 ' . $demand->getId()));

                    $reactions = $demand->getReactions()->toArray();
                    $ss_proposals = [];
                    foreach ($reactions as $reaction) {
                        if (array_key_exists(1, $reaction->getProposal()->toArray())) {
                            $ss_proposals[] = $reaction->getProposal()->toArray()[1];
                        }
                    }

                    if ($demand->isExtendedSubOffer()) {
                        if (count($ss_proposals) == 0) {
                            $this->demandFailed($demand);
                            $console->writeln(sprintf('demand failed ' . $demand->getId()));
                        }

                        if (count($ss_proposals) == 1) {
                            $winner = $ss_proposals[1];
                            $this->demandWinner($winner);
                            $console->writeln(sprintf('demand fast winner ' . $demand->getId() . ' ' . $winner->getId()));
                        }

                        /** @var Reaction $reaction */
                        foreach ($reactions as $reaction) {
                            if (($reaction->getStatus() == Reaction::STATUS_APPROVE) || ($reaction->getStatus() == Reaction::STATUS_DIRECT)) {
                                if (count($reaction->getProposal()->toArray()) == 1) {
                                    $this->disqualReaction($reaction);
                                    $console->writeln(sprintf('disqual ' . $reaction->getId()));
                                }
                            }
                        }
                    } else {
                        if ((count($ss_proposals) == 0) || (count($ss_proposals) == 1)) {
                            $this->extendDemandSubOffer($demand);
                            $console->writeln(sprintf('extend offer step ' . $demand->getId()));
                        }

                        /** @var Reaction $reaction */
                        foreach ($reactions as $reaction) {
                            if (($reaction->getStatus() == Reaction::STATUS_APPROVE) || ($reaction->getStatus() == Reaction::STATUS_DIRECT)) {
                                if (count($reaction->getProposal()->toArray()) == 1) {
                                    $this->disqualReaction($reaction);
                                    $console->writeln(sprintf('disqual ' . $reaction->getId()));
                                }
                            }
                        }
                    }
                }

                //третий этап
                if (($demand->getExpireDecisionAt() < $now) && ($demand->getWinner() == null)) {
                    if ($demand->isExtendedWinner()) {
                        $proposals = [];
                        $reactions = $demand->getReactions();
                        foreach ($reactions as $reaction) {
                            $proposals = array_merge($proposals, $reaction->getProposal()->toArray());
                        }
                        if (count($proposals) == 1) {
                            $this->demandWinner($proposals[0]);
                            $console->writeln(sprintf('demand only winner ' . $demand->getId() . ' ' . $proposals[0]->getId()));
                        } else {
                            $this->demandFailed($demand);
                            $console->writeln(sprintf('demand failed ' . $demand->getId()));
                        }
                    } else {
                        $this->extendDemandDecision($demand);
                        $console->writeln(sprintf('extend decision step ' . $demand->getId()));
                    }
                }
            }
        }
    }

    private function disqualReaction(Reaction $reaction)
    {
        $this->getContainer()->get('easyb.reaction.manager')->disqual($reaction);
    }

    private function declineReaction(Reaction $reaction, $send_email = true)
    {
        $this->getContainer()->get('easyb.reaction.manager')->decline($reaction, $send_email);
    }

    private function extendDemandOffer(Demand $demand)
    {
        $this->getContainer()->get('easyb.demand.manager')->extendOffer($demand);
    }

    private function extendDemandSubOffer(Demand $demand)
    {
        $this->getContainer()->get('easyb.demand.manager')->extendSubOffer($demand);
    }

    private function extendDemandDecision(Demand $demand)
    {
        $this->getContainer()->get('easyb.demand.manager')->extendDecision($demand);
    }

    private function demandFailed(Demand $demand)
    {
        $this->getContainer()->get('easyb.demand.manager')->noWinner($demand);
    }

    private function demandWinner(Proposal $proposal)
    {
        $this->getContainer()->get('easyb.reaction.manager')->setWinner($proposal, true);
    }
}