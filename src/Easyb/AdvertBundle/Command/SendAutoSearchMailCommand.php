<?php
/**
 * User: scorp
 * Date: 09.07.13
 * Time: 14:05
 */
namespace Easyb\AdvertBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Easyb\UserBundle\Entity\Mail;
use Easyb\AdvertBundle\Entity\AutoSearch;

/**
 * @author scorp
 */
class SendAutoSearchMailCommand extends ContainerAwareCommand
{
    const STEP_SEARCH = 100;

    /**
     * @see Command
     */
    protected function configure()
    {
        $this
            ->setName('easyb:mail:auto_search')
            ->setDescription('Sends a mail if there are new AutoSearch results');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $firstResult = 0;
        $mail = $this->getMail();
        $searchPart = $this->getAutoSearchRepository()->getPartOfSearch($firstResult, self::STEP_SEARCH);
        while ($searchPart) {
            foreach ($searchPart as $search) {
                $newSearchResults = $this->getSphinxSearchHelper()->getSearchResults($search, true);
                if (($total=$newSearchResults['total']) > 0) {
                    $this->sendEmailData($search, $mail, $total);
                }
            }
            $searchPart = $this->getAutoSearchRepository()->getPartOfSearch($firstResult+=self::STEP_SEARCH, self::STEP_SEARCH);
        }
    }

    /**
     * @param AutoSearch $search
     * @param Mail       $mail
     * @param integer    $total
     */
    private function sendEmailData(AutoSearch $search, Mail $mail, $total)
    {
        $email = $search->getUser()->getEmail();
        $template = 'EasybMainBundle:Mail:mail.html.twig';
        $parameters = [
            'subject' => $this->getSubject($mail, $total),
            'text'    => $this->getText($mail, $search, $total)
        ];

        $this->sendEmail($template, $parameters, $email);
    }

    /**
     * @param Mail    $mail
     * @param integer $total
     *
     * @return string
     */
    private function getSubject(Mail $mail, $total)
    {
        $subject = $mail->getSubject();
        $subject = str_replace("[total]", $total, $subject);

        return $subject;
    }

    /**
     * @param Mail       $mail
     * @param AutoSearch $search
     * @param integer    $total
     *
     * @return string
     */
    private function getText(Mail $mail, AutoSearch $search, $total)
    {
        $text = $mail->getMessage();
        $text = str_replace("[username]", $search->getUser()->getFio(), $text);
        $text = str_replace("[total]", $total, $text);
        $text = str_replace("[autosearch_name]", $search->getText(), $text);
        $text = str_replace("[advert_link]",  $this->getCatalogUrl($search), $text);
        $text = str_replace("[unsubscribe_link]", $this->getRouter()->generate('autosearch', [], true), $text);

        return $text;

    }

    /**
     * @param AutoSearch $search
     *
     * @return string
     */
    private function getCatalogUrl(AutoSearch $search)
    {
        $categoryId = ($search->getCategory() ? $search->getCategory()->getId() : '');
        $searchText = ($search->getText() ? $search->getText() : '');
        $advertTypeId = ($search->getType() ? $search->getType() : '');
        $cityId = ($search->getCity() ? $search->getCity()->getId() : '');

        $url = $this->getRouter()->generate('catalog', [
            'categoryId'   => $categoryId,
            'search_text'  => $searchText,
            'advertTypeId' => $advertTypeId,
            'cityId'       => $cityId,
            'newResults'   => (int)($search->getViewingAt()->format('U')),
            'autoId'       => $search->getId()
        ], true);

        return $url;
    }

    /**
     * @param string $template
     * @param array  $parameters
     * @param string $email
     */
    private function sendEmail($template, $parameters, $email)
    {
        $this->getMailing()->sendEmail($template, $parameters, $email);
    }

    /**
     * @return \Easyb\AdvertBundle\Helper\SphinxSearchHelper
     */
    private function getSphinxSearchHelper()
    {
        return $this->getContainer()->get('easyb.sphinx_search.helper');
    }

    /**
     * @return \Easyb\MainBundle\Mailing\MailingInterface
     */
    private function getMailing()
    {
        return $this->getContainer()->get('easyb.mailer');
    }

    /**
     * @return EntityManager
     */
    private function getEm()
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @return \Symfony\Bundle\FrameworkBundle\Routing\Router
     */
    private function getRouter()
    {
        return $this->getContainer()->get('router');
    }

    /**
     * @return \Easyb\UserBundle\Entity\UserRepository
     */
    private function getAutoSearchRepository()
    {
        return $this->getEm()->getRepository('EasybAdvertBundle:AutoSearch');
    }

    /**
     * @return Mail
     */
    private function getMail()
    {
        return $this->getEm()->getRepository('EasybUserBundle:Mail')->findOneByType(Mail::TYPE_AUTOSEARCH);
    }
}