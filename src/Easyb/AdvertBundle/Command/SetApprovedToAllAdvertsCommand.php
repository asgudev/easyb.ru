<?php
namespace Easyb\AdvertBundle\Command;

use Doctrine\ORM\EntityManager;
use Easyb\AdvertBundle\Entity\AbstractAdvert;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Easyb\UserBundle\Entity\Mail;

/**
 * @author scorp
 */
class SetApprovedToAllAdvertsCommand extends ContainerAwareCommand
{
    /**
     * @see Command
     */
    protected function configure()
    {
        $this
            ->setName('easyb:fix:set_is_approved_adverts')
            ->setDescription('Set is_approved to true for all adverts.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $adverts = $this->getEm()->getRepository('EasybAdvertBundle:AbstractAdvert')->findAll();
        try {
            /** @var AbstractAdvert $advert */
            foreach ($adverts as $advert) {
                $advert->setIsApproved(true);
                $this->getEm()->persist($advert);
            }
            $this->getEm()->flush();

            $output->writeln(sprintf('<info>Set is_approved to true was successful </info>'));
        } catch (\Exception $e) {
            $output->writeln(sprintf('<error>Set is_approved to true was unsuccessful </error>'));
            $output->writeln(sprintf('<error>%s</error>', $e->getMessage()));
        }
    }

    /**
     * @return EntityManager
     */
    private function getEm()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }
}