<?php
namespace Easyb\AdvertBundle\Command;

use Doctrine\ORM\EntityManager;
use Easyb\MainBundle\Entity\Spam;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Easyb\UserBundle\Entity\Mail;
use Easyb\UserBundle\Entity\User;

/**
 * @author scorp
 */
class SendSpamCommand extends ContainerAwareCommand
{
    /**
     * @see Command
     */
    protected function configure()
    {
        $this
            ->setName('easyb:mail:spam')
            ->setDescription('Sends a spam for all users.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $template = 'EasybMainBundle:Mail:mail.html.twig';

        $users = $this->getContainer()->get('doctrine.orm.entity_manager')
            ->createQueryBuilder()
            ->select('u.email')
            ->from('EasybUserBundle:User', 'u')
            ->getQuery()
            ->getResult();

        $mails = $this->getContainer()->get('doctrine.orm.entity_manager')
            ->createQueryBuilder()
            ->select('s')
            ->from('EasybMainBundle:Spam', 's')
            ->where('s.type = :status')
            ->setParameter('status', Spam::TYPE_WAITING)
            ->getQuery()
            ->getResult();

        foreach ($mails as $mail) {
            foreach ($users as $user) {
                $email = $user['email'];
                $parameters = [
                    'subject' => 'Почтовая рассылка Easyb',
                    'text'    => $mail->getContents()
                ];

                $this->sendEmail($template, $parameters, $email);
            }
            $mail->setType(Spam::TYPE_DONE);
            $this->getContainer()->get('doctrine.orm.entity_manager')->persist($mail);
        }
        $this->getContainer()->get('doctrine.orm.entity_manager')->flush();
    }

    /**
     * @param string $template
     * @param array  $parameters
     * @param string $email
     */
    private function sendEmail($template, $parameters, $email)
    {
        $this->getMailing()->sendEmail($template, $parameters, $email);
    }

    /**
     * @return \Easyb\MainBundle\Mailing\MailingInterface
     */
    private function getMailing()
    {
        return $this->getContainer()->get('easyb.mailer');
    }
}