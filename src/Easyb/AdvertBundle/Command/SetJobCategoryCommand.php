<?php
/**
 * This file is part of easyb package.
 *
 * (c) Evgeniy Marchenkov <evgeniy.public@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Easyb\AdvertBundle\Command;

use Doctrine\Common\Persistence\ObjectManager;
use Easyb\SubscribeBundle\Entity\Subscriber;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Easyb\UserBundle\Entity\Job;

class SetJobCategoryCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            ->setName('easyb:setcategory')
            ->setDescription('Update job category');
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $jobs = $this->getEm()->getRepository("EasybUserBundle:Job")->findAll();

            $this->setCategory($jobs, $output);

            $output->writeln(sprintf('<info>Update demand status on non approved when the time has expired was successful done!</info>'));
        } catch (\Exception $e) {
            $output->writeln(sprintf('<error>Update demand status on non approved when the time has expired was unsuccessful done!</error>'));
            $output->writeln(sprintf('<error>%s</error>', $e->getMessage()));
        }
    }

    /**
     * @return ObjectManager
     */
    private function getEm()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }

    /**
     * @return \Easyb\AdvertBundle\Manager\AllAdvertManager
     */
    private function getAllAdvertManager()
    {
        return $this->getContainer()->get('easyb.all_advert.manager');
    }

    private function setCategory($jobs, $output)
    {

        $em = $this->getEm();
        $offer_repository = $em->getRepository("EasybAdvertBundle:Offer");

        /**
         * @var Job $job
         */
        foreach ($jobs as $job) {
            $offers = $offer_repository->findOneByJob($job);
            $offer = $offers[0];
            if ($offer != null) {
                if ($job->getCategories()->toArray() == null) {
                    $job->addCategory($offer->getCategory());
                    $em->persist($job);
                    $em->flush();
                }
            }

            $subscriber = new Subscriber();
            $subscriber->setCity($job->getCity()->getParent());
            $subscriber->setEmail($job->getEmail());
            if ($job->getPhone()) {
                $subscriber->setPhone($job->getPhone());
            } elseif ($job->getPhoneMobile()) {
                $subscriber->setPhone($job->getPhoneMobile());
            }
            $subscriber->setCategory($job->getCategories()[0]);

            $em->persist($subscriber);
            $em->flush();
        }
    }
}