<?php
/**
 * This file is part of easyb package.
 *
 * (c) Evgeniy Marchenkov <evgeniy.public@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Easyb\AdvertBundle\Command;

use Easyb\UserBundle\Entity\User;
use Easyb\AdvertBundle\Entity\AbstractAdvert;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class UpdateViewsUsersCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            ->setName('easyb:update:views:user_adverts')
            ->setDescription('Updates quantity of the advert views for the users');
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $users = $this->getEm()->getRepository('EasybUserBundle:User')->findAll();

            $this->updateAdvertsViewsForUsers($users);

            $output->writeln(sprintf('<info>Updates quantity of the advert views for the users was successful done!</info>'));
        } catch (\Exception $e) {
            $output->writeln(sprintf('<error>Updates quantity of the advert views for the users was unsuccessful done!</error>'));
            $output->writeln(sprintf('<error>%s</error>', $e->getMessage()));
        }
    }

    /**
     * @return ObjectManager
     */
    private function getEm()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }

    /**
     * @param array $users
     */
    private function updateAdvertsViewsForUsers(array $users)
    {
        $em = $this->getEm();

        $cheatViewsCount = rand(1, 10);

        /**
         * @var User[] $users
         */
        foreach ($users as $user) {
            $adverts = $this->getEm()->getRepository('EasybAdvertBundle:Offer')->findActiveApprovedAdvertsByUser($user);

            /**
             * @var AbstractAdvert[] $adverts
             */
            foreach ($adverts as $advert) {
                $advert->setViews($advert->getViews() + $cheatViewsCount);
                $em->persist($advert);
            }
        }

        $em->flush();
    }
}
