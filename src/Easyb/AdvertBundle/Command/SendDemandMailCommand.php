<?php
/**
 * User: scorp
 * Date: 09.07.13
 * Time: 14:05
 */
namespace Easyb\AdvertBundle\Command;

use Doctrine\ORM\EntityManager;
use Easyb\AdvertBundle\Entity\Reaction;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Easyb\UserBundle\Entity\Mail;

/**
 * @author scorp
 */
class SendDemandMailCommand extends ContainerAwareCommand
{
    const STEP_REACTION = 100;

    /**
     * @see Command
     */
    protected function configure()
    {
        $this
            ->setName('easyb:mail:demand')
            ->setDescription('Sends a mail if there are new demands notification');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $demand = null;
        $mail = $this->getMail();
        $reactions = $this->reactionManager()->getRepository()->getDontApprovedForLastDay();
        foreach ($reactions as $reaction) {
            if ($demand != $reaction->getDemand()) {
                $demand = $reaction->getDemand();
                $this->sendEmailData($reaction, $mail);
            }
        }
    }

    /**
     * @param Reaction $reaction
     * @param Mail     $mail
     */
    private function sendEmailData(Reaction $reaction, Mail $mail)
    {
        $email = $reaction->getDemand()->getUser()->getEmail();
        $template = 'EasybMainBundle:Mail:mail.html.twig';
        $parameters = [
            'subject' => $this->getSubject($mail),
            'text'    => $this->getText($mail, $reaction)
        ];

        $this->sendEmail($template, $parameters, $email);
    }

    /**
     * @param Mail $mail
     *
     * @return string
     */
    private function getSubject(Mail $mail)
    {
        $subject = $mail->getSubject();

        return $subject;
    }

    /**
     * @param Mail     $mail
     * @param Reaction $reaction
     *
     * @return string
     */
    private function getText(Mail $mail, Reaction $reaction)
    {
        $demand = $reaction->getDemand();

        $text = $mail->getMessage();
        $text = str_replace("[username]", $demand->getUser()->getFio(), $text);
        $text = str_replace("[advert_name]", $demand->getName(), $text);
        $text = str_replace("[advert_link]", $this->getRouter()->generate('my_demand', [], true), $text);
        $text = str_replace("[offer_link]", $this->getRouter()->generate('show_demand', [
            'slug'     => $demand->getSlug(),
            'company'  => $demand->getJob()->getSlug(),
            'city'     => $demand->getJob()->getCity()->getSlug(),
            'category' => $demand->getCategory()->getSlug(),
        ], true), $text);
        $text = str_replace("[unsubscribe_link]", $this->getRouter()->generate('fos_user_profile_edit', [], true), $text);

        return $text;

    }

    /**
     * @param string $template
     * @param array  $parameters
     * @param string $email
     */
    private function sendEmail($template, $parameters, $email)
    {
        $this->getMailing()->sendEmail($template, $parameters, $email);
    }

    /**
     * @return \Easyb\AdvertBundle\Manager\ReactionManagerInterface
     */
    private function reactionManager()
    {
        return $this->getContainer()->get('easyb.reaction.manager');
    }

    /**
     * @return \Easyb\MainBundle\Mailing\MailingInterface
     */
    private function getMailing()
    {
        return $this->getContainer()->get('easyb.mailer');
    }

    /**
     * @return EntityManager
     */
    private function getEm()
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @return \Symfony\Bundle\FrameworkBundle\Routing\Router
     */
    private function getRouter()
    {
        return $this->getContainer()->get('router');
    }

    /**
     * @return Mail
     */
    private function getMail()
    {
        return $this->getEm()->getRepository('EasybUserBundle:Mail')->findOneByType(Mail::TYPE_DEMAND_REACTION);
    }
}

