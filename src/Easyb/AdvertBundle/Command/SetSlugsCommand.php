<?php
namespace Easyb\AdvertBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Easyb\UserBundle\Entity\Mail;

/**
 * @author scorp
 */
class SetSlugsCommand extends ContainerAwareCommand
{
    /**
     * @see Command
     */
    protected function configure()
    {
        $this
            ->setName('easyb:set:slugs')
            ->setDescription('Set slugs to Category, City');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            // City
//            $this->setSlugs($this->getEm()->getRepository('EasybMainBundle:City')->findAll());
            // Category
//            $this->setSlugs($this->getEm()->getRepository('EasybMainBundle:Category')->findAll());
            // Adverts
            $this->setSlugs($this->getEm()->getRepository('EasybAdvertBundle:AbstractAdvert')->findAll());
            // Job
//            $this->setSlugs($this->getEm()->getRepository('EasybUserBundle:Job')->findAll());

            $this->getEm()->flush();

            $output->writeln(sprintf('<info>Set slugs was successful </info>'));
        } catch (\Exception $e) {
            $output->writeln(sprintf('<error>Set slugs was unsuccessful </error>'));
            $output->writeln(sprintf('<error>%s</error>', $e->getMessage()));
        }
    }

    /**
     * @param array $objects
     */
    private function setSlugs($objects)
    {
        foreach ($objects as $object) {
//            if (!$object->getSlug()) {
                $object->setSlug($object->getName());
                $this->getEm()->persist($object);
//            }
        }
    }

    /**
     * @return EntityManager
     */
    private function getEm()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }
}