<?php
/**
 * This file is part of easyb package.
 *
 * (c) Evgeniy Marchenkov <evgeniy.public@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Easyb\AdvertBundle\Command;

use Easyb\AdvertBundle\Entity\AbstractAdvert;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class UpdatePublishDateAdminAdvertsCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            ->setName('easyb:update:publish_date:admin_adverts')
            ->setDescription('Updates publish date for the admin adverts')
            ->addOption('percentage', 'p', InputOption::VALUE_REQUIRED, 'The percentage of the admin adverts for the updating');
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $percentage = $input->getOption('percentage');

        try {
            $adminAdverts = $this->getEm()->getRepository('EasybAdvertBundle:Offer')->findAdvertsByAdmin();

            shuffle($adminAdverts);

            $filteredAdminAdverts = array_slice($adminAdverts, 0, ceil(count($adminAdverts) / 100 * (int) $percentage));

            $this->updateAdminAdvertsPublishDate($filteredAdminAdverts);

            $output->writeln(sprintf('<info>Updates publish date for the admin adverts was successful done!</info>'));
        } catch (\Exception $e) {
            $output->writeln(sprintf('<error>Updates publish date for the admin adverts was unsuccessful done!</error>'));
            $output->writeln(sprintf('<error>%s</error>', $e->getMessage()));
        }
    }

    /**
     * @return ObjectManager
     */
    private function getEm()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }

    /**
     * @param array $adminAdverts
     */
    private function updateAdminAdvertsPublishDate(array $adminAdverts)
    {
        $em = $this->getEm();

        /**
         * @var AbstractAdvert[] $adminAdverts
         */
        foreach ($adminAdverts as $adminAdvert) {
            $adminAdvert->setPublicationAt(new \DateTime());
            $em->persist($adminAdvert);
        }

        $em->flush();
    }
}
