<?php
namespace Easyb\AdvertBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Easyb\UserBundle\Entity\Mail;

/**
 * @author scorp
 */
class FixAdminAdvertsCommand extends ContainerAwareCommand
{
    /**
     * @see Command
     */
    protected function configure()
    {
        $this
            ->setName('easyb:fix:admin_adverts')
            ->setDescription('Fix admin adverts');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            foreach ($this->getEm()->getRepository('EasybAdvertBundle:AbstractAdvert')->findByFromAdmin(true) as $advert) {
                $advert->setJob($advert->getUser()->getJobs()->first());
                $this->getEm()->persist($advert);
            }
            $this->getEm()->flush();

            $output->writeln(sprintf('<info>Fix admin adverts was successful </info>'));
        } catch (\Exception $e) {
            $output->writeln(sprintf('<error>AFix admin adverts was unsuccessful </error>'));
            $output->writeln(sprintf('<error>%s</error>', $e->getMessage()));
        }
    }

    /**
     * @return EntityManager
     */
    private function getEm()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }
}