<?php

namespace Easyb\AdvertBundle\Controller;

use Easyb\AdvertBundle\Entity\Offer;
use Easyb\AdvertBundle\Entity\Reaction;
use Easyb\AdvertBundle\Events\OfferEvent;
use Easyb\AdvertBundle\Form\Type\FrontendOfferType;
use Easyb\MainBundle\Entity\SiteSettings;
use Easyb\UserBundle\Entity\Mail;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class OfferController extends AbstractAdvertController
{
    /**
     * @Template()
     *
     * @Route("/offers", name="offers")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     * @return array
     */
    public function indexAction()
    {
        $siteSettings = $this->getDoctrine()->getRepository('EasybMainBundle:SiteSettings')->findBy(array('route' => SiteSettings::OFFERS_ROUTE));

        $count = $this->getRequest()->query->get('count');
        $this->getBreadCrumbsManager()->create(['EasyB' => 'homepage', 'Мое предложение' => 'offers', 'Предложения' => null]);
        $offers = $this->getPager($this->getManager()->getRepository()->getQueryByUser($this->getUser()), $count ? $count : Offer::COUNT_ON_LIST_PAGE);

        return [
            'adverts' => $offers,
            'metadata' => $this->getMetadata($siteSettings[0] ?: null)
        ];
    }

    /**
     * @param string $slug
     * @Route("/offer/{category}/{city}/{slug}", name="show_offer")
     *
     * @return array
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function showOfferAction($slug)
    {
        /**
         * @var Offer $offer
         */
        $offer = $this->getDoctrine()->getRepository('EasybAdvertBundle:AbstractAdvert')->findOneBySlug($slug);

        if (($this->getUser() != null) && ($this->getUser() != $offer->getUser())) {
            if ((!$offer) || (!$offer->isApproved())) {
                throw new NotFoundHttpException('Такого объявления не существует.');
            }
        }

        $similarAds = $this->getDoctrine()->getRepository("EasybAdvertBundle:Offer")->findSimilarAds($offer);

        $canAskQuestion = false;
        $canAnswer = false;
        $hasReactions = false;
        $hasCategories = false;

        if ($this->getUser() != null) {
            $jobs = $this->getUser()->getJobs();

            foreach ($jobs as $key => $job) {
                if (!in_array($offer->getCategory(), $job->getCategories()->toArray())) {
                    unset($jobs[$key]);
                }
            }
            if (count($jobs) > 0) $hasCategories = true;

            //TODO: move logic to DQL

            foreach ($jobs as $key => $job) {
                $reactions = $this->getDoctrine()->getRepository("EasybAdvertBundle:Reaction")->findBy(
                    array(
                        'user' => $this->getUser(),
                        'demand' => $offer,
                        'job' => $job
                    ));
                if (count($reactions) > 0) {
                    $hasReactions = true;
                    unset($jobs[$key]);
                }
                foreach ($reactions as $reaction) {
                    if ($reaction->getStatus() == Reaction::STATUS_APPROVE) {
                        $canAskQuestion = true;
                    }
                }
            }
            if (count($jobs) > 0) $canAnswer = true;
        }

        $showContact = $this->getUser() == $offer->getUser() ? true : false;


        $this->getBreadCrumbsManager()->create(
            [
                'EasyB' => 'homepage',
                'Каталог' => 'catalog_only_offer',
                $offer->getCategory()->getName() => 'catalog_offer',
                $offer->getName() => null
            ],
            [
                $offer->getCategory()->getName() => [
                    'url' => $offer->getCategory()->getSlug()
                ],
                $offer->getJob()->getName() => [
                    'slug' => $offer->getJob()->getSlug()
                ]
            ]);
        $this->getManager()->addViews($offer);

        $offers = $this->get('easyb.offer.manager')->getOffersByJob($offer->getJob());
        $demands = $this->get('easyb.demand.manager')->getDemandsByJob($offer->getJob());

        return $this->render('EasybAdvertBundle:Offer:show.html.twig', [
            'similarAds' => $similarAds,
            'metadata' => $this->getMetadata($offer, $this->getDoctrine()->getRepository('EasybAdvertBundle:AdvertMetaData')->findAll()[0]),
            'offer' => $offer,
            'files' => $offer->getFiles(),
            'isOffer' => true,
            'canAnswer' => $canAnswer,
            'hasReactions' => $hasReactions,
            'hasCategories' => $hasCategories,
            'canAskQuestion' => $canAskQuestion,
            'showContact' => $showContact,
            'offers' => $offers,
            'demands' => $demands,
        ]);
    }

    /**
     * @return \Easyb\AdvertBundle\Manager\OfferManager
     */
    public function getManager()
    {
        return $this->get('easyb.offer.manager');
    }

    /**
     * @Template()
     * @Route("/add_offer", name="add_offer")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     *
     * @return array
     */
    public function addOfferAction()
    {
        $siteSettings = $this->getDoctrine()->getRepository('EasybMainBundle:SiteSettings')->findBy(array('route' => SiteSettings::ADD_OFFER_ROUTE));

        $this->getBreadCrumbsManager()->create(['EasyB' => 'homepage', 'Мое предложение' => 'offers', 'Добавить предложение' => null]);
        $offer = $this->getManager()->getInstance($this->getUser());

        $form = $this->createForm(new FrontendOfferType(), $offer);

        if ($this->processForm($form, $offer)) {
            $this->get('session')->getFlashBag()->add(
                'newAdvert',
                'После проверки администратором информация о Вашем коммерческом предложении будет будет опубликована. Пожалуйста, соблюдайте <a href="/page/add_rules">правила</a> размещения информации на сайте'
            );
            return $this->redirect('offers');
        }

        return [
            'advert' => $offer,
            'isOffer' => ($offer instanceof Offer ? true : false),
            'form' => $form->createView(),
            'savePath' => $this->generateUrl('add_offer'),
            'metadata' => $this->getMetadata($siteSettings[0] ?: null),
            'errors' => null
        ];
    }

    /**
     * @param \Easyb\AdvertBundle\Entity\Offer $offer
     * @Route("/offer/up/{id}", name="offer_up")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     *
     * @return JsonResponse
     */
    public function upAction(Offer $offer)
    {
        if ($offer->validateUser($this->getUser())) {
            $this->getManager()->up($offer, $this->getUser());
        }

        return new JsonResponse([
            'newTemplate' => $this->renderView('EasybAdvertBundle:Offer:offer.html.twig', ['advert' => $offer]),
            'id' => $offer->getId()
        ]);
    }

    /**
     * @param \Easyb\AdvertBundle\Entity\Offer $offer
     * @Route("/offer/delete/{id}", name="offer_delete")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     *
     * @return array
     */
    public function deleteAction(Offer $offer)
    {
        if ($offer->validateUser($this->getUser())) {
            $event = new OfferEvent($offer);

            $dispatcher = $this->container->get('event_dispatcher');
            $dispatcher->dispatch(OfferEvent::DELETE, $event);
        }

        return $this->redirect($this->generateUrl('offers'));
    }

    /**
     * @param \Easyb\AdvertBundle\Entity\Offer $offer
     * @Route("/offer/active/{id}", name="offer_active")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     *
     * @return JsonResponse
     */
    public function activeAction(Offer $offer)
    {
        if ($offer->validateUser($this->getUser())) {
            $this->getManager()->changeActive($offer, $this->getUser());
        }

        return new JsonResponse([
            'newTemplate' => $this->renderView('EasybAdvertBundle:Offer:offer.html.twig', ['advert' => $offer]),
            'id' => $offer->getId()
        ]);
    }

    /**
     * @Template("EasybAdvertBundle:Offer:addOffer.html.twig")
     * @param \Easyb\AdvertBundle\Entity\Offer $offer
     * @Route("/offer/edit/{id}", name="offer_edit")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(Offer $offer)
    {
        $this->getBreadCrumbsManager()->create(['EasyB' => 'homepage', 'Мой тендер' => 'offers', 'Редактирование объявления' => null]);
        $form = $this->getForm($this->get('easyb.form.type.offer'), $offer);
        if ($offer->validateUser($this->getUser()) && $this->processForm($form, $offer)) {
            return $this->redirect($this->generateUrl('offers'));
        }

        return [
            'isOffer' => ($offer instanceof Offer ? true : false),
            'form' => $form->createView(),
            'savePath' => $this->generateUrl('offer_edit', array('id' => $offer->getId())),
            'errors' => null
        ];
    }

    /**
     * @Template()
     * @Route("/my_offer", name="my_offer")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function myOfferAction()
    {
        $siteSettings = $this->getDoctrine()->getRepository('EasybMainBundle:SiteSettings')->findBy(array('route' => SiteSettings::MY_OFFER_ROUTE));

        $count = $this->getRequest()->query->get('count');
        $this->getBreadCrumbsManager()->create(['EasyB' => 'homepage', 'Мое предложение' => 'my_offer', 'Заявки' => null]);

        $pager = $this
            ->getDoctrine()
            ->getRepository('EasybAdvertBundle:Reaction')
            ->createQueryBuilder('r')
            ->where('r.user = :user')
            ->andWhere('r.deletedFromOffer = false')
            ->setParameter('user', $this->getUser())
            ->orderBy('r.createdAt', 'DESC')
            ->getQuery();

        $myOffers = $this->getPager($pager, $count ? $count : Offer::COUNT_ON_LIST_PAGE);
        $this->getRequest()->getSession()->set('referer', $this->getRequest()->getRequestUri());

        return [
            'reactions' => $myOffers,
            'metadata' => $this->getMetadata($siteSettings[0] ?: null)
        ];
    }

    public function getMailMessageAction(Reaction $reaction)
    {
        $mailer_listener = $this->get("easyb.mailer.listener");
        $mail = $this->getDoctrine()->getRepository('EasybUserBundle:Mail')->findOneByType(Mail::TYPE_DEMAND_APPROVE);

        $parameters = [
            'subject' => $mailer_listener->getOfferReactionSubject($mail, $reaction),
            'text' => $mailer_listener->getOfferReactionText($mail, $reaction)
        ];

        $message = "<h3>" . $parameters["subject"] . "</h3>";
        $message .= str_replace("\n", "<br>", "<p>" . $parameters["text"] . "</p>");

        return new Response($message);
    }


    /**
     * @Route("/my_offer/viewed", name="check_offer_notification")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     *
     * @return JsonResponse
     */
    public function checkOfferNotificationAction()
    {
        $reaction = $this->getReactionManager()->getRepository()->findOneById($this->getRequest()->get('id'));
        if ($reaction && $reaction->getOffer()->validateUser($this->getUser())) {
            $this->getReactionManager()->setViewedStatusToApprovedReaction($reaction);
        }

        return new JsonResponse([
            'offerNotification' => $this->getUser()->getOfferNotification()
        ]);
    }
}
