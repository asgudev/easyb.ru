<?php
/**
 * Created by IntelliJ IDEA.
 * User: User
 * Date: 16.02.2016
 * Time: 18:41
 */

namespace Easyb\AdvertBundle\Controller;

use Easyb\AdvertBundle\Entity\Demand;
use Easyb\AdvertBundle\Entity\Question;
use Easyb\AdvertBundle\Entity\Reaction;
use Easyb\AdvertBundle\Events\QuestionEvent;
use Easyb\AdvertBundle\Form\Type\FrontendAnswerType;
use Easyb\AdvertBundle\Form\Type\FrontendQuestionType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class QuestionController extends Controller
{

    /**
     * @Template()
     * @Route("/ask_question/{id}", name="ask_question")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     */
    public function askQuestionAction(Request $request, Demand $demand)
    {
        $user = $this->getUser();
        $question = new Question($user);

        $reactions = $demand->getReactions();
        $jobs = [];
        foreach ($reactions as $reaction) {
            if (($reaction->getUser() == $user) && (($reaction->getStatus() == Reaction::STATUS_APPROVE) || ($reaction->getStatus() == Reaction::STATUS_DIRECT))) {
                $jobs[] = $reaction->getJob();
            }
        }

        $form = $this->createForm(new FrontendQuestionType(), $question, [
            'jobs' => $jobs,
        ]);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $question->setDemand($demand);
            $em = $this->getDoctrine()->getManager();
            $em->persist($question);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'newReaction',
                "Ваш вопрос опубликован. Организатор тендера уведомлен. Как только будет получен ответ, Вы сразу же получите письмо на почту."
            );

            $dispatcher = $this->container->get('event_dispatcher');
            $event = new QuestionEvent($question);

            $dispatcher->dispatch(QuestionEvent::QUESTION_CREATED, $event);

            return $this->redirect($this->generateUrl("show_demand", array(
                    "slug" => $demand->getSlug(),
                    "city" => $demand->getJob()->getCity()->getSlug(),
                    "category" => $demand->getCategory()->getSlug()
                )) . "#questions");
        }

        return [
            'form' => $form->createView(),
            'demand' => $demand
        ];
    }

    /**
     * @Route("/delete_question/{id}", name="delete_question")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     */
    public function deleteQuestionAction(Request $request, Question $question)
    {
        $user = $this->getUser();
        $demand = $question->getDemand();
        $em = $this->getDoctrine()->getManager();

        if (($user == $question->getDemand()->getUser())) {
            if ($question->getQuestion() == 'deleted') {
                $em->remove($question);
            } else {
                $question->setAnswer("deleted");
            }
        } elseif ($user == $question->getUser()) {
            if ($question->getAnswer() == '') {
                $em->remove($question);
            } elseif ($question->getAnswer() == 'deleted') {
                $em->remove($question);
            } else {
                $question->setQuestion("deleted");
            }
        }

        $em->flush();

        return $this->redirect($this->generateUrl("show_demand", array(
                "slug" => $demand->getSlug(),
                "city" => $demand->getJob()->getCity()->getSlug(),
                "category" => $demand->getCategory()->getSlug()
            )) . "#questions");
    }

    /**
     * @Template()
     * @Route("/edit_question/{id}", name="edit_question")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     */
    public function editQuestionAction(Request $request, Question $question)
    {
        $demand = $question->getDemand();
        $reactions = $demand->getReactions();
        $user = $question->getUser();

        $jobs = [];
        foreach ($reactions as $reaction) {
            if (($reaction->getUser() == $user) && ($reaction->getStatus() == Reaction::STATUS_APPROVE)) {
                $jobs[] = $reaction->getJob();
            }
        }

        $form = $this->createForm(new FrontendQuestionType(), $question, [
            'jobs' => $jobs,
        ]);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($question);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'newReaction',
                "Вопрос отредактирован. Ожидайте ответа от организатора."
            );

            return $this->redirect($this->generateUrl("show_demand", array(
                    "slug" => $demand->getSlug(),
                    "city" => $demand->getJob()->getCity()->getSlug(),
                    "category" => $demand->getCategory()->getSlug()
                )) . "#questions");
        }

        return [
            'form' => $form->createView(),
            'question' => $question,
        ];

    }


    /**
     * @Template()
     * @Route("/edit_answer/{id}", name="edit_answer")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     */
    public function editAnswerAction(Request $request, Question $question)
    {
        $demand = $question->getDemand();

        $user = $this->getUser();

        $form = $this->createForm(new FrontendAnswerType(), $question, [

        ]);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $question->setAnsweredAt(new \DateTime());
            $em = $this->getDoctrine()->getManager();
            $em->persist($question);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'newReaction',
                "Ответ отправлен. Если появятся новые, мы Вас уведомим."
            );
            return $this->redirect($this->generateUrl("show_demand", array(
                    "slug" => $demand->getSlug(),
                    "city" => $demand->getJob()->getCity()->getSlug(),
                    "category" => $demand->getCategory()->getSlug()
                )) . "#questions");
        }

        return [
            'form' => $form->createView(),
            'demand' => $demand,
            'question' => $question,
        ];
    }

    /**
     * @Template()
     * @Route("/answer_question/{id}", name="answer_question")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     */
    public function answerQuestionAction(Request $request, Question $question)
    {

        $demand = $question->getDemand();

        $user = $this->getUser();

        $form = $this->createForm(new FrontendAnswerType(), $question, [

        ]);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $question->setAnsweredAt(new \DateTime());
            $em = $this->getDoctrine()->getManager();
            $em->persist($question);
            $em->flush();

            $dispatcher = $this->container->get('event_dispatcher');
            $event = new QuestionEvent($question);

            $dispatcher->dispatch(QuestionEvent::QUESTION_ANSWERED, $event);

            $this->get('session')->getFlashBag()->add(
                'newReaction',
                "Ответ отправлен. Если появятся новые, мы Вас уведомим."
            );
            return $this->redirect($this->generateUrl("show_demand", array(
                    "slug" => $demand->getSlug(),
                    "city" => $demand->getJob()->getCity()->getSlug(),
                    "category" => $demand->getCategory()->getSlug()
                )) . "#questions");
        }

        return [
            'form' => $form->createView(),
            'demand' => $demand,
            'question' => $question,
        ];
    }

}