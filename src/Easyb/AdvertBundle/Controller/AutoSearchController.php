<?php

namespace Easyb\AdvertBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Response;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter as PagerAdapter;
use Symfony\Component\HttpFoundation\JsonResponse;

use Easyb\UserBundle\Entity\User;
use Easyb\AdvertBundle\Entity\AutoSearch;

use Easyb\MainBundle\Controller\ControllerHelperTrait;

class AutoSearchController extends Controller
{
    const COUNT_ON_LIST_PAGE = 20;

    use ControllerHelperTrait;

    /**
     * @return \Easyb\AdvertBundle\Manager\AutoSearchManager
     */
    public function getAutoSearchManager()
    {
        return $this->get('easyb.autosearch.manager');
    }

    /**
     * @Template()
     * @Route("/autosearch", name="autosearch")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     *
     * @return array
     */
    public function autoSearchAction()
    {
        $this->getBreadCrumbsManager()->create(['EasyB' => 'homepage', 'Автопоиск' => null]);
        $count = $this->getRequest()->query->get('count');
        $result = $this->getPager($this->getAutoSearchManager()->getRepository()->findByUser($this->getUser()), $count ? $count : self::COUNT_ON_LIST_PAGE);
        $countsArray = $this->getSearchCounts($result);

        return [
            'result'      => $result,
            'allCount' => $countsArray['allCount'],
            'newCount' => $countsArray['newCount']
        ];
    }

    /**
     * @param     $query
     * @param int $maxPerPage
     *
     * @return \Pagerfanta\Pagerfanta
     */
    public function getPager($query, $maxPerPage)
    {
        $paginator = new Pagerfanta(new PagerAdapter($query, false));
        $paginator->setMaxPerPage($maxPerPage);
        $paginator->setCurrentPage($this->getPage(), false, true);

        return $paginator;
    }

    /**
     * @return int|mixed|null
     */
    public function getPage()
    {
        return $this
            ->getRequest()
            ->get('page') ? $this
            ->getRequest()
            ->get('page') : 1;
    }

    /**
     * @param AutoSearch $autoSearch
     * @Route("/autosearch/delete/{id}", name="autosearch_delete")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     *
     * @return array
     */
    public function deleteAction(AutoSearch $autoSearch)
    {
        $type = $autoSearch->getType();
        if ($autoSearch->validateUser($this->getUser())) {
            $this->getAutoSearchManager()->delete($autoSearch);
        }

        return $this->redirect($this->generateUrl('autosearch', ['type' => $type]));
    }

    /**
     * @Route("/add_autosearch", name="add_autosearch")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     *
     * @return array
     */
    public function addAutoSearchAction()
    {
        $searchInputId = $this->getRequest()->request->get('searchInputId');
        $categoryId = $this->getRequest()->request->get('categoryId');
        $cityId = $this->getRequest()->request->get('cityId');
        $advertTypeId = $this->getRequest()->request->get('advertTypeId');
        $user = $this->getUser();

        $this->getAutoSearchManager()->create($this->getAutoSearchManager()->createInstance($user, $searchInputId, $categoryId, $cityId, $advertTypeId));

        return new JsonResponse([
            'status' => 'success',
            'type'   => $advertTypeId
        ]);
    }

    /**
     * @Route("/autosearch/change_active", name="autosearch_change_active")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     *
     * @return JsonResponse
     */
    public function changeActiveAction()
    {
        $autoSearch = $this->getAutoSearchManager()->getRepository()->findOneById($this->getRequest()->get('id'));
        if ($autoSearch && $autoSearch->validateUser($this->getUser())) {
            $this->getAutoSearchManager()->changeActive($autoSearch);
        }

        return new JsonResponse([
            'status' => 'success'
        ]);
    }

    /**
     * @param Pagerfanta $autoSearch
     *
     * @return array
     */
    private function getSearchCounts($autoSearch)
    {
        $allCount = [];
        $newCount = [];
        foreach ($autoSearch as $search) {
            $searchResults = $this->getSphinxSearchHelper()->getSearchResults($search);
            $newSearchResults = $this->getSphinxSearchHelper()->getSearchResults($search, true);
            $allCount[$search->getId()] = $searchResults['total'];
            $newCount[$search->getId()] = $newSearchResults['total'];
        }

        return [
            'allCount' => $allCount,
            'newCount' => $newCount
        ];
    }

    /**
     * @return \Easyb\AdvertBundle\Helper\SphinxSearchHelper
     */
    private function getSphinxSearchHelper()
    {
        return $this->get('easyb.sphinx_search.helper');
    }
}
