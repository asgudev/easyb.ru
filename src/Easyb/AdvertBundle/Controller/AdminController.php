<?php
namespace Easyb\AdvertBundle\Controller;

use Easyb\AdvertBundle\Events\ReactionEvent;
use Easyb\AdvertBundle\Entity\AbstractAdvert;
use Easyb\AdvertBundle\Events\DemandEvent;
use Symfony\Component\HttpFoundation\Response;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sonata\AdminBundle\Exception\ModelManagerException;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class AdminController extends Controller
{
    /**
     * @param ProxyQueryInterface $query
     *
     * @return RedirectResponse
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function batchActionRefreshPublicationDate(ProxyQueryInterface $query)
    {
        if (false === $this->admin->isGranted('DELETE')) {
            throw new AccessDeniedException();
        }

        try {
            $this->batchPublicationDateUpdate($this->admin->getClass(), $query);
            $this->addFlash('sonata_flash_success', 'Обновление дат публикации прошло успешно');
        } catch (ModelManagerException $e) {
            $this->addFlash('sonata_flash_error', 'При обновлении дат публикации произошла ошибка');
        }

        return new RedirectResponse(
            $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters())) . '#to-do'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function batchPublicationDateUpdate($class, ProxyQueryInterface $queryProxy)
    {
        $queryProxy->select('DISTINCT ' . $queryProxy->getRootAlias());
        $em = $this->getDoctrine()->getManager();

        foreach ($queryProxy->getQuery()->iterate() as $object) {
            $object[0]->setPublicationAt(new \DateTime());
            $em->persist($object[0]);
        }

        try {
            $em->flush();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param ProxyQueryInterface $query
     *
     * @return RedirectResponse
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function batchActionDeleteUser(ProxyQueryInterface $query)
    {
        if (false === $this->admin->isGranted('DELETE')) {
            throw new AccessDeniedException();
        }

        try {
            $this->batchDeleteUser($this->admin->getClass(), $query);
            $this->addFlash('sonata_flash_success', 'Удаление пользователя прошло успешно');
        } catch (ModelManagerException $e) {
            $this->addFlash('sonata_flash_error', 'При удалении пользователя произошла ошибка');
        }

        return new RedirectResponse(
            $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters())) . '#to-do'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function batchDeleteUser($class, ProxyQueryInterface $queryProxy)
    {
        $queryProxy->select('DISTINCT ' . $queryProxy->getRootAlias());
        $em = $this->getDoctrine()->getManager();

        foreach ($queryProxy->getQuery()->iterate() as $object) {
            $user = $object[0]->getUser();
            $em->remove($user);
        }

        try {
            $em->flush();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param ProxyQueryInterface $query
     *
     * @return RedirectResponse
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function batchActionApprove(ProxyQueryInterface $query)
    {
        if (false === $this->admin->isGranted('DELETE')) {
            throw new AccessDeniedException();
        }

        try {
            $this->batchApproveUpdate($this->admin->getClass(), $query);
            $this->addFlash('sonata_flash_success', 'Одобрение прошло успешно');
        } catch (ModelManagerException $e) {
            $this->addFlash('sonata_flash_error', 'При одобрении произошла ошибка');
        }

        return new RedirectResponse(
            $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters())) . '#to-do'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function batchApproveUpdate($class, ProxyQueryInterface $queryProxy)
    {
        $queryProxy->select('DISTINCT ' . $queryProxy->getRootAlias());

        try {
            /** @var AbstractAdvert[] $object */
            foreach ($queryProxy->getQuery()->iterate() as $object) {
                $this->get('easyb.all_advert.manager')->adminApprove($object[0]);

                /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
                $dispatcher = $this->container->get('event_dispatcher');
                if ($object[0]->isOffer() && !$object[0]->getFromAdmin()) {

                    $event = new ReactionEvent();
                    $event->setFormData($object[0]);
                    $event->setUser($object[0]->getUser());
                    $dispatcher->dispatch(ReactionEvent::SUBMIT_ADMIN_OFFER, $event);

                } else {
                    $event = new DemandEvent($object[0]);
                    $dispatcher->dispatch(DemandEvent::ADMIN_DEMAND_APPROVE, $event);
                }



            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param ProxyQueryInterface $query
     *
     * @return RedirectResponse
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function batchActionImprove(ProxyQueryInterface $query)
    {
        if (false === $this->admin->isGranted('DELETE')) {
            throw new AccessDeniedException();
        }

        try {
            $this->batchImproveUpdate($this->admin->getClass(), $query);
            $this->addFlash('sonata_flash_success', 'Отправка на доработку прошла успешно');
        } catch (ModelManagerException $e) {
            $this->addFlash('sonata_flash_error', 'При отправке на доработку произошла ошибка');
        }

        return new RedirectResponse(
            $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters())) . '#to-do'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function batchImproveUpdate($class, ProxyQueryInterface $queryProxy)
    {
        $queryProxy->select('DISTINCT ' . $queryProxy->getRootAlias());

        try {
            /** @var AbstractAdvert[] $object */
            foreach ($queryProxy->getQuery()->iterate() as $object) {
                $this->get('easyb.all_advert.manager')->adminImprove($object[0]);

                /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
                $dispatcher = $this->container->get('event_dispatcher');
                $event = new ReactionEvent();
                $event->setFormData($object[0]);
                $event->setUser($object[0]->getUser());

                $object[0]->isOffer() && !$object[0]->getFromAdmin() ?
                    $dispatcher->dispatch(ReactionEvent::SUBMIT_ADMIN_OFFER_IMPROVE, $event) :
                    $dispatcher->dispatch(ReactionEvent::SUBMIT_ADMIN_DEMAND_IMPROVE, $event);
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param ProxyQueryInterface $query
     *
     * @return RedirectResponse
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function batchActionDeleteAbstract(ProxyQueryInterface $query)
    {
        if (false === $this->admin->isGranted('DELETE')) {
            throw new AccessDeniedException();
        }

        try {
            $this->batchDeleteAbstract($this->admin->getClass(), $query);
            $this->addFlash('sonata_flash_success', 'Удаление обьявления прошло успешно');
        } catch (ModelManagerException $e) {
            $this->addFlash('sonata_flash_error', 'При удалении обьявления произошла ошибка');
        }

        return new RedirectResponse(
            $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters())) . '#to-do'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function batchDeleteAbstract($class, ProxyQueryInterface $queryProxy)
    {
        $queryProxy->select('DISTINCT ' . $queryProxy->getRootAlias());
        $em = $this->getDoctrine()->getManager();

        foreach ($queryProxy->getQuery()->iterate() as $object) {
            $em->remove($object[0]);
        }

        try {
            $em->flush();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param ProxyQueryInterface $query
     *
     * @return RedirectResponse
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function batchActionDecline(ProxyQueryInterface $query)
    {
        if (false === $this->admin->isGranted('DELETE')) {
            throw new AccessDeniedException();
        }

        $modelManager = $this->admin->getModelManager();
        try {
            $modelManager->batchDelete($this->admin->getClass(), $query);
            $this->addFlash('sonata_flash_success', 'Отклонение прошло успешно');
        } catch (ModelManagerException $e) {
            $this->addFlash('sonata_flash_error', 'При отклонении произошла ошибка');
        }

        return new RedirectResponse(
            $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters())) . '#to-do'
        );
    }

    /**
     * return the Response object associated to the edit action
     *
     *
     * @param mixed $id
     *
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @return Response
     */
    public function editAction($id = null)
    {
        // the key used to lookup the template
        $templateKey = 'edit';

        $id = $this->get('request')->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }

        $this->admin->setSubject($object);

        /** @var $form \Symfony\Component\Form\Form */
        $form = $this->admin->getForm();
        $form->setData($object);

        if ($this->getRestMethod() == 'POST') {
            $form->bind($this->get('request'));

            $isFormValid = $form->isValid();

            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {
                $object->setIsApproved(true);
                $this->admin->update($object);

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(
                        array(
                            'result' => 'ok',
                            'objectId' => $this->admin->getNormalizedIdentifier($object)
                        )
                    );
                }

                $this->addFlash(
                    'sonata_flash_success',
                    $this->admin->trans(
                        'flash_edit_success',
                        array('%name%' => $this->admin->toString($object)),
                        'SonataAdminBundle'
                    )
                );

                // redirect to edit mode
                return $this->redirectTo($object);
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->addFlash(
                        'sonata_flash_error',
                        $this->admin->trans(
                            'flash_edit_error',
                            array('%name%' => $this->admin->toString($object)),
                            'SonataAdminBundle'
                        )
                    );
                }
            } elseif ($this->isPreviewRequested()) {
                // enable the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
                $this->admin->getShow();
            }
        }

        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render(
            $this->admin->getTemplate($templateKey),
            array(
                'action' => 'edit',
                'form' => $view,
                'object' => $object,
            )
        );
    }
}
