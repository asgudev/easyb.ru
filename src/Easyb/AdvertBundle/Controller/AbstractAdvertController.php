<?php

namespace Easyb\AdvertBundle\Controller;

use Easyb\AdvertBundle\Entity\AbstractAdvert;
use Easyb\AdvertBundle\Entity\AdvertFile;
use Easyb\AdvertBundle\Entity\Demand;
use Easyb\AdvertBundle\Entity\Offer;
use Easyb\AdvertBundle\Entity\Reaction;
use Easyb\MainBundle\Controller\ControllerHelperTrait;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Pagerfanta\Adapter\DoctrineORMAdapter as PagerAdapter;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/")
 */
class AbstractAdvertController extends Controller
{
    use ControllerHelperTrait;

    /**
     * @param Form $form
     * @param \Easyb\AdvertBundle\Entity\AbstractAdvert $advert
     * @return bool
     */
    public function processForm(Form $form, AbstractAdvert $advert)
    {
        if ($this->getRequest()->isMethod('POST')) {
            $request = $this->getRequest();
            //dump($request);
            //dump($advert);
            $form->handleRequest($request);
            //dump($advert); die();
            if ($form->isValid()) {
                if ($form->getData()->getId()) {
                    $allAdvertManager = $this->get('easyb.all_advert.manager');
                    if ($advert->isApproved()) {
                        $advert->setIsApproved(false);
                    }
                    $allAdvertManager->adminClearNotes($advert);
                    $allAdvertManager->adminClearImproved($advert);
                    if ($this->validateFiles($advert, $form)) {
                        return false;
                    }
                    $this->getManager()->update($advert);
                } else {
                    if ($this->validateFiles($advert, $form)) {
                        return false;
                    }
                    $this->getManager()->create($advert);
                }

                return true;
            }
        }

        return false;
    }

    /**
     * @param $object
     * @param $form
     *
     * @return bool
     */
    private function validateFiles($object, $form)
    {
        if (count($object->getFiles())) {
            foreach ($object->getFiles() as $file) {
                if ($file->getFile() == null && $file->getDisplayName() && !$file->getName()) {
                    $form->addError(new FormError('Прикрепляемым Вами файл не соответствует формату или превышает установленный размер 10 Mb.'));

                    return true;
                }
                if ($file->getFile() != null && !array_key_exists(mb_convert_case($file->getFile()->getClientOriginalExtension(), MB_CASE_LOWER), AdvertFile::$filesTypes)) {
                    $form->addError(new FormError('Прикрепляемым Вами файл не соответствует формату или превышает установленный размер 10 Mb.'));

                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return \Easyb\AdvertBundle\Manager\ReactionManagerInterface
     */
    public function getReactionManager()
    {
        return $this->get('easyb.reaction.manager');
    }


    protected function getMetaData($object = null, $customMetadata = null)
    {
        return $this->get('easyb.manager.metadata')->getMetadata($this->getRequest()->get('_route'), $object, $customMetadata);
    }



    /**
     * @return Response
     * @param \Easyb\AdvertBundle\Entity\AdvertFile $file
     *
     * @Route("/save_file/{id}", name="save_file", requirements={"id" = "\d+"})
     */
    public function saveFileAction(AdvertFile $file)
    {
        if ($file->getAdvert() != null) {
            $company = $file->getAdvert()->getJob();
        } elseif ($file->getReaction() != null) {
            $company = $file->getReaction()->getJob();
        } elseif ($file->getProposal() != null) {
            $company = $file->getProposal()->getReaction()->getJob();
        } elseif ($file->getDemand() != null) {
            $company = $file->getDemand()->getJob();
        } else {
            $company = $file->getJob();
        }
        $ext = end(explode(".", $file->getName()));

        $response = new Response();
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $company->getName() . "_" . $file->getDisplayName() . "." . $ext . '"');
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->setContent(file_get_contents($file->getAbsolutePath()));

        return $response;
    }

    /**
     *
     * @param \Easyb\AdvertBundle\Entity\AdvertFile $file
     * @return Response
     *
     * @Route("/preview_file/{id}", name="preview_file", requirements={"id" = "\d+"})
     */
    public function previewFileAction(Request $request, AdvertFile $file)
    {
        $file_path = $request->getHost() . $file->getWebPath();
        $ext = end(explode('.', $file->getName()));
        $type = !in_array($ext, ['gif', 'jpeg', 'jpg', 'png', 'bmp',]);


        return $this->render("EasybAdvertBundle:Advert:previewFile.html.twig", [
            'file' => $file,
            'type' => $type,
            'file_path' => $file_path
        ]);
    }

    /**
     * @param     $query
     * @param int $maxPerPage
     *
     * @return \Pagerfanta\Pagerfanta
     */
    public function getPager($query, $maxPerPage = AbstractAdvert::COUNT_ON_LIST_PAGE)
    {
        $paginator = new Pagerfanta(new PagerAdapter($query, false));
        $paginator->setMaxPerPage($maxPerPage);
        $paginator->setCurrentPage($this->getPage(), false, true);

        return $paginator;
    }

    /**
     * @return int|mixed|null
     */
    public function getPage()
    {
        return $this
            ->getRequest()
            ->get('page') ? $this
            ->getRequest()
            ->get('page') : 1;
    }

    /**
     * @param \Easyb\AdvertBundle\Entity\AbstractAdvert $advert
     * @Route("/admin_up/{id}", name="advert_up")
     * @Secure(roles="ROLE_ADMIN")
     *
     * @return RedirectResponse
     */
    public function upAdminAction(AbstractAdvert $advert)
    {
        if ($advert instanceof Demand) {
            $this->get('easyb.demand.manager')->upNow($advert);

            return new RedirectResponse($this->generateUrl('admin_easyb_advert_demand_list') . '#id-' . $advert->getId());
        } elseif ($advert instanceof Offer) {
            $this->get('easyb.offer.manager')->upNow($advert);

            return new RedirectResponse($this->generateUrl('admin_easyb_advert_offer_list') . '#id-' . $advert->getId());
        }
    }

    /**
     * @param \Easyb\AdvertBundle\Entity\AbstractAdvert $advert
     * @Route("/admin/advert/approve/{id}", name="approve")
     * @Secure(roles="ROLE_ADMIN")
     *
     * @return RedirectResponse
     */
    public function approveAdminAction(AbstractAdvert $advert)
    {
        $this->get('easyb.all_advert.manager')->adminApprove($advert);


        return new RedirectResponse($this->generateUrl('admin_easyb_advert_abstractadvert_list'));
    }

    /**
     * @param \Easyb\AdvertBundle\Entity\AbstractAdvert $advert
     * @Route("/admin/advert/decline/{id}", name="decline")
     * @Secure(roles="ROLE_ADMIN")
     *
     * @return RedirectResponse
     */
    public function declineAdminAction(AbstractAdvert $advert)
    {
        $this->get('easyb.all_advert.manager')->adminDecline($advert);

        return new RedirectResponse($this->generateUrl('admin_easyb_advert_abstractadvert_list'));
    }

    /**
     * @Route("/admin/advert/improve/{id}", name="improve")
     * @Secure(roles="ROLE_ADMIN")
     *
     * @param \Easyb\AdvertBundle\Entity\AbstractAdvert $advert
     *
     * @return Response
     */
    public function improveAdminAction(AbstractAdvert $advert)
    {
        // Only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return $this->redirect($this->generateUrl('admin_easyb_advert_abstractadvert_list'));
        }

        $isOffer = $advert->isOffer();

        try {
            $form = $isOffer ?
                $this->createForm('admin_offer_improve_type', $advert) :
                $this->createForm('admin_demand_improve_type', $advert);

            $form->handleRequest($this->getRequest());

            $status = $content = '';

            if ($form->isValid()) {
                $status = 'improved';

                if ($isOffer) {
                    $rule1 = $form->get('rule1')->getData();
                    $rule4 = $form->get('rule4')->getData();
                    $rule5 = $form->get('rule5')->getData();

                    $this->get('easyb.all_advert.manager')->adminSetNotesWithRules($advert, $rule1, $rule4, $rule5);
                } else {
                    $notes = $form->get('notes')->getData();

                    $this->get('easyb.all_advert.manager')->adminSetNotes($advert, $notes);
                }

                return JsonResponse::create(array('status' => $status, 'content' => $content));
            }

            $status = 'ok';
            $contentTemplate = $isOffer ? 'EasybAdvertBundle:Admin:approveOfferImprove.html.twig' : 'EasybAdvertBundle:Admin:approveDemandImprove.html.twig';
            $content = $this->renderView($contentTemplate, array('form' => $form->createView()));
        } catch (\Exception $e) {
            $status = 'error';
            $content = $e->getMessage();
        }

        return JsonResponse::create(array('status' => $status, 'content' => $content));
    }

    /**
     * @param AbstractAdvert $advert
     *
     * @return array
     */
    private function setInitialDataCatalog(AbstractAdvert $advert)
    {
        return [
            'advertTypeId' => $advert instanceof Demand ? AbstractAdvert::DEMAND : AbstractAdvert::OFFER,
            'cityId' => $advert->getJob()->getCity()->getId(),
            'categoryId' => $advert->getCategory()->getId()
        ];
    }
}
