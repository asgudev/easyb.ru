<?php

namespace Easyb\AdvertBundle\Controller;

use Easyb\AdvertBundle\Entity\AbstractAdvert;
use Easyb\AdvertBundle\Entity\AdvertFile;
use Easyb\AdvertBundle\Entity\Demand;
use Easyb\AdvertBundle\Entity\Offer;
use Easyb\AdvertBundle\Entity\Reaction;
use Easyb\AdvertBundle\Events\DemandEvent;
use Easyb\AdvertBundle\Form\Type\FrontendDemandType;
use Easyb\AdvertBundle\Form\Type\FrontendReactionType;
use Easyb\MainBundle\Entity\SiteSettings;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DemandController extends AbstractAdvertController
{
    /**
     * @Template()
     *
     * @Route("/demands", name="demands")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     *
     * @return array
     */
    public function indexAction()
    {
        //dump($this->get('session')->getFlashBag()); die();
        $siteSettings = $this->getDoctrine()->getRepository('EasybMainBundle:SiteSettings')->findBy(array('route' => SiteSettings::DEMANDS_ROUTE));

        $count = $this->getRequest()->query->get('count');
        $this->getBreadCrumbsManager()->create(['EasyB' => 'homepage', 'Мой тендер' => 'add_demand', 'Тендеры' => null]);

        $demands = $this->getPager($this->getManager()->getRepository()->getQueryByUser($this->getUser()), $count ? $count : Demand::COUNT_ON_LIST_PAGE);

        return [
            'metadata' => $this->getMetadata($siteSettings[0] ?: null),
            'adverts' => $demands,
        ];
    }

    /**
     * @param $hash
     *
     * @Route("/hidden/demand/{private_hash}", name="hidden_demand")
     * @ParamConverter("demand", class="EasybAdvertBundle:Demand", options={"hash" = "private_hash"})
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     */
    public function showHiddenDemandAction(Demand $demand)
    {
        if ((!$demand) || ($this->getUser() == null) || ($demand->isApproved() == false)) {
            throw new NotFoundHttpException('Такого объявления не существует.');
        }

        $similarAds = $this->getDoctrine()->getRepository("EasybAdvertBundle:Demand")->findSimilarAds($demand);
        /*if ($this->getUser() != $demand->getUser() && !$demand->isApproved()) {
            throw new NotFoundHttpException('Такого объявления не существует.');
        }*/

        $canAskQuestion = false;
        $canAnswer = false;
        $hasReactions = false;
        $hasCategories = false;

        if ($this->getUser() != null) {
            $jobs = $this->getUser()->getJobs();

            foreach ($jobs as $key => $job) {
                if (!in_array($demand->getCategory(), $job->getCategories()->toArray())) {
                    unset($jobs[$key]);
                }
            }
            if (count($jobs) > 0) $hasCategories = true;

            foreach ($jobs as $key => $job) {
                $reactions = $this->getDoctrine()->getRepository("EasybAdvertBundle:Reaction")->findBy(
                    array(
                        'user' => $this->getUser(),
                        'demand' => $demand,
                        'job' => $job
                    ));
                if (count($reactions) > 0) {
                    $hasReactions = true;
                    unset($jobs[$key]);
                }
                foreach ($reactions as $reaction) {
                    if (($reaction->getStatus() == Reaction::STATUS_APPROVE) || ($reaction->getStatus() == Reaction::STATUS_DIRECT)) {
                        $canAskQuestion = true;
                    }
                }
            }
            if (count($jobs) > 0) $canAnswer = true;
        }

        $this->getBreadCrumbsManager()->create(
            [
                'EasyB' => 'homepage',
                'Каталог' => 'catalog_only_demand',
                $demand->getCategory()->getName() => 'catalog_demand',
                $demand->getName() => null
            ],
            [
                $demand->getCategory()->getName() => [
                    'url' => $demand->getCategory()->getSlug()
                ],
                $demand->getJob()->getName() => [
                    'slug' => $demand->getJob()->getSlug()
                ]
            ]);
        $this->getManager()->addViews($demand);

        $offers = $this->get('easyb.offer.manager')->getOffersByJob($demand->getJob());
        $demands = $this->get('easyb.demand.manager')->getDemandsByJob($demand->getJob());

        return $this->render('EasybAdvertBundle:Demand:show.html.twig', [
            'similarAds' => $similarAds,
            'metadata' => $this->getMetadata($demand, $this->getDoctrine()->getRepository('EasybAdvertBundle:AdvertMetaData')->findAll()[0]),
            'demand' => $demand,
            'files' => $demand->getFiles(),
            'isOffer' => false,
            'canAnswer' => $canAnswer,
            'hasReactions' => $hasReactions,
            'hasCategories' => $hasCategories,
            'canAskQuestion' => $canAskQuestion,
            'offers' => $offers,
            'demands' => $demands,
        ]);
    }


    /**
     * @param string $slug
     * @Route("/demand/{category}/{city}/{slug}", name="show_demand")
     *
     * @return array
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function showDemandAction($slug)
    {
        /**
         * @var Demand $demand
         */
        $demand = $this->getDoctrine()->getRepository('EasybAdvertBundle:Demand')->findOneBySlug($slug);
        if ($this->getUser() != $demand->getUser()) {
            if ((!$demand) || (!$demand->isApproved())) {
                throw new NotFoundHttpException('Такого объявления не существует.');
            }
        }

        $inDemand = false;
        if ($this->getUser() != null) {
            $reactions = $this->getDoctrine()->getRepository("EasybAdvertBundle:Reaction")->findByUserAndDemand($this->getUser(), $demand);
            if (count($reactions) > 0) {
                $inDemand = true;
            }
        }

        if ((!$demand->getIsPublic()) && ($this->getUser() != $demand->getUser()) && (!$inDemand)) {
            throw new NotFoundHttpException('Приватный тендер');
        }


        /*
            if ($demand->isDeleted()) {
                throw new NotFoundHttpException('Объявление удалено');
            }*/

        $similarAds = $this->getDoctrine()->getRepository("EasybAdvertBundle:Demand")->findSimilarAds($demand);
        /*if ($this->getUser() != $demand->getUser() && !$demand->isApproved()) {
            throw new NotFoundHttpException('Такого объявления не существует.');
        }*/

        $canAskQuestion = false;
        $canAnswer = false;
        $hasReactions = false;
        $hasCategories = false;

        if ($this->getUser() != null) {
            $jobs = $this->getUser()->getJobs();

            foreach ($jobs as $key => $job) {
                if (!in_array($demand->getCategory(), $job->getCategories()->toArray())) {
                    unset($jobs[$key]);
                }
                if (!$job->isApproved())
                {
                    unset($jobs[$key]);
                }
            }
            if (count($jobs) > 0) $hasCategories = true;

            foreach ($jobs as $key => $job) {
                $reactions = $this->getDoctrine()->getRepository("EasybAdvertBundle:Reaction")->findBy(
                    array(
                        'user' => $this->getUser(),
                        'demand' => $demand,
                        'job' => $job
                    ));
                if (count($reactions) > 0) {
                    $hasReactions = true;
                    unset($jobs[$key]);
                }
                foreach ($reactions as $reaction) {
                    if (($reaction->getStatus() == Reaction::STATUS_APPROVE) || ($reaction->getStatus() == Reaction::STATUS_DIRECT)) {
                        $canAskQuestion = true;
                    }
                }
            }
            if (count($jobs) > 0) $canAnswer = true;
        }

        if ($demand->getExpireProposalAt() < (new \DateTime())) {
            $canAskQuestion = false;
        }

        $showContact = $this->getUser() == $demand->getUser() ? true : false;

        $this->getBreadCrumbsManager()->create(
            [
                'EasyB' => 'homepage',
                'Каталог' => 'catalog_only_demand',
                $demand->getCategory()->getName() => 'catalog_demand',
                $demand->getName() => null
            ],
            [
                $demand->getCategory()->getName() => [
                    'url' => $demand->getCategory()->getSlug()
                ],
                $demand->getJob()->getName() => [
                    'slug' => $demand->getJob()->getSlug()
                ]
            ]);
        $this->getManager()->addViews($demand);

        $offers = $this->get('easyb.offer.manager')->getOffersByJob($demand->getJob());
        $demands = $this->get('easyb.demand.manager')->getDemandsByJob($demand->getJob());

        return $this->render('EasybAdvertBundle:Demand:show.html.twig', [
            'similarAds' => $similarAds,
            'metadata' => $this->getMetadata($demand, $this->getDoctrine()->getRepository('EasybAdvertBundle:AdvertMetaData')->findAll()[0]),
            'demand' => $demand,
            'files' => $demand->getFiles(),
            'isOffer' => false,
            'canAnswer' => $canAnswer,
            'hasReactions' => $hasReactions,
            'hasCategories' => $hasCategories,
            'canAskQuestion' => $canAskQuestion,
            'showContact' => $showContact,
            'offers' => $offers,
            'demands' => $demands,
        ]);
    }


    /**
     * @return \Easyb\AdvertBundle\Manager\DemandManager
     */
    public function getManager()
    {
        return $this->get('easyb.demand.manager');
    }

    /**
     * @Template()
     * @Route("/add_demand", name="add_demand")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     *
     * @return array
     */
    public function addDemandAction()
    {
        $siteSettings = $this->getDoctrine()->getRepository('EasybMainBundle:SiteSettings')->findBy(array('route' => SiteSettings::ADD_DEMAND_ROUTE));

        $this->getBreadCrumbsManager()->create(['EasyB' => 'homepage', 'Мой тендер' => 'demands', 'Добавить тендер' => null]);
        $demand = $this->getManager()->getInstance($this->getUser());

        $inviteFile = new AdvertFile();
        $inviteFile->setDisplayName("Приглашение к участию в тендере");
        $inviteFiles[] = $inviteFile;

        $form = $this->createForm(new FrontendDemandType(), $demand, [
            'inviteFiles' => $inviteFiles,
        ]);

        if ($this->processDemandForm($form, $demand)) {

            $this->get('session')->getFlashBag()->add(
                'newAdvert',
                'После проверки администратором информация о Вашем тендере будет будет опубликована. Пожалуйста, соблюдайте <a href="/page/add_rules">правила</a> размещения информации на сайте'
            );
            return $this->redirect('demands');
        }

        return [
            'form' => $form->createView(),
            'savePath' => $this->generateUrl('add_demand'),
            'metadata' => $this->getMetadata($siteSettings[0] ?: null),
            'demand' => $demand,
            //'categories' => $categories,
            'errors' => null
        ];
    }

    /**
     * @param Form $form
     * @param \Easyb\AdvertBundle\Entity\Demand $advert
     * @return bool
     */
    public function processDemandForm(Form $form, Demand $demand)
    {
        if ($this->getRequest()->isMethod('POST')) {
            $request = $this->getRequest();
            $form->handleRequest($request);

            if ($form->isValid()) {
                if ($form->getData()->getId()) {
                    $allAdvertManager = $this->get('easyb.all_advert.manager');
                    if ($demand->isApproved()) {
                        $demand->setIsApproved(false);
                    }
                    $allAdvertManager->adminClearNotes($demand);
                    $allAdvertManager->adminClearImproved($demand);
                    if ($this->validateFiles($demand, $form)) {
                        return false;
                    }
                    $this->getManager()->update($demand);
                } else {
                    if ($this->validateFiles($demand, $form)) {
                        return false;
                    }
                    $this->getManager()->create($demand);
                }

                return true;
            }
        }

        return false;
    }

    /**
     * @param $object
     * @param $form
     *
     * @return bool
     */
    private function validateFiles($object, $form)
    {
        if (count($object->getFiles())) {
            foreach ($object->getFiles() as $file) {
                if ($file->getFile() == null && $file->getDisplayName() && !$file->getName()) {
                    $form->addError(new FormError('Прикрепляемым Вами файл не соответствует формату или превышает установленный размер 10 Mb.'));

                    return true;
                }
                if ($file->getFile() != null && !array_key_exists(mb_convert_case($file->getFile()->getClientOriginalExtension(), MB_CASE_LOWER), AdvertFile::$filesTypes)) {
                    $form->addError(new FormError('Прикрепляемым Вами файл не соответствует формату или превышает установленный размер 10 Mb.'));

                    return true;
                }
            }
        }

        return false;
    }


    /**
     * @param \Easyb\AdvertBundle\Entity\Demand $demand
     * @Route("/demand/up/{id}", name="demand_up")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     *
     * @return JsonResponse
     */
    public function upAction(Demand $demand)
    {
        if ($demand->validateUser($this->getUser())) {
            $this->getManager()->up($demand);
        }

        return new JsonResponse([
            'newTemplate' => $this->renderView('EasybAdvertBundle:Demand:demand.html.twig', ['advert' => $demand]),
            'id' => $demand->getId()
        ]);
    }

    /**
     * @param \Easyb\AdvertBundle\Entity\Demand $demand
     * @Route("/demand/delete/{id}", name="demand_delete")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     *
     * @return array
     */
    public function deleteAction(Demand $demand)
    {
        if ($demand->validateUser($this->getUser())) {
            $dispatcher = $this->container->get('event_dispatcher');
            $event = new DemandEvent($demand);

            $dispatcher->dispatch(DemandEvent::DELETE, $event);
        }
        return $this->redirect($this->generateUrl('demands'));
    }

    /**
     * @param \Easyb\AdvertBundle\Entity\Demand $demand
     * @Route("/demand/stop/{id}", name="demand_stop")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     *
     * @return array
     */
    public function stopAction(Demand $demand)
    {
        if ($demand->validateUser($this->getUser())) {
            $dispatcher = $this->container->get('event_dispatcher');
            $event = new DemandEvent($demand, false);

            $dispatcher->dispatch(DemandEvent::DELETE, $event);
        }
        return $this->redirect($this->generateUrl('demands'));
    }

    /**
     * @param \Easyb\AdvertBundle\Entity\Demand $demand
     * @Route("/demand/active/{id}", name="demand_active")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     *
     * @return JsonResponse
     */
    public function activeAction(Demand $demand)
    {
        if ($demand->validateUser($this->getUser())) {
            $this->getManager()->changeActive($demand);
        }

        return new JsonResponse([
            'newTemplate' => $this->renderView('EasybAdvertBundle:Demand:demand.html.twig', ['advert' => $demand]),
            'id' => $demand->getId()
        ]);
    }

    /**
     * @Template("EasybAdvertBundle:Demand:addDemand.html.twig")
     * @param \Easyb\AdvertBundle\Entity\Demand $demand
     * @Route("/demand/edit/{id}", name="demand_edit")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(Demand $demand)
    {
        $this->getBreadCrumbsManager()->create(['EasyB' => 'homepage', 'Мой тендер' => 'demands', 'Редактирование объявления' => null]);

        if (count($demand->getInviteFile()) == 0 ) {
            $inviteFile = new AdvertFile();
            $inviteFile->setDisplayName("Приглашение к участию в тендере");
            $inviteFiles[] = $inviteFile;
        } else {
            $inviteFiles = $demand->getInviteFile()->toArray();
        }

        $form = $this->getForm($this->get('easyb.form.type.demand'), $demand, [
            'inviteFiles' => $inviteFiles
        ]);

        if ($demand->validateUser($this->getUser()) && $this->processDemandForm($form, $demand)) {
            return $this->redirect($this->generateUrl('demands'));
        }

        return [
            'form' => $form->createView(),
            'demand' => $demand,
            'savePath' => $this->generateUrl('demand_edit', array('id' => $demand->getId())),
            'errors' => null
        ];
    }

    /**
     * @Template()
     * @Route("/my_demand", name="my_demand")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function myDemandAction()
    {
        $siteSettings = $this->getDoctrine()->getRepository('EasybMainBundle:SiteSettings')->findBy(array('route' => SiteSettings::MY_DEMAND_ROUTE));

        $count = $this->getRequest()->query->get('count');
        $this->getBreadCrumbsManager()->create(['EasyB' => 'homepage', 'Мой тендер' => 'demands', 'Статус моих тендеров' => null]);

        //$pager = $this->getReactionManager()->getRepository()->getDemandReactionQuery($this->getUser());
        $pager = $this->getDoctrine()->getRepository("EasybAdvertBundle:Demand")->findBy([
            'user' => $this->getUser()
        ], [
            'createdAt' => "DESC"
        ]);

        //->getDemandReactionQuery($this->getUser());

        //$myDemands = $this->getPager($pager, $count ? $count : Demand::COUNT_ON_LIST_PAGE);
        //$this->getRequest()->getSession()->set('referer', $this->getRequest()->getRequestUri());

        return [
            'reactions' => $pager,
            'demands' => $pager,
            'metadata' => $this->getMetadata($siteSettings[0] ?: null),
        ];
    }

    /**
     * @param \Easyb\AdvertBundle\Entity\Reaction $reaction
     * @Route("/my_demand/approve/{id}", name="demand_approve")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     *
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function approveAction(Reaction $reaction)
    {
        if ($reaction->getDemand()->validateUser($this->getUser())) {
            $this->getReactionManager()->approve($reaction);
        }

        return $this->redirectInReactions($reaction);
    }

    /**
     * @param Reaction $reaction
     *
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function redirectInReactions(Reaction $reaction)
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            return new JsonResponse([
                'newTemplate' => $this->renderView('EasybAdvertBundle:Demand:reaction.html.twig', [
                    'reaction' => $reaction,
                    'show_demand' => true,
                ]),
                'id' => $reaction->getId(),
                'demandNotification' => $this->getUser()->getDemandNotification(),
            ]);
        } else {
            return $this->redirect($this->generateUrl('my_demand'));
        }
    }

    /**
     * @param \Easyb\AdvertBundle\Entity\Reaction $reaction
     * @Route("/my_demand/decline/{id}", name="demand_decline")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     *
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function declineAction(Reaction $reaction)
    {
        if ($reaction->getDemand()->validateUser($this->getUser())) {
            $this->getReactionManager()->decline($reaction);
        }

        return $this->redirectInReactions($reaction);
    }

    /**
     * Одобрить предложение
     *
     * @Template()
     * @param \Easyb\AdvertBundle\Entity\Offer $advert
     * @Route("/send_demand/{id}", name="send_demand")
     *
     * @return array
     */
    public function sendDemandAction(Offer $advert)
    {
        $adverts = $this->getManager()->getRepository()->findByUserAndCategory($this->getUser(), $advert->getCategory());
        if (empty($adverts)) {
            return new Response($this->renderView('EasybAdvertBundle:Demand:noDemands.html.twig'));
        }
        $form = $this->createForm(new FrontendReactionType());

        if ($this->getRequest()->getMethod() == 'POST') {
            $form->submit($this->getRequest());
            $secondAdvert = $this->getRequest()->request->all();
            if ($form->isValid()) {
                $this->get('easyb.reaction.manager')->addNewReactionForOffer($advert, $secondAdvert['reaction_type']['proposal'], $form->getData());

                return $this->redirect($this->generateUrl('catalog', [
                    'advertTypeId' => AbstractAdvert::OFFER
                ]));
            }
        }

        return [
            'adverts' => $adverts,
            'advert' => $advert,
            'form' => $form->createView()
        ];
    }

    public function listAction()
    {
        return new Response();
    }
}
