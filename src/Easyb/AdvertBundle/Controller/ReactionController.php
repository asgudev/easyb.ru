<?php

namespace Easyb\AdvertBundle\Controller;

use Easyb\AdvertBundle\Entity\AbstractAdvert;
use Easyb\AdvertBundle\Entity\AdvertFile;
use Easyb\AdvertBundle\Entity\Demand;
use Easyb\AdvertBundle\Entity\Proposal;
use Easyb\AdvertBundle\Entity\Reaction;
use Easyb\AdvertBundle\Events\ProposalEvent;
use Easyb\AdvertBundle\Events\ReactionEvent;
use Easyb\AdvertBundle\Form\Type\AdminAdvertType;
use Easyb\AdvertBundle\Form\Type\FrontendProposalType;
use Easyb\AdvertBundle\Form\Type\FrontendReactionEditType;
use Easyb\AdvertBundle\Form\Type\FrontendReactionOfferType;
use Easyb\AdvertBundle\Form\Type\FrontendDirectProposalType;
use Easyb\MainBundle\Controller\ControllerHelperTrait;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ReactionController extends Controller
{
    use ControllerHelperTrait;

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/my_demand/delete", name="delete_demand_reactions")
     */
    public function deleteDemandReactionsAction()
    {
        if ($this->getRequest()->getMethod() == 'POST') {
            $data = $this->getRequest()->request->all();
            if (!empty($data)) {
                $this->getReactionManager()->removeReactions($data['reactions'], Reaction::DEMAND, $this->getUser());
            }
        }

        return $this->redirect($this->generateUrl('my_demand'));
    }

    /**
     * @return \Easyb\AdvertBundle\Manager\ReactionManagerInterface
     */
    public function getReactionManager()
    {
        return $this->get('easyb.reaction.manager');
    }


    /**
     * @param Reaction $reaction
     * @return Response
     *
     * @Route("/demand/winner/{id}", name="winner_popup")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     */
    public function selectWinnerPopupAction(Proposal $proposal)
    {
        return $this->render('EasybAdvertBundle:Reaction:selectWinner.html.twig', [
            'proposal' => $proposal
        ]);
    }

    /**
     * @param Reaction $reaction
     * @return Response
     *
     * @Route("/demand/setwinner/{id}", name="select_winner")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     */
    public function selectWinnerAction(Proposal $proposal)
    {
        $this->getReactionManager()->setWinner($proposal);

        return $this->redirect("/my_demand");
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/my_offer/delete", name="delete_offer_reactions")
     */
    public function deleteOfferReactionsAction()
    {
        if ($this->getRequest()->getMethod() == 'POST') {
            $data = $this->getRequest()->request->all();
            if (!empty($data)) {

                //$this->getReactionManager()->removeReactions($data['reactions'], Reaction::OFFER, $this->getUser());

                $this->getDoctrine()->getRepository("EasybAdvertBundle:Reaction")->hideReactionFromOffers($data["reactions"]);
            }
        }

        return $this->redirect($this->getRequest()->getSession()->get('referer'));
    }


    /**
     * @Template("EasybAdvertBundle:Reaction:edit.html.twig")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/my_offer/edit/{id}", name="edit_reaction")
     */
    public function editReactionsAction(Reaction $reaction)
    {
        $jobs = $this->getUser()->getJobs();
        $advert = $reaction->getDemand();

        if (($this->getUser() == $reaction->getUser()) && ((new \DateTime()) < $reaction->getDemand()->getExpireAt()) && ($reaction->getStatus() == Reaction::STATUS_START)) {

            $form = $this->createForm(new FrontendReactionEditType(), $reaction, array('jobs' => $jobs));
            $request = $this->getRequest();
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $em = $this->getDoctrine()->getManager();
                $em->persist($reaction);
                $em->flush();

                return $this->redirect($this->generateUrl('my_offer'));
            }

            return [
                'reaction' => $reaction,
                'advert' => $advert,
                'form' => $form->createView()
            ];

        } else {
            return $this->redirect($this->generateUrl('my_offer'));
        }
    }

    /**
     * @Template("EasybAdvertBundle:Reaction:show.html.twig")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/my_demands/reaction/{id}", name="show_reaction")
     */
    public function showReactionsAction(Reaction $reaction)
    {
        return array(
            'reaction' => $reaction
        );
    }


    /**
     * @Template()
     * @param \Easyb\AdvertBundle\Entity\Offer $offer
     * @param \Easyb\AdvertBundle\Entity\Demand $demand
     * @Route("/proposal/{demand}/{reaction}", name="proposal")
     *
     * @return array
     */
    /*
    public function proposalAction(Demand $demand, Reaction $reaction)
    {
        $this->getBreadCrumbsManager()->create(['EasyB' => 'homepage', 'Мой тендер' => 'offers', 'Отклики' => 'my_demand', 'Просмотр отклика' => null]);
        $reaction = $this->getReactionManager()->getRepository()->findByOfferAndDemand($offer, $demand);
        if ($reaction) {
            $this->getReactionManager()->viewedProposal($reaction);
        }
        $this->container->get('easyb.offer.manager')->addViews($offer);

        return [
            'advert' => $offer,
            'job' => $offer->getJob(),
            'files' => $offer->getFiles(),
            'reaction' => $reaction,
            'isOffer' => $offer instanceof Offer
        ];
    }*/

    /**
     * Связаться с компанией
     *
     * @Template()
     * @param \Easyb\AdvertBundle\Entity\AbstractAdvert $advert
     * @Route("/send_fromadmin/{id}", name="send_fromadmin")
     *
     * @return array
     */
    public function sendFromAdminAction(AbstractAdvert $advert)
    {
        $form = $this->createForm(new AdminAdvertType());

        if ($this->getRequest()->getMethod() == 'POST') {
            $form->submit($this->getRequest());
            /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
            $dispatcher = $this->container->get('event_dispatcher');
            $event = new ReactionEvent();
            $event->setFormData($form->getData());
            $event->setUser($advert->getUser());
            $dispatcher->dispatch(ReactionEvent::SUBMIT_ADMIN_ADVERT, $event);

            return $this->redirect($this->generateUrl('catalog'));
        }

        return [
            'advert' => $advert,
            'form' => $form->createView()
        ];
    }


    /**
     * Отправить предложение
     *
     * @Template()
     * @param \Easyb\AdvertBundle\Entity\Demand $advert
     * @Route("/send_reaction/{id}", name="send_reaction")
     *
     * @return array
     */
    public function sendReactionAction(Demand $advert)
    {
        $user = $this->getUser();
        $jobs = [];
        $required_files = [];

        if ($user != null) {
            $jobs = $user->getJobs();
            foreach ($jobs as $key => $job) {
                if (!in_array($advert->getCategory(), $job->getCategories()->toArray())) {
                    unset($jobs[$key]);
                } elseif (!$job->isApproved()) {
                    unset($jobs[$key]);
                } else {
                    $reactions = $this->getDoctrine()->getRepository("EasybAdvertBundle:Reaction")->findBy(
                        array(
                            'user' => $this->getUser(),
                            'demand' => $advert,
                            'job' => $job
                        ));
                    if (count($reactions) > 0) unset($jobs[$key]);
                }
            }
        }

        $reaction = new Reaction($user);
        $files = $this->bit_analysis($advert->getRequiredDocs());
        $required_files_const = Demand::REQUIRED_DOCS_ARRAY;

        foreach ($files as $req_file) {
            $file = new AdvertFile();
            $file->setDisplayName($required_files_const[$req_file]);
            $file->setRequired($req_file);
            $required_files[] = $file;
        }

        $form = $this->createForm(
            new FrontendReactionOfferType(),
            $reaction,
            array(
                'jobs' => $jobs,
                'required_files' => $required_files
            ));


        $form->handleRequest($this->getRequest());

        if ($form->isValid()) {
            $reaction->setDemand($advert);
            $em = $this->getDoctrine()->getManager();
            $em->persist($reaction);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'newReaction',
                "Заявка отправлена. Отследить статус заявки Вы сможете на странице <a href=\"" .
                $this->generateUrl("my_offer", [], true)
                . "\">Статус моего участия в тендере</a>"
            );
            return $this->redirect($this->generateUrl("show_demand", array(
                "slug" => $advert->getSlug(),
                "city" => $advert->getJob()->getCity()->getSlug(),
                "category" => $advert->getCategory()->getSlug()
            )));
        }

        return [
            'advert' => $advert,
            'form' => $form->createView()
        ];
    }


    /**
     * Отправить КП в тендер
     *
     * @param \Easyb\AdvertBundle\Entity\Demand $demand
     * @Route("/send_direct_proposal/{id}", name="send_direct_proposal")
     *
     * @return array
     */
    public function sendDirectProposalAction(Request $request, Demand $demand)
    {
        /*$reaction = new Reaction($this->getUser());
        $reaction->setDemand($demand);
        $reaction->setMessage('direct');
        $reaction->setStatus(Reaction::STATUS_APPROVE);

        $em = $this->getDoctrine()->getManager();
        $em->persist($reaction);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'newReaction',
            "Вы приняты для участия в тендере. Отправить комерческое предложение Вы сможете на странице <a href=\"" .
            $this->generateUrl("my_offer", [], true)
            . "\">Статус моего участия в тендере</a>"
        );
        return $this->redirect($this->generateUrl("my_offer"));*/
        $reaction = new Reaction($this->getUser());
        $reaction->setMessage("direct invite");
        $reaction->setDemand($demand);
        $reaction->setStatus(Reaction::STATUS_DIRECT);

        $jobs = $this->getUser()->getJobs();

        foreach ($jobs as $key => $job) {
            if (!in_array($demand->getCategory(), $job->getCategories()->toArray())) {
                unset($jobs[$key]);
            } elseif (!$job->isApproved()) {
                unset($jobs[$key]);
            } else {
                $reactions = $this->getDoctrine()->getRepository("EasybAdvertBundle:Reaction")->findBy(
                    array(
                        'user' => $this->getUser(),
                        'demand' => $demand,
                        'job' => $job
                    ));
                if (count($reactions) > 0) unset($jobs[$key]);
            }
        }


        $files = $this->bit_analysis($demand->getRequiredDocs());
        $required_files_const = Demand::REQUIRED_DOCS_ARRAY;

        foreach ($files as $req_file) {
            $file = new AdvertFile();
            $file->setDisplayName($required_files_const[$req_file]);
            $file->setRequired($req_file);
            $required_files[] = $file;
        }


        $form = $this->createForm(
            new FrontendDirectProposalType(),
            $reaction,
            [
                'jobs' => $jobs,
                'required_files' => $required_files,
            ]
        );


        $form->handleRequest($this->getRequest());

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($reaction);
            $em->flush();

            /*$this->get('session')->getFlashBag()->add(
                'newReaction',
                "Коммерческое предложение отправлено. Отследить статус заявки Вы сможете на странице <a href=\"" .
                $this->generateUrl("my_offer", [], true)
                . "\">Статус моего участия в тендере</a>"
            );*/
            return $this->redirect($this->generateUrl("my_offer"));
        }

        return $this->render('EasybAdvertBundle:Reaction:sendDirectProposal.html.twig',
            [
                'demand' => $demand,
                'form' => $form->createView()
            ]);

    }

    /**
     * Отправить КП в тендер
     *
     * @Template()
     * @param \Easyb\AdvertBundle\Entity\Reaction $reaction
     * @Route("/send_proposal/{id}", name="send_proposal")
     *
     * @return array
     */
    public function sendProposalAction(Request $request, Reaction $reaction)
    {
        $proposal = new Proposal($reaction);
        $user = $this->getUser();
        $demand = $reaction->getDemand();

        $jobs = [];
        if (($user != null) && ($user == $reaction->getUser())) {
            $jobs = $user->getJobs();
            foreach ($jobs as $key => $job) {
                if (!in_array($demand->getCategory(), $job->getCategories()->toArray())) {
                    unset($jobs[$key]);
                } elseif (!$job->isApproved()) {
                    unset($jobs[$key]);
                } else {
                    $reactions = $this->getDoctrine()->getRepository("EasybAdvertBundle:Reaction")->findBy(
                        array(
                            'user' => $this->getUser(),
                            'demand' => $demand,
                            'job' => $job
                        ));
                    if (count($reactions) > 0) unset($jobs[$key]);
                }
            }
        }


        $form = $this->createForm(
            new FrontendProposalType(),
            $proposal,
            [
                'add_job' => $reaction->getJob() == null ? true : false,
                'jobs' => $jobs,
            ]
        );

        $form->handleRequest($this->getRequest());

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            //$reaction->setJob($form->get("job")->getNormData());
            $em->persist($proposal);
            $em->persist($reaction);
            $em->flush();


            $dispatcher = $this->container->get('event_dispatcher');
            $event = new ProposalEvent($proposal);

            $dispatcher->dispatch(ProposalEvent::PROPOSAL_SEND, $event);
/*
            $this->get('session')->getFlashBag()->add(
                'newReaction',
                "Коммерческое предложение отправлено. Отследить статус заявки Вы сможете на странице <a href=\"" .
                $this->generateUrl("my_offer", [], true)
                . "\">Статус моего участия в тендере</a>"
            );*/
            return $this->redirect($this->generateUrl("my_offer"));
        }

        return [
            'reaction' => $reaction,
            'form' => $form->createView()
        ];
    }

    /**
     * Показать КП для тендера
     *
     * @param \Easyb\AdvertBundle\Entity\Proposal $proposal
     * @Route("/show_proposal/{id}", name="show_proposal")
     *
     * @return array
     */
    public function showProposalAction(Proposal $proposal)
    {
        //TODO: validate user
        return $this->render('@EasybAdvert/Reaction/showProposal.html.twig',
            [
                'proposal' => $proposal
            ]);
    }

    /**
     * Редактировать КП для тендера
     *
     * @param \Easyb\AdvertBundle\Entity\Proposal $proposal
     * @Route("/edit_proposal/{id}", name="edit_proposal")
     *
     * @return array
     */
    public function editProposalAction(Proposal $proposal)
    {
        //TODO: validate user
        $form = $this->createForm(
            new FrontendProposalType(),
            $proposal
        );

        $form->handleRequest($this->getRequest());

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($proposal);
            $em->flush();

            /*$this->get('session')->getFlashBag()->add(
                'newReaction',
                "Коммерческое предложение обновлено. Отследить статус заявки Вы сможете на странице <a href=\"" .
                $this->generateUrl("my_offer", [], true)
                . "\">Статус моего участия в тендере</a>"
            );*/
            return $this->redirect($this->generateUrl("my_offer"));
        }

        return $this->render('@EasybAdvert/Reaction/editProposal.html.twig',
            [
                'proposal' => $proposal,
                'form' => $form->createView()
            ]);
    }

    private function bit_analysis($n)
    {
        $bin_powers = array();
        for ($bit = 0; $bit < count(Demand::REQUIRED_DOCS_ARRAY); $bit++) {
            $bin_power = 1 << $bit;
            if ($bin_power & $n) $bin_powers[$bit] = $bin_power;
        }
        return $bin_powers;
    }

}
