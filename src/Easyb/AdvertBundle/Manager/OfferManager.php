<?php
namespace Easyb\AdvertBundle\Manager;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Easyb\AdvertBundle\Entity\Offer;
use Easyb\UserBundle\Entity\User;
use Easyb\AdvertBundle\Events\OfferEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

use Easyb\UserBundle\Entity\Job;

/**
 * manager OfferManager
 */
class OfferManager extends AbstractAdvertManager
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;
    /**
     * @var \Easyb\AdvertBundle\Entity\OfferRepository
     */
    protected $repository;
    /**
     * @var \Symfony\Component\EventDispatcher\EventDispatcher
     */
    protected $dispatcher;

    /**
     * @param \Doctrine\ORM\EntityManager                        $em
     * @param \Symfony\Component\EventDispatcher\EventDispatcher $dispatcher
     */
    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository('EasybAdvertBundle:Offer');
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param User $user
     *
     * @return Offer
     */
    public function getInstance(User $user)
    {
        return new Offer($user);
    }

    /**
     * @return \Doctrine\ORM\EntityRepository|\Easyb\AdvertBundle\Entity\OfferRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @param \Easyb\AdvertBundle\Entity\Offer $offer
     *
     * @return mixed|void
     */
    public function create(Offer $offer)
    {
        $this->save($offer);
    }

    /**
     * @param \Easyb\AdvertBundle\Entity\Offer $offer
     *
     * @return mixed|void
     */
    public function update(Offer $offer)
    {
        $this->save($offer);
    }

    /**
     * @param \Easyb\AdvertBundle\Entity\Offer $offer
     *
     * @return mixed|void
     * @throws \Exception
     */
    public function delete(Offer $offer)
    {
        $this->deleteObject($offer);

        $this->dispatch(new OfferEvent($offer), OfferEvent::DELETE);
    }

    /**
     * @param OfferEvent  $event
     * @param string      $name
     */
    private function dispatch($event, $name)
    {
        $this->dispatcher->dispatch($name, $event);
    }

    public function getOffersByJob(Job $job)
    {
        return $this->em->getRepository('EasybAdvertBundle:Offer')->findAdvertsByJob($job);

    }
}
