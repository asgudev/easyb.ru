<?php
namespace Easyb\AdvertBundle\Manager;

use Doctrine\ORM\EntityManager;
use Easyb\AdvertBundle\Entity\Demand;
use Easyb\AdvertBundle\Entity\Offer;
use Easyb\AdvertBundle\Entity\Proposal;
use Easyb\AdvertBundle\Entity\Reaction;
use Easyb\AdvertBundle\Events\ProposalEvent;
use Easyb\AdvertBundle\Events\ReactionEvent;
use Easyb\UserBundle\Entity\User;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * manager ReactionManager
 */
class ReactionManager implements ReactionManagerInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;
    /**
     * @var \Easyb\AdvertBundle\Entity\ReactionRepository
     */
    protected $repository;
    /**
     * @var \Symfony\Component\EventDispatcher\EventDispatcher
     */
    protected $dispatcher;

    /**
     * @param \Doctrine\ORM\EntityManager $em
     * @param \Symfony\Component\EventDispatcher\EventDispatcher $dispatcher
     */
    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository('EasybAdvertBundle:Reaction');
        $this->offerRepository = $this->em->getRepository('EasybAdvertBundle:Offer');
        $this->demandRepository = $this->em->getRepository('EasybAdvertBundle:Demand');
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param Reaction $reaction
     *
     * @return mixed
     */
    public function create(Reaction $reaction)
    {
        $this->saveObject($reaction);
    }

    /**
     * @param Reaction $reaction
     *
     * @throws \Exception
     */
    private function saveObject(Reaction $reaction)
    {
        try {
            $this->em->persist($reaction);
            $this->em->flush();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Reaction $reaction
     *
     * @return mixed
     */
    public function delete(Reaction $reaction)
    {
        $this->deleteObject($reaction);

        $this->dispatch(new ReactionEvent($reaction), ReactionEvent::DELETE);
    }

    /**
     * @param Reaction $reaction
     *
     * @throws \Exception
     */
    private function deleteObject(Reaction $reaction)
    {
        try {
            $this->em->remove($reaction);
            $this->em->flush();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param string $name
     */
    private function dispatch($event, $name)
    {
        $this->dispatcher->dispatch($name, $event);
    }

    /**
     * @param Demand $demand
     * @param Offer $offer
     *
     * @return mixed
     */
    public function createInstance(Demand $demand, Offer $offer)
    {
        $reaction = new Reaction();
        $reaction->setDemand($demand);
        $reaction->setOffer($offer);

        return $reaction;
    }

    /**
     * Нажали одобрить отклик
     *
     * @param Reaction $reaction
     *
     * @return mixed
     */
    public function approve(Reaction $reaction)
    {
        $reaction->setStatus(Reaction::STATUS_APPROVE);
        $this->saveObject($reaction);

        $this->dispatch(new ReactionEvent($reaction), ReactionEvent::APPROVE_OR_DECLINE);
    }

    /**
     * Нажали отклонить отклик
     *
     * @param Reaction $reaction
     *
     * @return mixed
     */
    public function decline(Reaction $reaction, $send_email)
    {

        $reaction->setStatus(Reaction::STATUS_DECLINE);
        $this->saveObject($reaction);
        if ($send_email) $this->dispatch(new ReactionEvent($reaction), ReactionEvent::APPROVE_OR_DECLINE);
    }

    /**
     * Дисквалификация по времени
     */
    public function disqual(Reaction $reaction)
    {
        $reaction->setStatus(Reaction::STATUS_DISQUALIFIED);
        $this->saveObject($reaction);

        $this->dispatch(new ReactionEvent($reaction), ReactionEvent::NO_PROPOSAL);
    }

    /**
     * Предложение было просмотрено
     *
     * @param Reaction $reaction
     *
     * @return mixed
     */
    public function viewedProposal(Reaction $reaction)
    {
        if ($reaction->getStatus() == Reaction::STATUS_START) {
            $reaction->setStatus(Reaction::STATUS_CONSIDERED);
            $this->saveObject($reaction);
        }
        $this->dispatch(new ReactionEvent($reaction), ReactionEvent::HAS_BEEN_VIEWED);
    }

    /**
     * Одобрить предложение
     * отклик должен автоматически переходить в статус "Предложение одобрено"
     *
     * @param Offer $offer
     * @param string $demand
     * @param Reaction $reaction
     *
     * @return mixed
     */
    public function addNewReactionForOffer(Offer $offer, $demand, Reaction $reaction)
    {
        $reaction->setOffer($offer);
        $reaction->setDemand($demand = $this->demandRepository->findOneById($demand));
        $reaction->setStatus(Reaction::STATUS_APPROVE);
        if ($this->validateReaction($offer, $demand)) {
            $this->saveObject($reaction);

            $this->dispatch(new ReactionEvent($reaction, ReactionEvent::FOR_OFFER), ReactionEvent::CREATE);
        }
    }

    /**
     * Отклик должен быть уникальным и не может повторяться
     *
     * @param Offer $offer
     * @param Demand $demand
     *
     * @return bool
     */
    private function validateReaction(Offer $offer, Demand $demand)
    {
        return $this->repository->findByOfferAndDemand($offer, $demand) ? false : true;
    }

    /**
     * Отправить предложение
     *
     * @param Demand $demand
     * @param string $offer
     * @param Reaction $reaction
     *
     * @return mixed
     */
    public function addNewReactionForDemand(Demand $demand, $offer, Reaction $reaction)
    {
        $reaction->setDemand($demand);
        $reaction->setOffer($offer = $this->offerRepository->findOneById($offer));
        if ($this->validateReaction($offer, $demand)) {
            $this->saveObject($reaction);

            $this->dispatch(new ReactionEvent($reaction, ReactionEvent::FOR_DEMAND), ReactionEvent::CREATE);
        }
    }

    /**
     * Удаление откликов выбранных по чекбоксам
     *
     * @param array $ids
     * @param integer $type (1 - Demand, 2 - Offer)
     * @param User $user
     *
     * @return mixed|void
     */
    public function removeReactions($ids, $type, User $user)
    {
        /*dump($ids, $type, $user);
        die();*/
        if ($type == Reaction::DEMAND) {
            $this->getRepository()->setDeleteStatusToDemands($ids, $type);

            $event = new ReactionEvent(null, ReactionEvent::FOR_DEMAND);
            $event->setUser($user);
            $this->dispatch($event, ReactionEvent::DELETE);

        } elseif ($type == Reaction::OFFER) {
            $this->getRepository()->setDeleteStatusToOffers($ids, $type);
            $event = new ReactionEvent(null, ReactionEvent::FOR_OFFER);
            $event->setUser($user);
            $this->dispatch($event, ReactionEvent::DELETE);
        }
    }

    /**
     * @return \Doctrine\ORM\EntityRepository|\Easyb\AdvertBundle\Entity\ReactionRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * Есть ли активные уведомления по "мой спрос"
     *
     * @param User $user
     *
     * @return mixed
     */
    public function checkDemandNotification(User $user)
    {
        return $this->repository->checkDemandNotification($user);
    }

    /**
     * Есть ли активные уведомления по "мое предложение"
     *
     * @param User $user
     *
     * @return mixed
     */
    public function checkOfferNotification(User $user)
    {
        return $this->repository->checkOfferNotification($user);
    }

    /**
     * @param Reaction $reaction
     *
     * @return mixed
     */
    public function setViewedStatusToApprovedReaction(Reaction $reaction)
    {
        $reaction->setStatus(Reaction::STATUS_SEE_MY_CONTACTS);
        $this->saveObject($reaction);

        $this->dispatch(new ReactionEvent($reaction), ReactionEvent::SET_VIEWED_STATUS_TO_APPROVED_REACTION);
    }

    public function setWinner(Proposal $proposal, $auto = false)
    {
        $proposal->setStatus(Proposal::STATUS_WINNER);
        $demand = $proposal->getReaction()->getDemand();
        $demand->setWinner($proposal);
        $demand->setIsFinished(true);

        $this->dispatch(new ProposalEvent($proposal, $auto), ProposalEvent::PROPOSAL_WINNER);

        if (!$auto) {
            $otherReaction = $proposal->getReaction()->getDemand()->getReactions();
            foreach ($otherReaction as $reaction) {
                $otherProposals = $reaction->getProposal();
                foreach ($otherProposals as $otherProposal) {
                    if (($otherProposal->getStatus() != Proposal::STATUS_WINNER)) {
                        $otherProposal->setStatus(Proposal::STATUS_LOSER);

                        $this->dispatch(new ProposalEvent($otherProposal), ProposalEvent::PROPOSAL_LOSER);
                    }
                }
            }
        }

        $this->em->flush();
    }



}
