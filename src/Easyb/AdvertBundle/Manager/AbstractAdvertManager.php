<?php
namespace Easyb\AdvertBundle\Manager;

use Easyb\AdvertBundle\Entity\AbstractAdvert;
use Easyb\AdvertBundle\Entity\Demand;
use Easyb\AdvertBundle\Entity\Offer;
use Easyb\UserBundle\Entity\Job;

/**
 * interface AbstractAdvertManager
 */
abstract class AbstractAdvertManager
{
    /**
     * @param $object
     *
     * @throws \Exception
     */
    public function deleteObject($object)
    {
        try {
            //$this->em->remove($object);
            $object->setIsActive(false);
            $this->em->flush();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $object
     */
    public function addViews($object)
    {
        $object->setViews($object->getViews() + 1);
        $this->save($object);
    }

    /**
     * @param $object
     *
     * @throws \Exception
     */
    public function save($object)
    {
        try {
            $this->em->persist($object);
            $this->em->flush();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Обновить дату публикации, но не чаще чем раз в 1 час
     *
     * @param Offer|Demand $object
     *
     * @return bool
     */
    public function up($object)
    {
        if ($object->checkUpTime()) {
            $object->setPublicationAt(new \DateTime("now"));
            $this->save($object);
        }
    }

    /**
     * Просто обновить дату публикации
     *
     * @param $object
     */
    public function upNow($object)
    {
        $object->setPublicationAt(new \DateTime());
        $this->save($object);
    }

    /**
     * Снять с публикации или опубликовать
     *
     * @param Offer|Demand $object
     *
     * @return mixed|void
     */
    public function changeActive($object)
    {
        $object->setIsActive($object->getIsActive() ? false : true);
        if ($object->checkUpTime() && $object->getIsActive(true)) {
            $object->setPublicationAt(new \DateTime());
        }
        $this->update($object);
    }
}
