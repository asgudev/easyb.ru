<?php
namespace Easyb\AdvertBundle\Manager;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Easyb\AdvertBundle\Entity\Notification;
use Easyb\UserBundle\Entity\User;
use Easyb\AdvertBundle\Entity\AbstractAdvert;

/**
 * manager ReactionManager
 */
class NotificationManager
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var \Easyb\AdvertBundle\Entity\NotificationRepository
     */
    protected $repository;

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository('EasybAdvertBundle:Notification');
    }

    /**
     * @return \Doctrine\ORM\EntityRepository|\Easyb\AdvertBundle\Entity\NotificationRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @param Notification $notification
     *
     * @throws \Exception
     */
    private function saveObject(Notification $notification)
    {
        try {
            $this->em->persist($notification);
            $this->em->flush();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Notification $notification
     *
     * @throws \Exception
     */
    private function deleteObject(Notification $notification)
    {
        try {
            $this->em->remove($notification);
            $this->em->flush();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Notification $notification
     *
     * @return mixed
     */
    public function create(Notification $notification)
    {
        $this->saveObject($notification);
    }

    /**
     * @param Notification $notification
     *
     * @return mixed
     */
    public function delete(Notification $notification)
    {
        $this->deleteObject($notification);
    }

    /**
     * @param User           $user
     * @param AbstractAdvert $advert
     *
     * @return mixed
     */
    public function createInstance(User $user, AbstractAdvert $advert)
    {
        $reaction = new Notification($user, $advert);

        return $reaction;
    }
}
