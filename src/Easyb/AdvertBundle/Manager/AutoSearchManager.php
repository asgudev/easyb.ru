<?php
namespace Easyb\AdvertBundle\Manager;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Easyb\AdvertBundle\Entity\AutoSearch;
use Easyb\UserBundle\Entity\User;

/**
 * manager AutoSearchManager
 */
class AutoSearchManager
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var \Easyb\AdvertBundle\Entity\AutoSearchRepository
     */
    protected $repository;

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository('EasybAdvertBundle:AutoSearch');
        $this->categoryRepository = $this->em->getRepository('EasybMainBundle:Category');
        $this->cityRepository = $this->em->getRepository('EasybMainBundle:City');
    }

    /**
     * @return \Doctrine\ORM\EntityRepository|\Easyb\AdvertBundle\Entity\AutoSearchRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @param AutoSearch $autoSearch
     *
     * @throws \Exception
     */
    private function saveObject(AutoSearch $autoSearch)
    {
        try {
            $this->em->persist($autoSearch);
            $this->em->flush();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param AutoSearch $autoSearch
     *
     * @throws \Exception
     */
    private function deleteObject(AutoSearch $autoSearch)
    {
        try {
            $this->em->remove($autoSearch);
            $this->em->flush();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param AutoSearch $autoSearch
     *
     * @return mixed
     */
    public function create(AutoSearch $autoSearch)
    {
        $this->saveObject($autoSearch);
    }

    /**
     * @param AutoSearch $autoSearch
     *
     * @return mixed
     */
    public function delete(AutoSearch $autoSearch)
    {
        $this->deleteObject($autoSearch);
    }

    /**
     * @param User   $user
     * @param string $searchInputId
     * @param string $categoryId
     * @param string $cityId
     * @param string $advertTypeId
     *
     * @return mixed
     */
    public function createInstance(User $user, $searchInputId, $categoryId, $cityId, $advertTypeId)
    {
        $autoSearch = new AutoSearch();
        $autoSearch->setUser($user);
        if ($searchInputId) {
            $autoSearch->setText($searchInputId);
        }
        if ($advertTypeId) {
            $autoSearch->setType($advertTypeId);
        }
        $autoSearch = $this->setCategory($autoSearch, $categoryId);
        $autoSearch = $this->setCity($autoSearch, $cityId);

        return $autoSearch;
    }

    /**
     * @param AutoSearch $autoSearch
     * @param string     $categoryId
     *
     * @return AutoSearch
     */
    private function setCategory(AutoSearch $autoSearch, $categoryId)
    {
        if ($categoryId) {
            if ($category = $this->categoryRepository->findOneById($categoryId)) {
                $autoSearch->setCategory($category);
            }
        }

        return $autoSearch;
    }

    /**
     * @param AutoSearch $autoSearch
     * @param string     $cityId
     *
     * @return AutoSearch
     */
    private function setCity(AutoSearch $autoSearch, $cityId)
    {
        if ($cityId) {
            if ($city = $this->cityRepository->findOneById($cityId)) {
                $autoSearch->setCity($city);
            }
        }

        return $autoSearch;
    }

    /**
     * @param AutoSearch $autoSearch
     */
    public function changeActive(AutoSearch $autoSearch)
    {
        $autoSearch->setIsActive($autoSearch->getIsActive() ? false : true);
        $this->saveObject($autoSearch);
    }

    /**
     * @param $id
     */
    public function resetViews($id)
    {
        $autoSearch = $this->repository->findOneById($id);
        if ($autoSearch) {
            $date = new \DateTime();
            $autoSearch->setViewingAt($date);

            $this->saveObject($autoSearch);
        }
    }
}
