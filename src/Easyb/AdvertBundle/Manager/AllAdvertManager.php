<?php
namespace Easyb\AdvertBundle\Manager;

use Doctrine\ORM\EntityManager;
use Easyb\AdvertBundle\Entity\AbstractAdvert;
use Easyb\UserBundle\Entity\Job;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * manager AllAdvertManager
 */
class AllAdvertManager extends AbstractAdvertManager
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;
    /**
     * @var \Symfony\Component\EventDispatcher\EventDispatcher
     */
    protected $dispatcher;

    /**
     * @param \Doctrine\ORM\EntityManager                        $em
     * @param \Symfony\Component\EventDispatcher\EventDispatcher $dispatcher
     */
    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher)
    {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
    }

    public function adminApprove(AbstractAdvert $advert)
    {
        $advert->setIsApproved(true);
        $advert->setNotes(null);
        $advert->setInvalidRules(array());
        $advert->setIsImproved(false);
        $advert->setImprovedAt(null);
        $this->save($advert);
    }

    public function adminClearApproved(AbstractAdvert $advert)
    {
        $advert->setIsApproved(false);
        $this->save($advert);
    }

    public function adminImprove(AbstractAdvert $advert)
    {
        $advert->setIsApproved(false);
        $advert->setIsImproved(true);
        $advert->setImprovedAt(new \DateTime());
        $this->save($advert);
    }

    public function adminDecline(AbstractAdvert $advert)
    {
        $this->deleteObject($advert);
    }

    public function adminClearNotes(AbstractAdvert $advert)
    {

        $advert->setNotes(null);
        $advert->setInvalidRules(array());
        $this->save($advert);
    }

    public function adminClearImproved(AbstractAdvert $advert)
    {
        $advert->setIsImproved(false);
        $advert->setImprovedAt(null);
        $this->save($advert);
    }

    public function getAdvertsByJob(Job $job)
    {

        return $this->em->getRepository('EasybAdvertBundle:AbstractAdvert')->findAdvertsByJob($job);
    }

    public function adminSetNotesWithRules(AbstractAdvert $advert, $rule1 = false, $rule4 = false, $rule5 = false)
    {
        // Clear old advert invalid rules
        $advert->setInvalidRules(array());

        if ((bool) $rule1) {
            $advert->addInvalidRule('Запрещено размещать copy-past. Вся информация проверяется на уникальность администратором. Причина: Поисковые машины индексируют только уникальный текст. Если Вы хотите, чтобы Вас нашли и в поисковых системах, то необходимо следовать требованиями поисковиков. Уникальность текста рекомендуется проверять на сайте Text.ru.');
        }

        if ((bool) $rule4) {
            $advert->addInvalidRule('Запрещено рекламировать сторонние ресурсы.');
        }

        if ((bool) $rule5) {
            $advert->addInvalidRule('Запрещено дублировать информацию на сайте.');
        }

        $this->save($advert);
    }

    public function adminSetNotes(AbstractAdvert $advert, $notes)
    {
        // Clear old advert invalid rules
        $advert->setInvalidRules(array());

        $advert->setNotes((string) $notes);

        $this->save($advert);
    }
}
