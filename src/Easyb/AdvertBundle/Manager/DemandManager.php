<?php
namespace Easyb\AdvertBundle\Manager;

use Doctrine\ORM\EntityManager;
use Easyb\AdvertBundle\Entity\Demand;
use Easyb\AdvertBundle\Entity\Reaction;
use Easyb\AdvertBundle\Events\DemandEvent;
use Easyb\AdvertBundle\Events\ReactionEvent;
use Easyb\UserBundle\Entity\Job;
use Easyb\UserBundle\Entity\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * manager DemandManager
 */
class DemandManager extends AbstractAdvertManager
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;
    /**
     * @var \Easyb\AdvertBundle\Entity\DemandRepository
     */
    protected $repository;
    /**
     * @var \Symfony\Component\EventDispatcher\EventDispatcher
     */
    protected $dispatcher;

    /**
     * @param \Doctrine\ORM\EntityManager $em
     * @param \Symfony\Component\EventDispatcher\EventDispatcher $dispatcher
     */
    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository('EasybAdvertBundle:Demand');
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param User $user
     *
     * @return Demand
     */
    public function getInstance(User $user)
    {
        return new Demand($user);
    }

    /**
     * @return \Doctrine\ORM\EntityRepository|\Easyb\AdvertBundle\Entity\DemandRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @param \Easyb\AdvertBundle\Entity\Demand $demand
     *
     * @return mixed|void
     */
    public function create(Demand $demand)
    {
        if ((!$demand->getIsPublic()) && ($demand->getPrivateHash() == '')) {
            $demand->setPrivateHash(md5(time()));
            $demand->setExpireAt(new \DateTime());
        }
        $this->save($demand);
    }

    /**
     * @param \Easyb\AdvertBundle\Entity\Demand $demand
     *
     * @return mixed|void
     */
    public function update(Demand $demand)
    {
        $this->save($demand);
    }

    /**
     * @param \Easyb\AdvertBundle\Entity\Demand $demand
     *
     * @return mixed|void
     * @throws \Exception
     */
    public function delete(Demand $demand)
    {
        $this->deleteObject($demand);

        $this->dispatch(new DemandEvent($demand), DemandEvent::DELETE);
    }

    /**
     * @param string $name
     */
    private function dispatch($event, $name)
    {
        $this->dispatcher->dispatch($name, $event);
    }

    public function extendOffer(Demand $demand)
    {
        $demand->setExpireProposalAt($this->extendDate($demand->getExpireProposalAt()->modify("+3 minutes")));
        if ($demand->getExpireSubProposalAt() != null) {
            $demand->setExpireSubProposalAt($this->extendDate($demand->getExpireSubProposalAt()->modify("+3 minutes")));
        }
        $demand->setExpireDecisionAt($this->extendDate($demand->getExpireDecisionAt()->modify("+3 minutes")));
        $demand->setIsExtendedOffer(true);
        $this->em->flush();

        $this->dispatch(new DemandEvent($demand), DemandEvent::EXTEND_OFFER);

        $reactions = $demand->getReactions();
        foreach ($reactions as $reaction) {
            if (($reaction->getStatus() == Reaction::STATUS_APPROVE) || ($reaction->getStatus() == Reaction::STATUS_DIRECT)) {
                $this->dispatch(new ReactionEvent($reaction), ReactionEvent::EXTEND_OFFER);
            }
        }
    }

    public function extendSubOffer(Demand $demand)
    {
        $demand->setExpireSubProposalAt($this->extendDate($demand->getExpireSubProposalAt()->modify("+3 minutes")));
        $demand->setExpireDecisionAt($this->extendDate($demand->getExpireDecisionAt()->modify("+3 minutes")));
        $demand->setIsExtendedSubOffer(true);
        $this->em->flush();

        $this->dispatch(new DemandEvent($demand), DemandEvent::EXTEND_OFFER);

        $reactions = $demand->getReactions();
        foreach ($reactions as $reaction) {
            if (($reaction->getStatus() == Reaction::STATUS_APPROVE) || ($reaction->getStatus() == Reaction::STATUS_DIRECT)) {
                $this->dispatch(new ReactionEvent($reaction), ReactionEvent::EXTEND_OFFER);
            }
        }
    }

    public function extendDecision(Demand $demand)
    {
        $demand->setExpireDecisionAt($this->extendDate($demand->getExpireDecisionAt()->modify("+3 minutes")));
        $demand->setIsExtendedWinner(true);
        $this->em->flush();

        $this->dispatch(new DemandEvent($demand), DemandEvent::EXTEND_WINNER);

        $reactions = $demand->getReactions();
        foreach ($reactions as $reaction) {
            if ((($reaction->getStatus() == Reaction::STATUS_APPROVE) || ($reaction->getStatus() == Reaction::STATUS_DIRECT)) && ($reaction->getProposal() != null)) {
                $this->dispatch(new ReactionEvent($reaction), ReactionEvent::EXTEND_WINNER);
            }
        }
    }

    public function noWinner(Demand $demand)
    {
        $demand->setIsFinished(true);
        $demand->setIsActive(false);

        $this->dispatch(new DemandEvent($demand), DemandEvent::NO_WINNER);

        $reactions = $demand->getReactions();

        foreach ($reactions as $reaction) {
            if (($reaction->getStatus() == Reaction::STATUS_APPROVE) || ($reaction->getStatus() == Reaction::STATUS_DIRECT)) {
                $this->dispatch(new ReactionEvent($reaction), ReactionEvent::NO_WINNER);
            }
        }
        $this->em->flush();
    }

    public function extendDate(\DateTime $dateTime)
    {
        $offsetSeconds = $dateTime->getOffset();
        $day = gmdate("l", time() + $offsetSeconds);
        $new_date = clone $dateTime;
        if ($day == "Sunday") {
            $new_date = clone $new_date->modify("+1 day");
        } elseif ($day == "Saturday") {
            $new_date = clone $new_date->modify("+2 day");
        }
        return $new_date;
    }

    public function getDemandsByJob(Job $job)
    {
        return $this->em->getRepository('EasybAdvertBundle:Demand')->findDemandsByJob($job);

    }
}
