<?php
namespace Easyb\AdvertBundle\Manager;

use Easyb\AdvertBundle\Entity\Offer;
use Easyb\AdvertBundle\Entity\Demand;
use Easyb\AdvertBundle\Entity\Reaction;
use Easyb\UserBundle\Entity\User;
use Easyb\AdvertBundle\Entity\Proposal;

/**
 * interface ReactionManager
 */
interface ReactionManagerInterface
{
    /**
     * @param Reaction $reaction
     *
     * @return mixed
     */
    public function create(Reaction $reaction);

    /**
     * @param Reaction $reaction
     *
     * @return mixed
     */
    public function delete(Reaction $reaction);

    /**
     * @param Demand $demand
     * @param Offer  $offer
     *
     * @return mixed
     */
    public function createInstance(Demand $demand, Offer $offer);

    /**
     * @return mixed
     */
    public function getRepository();

    /**
     * @param Reaction $reaction
     *
     * @return mixed
     */
    public function approve(Reaction $reaction);

    /**
     * @param Reaction $reaction
     *
     * @return mixed
     */
    public function decline(Reaction $reaction, $send_email);

    /**
     * @param Reaction $reaction
     *
     * @return mixed
     */
    public function viewedProposal(Reaction $reaction);

    /**
     * @param Offer    $offer
     * @param string   $demand
     * @param Reaction $reaction
     *
     * @return mixed
     */
    public function addNewReactionForOffer(Offer $offer, $demand, Reaction $reaction);

    /**
     * @param Demand   $demand
     * @param string   $offer
     * @param Reaction $reaction
     *
     * @return mixed
     */
    public function addNewReactionForDemand(Demand $demand, $offer, Reaction $reaction);

    /**
     * @param array   $ids
     * @param integer $type
     * @param User    $user
     *
     * @return mixed
     */
    public function removeReactions($ids, $type, User $user);

    /**
     * @param User $user
     *
     * @return mixed
     */
    public function checkDemandNotification(User $user);

    /**
     * @param User $user
     *
     * @return mixed
     */
    public function checkOfferNotification(User $user);

    /**
     * @param Reaction $reaction
     *
     * @return mixed
     */
    public function setViewedStatusToApprovedReaction(Reaction $reaction);

    /**
     * @param Proposal $proposal
     *
     * @return mixed
     */
    public function setWinner(Proposal $proposal);

}
