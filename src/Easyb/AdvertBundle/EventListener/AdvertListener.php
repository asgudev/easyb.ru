<?php
/**
 * Created by JetBrains PhpStorm.
 * User: scorp
 * Date: 17.06.13
 * Time: 14:20
 * To change this template use File | Settings | File Templates.
 */
namespace Easyb\AdvertBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcher;

use Easyb\AdvertBundle\Entity\AbstractAdvert;
use Easyb\AdvertBundle\Entity\Reaction;
use Easyb\AdvertBundle\Events\DemandEvent;
use Easyb\AdvertBundle\Events\OfferEvent;
use Easyb\AdvertBundle\Events\ReactionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Easyb\AdvertBundle\Manager\DemandManager;
use Easyb\AdvertBundle\Manager\OfferManager;

class AdvertListener implements EventSubscriberInterface
{
    /**
     * @var \Easyb\AdvertBundle\Manager\DemandManager
     */
    protected $abstractAdvertManager;

    /**
     * @var \Easyb\AdvertBundle\Manager\OfferManager
     */
    protected $offerManager;

    /**
     * @var \Doctrine\ORM\EntityManager;
     */
    protected $em;

    /**
     * @param \Easyb\AdvertBundle\Manager\DemandManager $demandManager
     * @param \Easyb\AdvertBundle\Manager\OfferManager $offerManager
     */
    public function __construct(EntityManager $em, DemandManager $demandManager, OfferManager $offerManager)
    {
        $this->demandManager = $demandManager;
        $this->offerManager = $offerManager;
        $this->em = $em;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            ReactionEvent::CREATE => 'addSubmittedAdverts',

            OfferEvent::DELETE => 'deleteOffer',
        ];
    }

    public function deleteOffer(OfferEvent $event)
    {
        $offer = $event->getOffer();
        $offer->setIsDeleted(true);

    }

    /**
     * помечаем, что объявление уже было отправлено
     *
     * @param \Easyb\AdvertBundle\Events\ReactionEvent $event
     */
    public function addSubmittedAdverts(ReactionEvent $event)
    {
        $reaction = $event->getReaction();
        $type = $event->getType();
        if ($type == ReactionEvent::FOR_OFFER) {
            $demand = $reaction->getDemand();
            $demand->addSubmittedAdvert($reaction->getOffer()->getId());
            $this->demandManager->update($demand);
        } else if ($type == ReactionEvent::FOR_DEMAND) {
            $offer = $reaction->getOffer();
            $offer->addSubmittedAdvert($reaction->getDemand()->getId());
            $this->offerManager->update($offer);
        }
    }
}
