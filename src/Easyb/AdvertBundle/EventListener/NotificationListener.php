<?php
/**
 * User: scorp
 * Date: 14.06.13
 * Time: 18:46
 */
namespace Easyb\AdvertBundle\EventListener;

use Easyb\AdvertBundle\Entity\Reaction;
use Easyb\AdvertBundle\Events\ReactionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Easyb\AdvertBundle\Manager\NotificationManager;
use FOS\UserBundle\Model\UserManagerInterface as UserManagerInterface;

class NotificationListener implements EventSubscriberInterface
{
    /**
     * @var NotificationManager
     */
    protected $notificationManager;
    /**
     * @var UserManagerInterface
     */
    protected $fosUserManager;

    /**
     * @param NotificationManager  $notificationManager
     * @param UserManagerInterface $fosUserManager
     */
    public function __construct(NotificationManager $notificationManager, UserManagerInterface $fosUserManager)
    {
        $this->notificationManager = $notificationManager;
        $this->fosUserManager = $fosUserManager;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            ReactionEvent::CREATE             => 'createNotification',
            ReactionEvent::APPROVE_OR_DECLINE => 'addNotification'
        ];
    }

    /**
     * создаём уведомление об новом отклике
     *
     * @param \Easyb\AdvertBundle\Events\ReactionEvent $event
     */
    public function createNotification(ReactionEvent $event)
    {
        $reaction = $event->getReaction();
        $type = $event->getType();
        if ($type == ReactionEvent::FOR_OFFER) {
            $user = $reaction->getOffer()->getUser();
            $this->fosUserManager->updateUser($user->setOfferNotification($user->getOfferNotification()+1));
        } else if ($type == ReactionEvent::FOR_DEMAND) {
            $user = $reaction->getDemand()->getUser();
            $this->fosUserManager->updateUser($user->setDemandNotification($user->getDemandNotification()+1));
        }
    }

    /**
     * одобрение или отклонение отклика
     *
     * @param ReactionEvent $event
     */
    public function addNotification(ReactionEvent $event)
    {
        $user = $event->getReaction()->getDemand()->getUser();
        $this->fosUserManager->updateUser($user->setDemandNotification($user->getDemandNotification()-1));
        // Мое предложение
        // в случае отклонения предложения уведомление не создаётся
        // уведомление создаётся только в случае одобрения предложения
        $reaction = $event->getReaction();
        if ($reaction->getStatus() == Reaction::STATUS_APPROVE) {
            $user = $reaction->getDemand()->getUser();
            $this->fosUserManager->updateUser($user->setOfferNotification($user->getOfferNotification()+1));
        }
    }
}
