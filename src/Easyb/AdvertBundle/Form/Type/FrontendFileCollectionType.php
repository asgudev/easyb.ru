<?php
namespace Easyb\AdvertBundle\Form\Type;

use Symfony\Component\Form\AbstractType;

class FrontendFileCollectionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'file_collection';
    }

    public function getParent()
    {
        return 'collection';
    }
}
