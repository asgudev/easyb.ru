<?php

namespace Easyb\AdvertBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Easyb\MainBundle\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\CallbackTransformer;



class FrontendAdvertType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('text', null, array(
                'label' => 'Метка в шаблоне, т.к. там нужен HTML!',
                'required' => false,
                'attr' => array('class' => 'ckeditor')
            ))
            ->add('metaDescription', null, array(
                'label' => 'Ключевые слова (через запятую)',
                'required' => false
            ))

            ->add('files', 'file_collection', array(
                'type' => new FrontendFileType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Easyb\AdvertBundle\Entity\AbstractAdvert',
            'cascade_validation' => true
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return "advert_type";
    }
}