<?php
namespace Easyb\AdvertBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class AdminFileCollectionType extends CollectionType
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'admin_file_collection';
    }
}
