<?php
/**
 * Created by IntelliJ IDEA.
 * User: User
 * Date: 31.01.2016
 * Time: 15:09
 */

namespace Easyb\AdvertBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FrontendDirectProposalType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('job', 'job_choice', array(
                'label' => 'Выберите место работы',
                'class' => 'Easyb\UserBundle\Entity\Job',
                'choices' => $options["jobs"],
                'required' => true,
            ))
            ->add('files', 'file_collection', array(
                'type' => new FrontendFileType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'data' => $options["required_files"],

            ))
        ;
        /*
            ->add('message', null, array(
                'label' => 'Сопроводительное письмо',
                'required' => false,
                'attr' => array('class' => 'ckeditor')
            ))
            ->add('price', null, [
                'label' => 'Финальная стоимость лота',
                'required' => true,
            ])

            ->add('files', 'file_collection', array(
                'type' => new FrontendFileType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ));*/
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Easyb\\AdvertBundle\\Entity\\Reaction',
                'cascade_validation' => true,
                'jobs' => null,
                'required_files' => null,
            ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return "proposal_type";
    }
}