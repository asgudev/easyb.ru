<?php
/**
 * This file is part of easyb.ru package.
 *
 * (c) Evgeniy Marchenkov <evgeniy.public@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Easyb\AdvertBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AdminDemandImproveType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('notes', 'textarea', [
                    'label' => 'Замечания',
                    'label_attr' => ['class' => 'note-label'],
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'Введите Ваши замечания для указанного объявления ...'
                    ]]
            );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Easyb\\AdvertBundle\\Entity\\AbstractAdvert',
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'admin_demand_improve_type';
    }
}
