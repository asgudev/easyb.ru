<?php

namespace Easyb\AdvertBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FrontendFileType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('displayName', null, [
                'required' => true,
                'attr' => [
                    'class' => 'file_display_name',
                    'placeholder' => "__placeholder__",
                ]
            ])
            ->add('required', 'hidden', [
                'attr' => [
                    'class' => 'required'
                ]
            ])
            ->add('file', 'file', array(
                'file_id' => 'id',
                'file_name' => 'publicName',
                'file_size' => 'fileSize',
                'get_required' => 'required'));
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array('data_class' => 'Easyb\\AdvertBundle\\Entity\\AdvertFile'));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return "file_type";
    }
}