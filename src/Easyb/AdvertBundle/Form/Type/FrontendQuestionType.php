<?php
/**
 * Created by IntelliJ IDEA.
 * User: User
 * Date: 16.02.2016
 * Time: 18:01
 */

namespace Easyb\AdvertBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FrontendQuestionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("question", 'textarea');
        $builder->add("job", 'entity',[
            'label' => 'Выберите место работы',
            'class' => 'Easyb\UserBundle\Entity\Job',
            'choices' => $options["jobs"],
            'required' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Easyb\\AdvertBundle\\Entity\\Question',
                'jobs' => null,
            ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return "question_type";
    }
}