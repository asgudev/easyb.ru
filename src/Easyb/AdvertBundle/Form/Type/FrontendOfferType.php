<?php

namespace Easyb\AdvertBundle\Form\Type;

use Doctrine\ORM\EntityRepository;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Easyb\UserBundle\Entity\Job;
use Easyb\UserBundle\Entity\JobRepository;
use Easyb\MainBundle\Entity\Category;


class FrontendOfferType extends AbstractType
{
    private $job_first;
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array('label'    => 'Наименование коммерческого предложения',
                'required' => true
            ))
            ->add('job', 'job_choice', array(
                'label' => 'Выберите место работы',
                'class' => 'Easyb\UserBundle\Entity\Job',
                'required' => true,
                'query_builder' => function (JobRepository $repository) use ($builder) {
                    $query =  $repository->createQueryBuilder('j')
                        ->where('j.user = :user')
                        ->andWhere('j.isDeleted = false')
                        ->setParameter('user', $builder->getData()->getUser());
                    return $query;
                }
            ))
            ->add('category', 'entity', array(
                'label' => 'Раздел каталога',
                'class' => 'Easyb\MainBundle\Entity\Category',
                'required' => true,
                'query_builder' => function (EntityRepository $er) use ($builder) {
                    $query = $er->createQueryBuilder('c')
                        ->where("c.parent = c.id")
                        ->orderBy('c.name', 'ASC');
                    return $query;
                }
            ))
        ;

        $formModifier = function (FormInterface $form, Category $category = null) {

            $form->add('subcategory', 'entity', array(
                'class' => 'Easyb\MainBundle\Entity\Category',
                'label' => 'Подкатегория',
                'multiple' => true,
                'expanded' => true,
                'required' => false,
                'query_builder' => function (EntityRepository $cr) use ($category) {
                    $query =  $cr->createQueryBuilder('c')
                        ->where("c.parent = :category")
                        ->andWhere("c.id != :category")
                        ->setParameter("category", $category)
                        ->orderBy('c.id', 'ASC')
                    ;
                    return $query;
                }
            ));
        };

       /* $jobModifier = function (FormInterface $form, Job $job = null) {
            //dump($job); die();
            $form->add('category', 'entity', array(
                'class' => 'Easyb\MainBundle\Entity\Category',
                'label' => 'Подкатегория',
                'multiple' => true,
                'expanded' => true,
                'required' => false,
                'query_builder' => function (EntityRepository $cr) use ($job) {
                    $query =  $cr->createQueryBuilder('c')
                        ->where("c.parent = :category")
                        ->andWhere("c.id != :category")
                        ->setParameter("category", $job)
                        ->orderBy('c.id', 'ASC')
                    ;
                    return $query;
                }
            ));
        };
*/
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                $data = $event->getData();
/*
                if ($data->getJob() != null) {
                    $jobModifier($event->getForm(), $data->getJob());
                }
*/
                    $formModifier($event->getForm(), $data->getCategory());

            }
        );

        $builder->get('category')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                $category = $event->getForm()->getData();
                $formModifier($event->getForm()->getParent(), $category);
            }
        );
/*
        $builder->get('job')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($jobModifier) {
                $job = $event->getForm()->getData();
                //dump($job);die();

                $jobModifier($event->getForm()->getParent(), $job);
            }
        );*/
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Easyb\\AdvertBundle\\Entity\\Offer'
            ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return "offer_type";
    }

    /**
     * @return parent Type
     */
    public function getParent()
    {
        return 'advert_type';
    }
}