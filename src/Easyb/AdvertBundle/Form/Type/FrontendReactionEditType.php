<?php

namespace Easyb\AdvertBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FrontendReactionEditType extends AbstractType
{
    /*    private $user;

        public function __construct($user)
        {
            $this->user = $user;
        }*/
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $options['user'];
        $builder
            ->add('message', null, array(
                'label' => 'Сопроводительное письмо',
                'required' => false,
                'attr' => array('class' => 'ckeditor')
            ))
            ->add('job', 'job_choice', array(
                'label' => 'Выберите место работы',
                'class' => 'Easyb\UserBundle\Entity\Job',
                'choices' => $options["jobs"],
                'required' => true,
            ))
            ->add('files', 'file_collection', array(
                'type' => new FrontendFileType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false
            ));
        //ToDo заменить трансформером
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Easyb\\AdvertBundle\\Entity\\Reaction',
                'cascade_validation' => true,
                'user' => null,
                'jobs' => null,
            ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return "reaction_offer_type";
    }
}