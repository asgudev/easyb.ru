<?php
/**
 * Created by IntelliJ IDEA.
 * User: User
 * Date: 31.01.2016
 * Time: 15:09
 */

namespace Easyb\AdvertBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FrontendProposalType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('message', TextareaType::class, array(
                'label' => 'Сопроводительное письмо',
                'required' => false,
                'attr' => array('class' => 'ckeditor')
            ))
            ->add('price', TextType::class, [
                'label' => 'Финальная стоимость лота',
                'required' => true,
            ])
            ->add('files', 'file_collection', array(
                'type' => new FrontendFileType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ));

        if ($options["add_job"]) {
            $builder->add('job', 'job_choice', array(
                'label' => 'Выберите место работы',
                'class' => 'Easyb\UserBundle\Entity\Job',
                'choices' => $options["jobs"],
                'required' => true,
            ));
        }

    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Easyb\\AdvertBundle\\Entity\\Proposal',
                'cascade_validation' => true,
                'add_job' => false,
                'jobs' => null,
            ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return "proposal_type";
    }
}