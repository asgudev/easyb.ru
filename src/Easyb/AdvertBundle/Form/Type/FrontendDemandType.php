<?php

namespace Easyb\AdvertBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Easyb\AdvertBundle\Entity\Demand;
use Easyb\MainBundle\Entity\Category;
use Easyb\MainBundle\Entity\CategoryRepository;
use Easyb\UserBundle\Entity\JobRepository;
use Sonata\AdminBundle\Form\Type\CollectionType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class FrontendDemandType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /**
         * @var Demand $demand
         */
        $demand = $builder->getData();

        $expired_date = false;
        if ($demand->getExpireAt() != null) {
            $expired_date = ($demand->getExpireAt() < (new \DateTime()) ? true : false);
        }

        $expired_proposal_date = false;
        if ($demand->getExpireProposalAt() != null) {
            $expired_proposal_date = ($demand->getExpireProposalAt() < (new \DateTime()) ? true : false);
        }

        $expired_sub_proposal_date = false;
        if ($demand->getExpireSubProposalAt() != null) {
            $expired_sub_proposal_date = ($demand->getExpireSubProposalAt() < (new \DateTime()) ? true : false);
        }


        $expired_decision_date = false;
        if ($demand->getExpireDecisionAt() != null) {
            $expired_decision_date = ($demand->getExpireDecisionAt() < (new \DateTime()) ? true : false);
        }



        $isPublic = $demand->getIsPublic() ? 1 : 0;
        $payType = $demand->getPayType() == null ? 1 : $demand->getPayType();

        $builder
            ->add('name', null, array('label' => 'Наименование тендера',
                'required' => true
            ))
            ->add('expireAt', 'date', array(
                'label' => 'Дата и время окончания приема заявок в тендер',
                'format' => 'dd.MM.yyyy H:mm',
                'widget' => 'single_text',
                'empty_value' => array('year' => 'ГГГГ', 'month' => 'ММ', 'day' => 'ДД', 'hour' => 'ЧЧ', 'minute' => 'ММ'),
                'required' => true,
                'disabled' => $expired_date,
                'attr' => array(
                    'class' => 'demand-timing-wrapper'
                ),
            ))
            ->add('expireProposalAt', 'date', array(
                'label' => 'Дата и время окончания подачи коммерческих предложений',
                'attr' => array('class' => 'demand-timing-wrapper'),
                'format' => 'dd.MM.yyyy H:mm',
                'widget' => 'single_text',
                'empty_value' => array('year' => 'ГГГГ', 'month' => 'ММ', 'day' => 'ДД', 'hour' => 'ЧЧ', 'minute' => 'ММ'),
                'disabled' => $expired_proposal_date,
                'required' => true
            ))
            ->add('expireSubProposalAt', 'date', array(
                'label' => 'Дата и время окончания подачи коммерческих предложений (этап 2)',
                'attr' => array('class' => 'demand-timing-wrapper'),
                'format' => 'dd.MM.yyyy H:mm',
                'widget' => 'single_text',
                'empty_value' => array('year' => 'ГГГГ', 'month' => 'ММ', 'day' => 'ДД', 'hour' => 'ЧЧ', 'minute' => 'ММ'),
                'disabled' => $expired_sub_proposal_date,
                'required' => false,
            ))
            ->add('expireDecisionAt', 'date', array(
                'label' => 'Дата и время, до которых будет принято решение о выборе победителя тендера',
                'attr' => array('class' => 'demand-timing-wrapper'),
                'format' => 'dd.MM.yyyy H:mm',
                'widget' => 'single_text',
                'empty_value' => array('year' => 'ГГГГ', 'month' => 'ММ', 'day' => 'ДД', 'hour' => 'ЧЧ', 'minute' => 'ММ'),
                'disabled' => $expired_decision_date,
                'required' => true
            ))
            ->add('job', 'job_choice', array(
                'label' => 'Выберите место работы',
                'class' => 'Easyb\UserBundle\Entity\Job',
                'required' => true,
                'query_builder' => function (JobRepository $repository) use ($builder) {
                    return $repository->createQueryBuilder('j')
                        ->where('j.user = :user')
                        ->andWhere('j.isDeleted = false')
                        ->setParameter('user', $builder->getData()->getUser());
                }
            ))
            ->add('price', 'text', array(
                'label' => 'Предполагаемый бюджет в рублях со всеми налогами, комиссиями и сборами',
            ))
            ->add('winCryteria', 'textarea', array(
                'label' => 'Критерии победы',
                'required' => false,
            ))
            ->add('inviteFile', 'file_collection', array(
                'label' => 'Приглашение к участию в тендере',
                'type' => new FrontendFileType(),
                'allow_add' => true,
                'required' => false,
                'allow_delete' => true,
                //'delete_empty' => true,
                'by_reference' => false,
                'data' => $options['inviteFiles'],
            ))
            ->add('curators', CollectionType::class, array(
                'label' => 'Добавить куратора тендера',
                'required' => false,
                'prototype' => true,
                'allow_add' => true,
                'entry_type' => EmailType::class,
                'entry_options' => array(
                    'required' => false,
                    'attr' => [
                        'placeholder' => "Email",
                    ],
                )
            ))
            ->add("payType", "choice", array(
                'label' => 'Условиях оплаты',
                'required' => false,
                'empty_value' => false,
                'choices' => Demand::PAY_TYPES,
                'expanded' => false,
                'multiple' => false,
                //'data' => $payType,
            ))
            ->add('isPublic', 'choice', array(
                'label' => 'Открытый тендер',
                'required' => false,
                'empty_value' => false,
                'choices' => array(
                    true => 'Открытый тендер',
                    false => 'Закрытый тендер'
                ),
                'expanded' => true,
                'multiple' => false,
                'data' => $isPublic,
                //'choices_as_values' => true,
            ))
            ->add('invites', CollectionType::class, array(
                'label' => 'Укажите email адреса для приглашений',
                'prototype' => true,
                'allow_add' => true,
                'entry_type' => EmailType::class,
                'entry_options' => array(
                    'required' => false,
                    'attr' => [
                        'placeholder' => "Email",
                    ],
                )
            ))
            ->add('requiredDocs', 'choice', [
                'label' => 'Обязательное приложение к заявке',
                'choices' => Demand::REQUIRED_DOCS_ARRAY,
                'expanded' => true,
                'multiple' => true,
                'required' => false,
            ])
            ->add('text', null, array(
                'label' => 'Метка в шаблоне, т.к. там нужен HTML!',
                'required' => false,
                'attr' => array('class' => 'ckeditor')
            ))
            ->add('metaDescription', null, array(
                'label' => 'Ключевые слова (через запятую)',
                'required' => false
            ))
            ->add('category', 'entity', array(
                'label' => 'Раздел каталога',
                'class' => 'Easyb\MainBundle\Entity\Category',
                'required' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->where("c.parent = c.id")
                        ->orderBy('c.name', 'ASC');
                }
            ))
            ->add('files', 'file_collection', array(
                'type' => new FrontendFileType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false
            ));

        $formModifier = function (FormInterface $form, Category $category = null) {
            $form->add('subcategory', 'entity', array(
                'class' => 'Easyb\MainBundle\Entity\Category',
                'label' => 'Подраздел',
                'multiple' => true,
                'expanded' => true,
                'required' => true,
                'query_builder' => function (CategoryRepository $cr) use ($category) {
                    return $cr->createQueryBuilder('c')
                        ->where("c.parent = :category")
                        ->andWhere("c.id != :category")
                        ->setParameter("category", $category)
                        ->orderBy('c.id', 'ASC');
                }
            ));
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                $data = $event->getData();

                $formModifier($event->getForm(), $data->getCategory());
            }
        );

        $builder->get('category')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                $category = $event->getForm()->getData();
                $formModifier($event->getForm()->getParent(), $category);
            }
        );

        $builder->remove("metaDescription");

        $builder
            ->get('requiredDocs')->addModelTransformer(new CallbackTransformer(
                function ($output) {
                    return $this->bit_analysis($output);
                },
                function ($input) {
                    $bitmask = 0;
                    foreach ($input as $mask) {
                        $bitmask += $mask;
                    }
                    return $bitmask;
                }));
/*
        $builder
            ->get('invites')->addModelTransformer(new CallbackTransformer(
                function ($output) {
                    return serialize($output);
                    //return unserialize($output);
                },
                function ($input) {
                    //return serialize($input);
                    return unserialize($input);
                }));*/
    }

    public $category;


    private function bit_analysis($n)
    {
        $bin_powers = array();
        for ($bit = 0; $bit < count(Demand::REQUIRED_DOCS_ARRAY); $bit++) {
            $bin_power = 1 << $bit;
            if ($bin_power & $n) $bin_powers[$bit] = $bin_power;
        }
        return $bin_powers;
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Easyb\\AdvertBundle\\Entity\\Demand',
                'inviteFiles' => null
            ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return "demand_type";
    }

    /**
     * @return parent Type
     */
    public function getParent()
    {
        return 'advert_type';
    }
}