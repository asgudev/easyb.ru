<?php
namespace Easyb\AdvertBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class AdminAdvertType
 *
 * @package Easyb\UserBundle\Form\Type
 */
class AdminAdvertType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fio', null, [
                'label' => 'ФИО'
            ])
            ->add('phone', null, [
                'label' => 'Контактный телефон'
            ])
            ->add('email', null, [
                'label' => 'Email'
            ])
            ->add('question', 'textarea', [
                'label' => 'Текст сообщения',
                'attr'  => ['class' => 'big-text']
            ])
        ;
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'admin_advert';
    }
}