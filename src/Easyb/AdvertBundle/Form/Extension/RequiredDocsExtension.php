<?php
/**
 * Created by IntelliJ IDEA.
 * User: User
 * Date: 26.01.2016
 * Time: 12:20
 */

namespace Easyb\AdvertBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class RequiredDocsExtension extends AbstractTypeExtension
{
    public function getExtendedType()
    {
        return 'choice';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setOptional(array('docs_mask'));
    }
}