<?php
namespace Easyb\AdvertBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Util\PropertyPath;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;

class AdvertFileExtension extends AbstractTypeExtension
{
    public function getExtendedType()
    {
        return 'file';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setOptional(
            array(
                'file_id',
                'file_name',
                'file_size',
                'get_required',
            ));
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if (array_key_exists('file_id', $options)) {
            $parentData = $form->getParent()->getData();
            if (null !== $parentData) {
                $accessor = PropertyAccess::createPropertyAccessor();
                $fileId = $accessor->getValue($parentData, $options['file_id']);
            } else {
                $fileId = null;
            }

            $view->vars['file_id'] = $fileId;
        }

        if (array_key_exists('file_name', $options)) {
            $parentData = $form->getParent()->getData();
            if (null !== $parentData) {
                $accessor = PropertyAccess::createPropertyAccessor();
                $fileName = $accessor->getValue($parentData, $options['file_name']);
            } else {
                $fileName = null;
            }

            $view->vars['file_name'] = $fileName;
        }

        if (array_key_exists('file_size', $options)) {
            $parentData = $form->getParent()->getData();
            if (null !== $parentData) {
                $accessor = PropertyAccess::createPropertyAccessor();
                $fileSize = $accessor->getValue($parentData, $options['file_size']);
            } else {
                $fileSize = null;
            }

            $view->vars['file_size'] = $fileSize;
        }

        if (array_key_exists('get_required', $options)) {
            $parentData = $form->getParent()->getData();
            if (null !== $parentData) {
                $accessor = PropertyAccess::createPropertyAccessor();
                $getRequired = $accessor->getValue($parentData, $options['get_required']);
            } else {
                $getRequired = 0;
            }

            $view->vars['get_required'] = $getRequired;
        }
    }
}