(function (window, $, easyXDM) {
    "use strict";
    var TT_Advert = {

        fileLimit: 10,
        total_file_size: 31457280,
        single_file_size: 10485760,
        allowed_file_types: ['doc', 'docx', 'docm', 'txt', 'xlsx', 'xlsb', 'xls', 'pptx', 'pptm', 'ppt', 'pdf', 'gif', 'jpeg', 'jpg', 'png', 'bmp', 'pps'],

        form_errors: [],

        format_size: function (size) {
            var thresh = 1024;
            if (Math.abs(size) < thresh) {
                return size + ' B';
            }
            var units = ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
            var u = -1;
            do {
                size /= thresh;
                ++u;
            } while (Math.abs(size) >= thresh && u < units.length - 1);
            return size.toFixed(1) + ' ' + units[u];
        },

        disable_submit: function (error_message, type) {

            if ($('.' + type).length == 0) {
                $(".file_collection").before('<div class="file_input_error ' + type + '">' + error_message + '</div>')
            } else {
                $('.' + type).text(error_message);
            }
            $(".submit button").prop("disabled", true).addClass("active").addClass("btn");
        },

        enable_submit: function (type) {
            $("." + type).remove();
            if ($(".file_input_error").length == 0) {
                $(".submit button").prop("disabled", false).removeClass("active");
            }
            $(".file_info").each(function () {

            });
        },

        submit: function () {
            if (this.form_errors.length > 0) {
                return false;
            }
        },
        validate_form: function () {
            var total_size = 0,
                error_type,
                file_ext;
            $(".file_info").each(function (index) {
                    total_size += $(this).data("size");

                    error_type = "single_size_error";
                    if ($(this).data("size") > TT_Advert.single_file_size) {
                        TT_Advert.disable_submit("Превышен допустимый размер файла (" + TT_Advert.format_size(TT_Advert.single_file_size) + ")", error_type);

                    } else {
                        TT_Advert.enable_submit(error_type);
                    }

                    error_type = "total_size_error";
                    if (total_size > TT_Advert.total_file_size) {
                        TT_Advert.disable_submit("Превышен допустимый объём файлов (" + TT_Advert.format_size(TT_Advert.total_file_size) + ")", error_type);
                    } else {
                        TT_Advert.enable_submit(error_type);
                    }

                    file_ext = $(this).data("ext").toLowerCase();
                    error_type = "file_ext_error_" + index;
                    if (TT_Advert.allowed_file_types.indexOf(file_ext) == -1) {
                        TT_Advert.disable_submit("Такой тип файлов запрещен (" + file_ext + ")", error_type);
                    } else {
                        TT_Advert.enable_submit(error_type);
                    }
                }
            );
        },

        change: function () {
            var filename = this.files[0].name;
            var file_ext = filename.split('.').pop();
            var size = this.files[0].size;

            var ol;
            var upload_type = true;
            if ($(this).parent().hasClass("file_info")) {
                ol = $(this).parent();
            } else {
                ol = $(this).parent().parent().parent().find(".file_info");
                upload_type = false;
            }

            var file_name_html;
            if (filename.length > 30) {
                file_name_html = '<span class="dash">' + filename.substring(0, 30) + "...</span>";
            } else {
                file_name_html = '<span class="dash">' + filename + '</span>';
            }

            ol.children().not('input').remove();
            /*if (upload_type) {
             ol.find(".opt-link").remove();
             //ol.find("input").hide();
             } else {
             ol.empty();
             }*/

            var file_size_html = '<span>' + TT_Advert.format_size(size) + '</span>';
            ol.append(file_name_html + '<span> | </span>' + file_size_html);

            ol.data('size', size).data('ext', file_ext);


            TT_Advert.validate_form();
        },

        replace_required: function (element, value) {
            var text_box = element.find("input:text");
            var star = '';
            if (value != -1) {
                star = "<span class=\"red\">*</span>";
            }
            var span = "<span class='required'>" + text_box.val() + star + "</span>";
            text_box.attr("type", "hidden").after(span);
        },

        initialize: function (target, fileLimit) {
            //debugger;
            fileLimit = fileLimit !== undefined ? fileLimit : 10;
            $(document).on("change", "input:file", TT_Advert.change);

            var $addLink = $("<tr class='add_file_link'><td colspan='3'><span>Добавить файл</span></td></tr>");
            $addLink.data("fileLimit", fileLimit);
            var collectionHolder = $(target).find('.file_collection');

            collectionHolder.find('.file_form').each(function () {
                TT_Advert.addFileFormReplaceLink($(this));
                TT_Advert.replace_required($(this), $(this).find(".file_info").data('required'));

                /*if ($(this).find(".file_info").data('required') != undefined) {
                 TT_Advert.replace_required($(this), $(this).find(".file_info").data('required'));
                 } else {
                 TT_Advert.addFileFormDeleteLink($(this));
                 }*/

            });

            collectionHolder.append($addLink);
            collectionHolder.data('index', collectionHolder.find('.file_form').length);

            $addLink.on('click', function (e) {
                var currentLimit = $(this).data('fileLimit');
                TT_Advert.addFileForm(collectionHolder, $addLink, currentLimit);
                return false;
            });

            if (collectionHolder.data('index') == fileLimit) {
                collectionHolder.find(".add_file_link").hide();

            } else {
                if (fileLimit == 1) $addLink.click();
            }

        },

        addFileForm: function (collectionHolder, $newLinkLi, fileLimit) {
            // Get the data-prototype explained earlier
            var prototype = collectionHolder.data('prototype');

            // get the new index
            var index = collectionHolder.find('.file_form').length + 1;
            // Replace '__name__' in the prototype's HTML to
            // instead be a number based on how many items we have

            var newForm = prototype.replace(/__name__/g, index);
            var newForm = newForm.replace(/__placeholder__/g, "Документ №" + index + '"');

            // increase the index with one for the next item
            collectionHolder.data('index', index);

            // Display the form in the page in an li, before the "Add a tag" link li
            var $newFormLi = $('<tr class="file_form"></tr>').append(newForm);
            $newFormLi.find(".required").val(0);
            $newLinkLi.before($newFormLi);
            TT_Advert.addFileFormDeleteLink($newFormLi);
            if (index == fileLimit) {
                collectionHolder.find(".add_file_link").hide();
            }
        },

        addFileFormReplaceLink: function ($tagFormLi) {
            // debugger;
            var collectionHolder = $('.file_collection');

            var index = collectionHolder.data('index');

            var file_input_id = $($tagFormLi.find('input')[0]).attr("id").replace("displayName", "file")

            var file_input_name = $($tagFormLi.find('input')[0]).attr("name").replace("displayName", "file")
            var input = '<input type="file" id="' + file_input_id + '" name="' + file_input_name + '">';

            var $removeFormA = $('<div class="action"><a href="#" class="replace_file_link">Заменить</a>' + input + '</div>');

            if ($("#" + file_input_id).length == 0) $tagFormLi.find(".file_actions").append($removeFormA);


        },

        addFileFormDeleteLink: function ($tagFormLi) {
            var collectionHolder = $(this).parents('.file_collection');
            var $removeFormA = $('<div class="action"><span class="remove_file_link">Удалить</span></div>');
            var index = collectionHolder.data('index');

            $tagFormLi.find(".file_actions").append($removeFormA);


            $removeFormA.on('click', function (e) {
                // prevent the link from creating a "#" on the URL
                e.preventDefault();

                var file_ext = $tagFormLi.find(".file_info").data("ext");
                TT_Advert.enable_submit("file_ext_error_" + file_ext);
                // remove the li for the tag form
                $tagFormLi.remove();
                collectionHolder.data('index', index - 1);
                if (collectionHolder.data('index') < 10) {
                    collectionHolder.find(".add_file_link").show();
                }

                TT_Advert.validate_form();
                return false;
            });
        }
    };
    window.advert = TT_Advert || {};
})(window, window.jQuery, window.easyXDM);