<?php
namespace Easyb\AdvertBundle\Twig\Extension;


/**
 * Class AdvertExtension
 *
 * @package Easyb\AdvertBundle\Twig\Extenstion
 */
class AdvertExtension extends \Twig_Extension
{
    const MAIL_TO = 'mailto:';

    protected static $templates = array();

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array (
            'transliteration' => new \Twig_Function_Method($this, 'transliteration', array ()),
            'get_keywords'    => new \Twig_Function_Method($this, 'getKeywords', array ()),
            'get_title'       => new \Twig_Function_Method($this, 'getTitle', array ()),
            'mail_to'         => new \Twig_Function_Method($this, 'setMailTo', array ()),
        );
    }

    public function transliteration($text)
    {
        $cyr  = array('а','б','в','г','д','e','ж','з','и','й','к','л','м','н','о','п','р','с','т','у',
            'ф','х','ц','ч','ш','щ','ъ','ь', 'ы', 'ю','я','А','Б','В','Г','Д','Е','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У',
            'Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ь', 'Ю','Я' );
        $lat = array( 'a','b','v','g','d','e','zh','z','i','y','k','l','m','n','o','p','r','s','t','u',
            'f' ,'h' ,'ts' ,'ch','sh' ,'sht' ,'a' ,'y', 'y' ,'yu' ,'ya','A','B','V','G','D','E','Zh',
            'Z','I','Y','K','L','M','N','O','P','R','S','T','U',
            'F' ,'H' ,'Ts' ,'Ch','Sh' ,'Sht' ,'A' ,'Y' ,'Yu' ,'Ya' );

        return str_replace($cyr, $lat, $text);
    }

    public function getKeywords($route)
    {
        $result = '';
        foreach($route as $item) {
            $result .= $item;
        }

        $keywords = '';
        if ($result == 'bankovskiie_uslughi') {
            $keywords = $this->transliteration('депозитные вклады на различных условиях, кредиты долгосрочные и краткосрочные, расчетные операции, кассовое и документарное обслуживание, инкассо для документов, операции с трансферабельными и документарными аккредитивами');
        } elseif ($result == 'bukhghaltierskiie_auditorskiie_uslughi') {
            $keywords = $this->transliteration('Аутсорсинг бухгалтерских услуг, Бухгалтерские услуги, Бухгалтерские и аудиторские услуги, Бухгалтерские услуги Москва, Экспресс аудит, Ведение бухгалтерского учета, Аудит в Москве');
        } elseif ($result == 'dostavka_pit_ievoi_vody') {
            $keywords = $this->transliteration('Доставка воды по Москве, Доставка воды в офис в Москве, Доставка артезианской воды в офис, Бесплатная доставка воды в Санкт-Петербурге и Москве, доставка воды в день заказа, Бесплатная Доставка Воды');
        } elseif ($result == 'dostavka_produktov_pitaniia_kieitieringh') {
            $keywords = $this->transliteration('Доставка продуктов в офис, Купить продукты с доставкой в офис, Доставка продуктов питания, доставка еды в офис спб, Кейтеринг, организация выездных банкетов, кейтеринг москва, Кейтеринг - услуги, кейтеринг спб, выездной кейтеринг');
        } elseif ($result == 'dostavka_tsvietov') {
            $keywords = $this->transliteration('цветы доставка цветов, цветы с доставкой в офис по москве, цветы с доставкой в офис, доставка цветов недорого в офис');
        } elseif ($result == 'kadrovyie_uslughi') {
            $keywords = $this->transliteration('Абонентское обслуживание компании, Кадровое сопровождение, Аудит кадровой документации, Восстановление обязательных кадровых документов, Разработка внутренних локальных нормативных актов, Кадровый консалтинг: Консультации по вопросам трудового законодательства и кадрового делопроизводства.');
        } elseif ($result == 'klininghovyie_uslughi') {
            $keywords = $this->transliteration('услуги клининговой компании, оказание клининговых услуг, клининговые услуги цены, клининговые услуги москва, клининговые услуги уборка');
        } elseif ($result == 'komp_iutiernyie_i_it_uslughi') {
            $keywords = $this->transliteration('Разработка и создание сайтов от 1500 рублей, ИТ-услуги, ИТ-консалтинг, аудит ИТ-инфраструктуры, Ремонт компьютеров и ноутбуков, Создание и продвижение сайтов, IT-Консалтинг, Продвижение, Контекстная реклама');
        } elseif ($result == 'konsaltinghovyie_uslughi') {
            $keywords = $this->transliteration('Консалтинг, Антикризисное управление, Оценка стоимости бизнеса и компании, Сопровождение Банкротства, Налоговые споры, Банкротство Москва, Управленческий консалтинг, Консалтинговые услуги Москва');
        } elseif ($result == 'kur_ierskiie_pochtovyie_uslughi') {
            $keywords = $this->transliteration('Услуги курьерской доставки для интернет-магазинов, Отдел продаж, Доставка документов — Классическая экспресс-доставка, Курьерская доставка для интернет-магазинов, юридических и частных лиц, Экспресс-доставка документов в картонном конверте «от двери до двери», Услуги курьера и экспресс доставка грузов');
        } elseif ($result == 'linghvistichieskii_pierievod') {
            $keywords = $this->transliteration('Услуги нотариального перевода, Перевод текстов, Профессиональные юридические, финансовые, технические переводы, Нотариальное заверение перевода и Апостиль, Консульская легализация документов, Устный и письменный перевод, Услуга технического перевода');
        } elseif ($result == 'obrazovatiel_nyie_uslughi') {
            $keywords = $this->transliteration('Бизнес тренинг руководителей, Корпоративные семинары, Деловой туризм и официальные делегации - MICE, Корпоративные тренинги по продажам, Менеджмент, Тренинги для руководителей, Тренинги для руководителей');
        } elseif ($result == 'orghanizatsiia_dielovykh_poiezdok') {
            $keywords = $this->transliteration('Корпоративное обслуживание: Организация деловых поездок, Организация поездок, Трансфер, размещение, Культурные программы, Эффективное управление деловыми поездками, Организация деловых поездок в Москве, Бизнес-поездки Москва, Организация деловой поездки и встречи на высшем уровне, Деловой туризм с профессиональным подходом');
        } elseif ($result == 'orghanizatsiia_prazdnikov') {
            $keywords = $this->transliteration('Деловые мероприятия, корпоративные мероприятия, тимбилдинг, Организация семинаров, конференций, Новогодние мероприятия, Выездные мероприятия, Организация деловых и частных мероприятий, Event маркетинг и PR, Мероприятия в формате квеста');
        } elseif ($result == 'polighrafichieskiie_i_dizainierskiie_uslughi') {
            $keywords = $this->transliteration('Разработка фирменного стиля, Визитки, открытки, приглашения, буклеты, Изготовление и печать любой рекламной и представительской полиграфической, Разработка рекламной концепции в Москве, Полиграфия, Дизайн, Издательское дело.');
        } elseif ($result == 'rieklama_markietingh_pr') {
            $keywords = $this->transliteration('Размещение рекламы на радио, Поисковый маркетинг, Продвижение сайтов, Медийная (баннерная) реклама, Реклама в интернете, Маркетинговое сопровождение компании, Маркетинговые исследования, аутсорсинг, консалтинг');
        } elseif ($result == 'riemont_i_obluzhivaniie_ofisnoi_tiekhniki') {
            $keywords = $this->transliteration('Ремонт оргтехники Москва, Сервисное обслуживание компьютерной техники, Заправка катриджей Москва, IT-аутсорсинг в Москве, Ремонт и техобслуживание оргтехники ');
        } elseif ($result == 'rieltorskiie_uslughi') {
            $keywords = $this->transliteration('Аренда офисов, Агентские услуги: Офисные помещения, Офисная недвижимость, Снять офис в Москве, Риэлторские услуги в Туле и области, Коммерческая недвижимость');
        } elseif ($result == 'strakhovyie_uslughi') {
            $keywords = $this->transliteration('Страхование финансовых рисков, Добровольное медицинское страхование, Страхование бизнеса, Страхование малого и среднего бизнеса, Страхование сотрудников, Страхование имущества предприятий');
        } elseif ($result == 'transportnyie_uslughi') {
            $keywords = $this->transliteration('Офисный переезд, Грузоперевозки по РФ и СНГ, Трансфер сотрудников, перевозка людей, Переезд офиса недорого, Трансфер, ,Курьерские услуги, Услуги по экспресс-доставке');
        } elseif ($result == 'turistichieskiie_uslughi') {
            $keywords = $this->transliteration('Корпоративный выезд, Корпоративный отдых, Корпоративный туризм, Корпоративный отдых за границей, Корпоративный праздник в подмосковье');
        } elseif ($result == 'uslughi_v_sfierie_tieliekommunikatsii') {
            $keywords = $this->transliteration('Безлимитные тарифы по России, Номер 8-800 (услуга Бесплатный вызов), Виртуальная АТС, Подключение услуги многоканальный номер, Виртуальный номер, IP-телефония, Установка видеонаблюдения, Выделенная линия интернет');
        } elseif ($result == 'finansovyie_uslughi') {
            $keywords = $this->transliteration('Финансовые услуги, Антиколлекторские услуги, Деятельность по доверительному управлению, Управление финансами, Дилерская и брокерская деятельность');
        } elseif ($result == 'iuridichieskiie_uslughi') {
            $keywords = $this->transliteration('Консультации по арбитражным спорам, Регистрация Юридических лиц Москва, Сертификация в Таможенном Союзе, Помощь в регистрации ООО, Юридическое обслуживание, Представительство в арбитражном суде, Корпоративная практика');
        }

        if(!$keywords) {
            $keywords = 'коммерческие тендеры, тендеры, площадки тендеров, закупки тендеры, тендеры России, оказание услуг, предоставление услуг, портал услуг, сайт услуг, реклама бесплатно, размещение рекламы, моя реклама объявления, разместить рекламу, дать рекламу';
        }

        return $keywords;
    }

    public function getTitle($route, $category = null)
    {
        if (!$category) {
            return 'Easyb - реклама услуг бесплатно, коммерческие тендеры, сайт услуг.';
        }

        $result = '';
        foreach($route as $item) {
            $result .= $item;
        }

        $title = '';
        if ($result == 'bankovskiie_uslughi') {
            $title = $category->getName();
        } elseif ($result == 'bukhghaltierskiie_auditorskiie_uslughi') {
            $title = $category->getName();
        } elseif ($result == 'dostavka_pit_ievoi_vody') {
            $title = $category->getName();
        } elseif ($result == 'dostavka_produktov_pitaniia_kieitieringh') {
            $title = $category->getName();
        } elseif ($result == 'dostavka_tsvietov') {
            $title = $category->getName();
        } elseif ($result == 'kadrovyie_uslughi') {
            $title = $category->getName();
        } elseif ($result == 'klininghovyie_uslughi') {
            $title = $category->getName();
        } elseif ($result == 'komp_iutiernyie_i_it_uslughi') {
            $title = $category->getName();
        } elseif ($result == 'konsaltinghovyie_uslughi') {
            $title = $category->getName();
        } elseif ($result == 'kur_ierskiie_pochtovyie_uslughi') {
            $title = $category->getName();
        } elseif ($result == 'linghvistichieskii_pierievod') {
            $title = $category->getName();
        } elseif ($result == 'obrazovatiel_nyie_uslughi') {
            $title = $category->getName();
        } elseif ($result == 'orghanizatsiia_dielovykh_poiezdok') {
            $title = $category->getName();
        } elseif ($result == 'orghanizatsiia_prazdnikov') {
            $title = $category->getName();
        } elseif ($result == 'polighrafichieskiie_i_dizainierskiie_uslughi') {
            $title = $category->getName();
        } elseif ($result == 'rieklama_markietingh_pr') {
            $title = $category->getName();
        } elseif ($result == 'riemont_i_obluzhivaniie_ofisnoi_tiekhniki') {
            $title = $category->getName();
        } elseif ($result == 'rieltorskiie_uslughi') {
            $title = $category->getName();
        } elseif ($result == 'strakhovyie_uslughi') {
            $title = $category->getName();
        } elseif ($result == 'transportnyie_uslughi') {
            $title = $category->getName();
        } elseif ($result == 'turistichieskiie_uslughi') {
            $title = $category->getName();
        } elseif ($result == 'uslughi_v_sfierie_tieliekommunikatsii') {
            $title = $category->getName();
        } elseif ($result == 'finansovyie_uslughi') {
            $title = $category->getName();
        } elseif ($result == 'iuridichieskiie_uslughi') {
            $title = $category->getName();
        }

        if(!$title) {
            $title = 'Easyb - реклама услуг бесплатно, коммерческие тендеры, сайт услуг.';
        }

        return $title;
    }

    public function setMailTo($text)
    {
        if (strripos($text, self::MAIL_TO) === false) {
            return self::MAIL_TO.$text;
        }

        return $text;
    }

    /**
     * @return string The extension name
     */
    public function getName()
    {
        return 'advert.extension';
    }
}