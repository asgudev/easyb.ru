<?php
/**
 * User: scorp
 * Date: 08.07.13
 * Time: 11:52
 */
namespace Easyb\AdvertBundle\Twig\Extension;

use Easyb\AdvertBundle\Entity\AbstractAdvert;
use Easyb\AdvertBundle\Entity\AutoSearch;
use Easyb\AdvertBundle\Entity\Demand;
use Symfony\Component\Routing\Router;

/**
 * Class SearchExtenstion
 *
 * @package Easyb\AdvertBundle\Twig\Extenstion
 */
class SearchExtension extends \Twig_Extension
{
    /**
     * @var \Symfony\Component\Routing\Router
     */
    protected $router;

    /**
     * @param \Symfony\Component\Routing\Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array (
            'catalog_url'       => new \Twig_Function_Method($this, 'getCatalogUrl', array ()),
            'delete_tags'       => new \Twig_Function_Method($this, 'deleteTags', array ()),
            'advert_show_route' => new \Twig_Function_Method($this, 'getAdvertShowRoute', array ()),
        );
    }

    /**
     * @param AutoSearch $search
     * @param bool       $newResults
     *
     * @return string
     */
    public function getCatalogUrl(AutoSearch $search, $newResults = false)
    {
        $categoryId = ($search->getCategory() ? $search->getCategory()->getId() : '');
        $searchText = ($search->getText() ? $search->getText() : '');
        $advertTypeId = ($search->getType() ? $search->getType() : '');
        $cityId = ($search->getCity() ? $search->getCity()->getId() : '');

        $url = $this->router->generate('catalog', [
            'categoryId'   => $categoryId,
            'search_text'  => $searchText,
            'advertTypeId' => $advertTypeId,
            'cityId'       => $cityId,
            'newResults'   => $newResults ? (int)($search->getViewingAt()->format('U')) : '',
            'autoId'       => $search->getId()
        ]);

        return $url;
    }


    public function deleteTags($text, $all = false)
    {
        if ($all) {
            $text = strip_tags($text);
            $text = str_replace("\r\n", ' ', $text);
            $text = str_replace("\n", ' ', $text);
            $text = str_replace("&nbsp;", ' ',$text);
            $text = trim(preg_replace('/\s{2,}/', ' ', $text));
        } else {
            $text = strip_tags($text, '<strong><u><em><ol><li><ul><a><p><br>');
            $text = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $text);
        }
//
//        $text = str_replace('<strong>', '', $text);
//        $text = str_replace('</strong>', '', $text);
//        $text = str_replace('<u>', '', $text);
//        $text = str_replace('</u>', '', $text);
//        $text = str_replace('<em>', '', $text);
//        $text = str_replace('</em>', '', $text);

        // ставим атрибу nofollow для линка
        $text = str_replace('<a ', '<a rel="nofollow"', $text);

        return trim($text);
    }

    /**
     * @param AbstractAdvert $advert
     *
     * @return string
     */
    public function getAdvertShowRoute(AbstractAdvert $advert)
    {

        $url = $advert instanceof Demand ? 'show_'.AbstractAdvert::DEMAND_STRING : 'show_'.AbstractAdvert::OFFER_STRING;

        $route = $this->router->generate($url, [
            'category' => $advert->getCategory()->getSlug(),
            'city'     => $advert->getJob()->getCity()->getSlug(),
            'slug'     => $advert->getSlug(),
        ]);

        return $route;
    }


    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'search.extension';
    }
}