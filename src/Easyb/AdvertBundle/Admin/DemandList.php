<?php
/**
 * Created by IntelliJ IDEA.
 * User: Pc
 * Date: 15.02.2016
 * Time: 1:16
 */

namespace Easyb\AdvertBundle\Admin;

use Easyb\AdminBundle\Admin\Admin as BaseAdmin;
use Doctrine\ORM\EntityManager;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class DemandList extends BaseAdmin
{
    protected $em;
    protected $baseRouteName = 'demand_list';
    protected $baseRoutePattern = 'demand-list';


    /**
     * @param EntityManager $entityManager
     */
    public function __construct($code, $class, $baseControllerName, EntityManager $entityManager)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->em = $entityManager;

        if (!$this->hasRequest()) {
            $this->datagridValues = array(
                '_page' => 1,
                '_per_page' => $this->getMaxPerPage(),
                '_sort_order' => 'DESC',
                '_sort_by' => 'createdAt'
            );
        }
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        // here we set the fields of the ShowMapper variable,
        // $showMapper (but this can be called anything)
        $listMapper

            // The default option is to just display the
            // value as text (for boolean this will be 1 or 0)
            ->add('name', null, array(
                'label' => 'Тендера',)
            )

            ->add('job', null, array(
                'label' => 'Компания',
                'admin_code' => 'sonata.admin.jobs',)
            )
        ;

    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        // Here we set the fields of the ShowMapper variable, $showMapper (but this can be called anything)
        $showMapper

            /*
             * The default option is to just display the value as text (for boolean this will be 1 or 0)
             */
            ->add('name')
            ->add('description')
        ;

    }
}