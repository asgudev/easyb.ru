<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 29.08.14
 * Time: 17:17
 */

namespace Easyb\AdvertBundle\Admin;

use Doctrine\ORM\Query\Expr\Base;
use Easyb\AdminBundle\Admin\Admin as BaseAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;

use Knp\Menu\ItemInterface as MenuItemInterface;

class AdvertMetadataAdmin extends BaseAdmin
{
    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', null, array('label' => 'Title',
                                       'help' => 'Параметры в тексте: <br/>
                                                      [service_name] - название услуги,<br/>
                                                      [company_name] - название компании,<br/>
                                                      [service_section] - название раздела услуг.<br/>
                                                      '))
            ->add('description', null, array('label' => 'Description',
                                             'attr' => array('rows' => 5),
                                             'help' => 'Параметры в тексте: <br/>
                                                      [service_name] - название услуги,<br/>
                                                      [company_name] - название компании,<br/>
                                                      [service_section] - название раздела услуг.<br/>
                                                      '))
            ->add('keywords', null, array('label' => 'Keywords',
                                          'attr' => array('rows' => 5),
                                          'help' => 'Параметры в тексте: <br/>
                                                      [service_name] - название услуги,<br/>
                                                      [company_name] - название компании,<br/>
                                                      [service_section] - название раздела услуг.<br/>
                                                      '))
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->addIdentifier('title', null, array('label' => ' ', 'template' => 'EasybAdvertBundle:Admin:AdvertMetadata_show_field.html.twig'));

        $listMapper->add('_action', 'actions', array('actions' => array('edit' => array(),)));
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            ->remove('delete')
        ;
    }
}