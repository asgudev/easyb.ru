<?php
namespace Easyb\AdvertBundle\Admin;

use Doctrine\ORM\EntityManager;
use Easyb\AdminBundle\Admin\Admin as BaseAdmin;
use Easyb\AdvertBundle\Entity\AdvertFile;
use Easyb\AdvertBundle\Form\Type\AdminFileType;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class ApproveOfferAdmin extends BaseAdmin
{
    protected $em;
    protected $baseRouteName = 'offers_approval';
    protected $baseRoutePattern = 'offers-approval';

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     * @param EntityManager $entityManager
     */
    public function __construct($code, $class, $baseControllerName, EntityManager $entityManager)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->em = $entityManager;

        if (!$this->hasRequest()) {
            $this->datagridValues = array(
                '_page' => 1,
                '_per_page' => $this->getMaxPerPage(),
                '_sort_order' => 'DESC',
                '_sort_by' => 'createdAt'
            );
        }
    }

    public function configure()
    {
        $this->setTemplate('edit', 'EasybAdvertBundle:Admin:editApprove.html.twig');
    }

    public function getModelInstance($class)
    {
        return new $class();
    }

    /**
     * @param Object $queryBuilder
     * @param string $alias
     * @param boolean $field
     * @param array $value
     *
     * @return mixed
     */
    public function getUserFilter($queryBuilder, $alias, $field, $value)
    {
        if ($value['value'] == '') {
            return $queryBuilder;
        }
        $queryBuilder
            ->leftJoin(sprintf('%s.user', $alias), 'u')
            ->andWhere('u.id = :id')
            ->setParameter('id', $value['value']->getId());

        return $queryBuilder;
    }

    /**
     * @param Object $queryBuilder
     * @param string $alias
     * @param boolean $field
     * @param array $value
     *
     * @return mixed
     */
    public function getJobFilter($queryBuilder, $alias, $field, $value)
    {
        if ($value['value'] == '') {
            return $queryBuilder;
        }
        $queryBuilder
            ->leftJoin(sprintf('%s.job', $alias), 'j')
            ->andWhere('j.id = :id')
            ->setParameter('id', $value['value']->getId());

        return $queryBuilder;
    }

    /**
     * @param Object $queryBuilder
     * @param string $alias
     * @param boolean $field
     * @param array $value
     *
     * @return mixed
     */
    public function getCityFilter($queryBuilder, $alias, $field, $value)
    {
        if ($value['value'] == '') {
            return $queryBuilder;
        }
        $queryBuilder
            ->leftJoin(sprintf('%s.job', $alias), 'j')
            ->leftJoin(sprintf('j.city', $alias), 'c')
            ->andWhere('c.id = :id')
            ->setParameter('id', $value['value']->getId());

        return $queryBuilder;
    }

    /**
     * @param Object $queryBuilder
     * @param string $alias
     * @param boolean $field
     * @param array $value
     *
     * @return mixed
     */
    public function getCategoryFilter($queryBuilder, $alias, $field, $value)
    {
        if ($value['value'] == '') {
            return $queryBuilder;
        }
        $queryBuilder
            ->leftJoin(sprintf('%s.category', $alias), 'ca')
            ->andWhere('ca.id = :id')
            ->setParameter('id', $value['value']->getId());

        return $queryBuilder;
    }

    /**
     * @param FormEvent $event
     */
    public function preSubmit(FormEvent $event)
    {
        $data = $event->getData();
        if (array_key_exists('files', $data)) {
            foreach ($data['files'] as $file) {
                $this->validateFiles($file['file'], $event->getForm());
            }
        }

        $user = $this->em->getRepository('EasybUserBundle:User')->findOneById($data['user']['autocompleter_value']);
        $data['job'] = $user->getJobs()->first()->getId();
    }

    /**
     * @param $file
     * @param $form
     *
     * @return bool
     */
    private function validateFiles($file, $form)
    {
        if ($file == null) {
            return false;
        }
        if (!array_key_exists(
            mb_convert_case($file->getClientOriginalExtension(), MB_CASE_LOWER),
            AdvertFile::$filesTypes
        )
        ) {
            $form->addError(
                new FormError(
                    'Прикрепляемым Вами файл не соответствует формату или превышает установленный размер 10 Mb.'
                )
            );
        }
    }

    /**
     * @param FormEvent $event
     */
    public function submit(FormEvent $event)
    {
        $data = $event->getData();
        $data->setJob($data->getUser()->getJobs()->first());
    }

    /**
     * Добавляем свой баш - о тклонить и одобрить
     *
     * @return array
     */
    public function getBatchActions()
    {
        $actions = array();

        // check user permissions
        if ($this->hasRoute('edit') && $this->isGranted('EDIT') && $this->hasRoute('delete') && $this->isGranted(
                'DELETE'
            )
        ) {
            $actions['improve'] = [
                'label' => 'Отправить на доработку',
                'ask_confirmation' => false // If true, a confirmation will be asked before performing the action
            ];
            $actions['approve'] = [
                'label' => 'Одобрить',
                'ask_confirmation' => false // If true, a confirmation will be asked before performing the action
            ];
        }

        if ($this->hasRoute('delete') && $this->isGranted('DELETE')) {
            $actions['delete_abstract'] = [
                'label' => 'Удалить обьявление',
                'ask_confirmation' => false // If true, a confirmation will be asked before performing the action
            ];
            $actions['delete_user'] = [
                'label' => 'Удалить пользователя',
                'ask_confirmation' => false // If true, a confirmation will be asked before performing the action
            ];
        }

        return $actions;
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $this->getModelManager();
        $query = parent::createQuery($context);
        $qb = $query->getQueryBuilder();
        $qb
            ->where($qb->expr()->eq($qb->getRootAliases()[0] . '.isApproved', ':isApproved'))
            ->andWhere($qb->expr()->eq($qb->getRootAliases()[0] . '.isImproved', ':isImproved'))
            ->setParameters(['isApproved' => 'false', 'isImproved' => 'false']);

        return $query;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
    }

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add(
                'user',
                'em_autocompleter',
                [
                    'required' => true,
                    'label' => 'Пользователь',
                    'property' => 'username',
                    'route_name' => 'search_user',
                    'class' => 'Easyb\\UserBundle\\Entity\\User',
                    'help' => 'Начните вводить ФИО или Email и выберите автора из списка',
                    'attr' => ['class' => 'add-useradmin']
                ]
            )
            ->add(
                'category',
                null,
                [
                    'label' => 'Раздел каталога',
                    'required' => false
                ]
            )
            ->add(
                'name',
                null,
                [
                    'required' => true,
                    'label' => 'Заголовок объявления'
                ]
            )
            ->add(
                'text',
                null,
                [
                    'required' => true,
                    'label' => 'Текст сообщения',
                    'attr' => array('class' => 'ckeditor')
                ]
            )
            ->add(
                'metaDescription',
                null,
                [
                    'required' => false,
                    'label' => 'Ключевые слова'
                ]
            )
            ->add(
                'files',
                'file_collection',
                [
                    'type' => new AdminFileType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'label' => 'Выберите файл и дайте ему имя'
                ]
            )
            // hidden fields
            ->add('job', null, [], [
                'admin_code' => 'sonata.admin.jobs',
            ])
            ->add(
                'fromAdmin',
                null,
                [
                    'data' => true
                ]
            );

        $formMapper->getFormBuilder()->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'preSubmit'));
        $formMapper->getFormBuilder()->addEventListener(FormEvents::SUBMIT, array($this, 'submit'));
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('job', null, array(
                'label' => 'Наименование компании',
                'admin_code' => 'sonata.admin.jobs',))
            ->addIdentifier(
                'name',
                null,
                array(
                    'label' => 'Наименование коммерческого предложения',
                    'template' => 'EasybAdvertBundle:Admin:name.html.twig'
                )
            )
            ->add('category', null, array('label' => 'Категория'))
            ->add('user', null, array('label' => 'Пользователь'));

        $listMapper->add('_action', 'actions', array('actions' => array(
            'improve' => array('template' => 'EasybAdvertBundle:Admin:improve.html.twig')
        )));
    }

    /**
     * Configuration show view
     *
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name', null, array('label' => 'Название'))
            ->add('job', null, array(
                'label' => 'Компания',
                'admin_code' => 'sonata.admin.jobs',
            ))
            ->add('category', null, array('label' => 'Категория'))
            ->add('user', null, array('label' => 'Пользователь'))
            ->add('text', 'text', array('label' => 'Текст'))
            ->add('createdAt', null, array('label' => 'Дата'));
    }

    /**
     * @param DatagridMapper $filter
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add(
                'user',
                'doctrine_orm_callback',
                array(
                    'field_type' => 'em_autocompleter',
                    'field_options' => array(
                        'class' => 'Easyb\\UserBundle\\Entity\\User',
                        'property' => 'username',
                        'route_name' => 'search_user'
                    ),
                    'mapping_type' => 2,
                    'label' => 'Пользователь',
                    'callback' => array($this, 'getUserFilter')
                )
            )
            ->add(
                'job',
                'doctrine_orm_callback',
                array(
                    'field_type' => 'em_autocompleter',
                    'field_options' => array(
                        'class' => 'Easyb\\UserBundle\\Entity\\Job',
                        'property' => 'name',
                        'route_name' => 'search_job'
                    ),
                    'mapping_type' => 2,
                    'label' => 'Компания',
                    'admin_code' => 'sonata.admin.jobs',
                    'callback' => array($this, 'getJobFilter')
                )
            )
            ->add(
                'city',
                'doctrine_orm_callback',
                array(
                    'field_type' => 'em_autocompleter',
                    'field_options' => array(
                        'class' => 'Easyb\\MainBundle\\Entity\\City',
                        'property' => 'name',
                        'route_name' => 'search_city'
                    ),
                    'mapping_type' => 2,
                    'label' => 'Город',
                    'callback' => array($this, 'getCityFilter')
                )
            )
            ->add(
                'category',
                'doctrine_orm_callback',
                array(
                    'field_type' => 'em_autocompleter',
                    'field_options' => array(
                        'class' => 'Easyb\\MainBundle\\Entity\\Category',
                        'property' => 'name',
                        'route_name' => 'search_category'
                    ),
                    'mapping_type' => 2,
                    'label' => 'Каталог',
                    'callback' => array($this, 'getCategoryFilter')
                )
            );
    }
}
