<?php
namespace Easyb\AdvertBundle\Admin;

use Easyb\AdminBundle\Admin\Admin as BaseAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

use Easyb\AdvertBundle\Form\Type\AdminFileType;

use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

use Knp\Menu\ItemInterface as MenuItemInterface;


class OfferAdmin extends DemandAdmin
{

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('user', 'em_autocompleter', [
                'required' => true,
                'label' => 'Пользователь',
                'property' => 'username',
                'route_name' => 'search_user',
                'class' => 'Easyb\\UserBundle\\Entity\\User',
                'help' => 'Начните вводить ФИО или Email и выберите автора из списка',
                'attr' => ['class' => 'add-useradmin']
            ])
            ->add('name', null, [
                'required' => true,
                'label' => 'Наименование коммерческого предложения'
            ])
            ->add('category', null, [
                'label' => 'Раздел каталога',
                'required' => false
            ])
            ->add('text', null, [
                'required' => true,
                'label' => 'Текст сообщения',
                'attr' => array('class' => 'editor')
            ])
            ->add('metaDescription', null, [
                'required' => false,
                'label' => 'Ключевые слова'
            ])
            ->add('files', 'file_collection', [
                'type' => new AdminFileType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => 'Выберите файл и дайте ему имя'
            ])
            // hidden fields
            ->add('job', null, [

            ], [
                'admin_code' => 'sonata.admin.jobs',
            ])
            ->add('fromAdmin', null, [
                'data' => true
            ]);

        $formMapper->getFormBuilder()->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'preSubmit'));
        $formMapper->getFormBuilder()->addEventListener(FormEvents::SUBMIT, array($this, 'submit'));
    }
}
