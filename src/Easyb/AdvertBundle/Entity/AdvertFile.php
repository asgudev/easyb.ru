<?php

namespace Easyb\AdvertBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\Validator\Constraints as Assert;

use Easyb\UserBundle\Entity\Job;

/**
 * AdvertFile
 *
 * @ORM\Entity
 * @ORM\Table(name="advert_file")
 * @ORM\HasLifecycleCallbacks
 */
class AdvertFile
{
    public static $filesTypes = [
        'doc' => 'doc',
        'docx' => 'docx',
        'docm' => 'docm',
        'txt' => 'txt',
        'xlsx' => 'xlsx',
        'xlsb' => 'xlsb',
        'xls' => 'xls',
        'pptx' => 'pptx',
        'pptm' => 'pptm',
        'ppt' => 'ppt',
        'pdf' => 'pdf',
        'gif' => 'gif',
        'jpeg' => 'jpeg',
        'jpg' => 'jpg',
        'png' => 'png',
        'bmp' => 'bmp',
        'pps' => 'pps'
    ];

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="display_name", type="string", length=255)
     * @Assert\NotBlank(message="Введите имя файла")
     */
    private $displayName;

    /**
     * @var AbstractAdvert $advert
     *
     * @ORM\ManyToOne(targetEntity="Easyb\AdvertBundle\Entity\AbstractAdvert", inversedBy="files")
     * @ORM\JoinColumn(name="advert_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $advert;

    /**
     * @var AbstractAdvert $advert
     *
     * @ORM\ManyToOne(targetEntity="Easyb\AdvertBundle\Entity\Reaction", inversedBy="files")
     * @ORM\JoinColumn(name="reaction_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $reaction;

    /**
     * @var AbstractAdvert $advert
     *
     * @ORM\ManyToOne(targetEntity="Easyb\AdvertBundle\Entity\Proposal", inversedBy="files")
     * @ORM\JoinColumn(name="proposal_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $proposal;


    /**
     * @var AbstractAdvert $advert
     *
     * @ORM\ManyToOne(targetEntity="Easyb\UserBundle\Entity\Job", inversedBy="files")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $job;

    /**
     * @var AbstractAdvert $advert
     *
     * @ORM\ManyToOne(targetEntity="Easyb\AdvertBundle\Entity\Demand", inversedBy="inviteFile")
     * @ORM\JoinColumn(name="demand_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $demand;

    /**
     * @Assert\File(
     *     maxSize = "10240k",
     *     maxSizeMessage = "Максимальный размер 10Мб"
     * )
     *
     * @var string
     */
    private $file;

    /**
     * @var integer
     * @ORM\Column(name="required", type="integer", options={"default" = 0})
     */
    private $required;

    public function __construct()
    {
        $this->required = 0;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return AdvertFile
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get displayName
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * Set displayName
     *
     * @param string $displayName
     * @return AdvertFile
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * Get advert
     *
     * @return \Easyb\AdvertBundle\Entity\AbstractAdvert
     */
    public function getAdvert()
    {
        return $this->advert;
    }

    /**
     * Set advert
     *
     * @param \Easyb\AdvertBundle\Entity\AbstractAdvert $advert
     *
     * @return AdvertFile
     */
    public function setAdvert(AbstractAdvert $advert = null)
    {
        $this->advert = $advert;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getReaction()
    {
        return $this->reaction;
    }

    /**
     * @param mixed $reaction
     */
    public function setReaction(Reaction $reaction = null)
    {
        $this->reaction = $reaction;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProposal()
    {
        return $this->proposal;
    }

    /**
     * @param mixed $proposal
     */
    public function setProposal(Proposal $proposal = null)
    {
        $this->proposal = $proposal;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->displayName;
    }

    /**
     * @ORM\PreFlush()
     */
    public function upload()
    {
        // the file property can be empty if the field is not required

        if (null === $this->file) {
            return;
        }


        if ($this->name) {
            $this->removeUpload();
        }

        $ext = $this->file->guessClientExtension();

        $name = uniqid() . '.' . $ext;
        $this->checkDirectory($this->getUploadRootDir());
        $this->file->move($this->getUploadRootDir(), $name);
        $this->setName($name);

        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            //unlink($file);
        }
        $this->name = null;
    }

    /**
     * @return null|string
     */
    public function getAbsolutePath()
    {
        return null === $this->name ? null : $this->getUploadRootDir() . '/' . $this->name;
    }

    /**
     * @return string
     */
    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../../web' . $this->getUploadDir();
    }

    /**
     * @return string
     */
    protected function getUploadDir()
    {
        return '/upload/files';
    }

    private function checkDirectory($directory)
    {
        if (!is_dir($directory)) {
            if (false === @mkdir($directory, 0777, true)) {
                throw new FileException(sprintf('Unable to create the "%s" directory', $directory));
            }
        } elseif (!is_writable($directory)) {
            throw new FileException(sprintf('Unable to write in the "%s" directory', $directory));
        }
    }

    public function getPublicName()
    {
        $ext = end((explode(".", $this->name)));
        if ($this->advert != null) {
            $name = $this->advert->getJob()->getName();
        } elseif ($this->reaction != null) {
            $name = $this->reaction->getJob()->getName();
        } elseif ($this->demand != null) {
            $name = $this->demand->getJob()->getName();
        } elseif ($this->file == null) {
            $name = $this->name;
        } else {
            //$name = $this->getJob()->getName();
        }
        return $name . "_" . $this->displayName . "." . $ext;
    }

    /**
     * @return mixed
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * @param mixed $reaction
     */
    public function setJob(Job $job = null)
    {
        $this->job = $job;

        return $this;
    }

    public function getWebPath()
    {
        return null === $this->name
            ? null
            : $this->getUploadDir() . '/' . $this->name;
    }


    public function getFileSize()
    {
        /*if ($this->file != null) {


        } else {
            $filesize = 0;
        }*/
        $filesize = filesize($this->getAbsolutePath());
        //$filesize = 0;
        return $filesize;
    }

    private $tempName;

    public function getTempName()
    {
        return $this->tempName;
    }

    public function setTempName($tempname)
    {
        $this->tempName = $tempname;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * @param boolean $required
     */
    public function setRequired($required)
    {
        $this->required = $required;
        return $this;
    }

    /**
     * @return Demand
     */
    public function getDemand()
    {
        return $this->demand;
    }

    /**
     * @param mixed $demand
     */
    public function setDemand($demand)
    {
        $this->demand = $demand;
        return $this;
    }
}