<?php
/**
 * Created by IntelliJ IDEA.
 * User: User
 * Date: 16.02.2016
 * Time: 17:51
 */

namespace Easyb\AdvertBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Easyb\UserBundle\Entity\User;
use Easyb\UserBundle\Entity\Job;
use Symfony\Component\Validator\Constraints\DateTime;


/**
 * @ORM\Entity()
 * @ORM\Table(name="questions")
 */
class Question
{
    const MAIL_QUESTION_CREATED = 23;
    const MAIL_QUESTION_ANSWERED = 24;

    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Easyb\AdvertBundle\Entity\Demand", inversedBy="questions")
     * @ORM\JoinColumn(name="demand_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $demand;

    /**
     * @ORM\Column(name="question", type="text", nullable=false)
     */
    private $question;

    /**
     * @ORM\Column(name="answer", type="text", nullable=true)
     */
    private $answer;

    /**
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    private $createdAt;
    /**
     * @ORM\Column(name="answeredAt", type="datetime", nullable=true)
     */
    private $answeredAt;


    /**
     * @ORM\ManyToOne(targetEntity="Easyb\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Easyb\UserBundle\Entity\Job")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $job;


    public function __construct(User $user)
    {
        $this->createdAt = new \DateTime();
        $this->user = $user;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Demand
     */
    public function getDemand()
    {
        return $this->demand;
    }

    /**
     * @param string $demand
     */
    public function setDemand($demand)
    {
        $this->demand = $demand;
        return $this;

    }

    /**
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param string $question
     */
    public function setQuestion($question)
    {
        $this->question = $question;
        return $this;
    }

    /**
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @param string $answer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
        return $this;
    }

    /**
     * @return Question
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Question
     */
    public function getAnsweredAt()
    {
        return $this->answeredAt;
    }

    /**
     * @param \DateTime $answeredAt
     * @return Question
     */
    public function setAnsweredAt($answeredAt)
    {
        $this->answeredAt = $answeredAt;
        return $this;
    }

    /**
     * Get demand
     *
     * @return \Easyb\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set demand
     *
     * @param \Easyb\AdvertBundle\Entity\Demand $demand
     * @return Reaction
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get job
     *
     * @return \Easyb\UserBundle\Entity\Job
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set job
     *
     * @param \Easyb\UserBundle\Entity\Job $job
     * @return Question
     */
    public function setJob(Job $job = null)
    {
        $this->job = $job;

        return $this;
    }
}