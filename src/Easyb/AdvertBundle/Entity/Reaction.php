<?php

namespace Easyb\AdvertBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Easyb\UserBundle\Entity\Job;
use Easyb\UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Reaction
 *
 * @ORM\Table(name="reactions")
 * @ORM\Entity(repositoryClass="Easyb\AdvertBundle\Entity\ReactionRepository")
 */
class Reaction
{
    /**
     * Отклик в спросе
     */
    const DEMAND = 2;
    /**
     * Отклик в предложении
     */
    const OFFER = 1;

    /**
     * Ожидает подтверждения
     */
    const STATUS_START = 0;
    /**
     * Предложение одобрено, но контактные данные в попапе на странице "Отклики: Мои предложения" не просмотрены
     */
    const STATUS_APPROVE = 1;
    /**
     * Предложение отклонено
     */
    const STATUS_DECLINE = 2;

    /**
     * Предложение рассмотрено, но ответ не дан
     */
    const STATUS_CONSIDERED = 3;

    /**
     * Предложение одобрено, контактные данные в попапе на странице "Отклики: Мои предложения" просмотрено
     */
    const STATUS_SEE_MY_CONTACTS = 4;

    /**
     * Предложение дисквалифицировано по времени
     */
    const STATUS_DISQUALIFIED = 5;

    /**
     * Прямая заявка в приватный тендер
     */
    const STATUS_DIRECT = 6;


    /**
     * @var ArrayCollection $files
     * @ORM\OneToMany(targetEntity="Easyb\AdvertBundle\Entity\AdvertFile", mappedBy="reaction", cascade={"persist"}, orphanRemoval=true)
     */
    protected $files;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;
    /**
     * @ORM\ManyToOne(targetEntity="Easyb\AdvertBundle\Entity\Demand", inversedBy="reactions")
     * @ORM\JoinColumn(name="demand_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $demand;
    /**
     * @ORM\ManyToOne(targetEntity="Easyb\AdvertBundle\Entity\Offer")
     * @ORM\JoinColumn(name="offer_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $offer;
    /**
     * @ORM\ManyToOne(targetEntity="Easyb\UserBundle\Entity\Job")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $job;
    /**
     * @ORM\ManyToOne(targetEntity="Easyb\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text", nullable=true)
     * @Assert\NotBlank()
     */
    private $message;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted_from_offer", type="boolean", nullable=true)
     */
    private $deletedFromOffer;
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted_from_demand", type="boolean", nullable=true)
     */
    private $deletedFromDemand;

    /**
     * @var Proposal
     *
     * @ORM\OneToMany(targetEntity="Easyb\AdvertBundle\Entity\Proposal", mappedBy="reaction")
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    private $proposal;

    public function __construct($user)
    {
        $this->files = new ArrayCollection();
        $this->status = 0;
        $this->createdAt = new \DateTime();
        $this->deletedFromDemand = false;
        $this->deletedFromOffer = false;

        $this->user = $user;
        $this->proposal = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Reaction
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get demand
     *
     * @return \Easyb\AdvertBundle\Entity\Demand
     */
    public function getDemand()
    {
        return $this->demand;
    }

    /**
     * Set demand
     *
     * @param \Easyb\AdvertBundle\Entity\Demand $demand
     * @return Reaction
     */
    public function setDemand(\Easyb\AdvertBundle\Entity\Demand $demand = null)
    {
        $this->demand = $demand;

        return $this;
    }

    /**
     * Get offer
     *
     * @return \Easyb\AdvertBundle\Entity\Offer
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * Set offer
     *
     * @param \Easyb\AdvertBundle\Entity\Offer $offer
     * @return Reaction
     */
    public function setOffer(\Easyb\AdvertBundle\Entity\Offer $offer = null)
    {
        $this->offer = $offer;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Reaction
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Reaction
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get deletedFromOffer
     *
     * @return boolean
     */
    public function getDeletedFromOffer()
    {
        return $this->deletedFromOffer;
    }

    /**
     * Set deletedFromOffer
     *
     * @param boolean $deletedFromOffer
     * @return Reaction
     */
    public function setDeletedFromOffer($deletedFromOffer)
    {
        $this->deletedFromOffer = $deletedFromOffer;

        return $this;
    }

    /**
     * Get deletedFromDemand
     *
     * @return boolean
     */
    public function getDeletedFromDemand()
    {
        return $this->deletedFromDemand;
    }

    /**
     * Set deletedFromDemand
     *
     * @param boolean $deletedFromDemand
     * @return Reaction
     */
    public function setDeletedFromDemand($deletedFromDemand)
    {
        $this->deletedFromDemand = $deletedFromDemand;

        return $this;
    }

    /**
     * Get demand
     *
     * @return \Easyb\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set demand
     *
     * @param \Easyb\AdvertBundle\Entity\Demand $demand
     * @return Reaction
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get demand
     *
     * @return \Easyb\AdvertBundle\Entity\Demand
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set demand
     *
     * @param \Easyb\AdvertBundle\Entity\Demand $demand
     * @return Reaction
     */
    public function setJob(Job $job = null)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getProposal()
    {
        return $this->proposal;
    }

    /**
     * @param Proposal $proposal
     */
    public function addProposal($proposal)
    {
        $this->proposal[] = $proposal;
        return $this;
    }

    public function deleteProposal($proposal)
    {
        $this->proposal->removeElement($proposal);
        return $this;
    }

    /**
     * Add files
     *
     * @param \Easyb\AdvertBundle\Entity\AdvertFile $file
     *
     * @return AbstractAdvert
     */
    public function addFile(AdvertFile $file)
    {
        $file->setReaction($this);
        $this->files->add($file);

        return $this;
    }

    /**
     * Remove files
     *
     * @param \Easyb\AdvertBundle\Entity\AdvertFile $files
     */
    public function removeFile(AdvertFile $files)
    {
        $this->files->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }
}