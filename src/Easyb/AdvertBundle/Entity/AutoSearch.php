<?php

namespace Easyb\AdvertBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Easyb\UserBundle\Entity\User;
use Easyb\MainBundle\Entity\City;
use Easyb\MainBundle\Entity\Category;

/**
 * AutoSearch
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Easyb\AdvertBundle\Entity\AutoSearchRepository")
 */
class AutoSearch
{
    /**
     * Спрос
     */
    const TYPE_DEMAND = 1;
    /**
     * Предложение
     */
    const TYPE_OFFER = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="smallint")
     */
    private $type;

    /**
     * @var User $user
     *
     * @ORM\ManyToOne(targetEntity="Easyb\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @var City $city
     *
     * @ORM\ManyToOne(targetEntity="Easyb\MainBundle\Entity\City")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $city;

    /**
     * @var Category $category
     *
     * @ORM\ManyToOne(targetEntity="Easyb\MainBundle\Entity\Category")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=255)
     */
    private $text;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var boolean $isActive
     * @ORM\Column(name="is_active", type="boolean", nullable=true)
     */
    protected $isActive;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="viewing_at", type="datetime", nullable=true)
     */
    private $viewingAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->viewingAt = new \DateTime();
        $this->isActive = true;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return AutoSearch
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return AutoSearch
     */
    public function setText($text)
    {
        $this->text = $text;
    
        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return AutoSearch
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set user
     *
     * @param \Easyb\UserBundle\Entity\User $user
     * @return AutoSearch
     */
    public function setUser(\Easyb\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Easyb\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set city
     *
     * @param \Easyb\MainBundle\Entity\City $city
     * @return AutoSearch
     */
    public function setCity(\Easyb\MainBundle\Entity\City $city = null)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return \Easyb\MainBundle\Entity\City 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set category
     *
     * @param \Easyb\MainBundle\Entity\Category $category
     * @return AutoSearch
     */
    public function setCategory(\Easyb\MainBundle\Entity\Category $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \Easyb\MainBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function validateUser(User $user)
    {
        return $this->getUser() == $user ? true : false;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return AutoSearch
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set viewingAt
     *
     * @param \DateTime $viewingAt
     * @return AutoSearch
     */
    public function setViewingAt($viewingAt)
    {
        $this->viewingAt = $viewingAt;
    
        return $this;
    }

    /**
     * Get viewingAt
     *
     * @return \DateTime 
     */
    public function getViewingAt()
    {
        return $this->viewingAt;
    }
}