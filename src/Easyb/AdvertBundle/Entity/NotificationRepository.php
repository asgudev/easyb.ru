<?php

namespace Easyb\AdvertBundle\Entity;

use Easyb\AdvertBundle\Entity\AbstractAdvertRepository;

/**
 * NotificationRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class NotificationRepository extends AbstractAdvertRepository
{
}
