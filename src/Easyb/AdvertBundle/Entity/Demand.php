<?php

namespace Easyb\AdvertBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Easyb\AdvertBundle\Validator\Constraints as AdvertAssert;
use Easyb\UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * class Demand (класс для спросов)
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Easyb\AdvertBundle\Entity\DemandRepository")
 *
 * @AdvertAssert\DemandExpireAtCheck
 */
class Demand extends AbstractAdvert
{
    const REQUIRED_DOCS_ARRAY = array(
        1 => "Презентация Компании",
        2 => "Рекомендательные / Благодарственные письма",
        4 => "Устав",
        8 => "Свидетельство ИНН",
        16 => "Свидетельство ОГРН",
        32 => "Приказ о вступлении в должность генерального директора",
        64 => "Приказ о назначении главного бухгалтера",
        128 => "Выписка из ЕГРЮЛ (свежая)",
        256 => "Договор аренды офисного помещения",
        512 => "Справка об отсутствии задолженности по налогам и сборам",
        1024 => "Портфолио",
        2048 => "Соглашение о неразглашении коммерческой информации",
        4096 => "Информационное письмо о согласии принять участие в тендере",
    );

    const PAY_TYPES = array(
        1 => "Предоплата",
        2 => "Частичная предоплата",
        3 => "Постоплата",
    );

    const MAIL_DEMAND_EXTEND_OFFER = 16;
    const MAIL_DEMAND_EXTEND_WINNER = 20;
    const MAIL_DEMAND_NO_WINNER = 21;
    const MAIL_DEMAND_INVITE = 25;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expire_at", type="datetime", nullable=true)
     * @Assert\NotBlank(message="Заполните дату и время окончания приема заявок")
     */
    private $expireAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expire_proposal_at", type="datetime", nullable=true)
     * @Assert\NotBlank(message="Заполните дату и время окончания приема предложений")
     */
    private $expireProposalAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expire_sub_proposal_at", type="datetime", nullable=true)
     */
    private $expireSubProposalAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expire_decision_at", type="datetime", nullable=true)
     * @Assert\NotBlank(message="Заполните дату и время принятия решения")
     */
    private $expireDecisionAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="is_extended_offer", type="boolean", nullable=false, options={"default" = false})
     */
    private $isExtendedOffer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="is_extended_sub_offer", type="boolean", nullable=false, options={"default" = false})
     */
    private $isExtendedSubOffer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="is_extended_winner", type="boolean", nullable=false, options={"default" = false})
     */
    private $isExtendedWinner;

    /**
     * @ORM\OneToMany(targetEntity="Easyb\AdvertBundle\Entity\Reaction", mappedBy="demand")
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    private $reactions;

    /**
     * @var integer
     *
     * @ORM\Column(name="required_docs", type="integer", options={"default" = 0})
     */
    private $requiredDocs;

    /**
     * @var Reaction
     * @ORM\OneToOne(targetEntity="Easyb\AdvertBundle\Entity\Proposal")
     */
    private $winner;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="is_finished", type="boolean", nullable=false, options={"default" = false})
     */
    private $isFinished;

    /**
     * @ORM\OneToMany(targetEntity="Easyb\AdvertBundle\Entity\Question", mappedBy="demand")
     */
    private $questions;

    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="text", nullable=false, options={"default" = 0})
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="win_cryteria", type="text", nullable=true)
     */
    private $winCryteria;

    /**
     * @ORM\OneToMany(targetEntity="Easyb\AdvertBundle\Entity\AdvertFile", mappedBy="demand", cascade={"persist"}, orphanRemoval=true)
     */
    private $inviteFile;

    /**
     * @ORM\Column(name="curators", type="array", nullable=true)
     */
    private $curators;

    /**
     * @ORM\Column(name="is_public", type="boolean", nullable=false, options={"default" = false})
     */
    private $isPublic;

    /**
     * @ORM\Column(name="private_hash", type="text", nullable=true)
     */
    private $private_hash;

    /**
     * @ORM\Column(name="invites", type="array", nullable=true)
     */
    private $invites;

    /**
     * @ORM\Column(name="pay_type", type="integer", nullable=true)
     */
    private $payType;

    /**
     * @param User|null $user
     */
    public function __construct(User $user = null)
    {
        parent::__construct($user);
        $this->inviteFile = new ArrayCollection();
        $this->requiredDocs = 0;
        $this->isExtendedOffer = false;
        $this->isExtendedSubOffer = false;
        $this->isExtendedWinner = false;
        $this->isFinished = false;
        $this->isPublic = true;
    }

    /**
     * @return \DateTime
     */
    public function getExpireAt()
    {
        return $this->expireAt;
    }

    /**
     * @param \DateTime $expireAt
     *
     * @return Demand
     */
    public function setExpireAt($expireAt)
    {
        $this->expireAt = $expireAt;

        return $this;
    }

    /**
     * Add reactions
     *
     * @param \Easyb\AdvertBundle\Entity\Reaction $reactions
     * @return Demand
     */
    public function addReaction(\Easyb\AdvertBundle\Entity\Reaction $reactions)
    {
        $this->reactions[] = $reactions;

        return $this;
    }

    /**
     * Remove reactions
     *
     * @param \Easyb\AdvertBundle\Entity\Reaction $reactions
     */
    public function removeReaction(\Easyb\AdvertBundle\Entity\Reaction $reactions)
    {
        $this->reactions->removeElement($reactions);
    }

    /**
     * Get reactions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReactions()
    {
        return $this->reactions;
    }

    /**
     * Add reactions
     *
     * @param \Easyb\AdvertBundle\Entity\Question $question
     * @return Question[]
     */
    public function addQuestion(\Easyb\AdvertBundle\Entity\Question $question)
    {
        $this->questions[] = $question;

        return $this;
    }

    /**
     * Remove reactions
     *
     * @param \Easyb\AdvertBundle\Entity\Reaction $reactions
     */
    public function removeQuestion(\Easyb\AdvertBundle\Entity\Question $question)
    {
        $this->questions->removeElement($question);
    }

    /**
     * Get reactions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * @return int
     */
    public function getRequiredDocs()
    {
        return $this->requiredDocs;
    }

    /**
     * @param int $requiredDocs
     * @return Demand
     */
    public function setRequiredDocs($requiredDocs)
    {
        $this->requiredDocs = $requiredDocs;
        return $this;
    }


    /**
     * @return \DateTime
     */
    public function getExpireProposalAt()
    {
        return $this->expireProposalAt;
    }

    /**
     * @param \DateTime $expireOfferAt
     *
     * @return Demand
     */
    public function setExpireProposalAt($expireProposalAt)
    {
        $this->expireProposalAt = $expireProposalAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExpireDecisionAt()
    {
        return $this->expireDecisionAt;
    }

    /**
     * @param \DateTime $expireDecisionAt
     */
    public function setExpireDecisionAt($expireDecisionAt)
    {
        $this->expireDecisionAt = $expireDecisionAt;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isExtendedOffer()
    {
        return $this->isExtendedOffer;
    }

    /**
     * @param boolean $isExtendedOffer
     * @return Demand
     */
    public function setIsExtendedOffer($isExtendedOffer)
    {
        $this->isExtendedOffer = $isExtendedOffer;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isExtendedSubOffer()
    {
        return $this->isExtendedSubOffer;
    }

    /**
     * @param boolean $isExtendedSubOffer
     * @return Demand
     */
    public function setIsExtendedSubOffer($isExtendedSubOffer)
    {
        $this->isExtendedSubOffer = $isExtendedSubOffer;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isExtendedWinner()
    {
        return $this->isExtendedWinner;
    }

    /**
     * @param boolean $isExtendedWinner
     * @return Demand
     */
    public function setIsExtendedWinner($isExtendedWinner)
    {
        $this->isExtendedWinner = $isExtendedWinner;
        return $this;
    }

    /**
     * @return Proposal
     */
    public function getWinner()
    {
        return $this->winner;
    }

    /**
     * @param Proposal $winner
     */
    public function setWinner($winner)
    {
        $this->winner = $winner;
        return $this;
    }


    /**
     * @return boolean
     */
    public function isFinished()
    {
        return $this->isFinished;
    }

    /**
     * @param boolean $isFinished
     */
    public function setIsFinished($isFinished)
    {
        $this->isFinished = $isFinished;
        return $this;
    }

    /**
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param integer
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return string
     */
    public function getWinCryteria()
    {
        return $this->winCryteria;
    }

    /**
     * @param string $winCryteria
     */
    public function setWinCryteria($winCryteria)
    {
        $this->winCryteria = $winCryteria;
        return $this;
    }

    /**
     * Add files
     *
     * @param \Easyb\AdvertBundle\Entity\AdvertFile $file
     *
     * @return AbstractAdvert
     */
    public function addInviteFile(AdvertFile $file)
    {
        $file->setDemand($this);
        $this->inviteFile->add($file);

        return $this;
    }

    /**
     * Remove files
     *
     * @param \Easyb\AdvertBundle\Entity\AdvertFile $files
     */
    public function removeInviteFile(AdvertFile $files)
    {
        $this->inviteFile->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInviteFile()
    {
        return $this->inviteFile;
    }

    /**
     * @return mixed
     */
    public function getCurators()
    {
        return $this->curators;
    }

    /**
     * @param mixed $curators
     */
    public function setCurators($curators)
    {
        $this->curators = $curators;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * @param mixed $private
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getPrivateHash()
    {
        return $this->private_hash;
    }

    /**
     * @param mixed $private_hash
     */
    public function setPrivateHash($private_hash)
    {
        $this->private_hash = $private_hash;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getInvites()
    {
        return  $this->invites;
    }

    /**
     * @param mixed $invites
     * @return Demand
     */
    public function setInvites($invites)
    {
        $this->invites = $invites;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getPayType()
    {
        return $this->payType;
    }

    /**
     * @param mixed $payType
     */
    public function setPayType($payType)
    {
        $this->payType = $payType;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExpireSubProposalAt()
    {
        return $this->expireSubProposalAt;
    }

    /**
     * @param \DateTime $expireSubProposalAt
     */
    public function setExpireSubProposalAt($expireSubProposalAt)
    {
        $this->expireSubProposalAt = $expireSubProposalAt;
        return $this;
    }

    public function getRequiredDocsArray(){
        $array = $this->bit_analysis($this->requiredDocs);
        $s_array = self::REQUIRED_DOCS_ARRAY;
        $r_array = [];
        foreach ($array as $doc){
            $r_array[] = $s_array[$doc];
        }

        return $r_array;
    }

    private function bit_analysis($n)
    {
        $bin_powers = array();
        for ($bit = 0; $bit < count(self::REQUIRED_DOCS_ARRAY); $bit++) {
            $bin_power = 1 << $bit;
            if ($bin_power & $n) $bin_powers[$bit] = $bin_power;
        }
        return $bin_powers;
    }
}