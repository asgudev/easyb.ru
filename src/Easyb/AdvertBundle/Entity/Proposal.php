<?php
/**
 * Created by IntelliJ IDEA.
 * User: User
 * Date: 31.01.2016
 * Time: 15:24
 */

namespace Easyb\AdvertBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Proposal
 * @ORM\Entity()
 * @ORM\Table(name="proposals")
 */
class Proposal
{

    /**
     * Победитель в тендере
     */
    const STATUS_WINNER = 1;

    /**
     * Письмо Победитель в тендере
     */
    const MAIL_STATUS_WINNER = 17;

    /**
     * Письмо автоматический Победитель в тендере
     */
    const MAIL_STATUS_AUTO_WINNER = 26;

    /**
     * Проигравший в тендере
     */
    const STATUS_LOSER = 2;

    /**
     * Письмо Проигравший в тендере
     */
    const MAIL_STATUS_LOSER = 18;

    /**
     * Письмо организатору
     */
    const MAIL_STATUS_CHOOSE = 19;

    /**
     * @var $id
     *
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Reaction $reaction
     *
     * @ORM\ManyToOne(targetEntity="Easyb\AdvertBundle\Entity\Reaction", inversedBy="proposal")
     * @ORM\JoinColumn(name="reaction_id", referencedColumnName="id")
     */
    private $reaction;

    /**
     * @var integer
     * @ORM\Column(name="price", type="text")
     */
    private $price;

    /**
     * @var ArrayCollection $files
     * @ORM\OneToMany(targetEntity="Easyb\AdvertBundle\Entity\AdvertFile", mappedBy="proposal", cascade={"persist"}, orphanRemoval=true)
     */
    private $files;

    /**
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text", nullable=true)
     * @Assert\NotBlank()
     */
    private $message;

    public function __construct(Reaction $reaction)
    {
        $this->files = new ArrayCollection();
        $this->reaction = $reaction;
        $this->createdAt = new \DateTime();
        $this->status = 0;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Proposal
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Reaction
     */
    public function getReaction()
    {
        return $this->reaction;
    }

    /**
     * @param Reaction $reaction
     */
    public function setReaction($reaction)
    {
        $this->reaction = $reaction;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Reaction
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Proposal
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Add files
     *
     * @param \Easyb\AdvertBundle\Entity\AdvertFile $file
     *
     * @return Proposal
     */
    public function addFile(AdvertFile $file)
    {
        $file->setProposal($this);
        $this->files->add($file);

        return $this;
    }

    /**
     * Remove files
     *
     * @param \Easyb\AdvertBundle\Entity\AdvertFile $files
     */
    public function removeFile(AdvertFile $files)
    {
        $this->files->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }


    private $job;

    public function setJob($job)
    {
        $this->job = $job;
        return $this;
    }

    public function getJob()
    {
        return $this->job;
    }
}