<?php

namespace Easyb\AdvertBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Easyb\UserBundle\Entity\User;
use Easyb\AdvertBundle\Entity\AbstractAdvert;

/**
 * Notification
 *
 * @ORM\Table(name="notifications")
 * @ORM\Entity(repositoryClass="Easyb\AdvertBundle\Entity\NotificationRepository")
 */
class Notification
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Easyb\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Easyb\AdvertBundle\Entity\AbstractAdvert")
     * @ORM\JoinColumn(name="advert_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $advert;

    /**
     *
     */
    public function __construct(User $user, AbstractAdvert $advert)
    {
        $this->user = $user;
        $this->advert = $advert;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \Easyb\UserBundle\Entity\User $user
     * @return Notification
     */
    public function setUser(\Easyb\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Easyb\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set advert
     *
     * @param \Easyb\AdvertBundle\Entity\AbstractAdvert $advert
     * @return Notification
     */
    public function setAdvert(\Easyb\AdvertBundle\Entity\AbstractAdvert $advert = null)
    {
        $this->advert = $advert;
    
        return $this;
    }

    /**
     * Get advert
     *
     * @return \Easyb\AdvertBundle\Entity\AbstractAdvert 
     */
    public function getAdvert()
    {
        return $this->advert;
    }
}