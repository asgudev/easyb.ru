<?php

namespace Easyb\AdvertBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Easyb\AdvertBundle\Validator\Constraints as AdvertAssert;
use Easyb\MainBundle\Entity\Category;
use Easyb\MainBundle\Entity\Slugable\SlugableInterface;
use Easyb\MainBundle\Entity\Slugable\SlugableTrait;
use Easyb\UserBundle\Entity\Job;
use Easyb\UserBundle\Entity\User;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AbstractAdvert
 *
 * @ORM\Table(name="abstract_advert")
 * @ORM\Entity
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\Entity(repositoryClass="Easyb\AdvertBundle\Entity\AbstractAdvertRepository")
 *
 * @AdvertAssert\AbstractTextLengthCheck
 */
abstract class AbstractAdvert implements SlugableInterface
{
    use SlugableTrait;

    const DEMAND = 1;
    const OFFER = 2;

    const DEMAND_STRING = 'demand';
    const OFFER_STRING = 'offer';

    /**
     * стартовое количество своих объявлений на странице
     */
    const COUNT_ON_LIST_PAGE = 50;
    /**
     * @var ArrayCollection $files
     * @ORM\OneToMany(targetEntity="Easyb\AdvertBundle\Entity\AdvertFile", mappedBy="advert", cascade={"persist"}, orphanRemoval=true)
     */
    protected $files;
    /**
     * @var array $submittedAdverts
     * @ORM\Column(name="submitted_adverts", type="array", nullable=true)
     */
    protected $submittedAdverts;
    /**
     * @var array $submittedEmails
     * @ORM\Column(name="submitted_emails", type="array", nullable=true)
     */
    protected $submittedEmails;

    /**
     * @var array $invalidRules
     * @ORM\Column(name="invalid_rules", type="array", nullable=true)
     */
    protected $invalidRules;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(message="Введите заголовок объявления")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     * @Assert\NotBlank(message="Заполните текст сообщения")
     */
    private $text;

    /**
     * @var Job $job
     *
     * @ORM\ManyToOne(targetEntity="Easyb\UserBundle\Entity\Job")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id", onDelete="CASCADE")
     * @Assert\NotBlank(message="Выберите юридическое лицо")
     */
    private $job;

    /**
     * @var Category $category
     *
     * @ORM\ManyToOne(targetEntity="Easyb\MainBundle\Entity\Category")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE")
     * @Assert\NotBlank(message="Выберите категорию")
     */
    private $category;

    /**
     * @var Category $category
     *
     * @ORM\ManyToMany(targetEntity="Easyb\MainBundle\Entity\Category")
     * @Assert\NotBlank(message="Выберите категорию")
     */
    private $subcategory;

    /**
     * @var User $user
     *
     * @ORM\ManyToOne(targetEntity="Easyb\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publication_at", type="datetime", nullable=true)
     */
    private $publicationAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=true)
     */
    private $isActive;

    /**
     * @var integer
     * @ORM\Column(name="views", type="integer", nullable=true)
     */
    private $views;
    /**
     * @var boolean
     *
     * @ORM\Column(name="from_admin", type="boolean", nullable=true)
     */
    private $fromAdmin;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="text", nullable=true)
     */
    private $metaDescription;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_approved", type="boolean", nullable=true)
     */
    private $isApproved;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_improved", type="boolean", nullable=true)
     */
    private $isImproved;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     * @Assert\Length(
     *      min=5,
     *      max=2000
     * )
     */
    private $notes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="improved_at", type="datetime", nullable=true)
     */
    private $improvedAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=true)
     */
    private $isDeleted;




    /**
     * @param User $user
     */
    public function __construct(User $user = null)
    {
        $this->user = $user;
        $this->files = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->publicationAt = new \DateTime();
        $this->isActive = true;
        $this->views = 0;
        $this->submittedAdverts = array();
        $this->submittedEmails = array();
        $this->invalidRules = array();
        $this->fromAdmin = false;
        $this->isApproved = false;
        $this->isImproved = false;
        $this->isDeleted = false;
        $this->subcategory = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set name
     *
     * @param string $name
     * @return AbstractAdvert
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return AbstractAdvert
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param string $notes
     *
     * @return $this
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get job
     *
     * @return Job
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set job
     *
     * @param Job $job
     * @return Offer
     */
    public function setJob(Job $job = null)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get category
     *
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set category
     *
     * @param Category $category
     * @return AbstractAdvert
     */
    public function setCategory(Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Add files
     *
     * @param \Easyb\AdvertBundle\Entity\AdvertFile $file
     *
     * @return AbstractAdvert
     */
    public function addFile(AdvertFile $file)
    {
        $file->setAdvert($this);
        $this->files->add($file);

        return $this;
    }

    /**
     * Remove files
     *
     * @param \Easyb\AdvertBundle\Entity\AdvertFile $files
     */
    public function removeFile(AdvertFile $files)
    {
        $this->files->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return AbstractAdvert
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name ?: 'n/a';
    }

    /**
     * @return \DateTime
     */
    public function getImprovedAt()
    {
        return $this->improvedAt;
    }

    /**
     * @param \DateTime $improvedAt
     * @return AbstractAdvert
     */
    public function setImprovedAt($improvedAt = null)
    {
        $this->improvedAt = $improvedAt;

        return $this;
    }

    /**
     * Можно ли апать объявления
     * @return mixed
     */
    public function checkUpTime()
    {
        $now = new \DateTime();

        return ($now > $this->getPublicationAt()->modify('+1 hour'));
    }

    /**
     * Get publicationAt
     *
     * @return \DateTime
     */
    public function getPublicationAt()
    {
        return $this->publicationAt;
    }

    /**
     * Set publicationAt
     *
     * @param \DateTime $publicationAt
     * @return AbstractAdvert
     */
    public function setPublicationAt($publicationAt)
    {
        $this->publicationAt = $publicationAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function whenUpTime()
    {
        $start = new \DateTime();
        $end = $this->getPublicationAt()->modify('+1 hour');
        $diff = $start->diff($end);
        $result = $diff->format('%i минут %s секунд');

        return $result == '0 минут 0 секунд' ? '1 час' : $result;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return AbstractAdvert
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param boolean $isDeleted
     * @return AbstractAdvert
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
        return $this;
    }

    /**
     * Get views
     *
     * @return integer
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Set views
     *
     * @param integer $views
     * @return AbstractAdvert
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function validateUser(User $user)
    {
        return $this->getUser() == $user ? true : false;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return AbstractAdvert
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @param string $submittedAdverts
     *
     * @return bool
     */
    public function hasSubmittedAdvert($submittedAdverts)
    {
        return in_array(strtoupper($submittedAdverts), $this->getSubmittedAdverts(), true);
    }

    /**
     * Get submittedAdverts
     *
     * @return array
     */
    public function getSubmittedAdverts()
    {
        return $this->submittedAdverts;
    }

    /**
     * Set submittedAdverts
     *
     * @param array $submittedAdverts
     * @return AbstractAdvert
     */
    public function setSubmittedAdverts($submittedAdverts)
    {
        $this->submittedAdverts = $submittedAdverts;

        return $this;
    }

    /**
     * @param string $submittedEmails
     *
     * @return bool
     */
    public function hasSubmittedEmail($submittedEmails)
    {
        return in_array(strtoupper($submittedEmails), $this->getSubmittedEmails(), true);
    }

    /**
     * @return array
     */
    public function getSubmittedEmails()
    {
        return $this->submittedEmails;
    }

    /**
     * @param array $submittedEmails
     * @return AbstractAdvert
     */
    public function setSubmittedEmails($submittedEmails)
    {
        $this->submittedEmails = $submittedEmails;

        return $this;
    }

    /**
     * @param string $invalidRules
     *
     * @return bool
     */
    public function hasInvalidRule($invalidRules)
    {
        return in_array(strtoupper($invalidRules), $this->getInvalidRules(), true);
    }

    /**
     * @return array
     */
    public function getInvalidRules()
    {
        return $this->invalidRules;
    }

    /**
     * @param array $invalidRules
     * @return AbstractAdvert
     */
    public function setInvalidRules($invalidRules)
    {
        $this->invalidRules = $invalidRules;

        return $this;
    }

    /**
     * @param string $submittedAdverts
     *
     * @return $this
     */
    public function addSubmittedAdvert($submittedAdverts)
    {
        $submittedAdverts = strtoupper($submittedAdverts);
        if (!in_array($submittedAdverts, $this->submittedAdverts, true)) {
            $this->submittedAdverts[] = $submittedAdverts;
        }

        return $this;
    }

    /**
     * @param string $submittedEmails
     *
     * @return $this
     */
    public function addSubmittedEmail($submittedEmails)
    {
        $submittedEmails = strtoupper($submittedEmails);
        if (!in_array($submittedEmails, $this->submittedEmails, true)) {
            $this->submittedEmails[] = $submittedEmails;
        }

        return $this;
    }

    /**
     * @param string $invalidRules
     *
     * @return $this
     */
    public function addInvalidRule($invalidRules)
    {
        $invalidRules = strtoupper($invalidRules);
        if (!in_array($invalidRules, $this->invalidRules, true)) {
            $this->invalidRules[] = $invalidRules;
        }

        return $this;
    }

    /**
     * @param $submittedAdverts
     *
     * @return $this
     */
    public function removeSubmittedAdvert($submittedAdverts)
    {
        if (false !== $key = array_search(strtoupper($submittedAdverts), $this->submittedAdverts, true)) {
            unset($this->submittedAdverts[$key]);
            $this->submittedAdverts = array_values($this->submittedAdverts);
        }

        return $this;
    }

    /**
     * @param $submittedEmails
     *
     * @return $this
     */
    public function removeSubmittedEmail($submittedEmails)
    {
        if (false !== $key = array_search(strtoupper($submittedEmails), $this->submittedEmails, true)) {
            unset($this->submittedEmails[$key]);
            $this->submittedEmails = array_values($this->submittedEmails);
        }

        return $this;
    }

    /**
     * @param $invalidRules
     *
     * @return $this
     */
    public function removeInvalidRule($invalidRules)
    {
        if (false !== $key = array_search(strtoupper($invalidRules), $this->invalidRules, true)) {
            unset($this->invalidRules[$key]);
            $this->invalidRules = array_values($this->invalidRules);
        }

        return $this;
    }

    /**
     * Get fromAdmin
     *
     * @return boolean
     */
    public function getFromAdmin()
    {
        return $this->fromAdmin;
    }

    /**
     * Set fromAdmin
     *
     * @param boolean $fromAdmin
     * @return AbstractAdvert
     */
    public function setFromAdmin($fromAdmin)
    {
        $this->fromAdmin = $fromAdmin;

        return $this;
    }

    /**
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * @param string $metaDescription
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @return boolean
     */
    public function isApproved()
    {
        return $this->isApproved;
    }

    /**
     * @return boolean
     */
    public function isImproved()
    {
        return $this->isImproved;
    }

    public function isOffer()
    {
        return $this instanceof Offer;
    }

    /**
     * Get isApproved
     *
     * @return boolean
     */
    public function getIsApproved()
    {
        return $this->isApproved;
    }

    /**
     * @param boolean $isApproved
     */
    public function setIsApproved($isApproved)
    {
        $this->isApproved = $isApproved;
    }

    /**
     * Get isImproved
     *
     * @return boolean
     */
    public function getIsImproved()
    {
        return $this->isImproved;
    }

    /**
     * @param boolean $isImproved
     */
    public function setIsImproved($isImproved)
    {
        $this->isImproved = $isImproved;
    }

    public function getSubcategory()
    {
        return $this->subcategory;
    }

    public function addSubcategory($subcategory)
    {
        $this->subcategory[] = $subcategory;
        return $this;
    }

    public function removeSubcategory($subcategory)
    {
        $this->subcategory->removeElement($subcategory);
        return $this;
    }
}