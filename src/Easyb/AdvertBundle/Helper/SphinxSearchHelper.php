<?php
namespace Easyb\AdvertBundle\Helper;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Easyb\AdvertBundle\Entity\AutoSearch;
use EM\SphinxSearchBundle\Services\Search\Sphinxsearch;

/**
 * helper SphinxSearchHelper
 */
class SphinxSearchHelper
{
    /**
     * @var Sphinxsearch
     */
    protected $sphinxSearch;

    /**
     * @param Sphinxsearch $sphinxSearch
     */
    public function __construct(Sphinxsearch $sphinxSearch)
    {
        $this->sphinxSearch = $sphinxSearch;
    }

    /**
     * @param AutoSearch $search
     * @param bool       $newResults
     *
     * @return array
     */
    public function getSearchResults(AutoSearch $search, $newResults = false)
    {
        return $this->getSphinxSearch($search, $newResults)->search('@name ' . $search->getText(), $this->getIndexToSearch($search->getType()), [], false);
    }

    /**
     * @param integer $type
     *
     * @return string
     */
    private function getIndexToSearch($type)
    {
        if ($type == AutoSearch::TYPE_DEMAND) {
            return ['Demand'];
        }

        return ['Offer'];
    }

    /**
     * @param AutoSearch $search
     * @param bool       $new
     *
     * @return \EM\SphinxSearchBundle\Services\Search\Sphinxsearch
     */
    private function getSphinxSearch(AutoSearch $search, $new = false)
    {
        $sphinxSearch = $this->sphinxSearch;
        $sphinxSearch->resetFilters();
        $sphinxSearch->setMatchMode(SPH_MATCH_EXTENDED);
        if ($search->getCategory()) {
            $sphinxSearch->setFilter('category', [$search->getCategory()->getId()]);
        }
        if ($search->getCity()) {
            $sphinxSearch->setFilter('city', [$search->getCity()->getId()]);
        }
        if ($new) {
            $sphinxSearch->setFilterRange('date_public', (int)($search->getViewingAt()->format('U')), time());
        }

        return $sphinxSearch;
    }
}
