<?php
/**
 * Created by IntelliJ IDEA.
 * User: User
 * Date: 16.02.2016
 * Time: 19:26
 */

namespace Easyb\AdvertBundle\Events;

use Easyb\AdvertBundle\Entity\Question;
use Symfony\Component\EventDispatcher\Event;

class QuestionEvent extends Event
{
    /**
     * задан вопрос
     */
    const QUESTION_CREATED = 'question.created';

    /**
     * ответ на вопрос
     */
    const QUESTION_ANSWERED = 'question.answered';

    /**
     * @var \Easyb\AdvertBundle\Entity\Question
     */
    private $question;

    /**
     * @param \Easyb\AdvertBundle\Entity\Offer $offer
     */
    public function __construct(Question $question)
    {
        $this->question = $question;
    }

    /**
     * @return mixed
     */
    public function getQuestion()
    {
        return $this->question;
    }
}