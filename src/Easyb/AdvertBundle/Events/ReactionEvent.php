<?php
/**
 * Created by JetBrains PhpStorm.
 * User: scorp
 * Date: 04.06.13
 * Time: 13:34
 * To change this template use File | Settings | File Templates.
 */
namespace Easyb\AdvertBundle\Events;

use Easyb\AdvertBundle\Entity\Reaction;
use Easyb\UserBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

/**
 *  event class for Reaction
 */
class ReactionEvent extends Event
{
    /**
     * Одобрить предложение
     */
    const FOR_OFFER  = 1;
    /**
     * Отправить предложение
     */
    const FOR_DEMAND = 2;

    /**
     * удаление
     */
    const DELETE = 'reaction.delete';
    /**
     * создание
     */
    const CREATE = 'reaction.create';
    /**
     * Предложение было просмотрено
     */
    const HAS_BEEN_VIEWED = 'reaction.has_been_viewed';
    /**
     * предложение было одобрено или отклонено
     */
    const APPROVE_OR_DECLINE = 'reaction.approve_or_decline';
    /**
     * Одобренно предложение было просмотрено в "мое предложение"
     */
    const SET_VIEWED_STATUS_TO_APPROVED_REACTION = 'reaction.set_viewed_status_to_approved_reaction';
    /**
     * Одобренно предложение было просмотрено в "мое предложение"
     */
    const SUBMIT_ADMIN_ADVERT = 'reaction.submit_admin_advert';
    /**
     * предложение было отправлено на доработку
     */
    const SUBMIT_ADMIN_OFFER_IMPROVE = 'reaction.submit_admin_offer_improve';
    /**
     * тендер был отправлен на доработку
     */
    const SUBMIT_ADMIN_DEMAND_IMPROVE = 'reaction.submit_admin_demand_improve';
    /**
     * предложение было одобрено
     */
    const SUBMIT_ADMIN_OFFER = 'reaction.submit_admin_offer';

    /**
     * продлить сбор кп
     */
    const EXTEND_OFFER = 'reaction.extend_offer';

    /**
     * продлить выбор победителя
     */
    const EXTEND_WINNER = 'reaction.extend_winner';

    /**
     * тендер не состоялся
     */
    const NO_WINNER = 'reaction.no_winner';

    /**
     * не отправлено КП
     */
    const NO_PROPOSAL = 'reaction.no_proposal';

    /**
     * @var Reaction
     */
    private $reaction;

    /**
     * @var integer
     */
    private $type;

    /**
     * @var User
     */
    private $user;

    /**
     * @var array
     */
    private $formData;

    /**
     * @param Reaction $reaction
     * @param null     $type
     */
    public function __construct(Reaction $reaction = null, $type = null)
    {
        $this->reaction = $reaction;
        $this->type = $type;
    }

    /**
     * @return Reaction
     */
    public function getReaction()
    {
        return $this->reaction;
    }

    /**
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return array|object
     */
    public function getFormData()
    {
        return $this->formData;
    }

    /**
     * @param array|object $data
     */
    public function setFormData($data)
    {
        $this->formData = $data;
    }
}