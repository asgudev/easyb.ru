<?php
namespace Easyb\AdvertBundle\Events;

use Easyb\AdvertBundle\Entity\Offer;
use Symfony\Component\EventDispatcher\Event;

/**
 *  event class for Offer
 */
class OfferEvent extends Event
{
    /**
     * смена статуса
     */
    const CHANGE_ACTIVE = 'offer.active';
    /**
     * удаление
     */
    const DELETE = 'offer.delete';

    /**
     * @var \Easyb\AdvertBundle\Entity\Offer
     */
    private $offer;

    /**
     * @param \Easyb\AdvertBundle\Entity\Offer $offer
     */
    public function __construct(Offer $offer)
    {
        $this->offer = $offer;
    }

    /**
     * @return mixed
     */
    public function getOffer()
    {
        return $this->offer;
    }
}