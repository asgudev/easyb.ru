<?php
/**
 * Created by IntelliJ IDEA.
 * User: User
 * Date: 31.01.2016
 * Time: 17:04
 */

namespace Easyb\AdvertBundle\Events;


use Easyb\AdvertBundle\Entity\Proposal;
use Symfony\Component\EventDispatcher\Event;

class ProposalEvent extends Event
{

    /**
     *  КП отправлено
     */
    const PROPOSAL_SEND = 'proposal.send';

    /**
     *  КП победитель
     */
    const PROPOSAL_WINNER = 'proposal.winner';

    /**
     *  КП проигравший
     */
    const PROPOSAL_LOSER = 'proposal.loser';

    /**
     * @var Proposal
     */
    private $proposal;

    /**
     * @var string
     */
    private $type;

    private $auto;

    /**
     * ProposalEvent constructor.
     * @param Proposal $proposal
     */
    public function __construct(Proposal $proposal, $auto = false)
    {
        $this->proposal = $proposal;
        $this->auto = $auto;
    }

    /**
     * @return Proposal
     */
    public function getProposal()
    {
        return $this->proposal;
    }

    /**
     * @param Proposal $proposal
     */
    public function setProposal($proposal)
    {
        $this->proposal = $proposal;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function isAuto()
    {
        return $this->auto;
    }
}