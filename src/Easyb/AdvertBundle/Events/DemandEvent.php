<?php
/**
 * Created by JetBrains PhpStorm.
 * User: scorp
 * Date: 30.05.13
 * Time: 20:27
 * To change this template use File | Settings | File Templates.
 */
namespace Easyb\AdvertBundle\Events;

use Easyb\AdvertBundle\Entity\Demand;
use Symfony\Component\EventDispatcher\Event;

/**
 *  event class for Demand
 */
class DemandEvent extends Event
{
    /**
     * смена статуса
     */
    const CHANGE_ACTIVE = 'demand.active';
    /**
     * удаление
     */
    const DELETE = 'demand.delete';

    /**
     * тендер не состоялся
     */
    const NO_WINNER = 'demand.no_winner';

    /**
     * продлить сбор кп
     */
    const EXTEND_OFFER = 'demand.extend_offer';

    /**
     * продлить выбор победителя
     */
    const EXTEND_WINNER = 'demand.extend_winner';

    /**
     * апрув тендера администратором
     */
    const ADMIN_DEMAND_APPROVE = 'demand.admin_approve';

    /**
     * @var \Easyb\AdvertBundle\Entity\Demand
     */
    private $demand;

    private $delete;

    /**
     * @param \Easyb\AdvertBundle\Entity\Demand $demand
     * @param boolean $delete
     */
    public function __construct(Demand $demand, $delete = true)
    {
        $this->demand = $demand;
        $this->delete = $delete;
    }

    /**
     * @return mixed
     */
    public function getDemand()
    {
        return $this->demand;
    }

    /**
     * @return boolean
     */
    public function isDelete()
    {
        return $this->delete;
    }
}