<?php
namespace Easyb\AdvertBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Easyb\AdvertBundle\Entity\Offer;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadOfferData implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        return false;
        $text = 'Ночная фотосъемка подразумевает малое количество света и большие выдержки, зачастую более 30 секунд. Понятно что снимая «с руки» удержать камеру абсолютно неподвижно в течении этого времени – нереально.';

        $users = $this->container
            ->get('doctrine')
            ->getRepository('EasybUserBundle:User')
            ->findAll();

        $job = $this->container
            ->get('doctrine')
            ->getRepository('EasybUserBundle:Job')
            ->findAll();

        $category = $this->container
            ->get('doctrine')
            ->getRepository('EasybMainBundle:Category')
            ->findAll();

        for ($i = 0; $i < 100; $i++) {
            $offer = new Offer();
            $offer->setUser($users[mt_rand(0, count($users) - 1)]);
            $offer->setJob($job[mt_rand(0, count($job) - 1)]);
            $offer->setCategory($category[mt_rand(0, count($category) - 1)]);
            $offer->setName('Объявление № ' . $i);
            $offer->setText($text);
            $offer->setCreatedAt(new \DateTime());
            $offer->setPublicationAt(new \DateTime());
            $offer->setIsActive(fmod($i,2)==0 ? true : false);
            $offer->setViews($i);

            $manager->persist($offer);
        }
        $manager->flush();

    }

    public function getOrder()
    {
        return 7;
    }
}