<?php
namespace Easyb\AdvertBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Easyb\AdvertBundle\Entity\Demand;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadDemandData implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        return false;
        $text = 'Ночная фотосъемка подразумевает малое количество света и большие выдержки, зачастую более 30 секунд. Понятно что снимая «с руки» удержать камеру абсолютно неподвижно в течении этого времени – нереально.';

        $users = $this->container
            ->get('doctrine')
            ->getRepository('EasybUserBundle:User')
            ->findAll();

        $category = $this->container
            ->get('doctrine')
            ->getRepository('EasybMainBundle:Category')
            ->findAll();

        $admin = $this->container
            ->get('doctrine')
            ->getRepository('EasybUserBundle:User')
            ->findOneByUsername('admin');

        $this->setData($manager, $text, $users, $category);
        $this->setData($manager, $text, $users, $category, $admin);

    }

    private function setData($manager, $text, $users, $category, $admin = false)
    {
        for ($i = 0; $i < 100; $i++) {
            $demand = new Demand();
            $admin ? $demand->setUser($admin) : $demand->setUser($users[mt_rand(0, count($users) - 1)]);
            $demand->setJob($admin ? $admin->getJobs()[0] : $users[mt_rand(0, count($users) - 1)]->getJobs()[0]);
            $demand->setCategory($category[mt_rand(0, count($category) - 1)]);
            $demand->setName('Объявление № ' . $i);
            $demand->setText($text);
            $demand->setCreatedAt(new \DateTime());
            $demand->setPublicationAt(new \DateTime());
            $demand->setIsActive(fmod($i,2)==0 ? true : false);
            $demand->setViews($i);

            $manager->persist($demand);
        }
        $manager->flush();
    }

    public function getOrder()
    {
        return 6;
    }
}