<?php
/**
 * User: scorp
 * Date: 14.06.13
 * Time: 18:46
 */
namespace Easyb\UserBundle\EventListener;

use Easyb\UserBundle\Entity\User;
use Easyb\AdvertBundle\Events\ReactionEvent;
use Easyb\AdvertBundle\Manager\ReactionManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Easyb\AdvertBundle\Manager\NotificationManager;
use FOS\UserBundle\Model\UserManagerInterface as UserManagerInterface;

class UserListener implements EventSubscriberInterface
{
    /**
     * @var UserManagerInterface
     */
    protected $fosUserManager;
    /**
     * @var ReactionManagerInterface
     */
    protected $reactionManager;

    /**
     * @param UserManagerInterface     $fosUserManager
     * @param ReactionManagerInterface $reactionManager
     */
    public function __construct(UserManagerInterface $fosUserManager, ReactionManagerInterface $reactionManager)
    {
        $this->fosUserManager = $fosUserManager;
        $this->reactionManager = $reactionManager;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            ReactionEvent::DELETE                                 => 'actualizationNotofication',
            ReactionEvent::SET_VIEWED_STATUS_TO_APPROVED_REACTION => 'setOfferNotification'
        ];
    }

    /**
     * Актуализации уведомлений, после удаления батчем откликов
     *
     * @param ReactionEvent $event
     */
    public function actualizationNotofication(ReactionEvent $event)
    {
        $type = $event->getType();
        if ($type == ReactionEvent::FOR_DEMAND) {
            $this->actualizationDemandNotification($event->getUser());
        } elseif ($type == ReactionEvent::FOR_OFFER) {
            $this->actualizationOfferNotification($event->getUser());
        }
    }

    /**
     * Установка уведомлений для "мое предложение"
     *
     * @param ReactionEvent $event
     */
    public function setOfferNotification(ReactionEvent $event)
    {
        $reaction = $event->getReaction();
        $this->actualizationOfferNotification($reaction->getOffer()->getUser());
    }

    /**
     * @param User $user
     */
    private function actualizationDemandNotification(User $user)
    {
        $demandNotification = $this->reactionManager->checkDemandNotification($user);
        if ($demandNotification != $user->getDemandNotification()) {
            $user->setDemandNotification($demandNotification);
            $this->fosUserManager->updateUser($user);
        }
    }

    /**
     * @param User $user
     */
    private function actualizationOfferNotification(User $user)
    {
        $offerNotification = $this->reactionManager->checkOfferNotification($user);
        if ($offerNotification != $user->getOfferNotification()) {
            $user->setOfferNotification($offerNotification);
            $this->fosUserManager->updateUser($user);
        }
    }
}
