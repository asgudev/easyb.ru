<?php
/**
 * User: scorp
 * Date: 01.07.13
 * Time: 13:04
 */
namespace Easyb\UserBundle\EventListener;

use Easyb\UserBundle\Entity\User;
use FOS\UserBundle\Model\UserManagerInterface;
use Easyb\AdvertBundle\Manager\ReactionManagerInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;


class LoginListener
{
    /**
     * @var \FOS\UserBundle\Model\UserManagerInterface
     */
    protected $userManager;
    /**
     * @var \Easyb\AdvertBundle\Manager\ReactionManagerInterface
     */
    protected $reactionManager;

    /**
     * @param UserManagerInterface     $userManager
     * @param ReactionManagerInterface $reactionManager
     */
    public function __construct(UserManagerInterface $userManager, ReactionManagerInterface $reactionManager)
    {
        $this->userManager = $userManager;
        $this->reactionManager = $reactionManager;
    }

    /**
     * @param InteractiveLoginEvent $event
     */
    public function onSecurityInteractiveLogin( InteractiveLoginEvent $event )
    {
        $user = $event->getAuthenticationToken()->getUser();
        // Актуализация уведомлений
        $this->actualizationDemandNotification($user);
        $this->actualizationOfferNotification($user);
    }

    /**
     * @param User $user
     */
    private function actualizationDemandNotification(User $user)
    {
        $demandNotification = $this->reactionManager->checkDemandNotification($user);
        if ($demandNotification != $user->getDemandNotification()) {
            $user->setDemandNotification($demandNotification);
            $this->userManager->updateUser($user);
        }
    }

    /**
     * @param User $user
     */
    private function actualizationOfferNotification(User $user)
    {
        $offerNotification = $this->reactionManager->checkOfferNotification($user);
        if ($offerNotification != $user->getOfferNotification()) {
            $user->setOfferNotification($offerNotification);
            $this->userManager->updateUser($user);
        }
    }
}