<?php

namespace Easyb\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class EasybUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
