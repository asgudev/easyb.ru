<?php

namespace Easyb\UserBundle\Admin;

use Doctrine\ORM\EntityManager;
use Easyb\AdminBundle\Admin\Admin as BaseAdmin;
use Easyb\AdvertBundle\Form\Type\AdminFileType;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Validator\ErrorElement;


class ApproveJobAdmin extends BaseAdmin
{
    protected $em;
    protected $baseRouteName = 'jobs_approval';
    protected $baseRoutePattern = 'jobs-approval';

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     * @param EntityManager $entityManager
     */
    public function __construct($code, $class, $baseControllerName, EntityManager $entityManager)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->em = $entityManager;

        if (!$this->hasRequest()) {
            $this->datagridValues = array(
                '_page' => 1,
                '_per_page' => $this->getMaxPerPage(),
                '_sort_order' => 'DESC',
                '_sort_by' => 'createdAt'
            );
        }
    }

    public function configure()
    {
        $this->setTemplate('edit', 'EasybUserBundle:JobAdmin:editApprove.html.twig');
    }

    public function getModelInstance($class)
    {
        return new $class();
    }


    /**
     * Добавляем свой баш - отклонить и одобрить
     *
     * @return array
     */
    public function getBatchActions()
    {
        $actions = array();

        // check user permissions
        if ($this->hasRoute('edit') && $this->isGranted('EDIT') && $this->hasRoute('delete') && $this->isGranted('DELETE')) {
            $actions['improveJob'] = [
                'label' => 'Отправить на доработку',
                'ask_confirmation' => false // If true, a confirmation will be asked before performing the action
            ];
            $actions['approve'] = [
                'label' => 'Одобрить',
                'ask_confirmation' => false // If true, a confirmation will be asked before performing the action
            ];
        }

        if ($this->hasRoute('delete') && $this->isGranted('DELETE')) {
            $actions['delete_job'] = [
                'label' => 'Удалить компанию',
                'ask_confirmation' => false // If true, a confirmation will be asked before performing the action
            ];
            $actions['delete_user'] = [
                'label' => 'Удалить пользователя',
                'ask_confirmation' => false // If true, a confirmation will be asked before performing the action
            ];
        }

        return $actions;
    }

    /**
     * @param Object $queryBuilder
     * @param string $alias
     * @param boolean $field
     * @param array $value
     *
     * @return mixed
     */
    public function getUserFilter($queryBuilder, $alias, $field, $value)
    {
        if ($value['value'] == '') {
            return $queryBuilder;
        }
        $queryBuilder
            ->leftJoin(sprintf('%s.user', $alias), 'u')
            ->andWhere('u.id = :id')
            ->setParameter('id', $value['value']->getId());
//dump($queryBuilder); die();
        return $queryBuilder;
    }

    /**
     * @param Object $queryBuilder
     * @param string $alias
     * @param boolean $field
     * @param array $value
     *
     * @return mixed
     */
    public function getJobFilter($queryBuilder, $alias, $field, $value)
    {
        if ($value['value'] == '') {
            return $queryBuilder;
        }
        $queryBuilder
            ->leftJoin(sprintf('%s.job', $alias), 'j')
            ->andWhere('j.id = :id')
            ->setParameter('id', $value['value']->getId());

        return $queryBuilder;
    }

    /**
     * @param Object $queryBuilder
     * @param string $alias
     * @param boolean $field
     * @param array $value
     *
     * @return mixed
     */
    public function getCategoryFilter($queryBuilder, $alias, $field, $value)
    {
        if ($value['value'] == '') {
            return $queryBuilder;
        }
        $queryBuilder
            ->leftJoin(sprintf('%s.category', $alias), 'ca')
            ->andWhere('ca.id = :id')
            ->setParameter('id', $value['value']->getId());

        return $queryBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $this->getModelManager();
        $query = parent::createQuery($context);
        $qb = $query->getQueryBuilder();
        $qb
            ->where($qb->expr()->eq($qb->getRootAliases()[0] . '.isApproved', ':isApproved'))
            ->andWhere($qb->expr()->eq($qb->getRootAliases()[0] . '.isImproved', ':isImproved'))
            ->setParameters(['isApproved' => 'false', 'isImproved' => 'false']);

        return $query;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
    }

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, array(
                'required' => true,
                'label' => 'Наименование организации'
            ))
            ->add('city', null, array(
                'label' => 'Город',
                'required' => true
            ))
            ->add('categories', null, array(
                'label' => 'Категории',
                'required' => true,
                'expanded' => true,
                'multiple' => true,
            ))
            ->add('user', 'em_autocompleter', array(
                'required' => true,
                'label' => 'Пользователь',
                'property' => 'username',
                'route_name' => 'search_user',
                'class' => 'Easyb\\UserBundle\\Entity\\User',
                'help' => 'Начните вводить ФИО или Email и выберите автора из списка'
            ))
            ->add('position', null, array(
                'required' => true,
                'label' => 'Должность'
            ))
            ->add('description', null, array(
                'required' => false,
                'label' => 'Описание',
                'attr' => array(
                    'class' => 'ckeditor ckone'
                )))
            ->add('site', null, array(
                'required' => false,
                'label' => 'Сайт'
            ))
            ->add('work_email', null, array(
                'required' => true,
                'label' => 'Электронный адрес рабочей почты'
            ))
            ->add('email', null, array(
                'required' => false,
                'label' => 'Email'
            ))
            ->add('phone', null, array(
                'required' => false,
                'label' => 'Телефон'
            ))
            ->add('skype', null, array(
                'required' => false,
                'label' => 'Skype'
            ))
            ->add('linkedin', null, array(
                'label' => 'Ссылка на страницу компании в LinkedIn',
                "required" => false,
                'attr' => array(
                    'placeholder' => 'http://linkedin.com/company/google',
                ),
            ))
            ->add('files',
                'file_collection',
                [
                    'type' => new AdminFileType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'label' => 'Выберите файл и дайте ему имя'
                ]
            )
            ->add('file', 'em_file', array('required' => false,
                'absolute_path' => 'allFiles',
                'show_path' => 'logoToAdmin',
                'field' => 'logo',
                'type' => 'image',
                'label' => 'Логотип'));
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier("name", null, array(
                'label' => 'Наименование компании',
                'field_options' => array(
                    'class' => 'Easyb\\UserBundle\\Entity\\Job',
                    'property' => 'name',
                    'route_name' => 'search_job'
                ),
                'admin_code' => 'sonata.admin.jobs'))
            ->addIdentifier("categories", null, array(
                'label' => 'Категории',
                'field_options' => array(
                    'class' => 'Easyb\\MainBundle\\Entity\\Category',
                    'property' => 'name',
                    'route_name' => 'search_category'
                )))
            ->add('user', null, array(
                'label' => 'Пользователь',
                'field_options' => array(
                    'class' => 'Easyb\\UserBundle\\Entity\\User',
                    'property' => 'username',
                    'route_name' => 'search_user'
                ),
            ));

        $listMapper->add('_action', 'actions', array(
            'actions' => array(
                'improveJob' => array(
                    'template' => 'EasybUserBundle:Job:jobImprove.html.twig')
            )));

        //dump($listMapper); die();
    }

    /**
     * @param DatagridMapper $filter
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add(
                'user',
                'doctrine_orm_callback',
                array(
                    'field_type' => 'em_autocompleter',
                    'field_options' => array(
                        'class' => 'Easyb\\UserBundle\\Entity\\User',
                        'property' => 'username',
                        'route_name' => 'search_user'
                    ),
                    'mapping_type' => 2,
                    'label' => 'Пользователь',
                    'callback' => array($this, 'getUserFilter')
                )
            )
            ->add(
                'job',
                'doctrine_orm_callback',
                array(
                    'field_type' => 'em_autocompleter',
                    'field_options' => array(
                        'class' => 'Easyb\\UserBundle\\Entity\\Job',
                        'property' => 'name',
                        'route_name' => 'search_job'
                    ),
                    'mapping_type' => 2,
                    'label' => 'Компания',
                    'admin_code' => 'sonata.admin.jobs',
                    'callback' => array($this, 'getJobFilter')
                )
            )
            ->add(
                'category',
                'doctrine_orm_callback',
                array(
                    'field_type' => 'em_autocompleter',
                    'field_options' => array(
                        'class' => 'Easyb\\MainBundle\\Entity\\Category',
                        'property' => 'name',
                        'route_name' => 'search_category'
                    ),
                    'mapping_type' => 2,
                    'label' => 'Каталог',
                    'callback' => array($this, 'getCategoryFilter')
                )
            );
        //dump($filter); die();
    }

}