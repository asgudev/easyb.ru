<?php
namespace Easyb\UserBundle\Admin;

use Easyb\AdminBundle\Admin\Admin as BaseAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;
use Easyb\UserBundle\Entity\Report;


class ReportAdmin extends BaseAdmin
{

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
          ->add('id')
          ->add('user', null, array('label'  => 'На кого жалуются'))
          ->add('author', null, array('label' => 'Автор'))
          ->add('createdAt', null, array('label' => 'Дата'));

        $listMapper->add('_action', 'actions', array(
            'actions' => array(
                'delete'   => array('label' => 'delete'),
                'show'     => array(),
                'settings' => array(),
        )));
    }

    /**
     * Configuration show view
     *
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
          ->add('user', null, array('label'  => 'На кого жалуются'))
          ->add('author', null, array('label' => 'Автор'))
          ->add('text', 'text', array('label' => 'Жалоба'));
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
          ->add('user', 'doctrine_orm_callback', array('field_type'    => 'em_autocompleter',
                                                       'field_options' => array('class'      => 'Easyb\\UserBundle\\Entity\\User',
                                                                                'property'   => 'username',
                                                                                'route_name' => 'search_user'),
                                                       'mapping_type'  => 2,
                                                       'label'         => 'На кого жалуются',
                                                       'callback'      => array($this, 
                                                                                'getUserFilter')
          ))
          ->add('author', 'doctrine_orm_callback', array('field_type'    => 'em_autocompleter',
                                                         'field_options' => array('class'      => 'Easyb\\UserBundle\\Entity\\User',
                                                                                'property'   => 'username',
                                                                                'route_name' => 'search_user'),
                                                         'mapping_type'  => 2,
                                                         'label'         => 'Кто жалуется',
                                                         'callback'      => array($this,
                                                                                'getAuthorFilter')
          ));
    }

    /**
     * @param Object  $queryBuilder
     * @param string  $alias
     * @param boolean $field
     * @param array   $value
     *
     * @return mixed
     */
    public function getUserFilter($queryBuilder, $alias, $field, $value)
    {
        if ($value['value'] == '') {
            return $queryBuilder;
        }
        $queryBuilder
            ->leftJoin(sprintf('%s.user', $alias), 'u')
            ->andWhere('u.id = :id')
            ->setParameter('id', $value['value']->getId());

        return $queryBuilder;
    }

    /**
     * @param Object  $queryBuilder
     * @param string  $alias
     * @param boolean $field
     * @param array   $value
     *
     * @return mixed
     */
    public function getAuthorFilter($queryBuilder, $alias, $field, $value)
    {
        if ($value['value'] == '') {
            return $queryBuilder;
        }
        $queryBuilder
            ->leftJoin(sprintf('%s.author', $alias), 'a')
            ->andWhere('a.id = :id')
            ->setParameter('id', $value['value']->getId());

        return $queryBuilder;
    }

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        if (!$this->hasRequest()) {
            $this->datagridValues = array(
                '_page'       => 1,
                '_per_page'   => $this->getMaxPerPage(),
                '_sort_order' => 'DESC',
                '_sort_by'    => 'createdAt'
            );
        }
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            ->remove('edit')
        ;
        $collection
            ->add('delete_user', '/delete/user/{id}')
        ;
    }

    public function configure()
    {
        $this->setTemplate('show', 'EasybUserBundle:Admin:show.html.twig');
    }
}