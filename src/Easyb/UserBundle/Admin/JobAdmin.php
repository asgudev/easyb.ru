<?php
namespace Easyb\UserBundle\Admin;

use Easyb\AdminBundle\Admin\Admin as BaseAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Easyb\AdvertBundle\Form\Type\AdminFileType;


use Knp\Menu\ItemInterface as MenuItemInterface;


class JobAdmin extends BaseAdmin
{


    /**
     * @param Object $queryBuilder
     * @param string $alias
     * @param boolean $field
     * @param array $value
     *
     * @return mixed
     */
    public function getUserFilter($queryBuilder, $alias, $field, $value)
    {
        if ($value['value'] == '') {
            return $queryBuilder;
        }
        $queryBuilder
            ->leftJoin(sprintf('%s.user', $alias), 'u')
            ->andWhere('u.id = :id')
            ->setParameter('id', $value['value']->getId());

        return $queryBuilder;
    }

    public function prePersist($object)
    {
        $this->saveFile($object);
    }

    public function saveFile($object)
    {
        $basepath = $this
            ->getRequest()
            ->getBasePath();
        $object->upload($basepath);
    }

    public function preUpdate($object)
    {
        parent::preUpdate($object);

        $this->saveFile($object);
    }

    public function configure()
    {
        $this->setTemplate('edit', 'EasybUserBundle:JobAdmin:edit.html.twig');
    }

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, array('required' => true,
                'label' => 'Наименование организации'))
            ->add('city', null, array(
                'label' => 'Город',
                'required' => true))
            ->add('user', 'em_autocompleter', array(
                'required' => true,
                'label' => 'Пользователь',
                'property' => 'username',
                'route_name' => 'search_user',
                'class' => 'Easyb\\UserBundle\\Entity\\User',
                'help' => 'Начните вводить ФИО или Email и выберите автора из списка'))
            ->add('position', null, array(
                'required' => true,
                'label' => 'Должность'))
            ->add('categories', null, array(
                'required' => false,
                'label' => 'Категории',
                'expanded' => true,
                'attr' => array(
                    'class' => "category"
                )
            ))
            ->add('description', null, array(
                'required' => false,
                'label' => 'Описание',
                'attr' => array('class' => 'ckeditor ckone')))
            ->add('site', null, array(
                'required' => false,
                'label' => 'Сайт'))
            ->add('work_email', null, array(
                'required' => true,
                'label' => 'Рабочий Email'))
            ->add('email', null, array(
                'required' => false,
                'label' => 'Email'))
            ->add('phone', null, array(
                'required' => true,
                'label' => 'Телефон'))
            ->add('skype', null, array(
                'required' => false,
                'label' => 'Skype'))
            ->add('linkedin', null, array('label' => 'Ссылка на страницу компании в LinkedIn',
                "required" => false,
                'attr' => array(
                    'placeholder' => 'http://linkedin.com/company/google',
                ),
            ))
            ->add('files',
                'file_collection',
                [
                    'type' => new AdminFileType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'label' => 'Выберите файл и дайте ему имя'
                ]
            )
            ->add('file', 'em_file', array('required' => false,
                'absolute_path' => 'allFiles',
                'show_path' => 'logoToAdmin',
                'field' => 'logo',
                'type' => 'image',
                'label' => 'Логотип'));
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->addIdentifier('name', null, array('label' => 'Название'))
            ->add('city', null, array('label' => 'Город'))
            ->add('user', null, array('label' => 'Пользователь'))
            ->add('position', null, array('label' => 'Позиция'))//            ->add('description', null, array('label' => 'Описание'))
        ;


        $listMapper->add('_action', 'actions', array(
            'actions' => array(
                'delete' => array('label' => 'delete'),
                'edit' => array(),
                'show' => array(),
            )));
    }

    /**
     * Configuration show view
     *
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('name', null, array('label' => 'Название'))
            ->add('city', null, array('label' => 'Город'))
            ->add('user', null, array('label' => 'Пользователь'))
            ->add('position', null, array('label' => 'Активен?'))
            ->add('description', 'text', array('label' => 'Описание'))
            ->add('site', null, array('label' => 'Сайт'))
            ->add('email', null, array('label' => 'Email'))
            ->add('phone', null, array('label' => 'Телефон'))
            ->add('skype', null, array('label' => 'Skype'))
            ->add('logo', null, array('label' => 'Логотип'));
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('name', null, array('label' => 'Название'))
            ->add('user', 'doctrine_orm_callback', array(
                'field_type' => 'em_autocompleter',
                'field_options' => array(
                    'class' => 'Easyb\\UserBundle\\Entity\\User',
                    'property' => 'username',
                    'route_name' => 'search_user'),
                'mapping_type' => 2,
                'label' => 'На кого жалуются',
                'callback' => array(
                    $this,
                    'getUserFilter')
            ));
    }
}