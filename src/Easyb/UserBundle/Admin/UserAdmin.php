<?php
namespace Easyb\UserBundle\Admin;

use Easyb\AdminBundle\Admin\Admin as BaseAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Validator\ErrorElement;


class UserAdmin extends BaseAdmin
{

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('fio', null, array('required' => true,
                'label' => 'Имя пользователя и фамилия',
                'help' => 'Введите до 70 символов',
                'max_length' => 70))
            ->add('linkedin', null, array('label' => 'Ссылка на профиль в LinkedIn',
                "required" => false,
                'attr' => array(
                    'placeholder' => 'http://linkedin.com/profile/...',
                ),
            ))
            ->add('plainPassword', 'password', array('label' => 'Пароль',
                'always_empty' => false,
                'required' => false,
                'trim' => false,
                'help' => 'Введите от 4 до 20 символов',
                'max_length' => 20))
            ->add('email', null, array('required' => true,
                'label' => 'Email',
                'help' => 'Email пользователя'))
            ->add('enabled', null, array('required' => false,
                'label' => 'Активен?'))
            ->add('super_admin', 'checkbox', array('required' => false,
                'label' => 'Админ?'));
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->addIdentifier('email', null, array('label' => 'Email'))
            ->add('fio', null, array('label' => 'Имя пользователя и фамилия'))
            ->add('enabled', null, array('label' => 'Активен?'));

        $listMapper->add('_action', 'actions', array(
            'actions' => array(
                'delete' => array('label' => 'delete'),
                'edit' => array(),
                'show' => array(),
            )));
    }

    /**
     * Configuration show view
     *
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
            ->add('username', null, array('label' => 'Имя пользователя и фамилия'))
            ->add('email', null, array('label' => 'Email'))
            ->add('enabled', null, array('label' => 'Активен?'));
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('email', null, array('label' => 'Email'))
            ->add('username', null, array('label' => 'Имя пользователя и фамилия'));
    }

    /**
     * {@inheritdoc}
     */
    public function create($object)
    {
        $this->prePersist($object);
        $object->setUsername($object->getEmail());
        $userManager = $this->getConfigurationPool()->getContainer()->get('fos_user.user_manager');
        $userManager->updateUser($object);
        $this->postPersist($object);
    }

    /**
     * @param \Sonata\AdminBundle\Validator\ErrorElement $errorElement
     * @param Object $object
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        if (!$this->checkPassword($object)) {
            $errorElement
                ->with('plainPassword')
                ->addViolation('Поле не может быть пустым')
                ->end();
        }
    }

    /**
     * @param Object $object
     *
     * @return bool
     */
    public function checkPassword($object)
    {
        // check pass when create user
        if (!trim($object->getPlainPassword()) && !trim($object->getPassword())) {
            return false;
        } // check pass when edit user
        elseif ($object->getPlainPassword() == null && trim($object->getPassword())) {
            return true;
        } elseif (!trim($object->getPlainPassword()) && trim($object->getPassword())) {
            return false;
        }

        return true;
    }
}