<?php
namespace Easyb\UserBundle\Admin;

use Easyb\AdminBundle\Admin\Admin as BaseAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;
use Easyb\UserBundle\Entity\Mail;


class MailAdmin extends BaseAdmin
{

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
          ->add('name', null, array('required' => true,
                                    'label'    => 'Имя шаблонного письма',
                                    'disabled' => true
          ))
          ->add('subject', null, array('required' => true,
                                       'label'    => 'Тема'
          ))
          ->add('message', null, array('required' => true,
                                       'label'    => 'Текст',
                                       'help'     => 'Параметры в тексте: <br/>
                                                      [username] - ФИО пользователя,<br/>
                                                      [confirmationUrl] - адрес подтверждения,<br/>
                                                      [company_author] - наименования компании (организатора тендера),<br/>
                                                      [advert_name] - название объявления,<br/>
                                                      [advert_link] - ссылка на страницу тендера,<br/>
                                                      [advert_username] - имя пользователя у объявления,<br/>
                                                      [advert_phone] - телефон,<br/>
                                                      [advert_email] - email,<br/>
                                                      [advert_skype] - skype,<br/>
                                                      [demand_link] - ссылка на тендер,<br/>
                                                      [offer_link] - ссылка на предложение,<br/>
                                                      [unsubscribe_link] - ссылка для отписки от писем,<br/>
                                                      [total] - количество объявлениий в автопоиске,<br/>
                                                      [autosearch_name] - навзвание автопоиска,<br/><br/>

                                                      [company_user] - наименование компании (участник тендера),<br/>
                                                      [advert_files] -  ссылка на скачивание файлов (вложения),<br/>
                                                      [reaction_files] - файлы в ответе,<br/>
                                                      [advert_expireAt] - дата и время окончания сбора заявок,<br/>
                                                      [advert_createAt] - дата создания предложения,<br/>

                                                      [href target="http://google.com/" title="google"] - ссылка на target с отображением title,<br/>

                                                      [offer_expireAt] - дата и время окончания сбора КП,<br/>
                                                      [reaction_expireAt] - дата и время окончания процедуры принятия решения,<br/>

                                                      <br/>Например: "Добро пожаловать [username]! Для завершения регистрации перейдите по ссылке [confirmationUrl]."',
              'attr' => array(
                  'rows' => 10,
                  'class' => 'ckeditor'
              )
          ));
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
          ->addIdentifier('name', null, array('label' => 'Тип шаблонного письма'))
          ->add('subject', null, array('label'  => 'Тема'))
          ->add('message', null, array('label' => 'Текст'));

        $listMapper->add('_action', 'actions', array(
            'actions' => array(
                'edit'     => array(),
                'show'     => array(),
        )));
    }

    /**
     * Configuration show view
     *
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name', null, array('label' => 'Тип шаблонного письма'))
            ->add('subject', null, array('label'  => 'Тема'))
            ->add('message', 'text', array('label' => 'Текст'));
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
          ->add('name', null, array('label' => 'Тип'));
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            ->remove('delete')
        ;
    }
}