<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 25.09.14
 * Time: 14:10
 */

namespace Easyb\UserBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Easyb\UserBundle\Entity\JobMetadata;

class JobMetadataCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('easyb:init:job_metadata')
            ->setDescription('Init JobMetadata');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $init = new JobMetadata();
        $this->getEm()->persist($init);

        $this->getEm()->flush();
    }

    /**
     * @return EntityManager
     */
    private function getEm()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }
} 