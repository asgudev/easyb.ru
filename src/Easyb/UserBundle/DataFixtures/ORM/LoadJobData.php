<?php
namespace Easyb\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Easyb\UserBundle\Entity\Job;

class LoadJobData implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        return false;
        $text = 'Ночная фотосъемка подразумевает малое количество света и большие выдержки, зачастую более 30 секунд. Понятно что снимая «с руки» удержать камеру абсолютно неподвижно в течении этого времени – нереально.';

        $users = $this->container
            ->get('doctrine')
            ->getRepository('EasybUserBundle:User')
            ->findAll();

        $city = $this->container
            ->get('doctrine')
            ->getRepository('EasybMainBundle:City')
            ->findAll();

        foreach ($users as $user) {
            $job = new Job();
            $job->setCity($city[mt_rand(0, count($city) - 1)]);
            $job->setUser($user);
            $job->setName('Организация - '.$user->getId());
            $job->setPosition('Менеджер');
            $job->setDescription($text);
            $job->setEmail('email@test.test');
            $job->setPhone('+375 44 761 71 61');
            $job->setSite('http://www.site.com');
            $job->setSkype('test_skype');

            $manager->persist($job);
        }
        $manager->flush();

    }

    public function getOrder()
    {
        return 5;
    }
}