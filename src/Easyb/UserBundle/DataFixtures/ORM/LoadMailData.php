<?php
namespace Easyb\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Easyb\UserBundle\Entity\Mail;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadMailData implements FixtureInterface, OrderedFixtureInterface
{
    /**
    * {@inheritDoc}
    */
    public function load(ObjectManager $manager)
    {
        // шаблоны писем для регистрации и сброса пароля
        foreach(Mail::$types as $key => $type) {
            $mail = new Mail();
            $mail->setName(Mail::$types[$key]);
            $mail->setType($key);
            $subject = ($key == Mail::TYPE_REGISTRATION ? 'Подтверждение регистрации на сайте EasyB' : 'Сброс пароля');
            $text = ($key == Mail::TYPE_REGISTRATION ? 'Добро пожаловать [username]!
Для завершения регистрации пройдите по ссылке [confirmationUrl].

Команда EasyB'
                : 'Для сброса пароля перейдите по ссылке [confirmationUrl]' );
            $mail->setSubject($subject);
            $mail->setMessage($text);
            $manager->persist($mail);
        }
        // шаблоны писем при одобрении предложения
        $mail = new Mail();
        $mail->setName('Предложение одобрено');
        $mail->setType(Mail::TYPE_DEMAND_APPROVE);
        $subject = 'Ваше предложение заинтересовало [company].';
        $text = 'Здравствуйте, [username]!

В настоящий момент [company] опубликовано объявление [advert_name].
Ваше предложение нас заинтересовало. Подробное описание интересующих нас услуг Вы найдете здесь: [advert_link].
Пожалуйста, ознакомьтесь с объявлением и свяжитесь со мной в рабочее время: [advert_username], [advert_phone], [advert_email][advert_skype].


С наилучшими пожеланиями,
[advert_username]
[company]

Посмотреть на Ваше предложение можно тут: [offer_link]
Отписаться от рассылки Вы сможете тут: ([unsubscribe_link]).

ПОЖАЛУЙСТА, НЕ ОТВЕЧАЙТЕ НА ЭТО ПИСЬМО, так как оно отправлено с сервера.';
        $mail->setSubject($subject);
        $mail->setMessage($text);
        $manager->persist($mail);
        // отклик на спрос
        $mail = new Mail();
        $mail->setName('Отклик на Тендер');
        $mail->setType(Mail::TYPE_DEMAND_REACTION);
        $subject = 'На ваше объявление откликнулась одна или несколько компаний.';
        $text = 'Здравствуйте, [username]!

На ваше объявление [advert_name] откликнулась одна или несколько компаний.
Пожалуйста, пройдите по ссылке  и отклоните либо одобрите предложение(я): [advert_link]

Посмотреть на Ваше объявление можно тут: [offer_link]
Отписаться от рассылки Вы сможете тут: ([unsubscribe_link]).

ПОЖАЛУЙСТА, НЕ ОТВЕЧАЙТЕ НА ЭТО ПИСЬМО, так как оно отправлено с сервера.';
        $mail->setSubject($subject);
        $mail->setMessage($text);
        $manager->persist($mail);
        // автопоиске
        $mail = new Mail();
        $mail->setName('Автопоиск');
        $mail->setType(Mail::TYPE_AUTOSEARCH);
        $subject = 'Новые объявления по вашему запросу ([total]).';
        $text = 'Здравствуйте, [username]!

([total]) новых объявлений по вашему запросу ([autosearch_name])
Чтобы ознакомиться с новыми объявлениями, - Вам необходимо пройти по ссылке: [advert_link]
Отписаться от рассылки Вы сможете тут ([unsubscribe_link]).

ПОЖАЛУЙСТА, НЕ ОТВЕЧАЙТЕ НА ЭТО ПИСЬМО, так как оно отправлено с сервера.';
        $mail->setSubject($subject);
        $mail->setMessage($text);
        $manager->persist($mail);

        $manager->flush();
    }

    public function getOrder()
    {
        return 4;
    }
}