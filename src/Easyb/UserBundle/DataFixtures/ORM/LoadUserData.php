<?php
namespace Easyb\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Easyb\UserBundle\Entity\User;

class LoadUserData implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $manager = $this->container->get('fos_user.user_manager');

        $user = new User();
        $user->setUsername('admin');
        $user->setFio("admin");
        $user->setPlainPassword('password');
        $user->setEnabled(true);
        $user->setEmail('admin@test.test');
        $user->setRoles(array('ROLE_SUPER_ADMIN',
            'ROLE_ADMIN'));
        $manager->updateUser($user);

//        for ($i = 0; $i < 10; $i++) {
//            for ($j = 0; $j < 10; $j++) {
//                $user = new User();
//                $user->setUsername('test-'.$i.'-'. $j);
//                $user->setFio('test-'.$i.'-'. $j);
//                $user->setPlainPassword('password-'.$i.'-'. $j);
//                $user->setEnabled(($i*$j/3 == 0 ? false : true));
//                $user->setEmail('test-'.$i.'-'. $j.'@' . $i. 'test.com');
//                $user->setRoles(array());
//                $manager->updateUser($user);
//            }
//        }
    }

    public function getOrder()
    {
        return 0;
    }
}