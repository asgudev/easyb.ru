<?php
/**
 * Created by JetBrains PhpStorm.
 * User: shustrik
 * Date: 5/22/13
 * Time: 6:20 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Easyb\UserBundle\Controller;


use Easyb\MainBundle\Controller\ControllerHelperTrait;
use Easyb\UserBundle\Entity\Job;
use Easyb\UserBundle\Form\Type\Frontend\JobType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class JobController extends Controller
{
    use ControllerHelperTrait;

    /**
     * @Route("/profile/job/add", name="job_add")
     *
     * @Template()
     *
     * @param Request $request
     *
     * @return array|RedirectResponse
     */
    public function addJobAction(Request $request)
    {
        $this->getBreadCrumbsManager()->create(array('EasyB' => 'homepage', 'Редактирование личных данных' => 'fos_user_profile_edit', 'Добавление места работы' => ''));
        $type = new JobType($this->getDoctrine());
        $job = $this->getJobManager()->createInstance();
        $job->setUser($user = $this->getUser());
        $form = $this->container->get('form.factory')->create($type, $job);

        $regions = $this->getDoctrine()->getRepository('EasybMainBundle:City')->findRegionsAndSort();

        if ($this->processForm($form, $job)) {
            $this->get('session')->getFlashBag()->add(
                'newJob',
                "После проверки администратором информация о Вашей компании будет будет опубликована. Пожалуйста, соблюдайте <a href=\"/page/add_rules\">правила</a> размещения информации на сайте"
            );
            /*
            if ($user->getJobs()->count() == 1) {
                return $this->redirect($this->generateUrl('fos_user_profile_edit', array('first' => 1)));
            } else {
                return $this->redirect($this->generateUrl('fos_user_profile_edit'));
            }
            */
            return $this->redirect($this->generateUrl('jobs_list'));

        }

        return ['form' => $form->createView(),
            'regions' => $regions,
            'errors' => null];
    }

    /**
     * @return \Easyb\UserBundle\Manager\JobManager
     */
    private function getJobManager()
    {
        return $this->get('easyb.manager.job');
    }

    private function processForm(Form $form, Job $job)
    {
        if ($this->container->get('request')->isMethod('POST')) {
            $form->submit($this->container->get('request'));
            if ($form->isValid()) {
                $jobManager = $this->getJobManager();
                if ($form->getData()->getId()) {
                    if ($job->isApproved()) {
                        $job->setIsApproved(false);
                    }
                    $jobManager->adminClearNotes($job);
                    $jobManager->adminClearImproved($job);

                    $jobManager->update($form->getData());
                } else {
                    $jobManager->create($form->getData());
                }
                return true;
            }
        }

        return false;
    }

    /**
     * @Route("/profile/job/edit/{id}", name="job_edit")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     * @Template("EasybUserBundle:Job:addJob.html.twig")
     */
    public function editJobAction(Job $job)
    {
        if ($job->getUser() != $this->getUser()) {
            $this->createNotFoundException('job was not found');
        }
        $this->getBreadCrumbsManager()->create(array('EasyB' => 'homepage', 'Редактирование личных данных' => 'fos_user_profile_edit', 'Редактирование места работы' => ''));
        $type = new JobType($this->getDoctrine());
        $form = $this->container->get('form.factory')->create($type, $job);
        $regions = $this->getDoctrine()->getRepository('EasybMainBundle:City')->findRegionsAndSort();

        if ($this->processForm($form, $job)) {
            return new RedirectResponse($this->container->get('router')->generate('jobs_list'));
        }

        return [
            'form' => $form->createView(),
            'job' => $job,
            'regions' => $regions,
            'errors' => null];
    }

    /**
     * @Route("/profile/job/delete/{id}", name="job_delete")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     * @Template()
     */
    public function removeJobAction(Job $job)
    {
        if ($job->getUser() != $this->getUser()) {
            $this->createNotFoundException('job was not found');
        }

        $this->getJobManager()->remove($job);

        return new RedirectResponse($this->container->get('router')->generate('jobs_list'));
    }

    /**
     * @Template()
     * @Route("/get_job_ajax", name="get_job_ajax")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return array
     */
    public function getJobAjaxAction(Request $request)
    {
        $jobId = $request->request->get('jobId');
        if (!$jobId) {
            return new JsonResponse([]);
        }
        $job = $this->getJobManager()->getRepository()->findOneById($jobId);

        $jobInfoTemplate = $this->render('EasybUserBundle:Job:jobInfo.html.twig',
            ['job' => $job]
        );

        $categories = $job->getCategories()->toArray();

        $cat = [];
        foreach ($categories as $category) {
            $cat[$category->getId()] = $category->getName();
        }

        return new JsonResponse(['jobInfoTemplate' => $jobInfoTemplate->getContent(), 'categories' => $cat]);
    }

    /**
     * @param Job $job
     * @Template()
     * @Route("/company/{slug}", name="show_job")
     *
     * @return array
     */
    public function showAction(Job $job)
    {

        $this->getBreadCrumbsManager()->create(['EasyB' => 'homepage', 'Компании' => 'show_job', $job->getName() => null],
            ['Каталог' => 'catalog', 'Компании' => ['slug' => $job->getSlug()]]);

        $jobs = $this->get('easyb.manager.job')->getSimilarJobs($job);

        if ($job->isDeleted() && (!$job->isApproved() && ($job->getUser() != $this->getUser()))) {
            throw new NotFoundHttpException('Такой компании не существует.');
        }

        return [
            'metadata' => $this->getMetadata($job, $this->getDoctrine()->getRepository('EasybUserBundle:JobMetadata')->findAll()[0]),
            'job' => $job,
            'similarJobs' => $jobs,
            'offers' => $this->get('easyb.offer.manager')->getOffersByJob($job),
            'demands' =>  $this->get('easyb.demand.manager')->getDemandsByJob($job),
            'showContact' => false,
        ];
    }

    private function getMetaData($object = null, $customMetadata = null)
    {
        return $this->get('easyb.manager.metadata')->getMetadata($this->getRequest()->get('_route'), $object, $customMetadata);
    }

    /**
     * @Route("/save_photo", name="save_photo")
     *
     * @return array
     */
    public function savePhotoAction()
    {
        $file = $this->getRequest()->files->get('filedata');
        $data = $this->get('easyb.manager.photo')->upload($file);

        return new JsonResponse([
            'photo' => $data['photo'],
            'name' => $data['name']
        ]);
    }


    /**
     *
     * @Route("/profile/job/list", name="jobs_list")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     */
    public function listJobsAction()
    {
        return $this->render("EasybUserBundle:Job:list.html.twig");
    }

    /**
     * @Route("/delete_photo", name="delete_photo")
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     *
     * @return array
     */
    public function deletePhotoAction()
    {
        $logo = $this->getRequest()->query->get('logo');
        if ($logo) {
            $result = $this->get('easyb.manager.photo')->remove($logo, $this->getUser());

            return new JsonResponse([
                'success' => $result
            ]);
        }

        return new JsonResponse([
            'success' => false
        ]);
    }

    /**
     * @Route("/get_userjob_ajax", name="get_userjob_ajax")
     * @Secure(roles="ROLE_ADMIN")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return array
     */
    public function getUserJobAjaxAction(Request $request)
    {
        $userId = $request->request->get('userId');
        if (!$userId) {
            return new JsonResponse([]);
        }
        $user = $this->get('fos_user.user_manager')->findUserBy(['id' => $userId]);
        $result = str_replace('"', '&#034;', $user->getJobs()->first()->getName());

        return new JsonResponse(['job' => $result]);
    }

    /**
     * @param Job $job
     * @Route("/admin/job/approve/{id}", name="approve_job")
     * @Secure(roles="ROLE_ADMIN")
     *
     * @return RedirectResponse
     */
    public function approveJobAction(Job $job)
    {
        $this->get('easyb.manager.job')->adminApprove($job);

        return new RedirectResponse($this->generateUrl('admin_easyb_job_list'));
    }

    /**
     * @param Job $job
     * @Route("/admin/job/decline/{id}", name="decline_job")
     * @Secure(roles="ROLE_ADMIN")
     *
     * @return RedirectResponse
     */
    public function declineJobAction(Job $job)
    {
        $this->get('easyb.manager.job')->adminDecline($job);

        return new RedirectResponse($this->generateUrl('admin_easyb_advert_abstractadvert_list'));
    }

    /**
     * @Route("/admin/job/improve/{id}", name="job_improve")
     * @Secure(roles="ROLE_ADMIN")
     *
     * @param \Easyb\UserBundle\Entity\Job $job
     *
     * @return Response
     */
    public function improveAdminAction(Job $job)
    {
        // Only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return $this->redirect($this->generateUrl('admin_easyb_user_job_list'));
        }

        try {
            $form = $this->createForm('admin_job_improve_type', $job);

            $form->handleRequest($this->getRequest());
            $status = $content = '';

            if ($form->isValid()) {
                $status = 'improved';


                $rule1 = $form->get('rule1')->getData();
                $rule4 = $form->get('rule4')->getData();
                $rule5 = $form->get('rule5')->getData();
                $notes = $form->get('notes')->getData();
                $this->get('easyb.manager.job')->adminSetNotesWithRules($job, $notes, $rule1, $rule4, $rule5);

                return JsonResponse::create(array('status' => $status, 'content' => $content));
            }

            $status = 'ok';
            $contentTemplate = 'EasybUserBundle:Job:approveJobImprove.html.twig';
            $content = $this->renderView($contentTemplate, array('form' => $form->createView()));
        } catch (\Exception $e) {
            $status = 'error';
            $content = $e->getMessage();
        }

        return JsonResponse::create(array('status' => $status, 'content' => $content));
    }
}