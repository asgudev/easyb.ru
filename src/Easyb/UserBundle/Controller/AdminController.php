<?php
namespace Easyb\UserBundle\Controller;

use Easyb\UserBundle\Events\JobEvent;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Response;
use Sonata\AdminBundle\Exception\ModelManagerException;

use JMS\SecurityExtraBundle\Annotation\Secure;

use Easyb\UserBundle\Entity\User;
use Easyb\UserBundle\Entity\Job;



class AdminController extends Controller
{
    /**
     * @param \Easyb\UserBundle\Entity\User $user
     *
     * @return array
     */
    public function deleteUserAction(User $user)
    {
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->container->get('fos_user.user_manager');
        $userManager->deleteUser($user);

        return new RedirectResponse($this->container->get('router')->generate('admin_easyb_user_report_list'));
    }

    /**
     * @param ProxyQueryInterface $query
     *
     * @return RedirectResponse
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function batchActionApprove(ProxyQueryInterface $query)
    {
        if (false === $this->admin->isGranted('DELETE')) {
            throw new AccessDeniedException();
        }

        try {
            $this->batchApproveUpdate($this->admin->getClass(), $query);
            $this->addFlash('sonata_flash_success', 'Одобрение прошло успешно');
        } catch (ModelManagerException $e) {
            $this->addFlash('sonata_flash_error', 'При одобрении произошла ошибка');
        }

        return new RedirectResponse(
            $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters())) . '#to-do'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function batchApproveUpdate($class, ProxyQueryInterface $queryProxy)
    {
        $queryProxy->select('DISTINCT ' . $queryProxy->getRootAlias());

        try {

            /**
             * @var
             * Job[] $object
             */
            foreach ($queryProxy->getQuery()->iterate() as $object) {
                $this->get('easyb.manager.job')->adminApprove($object[0]);

                $dispatcher = $this->container->get('event_dispatcher');
                $event = new JobEvent($object[0]);

                $dispatcher->dispatch(JobEvent::ADMIN_JOB_APPROVE, $event);

            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * return the Response object associated to the edit action
     *
     *
     * @param mixed $id
     *
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @return Response
     */
    public function editAction($id = null)
    {
        // the key used to lookup the template
        $templateKey = 'edit';

        $id = $this->get('request')->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }

        $this->admin->setSubject($object);

        /** @var $form \Symfony\Component\Form\Form */
        $form = $this->admin->getForm();
        $form->setData($object);

        if ($this->getRestMethod() == 'POST') {
            $form->bind($this->get('request'));

            $isFormValid = $form->isValid();

            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {
                $object->setIsApproved(true);
                $this->admin->update($object);

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(
                        array(
                            'result' => 'ok',
                            'objectId' => $this->admin->getNormalizedIdentifier($object)
                        )
                    );
                }

                $this->addFlash(
                    'sonata_flash_success',
                    $this->admin->trans(
                        'flash_edit_success',
                        array('%name%' => $this->admin->toString($object)),
                        'SonataAdminBundle'
                    )
                );

                // redirect to edit mode
                return $this->redirectTo($object);
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->addFlash(
                        'sonata_flash_error',
                        $this->admin->trans(
                            'flash_edit_error',
                            array('%name%' => $this->admin->toString($object)),
                            'SonataAdminBundle'
                        )
                    );
                }
            } elseif ($this->isPreviewRequested()) {
                // enable the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
                $this->admin->getShow();
            }
        }

        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render(
            $this->admin->getTemplate($templateKey),
            array(
                'action' => 'edit',
                'form' => $view,
                'object' => $object,
            )
        );
    }




    /**
     * @param ProxyQueryInterface $query
     *
     * @return RedirectResponse
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function batchActionImproveJob(ProxyQueryInterface $query)
    {
        if (false === $this->admin->isGranted('DELETE')) {
            throw new AccessDeniedException();
        }

        try {
            $this->batchImproveUpdate($this->admin->getClass(), $query);
            $this->addFlash('sonata_flash_success', 'Отправка на доработку прошла успешно');
        } catch (ModelManagerException $e) {
            $this->addFlash('sonata_flash_error', 'При отправке на доработку произошла ошибка');
        }

        return new RedirectResponse(
            $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters())) . '#to-do'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function batchImproveUpdate($class, ProxyQueryInterface $queryProxy)
    {
        $queryProxy->select('DISTINCT ' . $queryProxy->getRootAlias());

        try {
            /** @var AbstractAdvert[] $object */
            foreach ($queryProxy->getQuery()->iterate() as $object) {
                $this->get('easyb.manager.job')->adminImprove($object[0]);

                /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
                $dispatcher = $this->container->get('event_dispatcher');
                $event = new JobEvent($object[0]);
                $dispatcher->dispatch(JobEvent::ADMIN_JOB_IMPROVE, $event);

            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param ProxyQueryInterface $query
     *
     * @return RedirectResponse
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function batchActionDeleteJob(ProxyQueryInterface $query)
    {
        if (false === $this->admin->isGranted('DELETE')) {
            throw new AccessDeniedException();
        }

        try {
            $this->batchDeleteJob($this->admin->getClass(), $query);
            $this->addFlash('sonata_flash_success', 'Удаление компании прошло успешно');
        } catch (ModelManagerException $e) {
            $this->addFlash('sonata_flash_error', 'При удалении компании произошла ошибка');
        }

        return new RedirectResponse(
            $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters())) . '#to-do'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function batchDeleteJob($class, ProxyQueryInterface $queryProxy)
    {
        $queryProxy->select('DISTINCT ' . $queryProxy->getRootAlias());
        $em = $this->getDoctrine()->getManager();

        foreach ($queryProxy->getQuery()->iterate() as $object) {
            $em->remove($object[0]);
        }

        try {
            $em->flush();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param ProxyQueryInterface $query
     *
     * @return RedirectResponse
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function batchActionDeleteUser(ProxyQueryInterface $query)
    {
        if (false === $this->admin->isGranted('DELETE')) {
            throw new AccessDeniedException();
        }

        try {
            $this->batchDeleteUser($this->admin->getClass(), $query);
            $this->addFlash('sonata_flash_success', 'Удаление пользователя прошло успешно');
        } catch (ModelManagerException $e) {
            $this->addFlash('sonata_flash_error', 'При удалении пользователя произошла ошибка');
        }

        return new RedirectResponse(
            $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters())) . '#to-do'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function batchDeleteUser($class, ProxyQueryInterface $queryProxy)
    {
        $queryProxy->select('DISTINCT ' . $queryProxy->getRootAlias());
        $em = $this->getDoctrine()->getManager();

        foreach ($queryProxy->getQuery()->iterate() as $object) {
            $user = $object[0]->getUser();
            $em->remove($user);
        }

        try {
            $em->flush();
        } catch (\Exception $e) {
            throw $e;
        }
    }
}