<?php
namespace Easyb\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Easyb\UserBundle\Form\Type\Frontend\CompleteRegistrationType;
use Easyb\UserBundle\Form\Type\Frontend\ReportType;
use Easyb\UserBundle\Entity\Job;
use Easyb\UserBundle\Entity\Report;
use Easyb\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;

class FrontendController extends Controller
{
    /**
     * @Route("/register/confirmed", name="confirmed")
     * @Template("EasybUserBundle:Registration:confirmed.html.twig")
     *
     * @return array
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function confirmationAction()
    {
        $user = $this->getUser();
        if ($user->getRegistrationComplete()) {
            throw new NotFoundHttpException(sprintf('Page not found'));
        }
        $form = $this->createForm(new CompleteRegistrationType(), $user);

        if ($this->getRequest()->getMethod() == 'POST') {
            $form->submit($this->getRequest());
            if ($form->isValid()) {
                /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
                $userManager = $this->container->get('fos_user.user_manager');
                $userManager->updateUser($user->setRegistrationComplete(true));
                $this->get('session')->getFlashBag()->add('success_confirm', 'user.set_password');

                return $this->redirect($this->generateUrl('fos_user_profile_edit'));
            }
        }

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @param \Easyb\UserBundle\Entity\Job $job
     *
     * @Route("/report/{id}", name="report")
     * @Template()
     * @Secure(roles="IS_AUTHENTICATED_FULLY")
     *
     * @return array
     */
    public function reportAction(Job $job)
    {
        /** @var $reportManager \Easyb\UserBundle\Manager\ReportManagerInterface */
        $reportManager = $this->container->get('easyb.manager.report');
        $report = $reportManager->createInstance($user = $job->getUser(), $this->getUser());
        $form = $this->createForm(new ReportType(), $report);

        if ($this->getRequest()->getMethod() == 'POST') {
            $form->submit($this->getRequest());
            if ($form->isValid()) {
                $reportManager->create($report);

                return new Response($this->renderView('EasybUserBundle:Frontend:reportSuccess.html.twig'));
            }
        }

        return [
            'user' => $user,
            'job'  => $job,
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/search_user", name="search_user")
     */
    public function ajaxUsersAction()
    {
        $result = array();
        $findString = $this->buildQueryString();
        if (!$findString) {
            return new JsonResponse($result);
        }
        $repository = $this->getDoctrine()->getRepository("EasybUserBundle:User");
        $users = $repository->findByEmailOrName($findString);
        $result = $this->buildResult($users);

        return new JsonResponse($result);
    }

    private function buildResult($users)
    {
        $result = array();
        foreach ($users as $user) {
            $result[] = array('data' => array($user->getFio(), $user->getId()),
                              'value'  => $user->getFio() . ' - ' . $user->getEmail(),
                              'result' => $user->getId());
        }

        return $result;
    }

    private function buildQueryString()
    {
        $str = $this
            ->get('request')
            ->get('q');
        if ($str) {
            return '%' . mb_strtolower($str, mb_detect_encoding($str)) . '%';
        }

        return false;
    }
}