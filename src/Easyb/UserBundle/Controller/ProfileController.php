<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Easyb\UserBundle\Controller;

use Easyb\MainBundle\Controller\ControllerHelperTrait;
use Easyb\UserBundle\Entity\Job;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Controller\ProfileController as BaseController;

/**
 * Controller managing the user profile
 *
 * @author Christophe Coevoet <stof@notk.org>
 */
class ProfileController extends BaseController
{
    use ControllerHelperTrait;

    /**
     * Show the user
     */
    public function showAction()
    {
        //После редактирования профиля
        $url = $this->container->get('router')->generate('fos_user_profile_edit');

        return new RedirectResponse($url);
    }

    public function editAction(Request $request)
    {


        $this->getBreadCrumbsManager()->create(array ('EasyB' => 'homepage', 'Редактирование личных данных' => ''));
        return parent::editAction($request);
    }
}
