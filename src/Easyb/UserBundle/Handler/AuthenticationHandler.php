<?php
/**
 * Created by JetBrains PhpStorm.
 * User: scorp
 * Date: 22.05.13
 * Time: 20:14
 * To change this template use File | Settings | File Templates.
 */
namespace Easyb\UserBundle\Handler;

use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class AuthenticationHandler
    implements AuthenticationSuccessHandlerInterface
{
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $refererUrl = $request->headers->get('Referer');
        if ($request->isXmlHttpRequest()) {
            return new \Symfony\Component\HttpFoundation\Response($refererUrl, 302);
        }
    }
}