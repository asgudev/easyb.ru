<?php
/**
 * Created by JetBrains PhpStorm.
 * User: scorp
 * Date: 30.05.13
 * Time: 15:49
 * To change this template use File | Settings | File Templates.
 */
namespace Easyb\UserBundle\Twig\Extension;

use Easyb\UserBundle\Entity\Mail;
use Doctrine\ORM\EntityManager;

/**
 * Class MailExtenstion
 *
 * @package Easyb\UserBundle\Twig\Extenstion
 */
class MailExtension extends \Twig_Extension
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('EasybUserBundle:Mail');
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array (
            'mail_have_custom_template' => new \Twig_Function_Method($this, 'haveCustomTemplate', array ()),
            'get_custom_mail_subject'   => new \Twig_Function_Method($this, 'getCustomMailSubject', array ()),
            'get_custom_mail_text'      => new \Twig_Function_Method($this, 'getCustomMailText', array ())
        );
    }

    public function haveCustomTemplate($type)
    {
        return $this->repository->findOneByType($type);
    }

    public function getCustomMailSubject($type, $username, $url)
    {
        $mail = $this->repository->findOneByType($type);
        $subject = $mail->getSubject();
        $subject = str_replace("[username]", $username, $subject);
        $subject = str_replace("[confirmationUrl]", $url, $subject);

        return $subject;
    }

    public function getCustomMailText($type, $username, $url)
    {
        $mail = $this->repository->findOneByType($type);
        $text = $mail->getMessage();
        $text = str_replace("[username]", $username, $text);
        $text = str_replace("[confirmationUrl]", $url, $text);

        return trim($text);
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'mail.extension';
    }
}