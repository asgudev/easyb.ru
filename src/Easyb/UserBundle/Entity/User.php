<?php

namespace Easyb\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="Easyb\UserBundle\Entity\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $fio
     * @ORM\Column(name="fio", type="string", length=255, nullable=true)
     */
    protected $fio;

    /**
     * @var string $linkedin
     * @ORM\Column(name="linkedin", type="string", length=255, nullable=true)
     * @Assert\Regex(
     *     pattern="/^(https?:\/\/)?(www.)?linkedin.com\/.+$/",
     *     message="Внимание! Размещению подлежит только ссылка на социальную сеть LinkedIn."
     * )
     */
    protected $linkedin;

    /**
     * @var \Datetime $createdAt
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var boolean $reviewNotification
     * @ORM\Column(name="review_notification", type="boolean")
     */
    protected $reviewNotification;

    /**
     * @var ArrayCollection $jobs
     * @ORM\OneToMany(targetEntity="Easyb\UserBundle\Entity\Job", mappedBy="user", cascade={"remove"})
     */
    protected $jobs;

    /**
     * @var boolean $registrationComplete
     * @ORM\Column(name="registration_complete", type="boolean", nullable=true)
     */
    protected $registrationComplete;

    /**
     * @var integer $offerNotification
     * @ORM\Column(name="offer_notification", type="integer", nullable=true)
     */
    protected $offerNotification;

    /**
     * @var integer $demandNotification
     * @ORM\Column(name="demand_notification", type="integer", nullable=true)
     */
    protected $demandNotification;

    /**
     * construct method
     */
    public function __construct()
    {
        parent::__construct();

        $this->reviewNotification = true;
        $this->registrationComplete = false;
        $this->createdAt = new \DateTime();
        $this->jobs = new ArrayCollection();
        $this->offerNotification = 0;
        $this->demandNotification = 0;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set reviewNotification
     *
     * @param boolean $reviewNotification
     *
     * @return User
     */
    public function setReviewNotification($reviewNotification)
    {
        $this->reviewNotification = $reviewNotification;

        return $this;
    }

    /**
     * Get reviewNotification
     *
     * @return boolean
     */
    public function getReviewNotification()
    {
        return $this->reviewNotification;
    }

    /**
     * Add jobs
     *
     * @param \Easyb\UserBundle\Entity\Job $jobs
     *
     * @return User
     */
    public function addJob(Job $jobs)
    {
        $this->jobs[] = $jobs;

        return $this;
    }

    /**
     * Remove jobs
     *
     * @param \Easyb\UserBundle\Entity\Job $jobs
     */
    public function removeJob(Job $jobs)
    {
        $this->jobs->removeElement($jobs);
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJobs()
    {
        $jobs = [];
        foreach ($this->jobs as $job)
        {
            if (!$job->isDeleted())
            {
                $jobs[] = $job;
            }
        }
        return $jobs;
    }

    /**
     * Set fio
     *
     * @param string $fio
     * @return User
     */
    public function setFio($fio)
    {
        $this->fio = $fio;

        return $this;
    }

    /**
     * Get fio
     *
     * @return string
     */
    public function getFio()
    {
        return $this->fio;
    }

    /**
     * Set registrationComplete
     *
     * @param boolean $registrationComplete
     *
     * @return User
     */
    public function setRegistrationComplete($registrationComplete)
    {
        $this->registrationComplete = $registrationComplete;

        return $this;
    }

    /**
     * Get registrationComplete
     *
     * @return boolean
     */
    public function getRegistrationComplete()
    {
        return $this->registrationComplete;
    }

    /**
     * Set offerNotification
     *
     * @param integer $offerNotification
     * @return User
     */
    public function setOfferNotification($offerNotification)
    {
        $this->offerNotification = $offerNotification;
    
        return $this;
    }

    /**
     * Get offerNotification
     *
     * @return integer 
     */
    public function getOfferNotification()
    {
        return $this->offerNotification;
    }

    /**
     * Set demandNotification
     *
     * @param integer $demandNotification
     * @return User
     */
    public function setDemandNotification($demandNotification)
    {
        $this->demandNotification = $demandNotification;
    
        return $this;
    }

    /**
     * Get demandNotification
     *
     * @return integer 
     */
    public function getDemandNotification()
    {
        return $this->demandNotification;
    }

    /**
     * Set linkedin
     *
     * @param string $linkedin
     * @return User
     */
    public function setLinkedin($linkedin)
    {
        $this->linkedin = $linkedin;

        return $this;
    }

    /**
     * Get linkedin
     *
     * @return string
     */
    public function getLinkedin()
    {
        return $this->linkedin;
    }
}