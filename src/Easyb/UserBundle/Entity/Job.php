<?php

namespace Easyb\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Easyb\AdvertBundle\Entity\AdvertFile;
use Easyb\MainBundle\Entity\Category;
use Easyb\MainBundle\Entity\City;
use Easyb\MainBundle\Entity\Slugable\SlugableInterface;
use Easyb\MainBundle\Entity\Slugable\SlugableTrait;
use Gedmo\Mapping\Annotation as Gedmo;
use Imagecow\Image;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Job
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Easyb\UserBundle\Entity\JobRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Job implements SlugableInterface
{
    use SlugableTrait;

    const UPLOAD_DIR_SHOW = 'show';
    const UPLOAD_DIR_ADVERT = 'files';
    const UPLOAD_DIR_LOGO = 'logo';

    public static $sizes = array(
        self::UPLOAD_DIR_SHOW => array(
            'width' => 290,
            'height' => 163
        ),
        self::UPLOAD_DIR_ADVERT => array(
            'width' => 130,
            'height' => 130
        ),
        self::UPLOAD_DIR_LOGO => array(
            'width' => 211,
            'height' => 211
        ),
    );

    /**
     * @var string $linkedin
     * @ORM\Column(name="linkedin", type="string", length=255, nullable=true)
     * @Assert\Regex(
     *     pattern="/^(https?:\/\/)?(www.)?linkedin.com\/.+$/",
     *     message="Внимание! Размещению подлежит только ссылка на социальную сеть LinkedIn."
     * )
     */
    protected $linkedin;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotNull()
     */
    private $name;
    /**
     * @var string
     *
     * @ORM\Column(name="position", type="string", length=255)
     * @Assert\NotNull()
     */
    private $position;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Assert\NotBlank(message="Заполните описание компании")
     * @Assert\Length(
     *      min=500,
     *      minMessage="Правило № 2. Запрещено размещать текст менее чем на 500 знаков и более чем на 2000 знаков. Причина: Объем текста прямо пропорционально влияет на поисковую выдачу. Чем информативнее текст, тем он полезнее, тем выше он в выдаче поисковиков.",
     *      max=2000,
     *      maxMessage="Правило № 2. Запрещено размещать текст менее чем на 500 знаков и более чем на 2000 знаков. Причина: Объем текста прямо пропорционально влияет на поисковую выдачу. Чем информативнее текст, тем он полезнее, тем выше он в выдаче поисковиков."
     * )
     */
    private $description;
    /**
     * @var string
     *
     * @ORM\Column(name="site", type="string", length=255, nullable=true)
     */
    private $site;
    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Assert\Email(message="Неверный email")
     */
    private $email;
    /**
     * @var string
     *
     * @ORM\Column(name="work_email", type="string", length=255, nullable=true)
     * @Assert\Email(message="Неверный email")
     */
    private $work_email;
    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="Введите номер телефона")
     */
    private $phone;
    /**
     * @var string
     *
     * @ORM\Column(name="skype", type="string", length=255, nullable=true)
     */
    private $skype;
    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=255, nullable=true)
     */
    private $logo;
    /**
     * @var City
     *
     * @ORM\ManyToOne(targetEntity="Easyb\MainBundle\Entity\City")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $city;
    /**
     * @var User $user
     *
     * @ORM\ManyToOne(targetEntity="Easyb\UserBundle\Entity\User", inversedBy="jobs")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;
    /**
     * @var string
     */
    private $file;
    /**
     * @var Category $category
     * @Assert\Count(
     *     min = "1",
     *     max = "3",
     *     minMessage = "Вы должны узказать хотя бы одну сферу деятельности",
     *     maxMessage = "Вы можете указать не более трех сфер деятельности"
     * )
     * @ORM\ManyToMany(targetEntity="Easyb\MainBundle\Entity\Category", inversedBy="jobs")
     */
    private $categories;


    /**
     * @var boolean
     *
     * @ORM\Column(name="is_approved", type="boolean", nullable=true)
     */
    private $isApproved = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_improved", type="boolean", nullable=true)
     */
    private $isImproved;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=true)
     */
    private $isDeleted;

    /**
     * @var array $invalidRules
     * @ORM\Column(name="invalid_rules", type="array", nullable=true)
     */
    protected $invalidRules;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     * @Assert\Length(
     *      min=5,
     *      max=2000
     * )
     */
    private $notes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="improved_at", type="datetime", nullable=true)
     */
    private $improvedAt;

    /**
     * @var ArrayCollection $files
     * @ORM\OneToMany(targetEntity="Easyb\AdvertBundle\Entity\AdvertFile", mappedBy="job", cascade={"persist"}, orphanRemoval=true)
     */
    private $files;


    /**
     * @ORM\Column(name="organization_type", type="string", nullable=true)
     */
    private $organizationType;

    /**
     * @ORM\Column(name="organization_id", type="string", nullable=true)
     */
    private $organizationId;


    /**
     * @ORM\Column(name="phone_mobile", type="string", nullable=true)
     */
    private $phoneMobile;


    public function __construct()
    {
        $this->files = new ArrayCollection();
        $this->invalidRules = array();

        $this->isApproved       = false;
        $this->isImproved       = false;
        $this->isDeleted        = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Job
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Job
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set position
     *
     * @param string $position
     *
     * @return Job
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Job
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get site
     *
     * @return string
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set site
     *
     * @param string $site
     *
     * @return Job
     */
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Job
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get skype
     *
     * @return string
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * Set skype
     *
     * @param string $skype
     *
     * @return Job
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set logo
     *
     * @param string $logo
     *
     * @return Job
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Easyb\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param \Easyb\UserBundle\Entity\User $user
     *
     * @return Job
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get city
     *
     * @return \Easyb\MainBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set city
     *
     * @param \Easyb\MainBundle\Entity\City $city
     *
     * @return Job
     */
    public function setCity(City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @ORM\PreFlush()
     */
    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->file) {
            return;
        }
        if ($this->logo) {
            $this->removeUpload();
        }
        $name = uniqid() . '.' . $this->file->guessExtension();
        $this->file->move($this->getUploadRootDir() . '/original', $name);
        foreach (self::$sizes as $key => $size) {
            $this->resizeImage($name, '/' . $key . '/', $size['width'], $size['height']);
        }
        $this->setLogo($name);
        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
        if ($this->logo) {
            foreach (self::$sizes as $name => $size) {
                unlink($this->getAbsolutePath($name));
            }
            $this->logo = null;
        }
    }

    /**
     * @param string $size
     *
     * @return null|string
     */
    private function getAbsolutePath($size = 'original')
    {
        return null === $this->logo ? null : $this->getUploadRootDir() . '/' . $size . '/' . $this->logo;
    }

    /**
     * @return string
     */
    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../../web' . $this->getUploadDir();
    }

    /**
     * @return string
     */
    protected function getUploadDir()
    {
        return '/upload/job/logo';
    }

    private function resizeImage($fileName, $dir, $weight, $height)
    {
        $filePath = $this->getUploadRootDir() . $dir . $fileName;
        $this->checkDirectory($this->getUploadRootDir() . $dir);
        $image = Image::create();
        $image->load($this->getUploadRootDir() . '/original/' . $fileName);
        $image->resize($weight, $height);
        $image->save($filePath);
    }

    private function checkDirectory($directory)
    {
        if (!is_dir($directory)) {
            if (false === @mkdir($directory, 0777, true)) {
                throw new FileException(sprintf('Unable to create the "%s" directory', $directory));
            }
        } elseif (!is_writable($directory)) {
            throw new FileException(sprintf('Unable to write in the "%s" directory', $directory));
        }
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name ?: 'n/a';
    }

    public function getLogoToAdmin()
    {
        return null === $this->logo ? '/bundles/easybmain/img/no-img.png' : $this->getWebLogo(self::UPLOAD_DIR_ADVERT);
    }

    public function getWebLogo($size)
    {
        if (array_key_exists($size, self::$sizes)) {
            return $this->getUploadDir() . '/' . $size . '/' . $this->logo;
        }
    }

    /**
     * @return array
     */
    public function getAllFiles()
    {
        $files = array();
        foreach (self::$sizes as $key => $size) {
            $files[] = $this->getAbsolutePathResizedFile($key);
        }
        $files[] = $this->getAbsolutePath();

        return $files;
    }

    /**
     * @param string $size
     *
     * @return null|string
     */
    public function getAbsolutePathResizedFile($size)
    {
        if (array_key_exists($size, self::$sizes)) {
            return null === $this->logo ? null : $this->getUploadRootDir() . '/' . $size . '/' . $this->logo;
        }
    }

    /**
     * @return string
     */
    public function getDefaultLogo()
    {
        return '/bundles/easybmain/img/no-img.png';
    }

    /**
     * Get site
     *
     * @return string
     */
    public function getLink()
    {
        $result = str_replace("http://", "", $this->site);

        return $result;
    }

    /**
     * Get linkedin
     *
     * @return string
     */
    public function getLinkedin()
    {
        return $this->linkedin;
    }

    /**
     * Set linkedin
     *
     * @param string $linkedin
     * @return User
     */
    public function setLinkedin($linkedin)
    {
        $this->linkedin = $linkedin;

        return $this;
    }

    /**
     * Get work_email
     *
     * @param string $work_email
     *
     * @return Job
     */
    public function getWorkEmail()
    {
        return $this->work_email;
    }

    /**
     * Set work_email
     *
     * @param string $work_email
     *
     * @return Job
     */
    public function setWorkEmail($work_email)
    {
        $this->work_email = $work_email;

        return $this;
    }

    /**
     * Add categories
     *
     * @param \Easyb\MainBundle\Entity\CategoryCategory $category
     *
     * @return User
     */
    public function addCategory(Category $category)
    {
        $this->categories[] = $category;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param \Easyb\MainBundle\Entity\Category $category
     */
    public function removeCategory(Category $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @return boolean
     */
    public function isApproved()
    {
        return $this->isApproved;
    }

    /**
     * @param boolean $isApproved
     */
    public function setIsApproved($isApproved)
    {
        $this->isApproved = $isApproved;
    }

    /**
     * @return boolean
     */
    public function isImproved()
    {
        return $this->isImproved;
    }

    /**
     * @param boolean $isImproved
     */
    public function setIsImproved($isImproved)
    {
        $this->isImproved = $isImproved;
    }

    public function isOffer()
    {
        return $this instanceof Offer;
    }

    /**
     * Add files
     *
     * @param \Easyb\AdvertBundle\Entity\AdvertFile $file
     *
     * @return AbstractAdvert
     */
    public function addFile(AdvertFile $file)
    {
        $file->setJob($this);
        $this->files->add($file);

        return $this;
    }

    /**
     * Remove files
     *
     * @param \Easyb\AdvertBundle\Entity\AdvertFile $files
     */
    public function removeFile(AdvertFile $files)
    {
        $this->files->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param string $invalidRules
     *
     * @return bool
     */
    public function hasInvalidRule($invalidRules)
    {
        return in_array(strtoupper($invalidRules), $this->getInvalidRules(), true);
    }

    /**
     * @return array
     */
    public function getInvalidRules()
    {
        return $this->invalidRules;
    }

    /**
     * @param array $invalidRules
     * @return Job
     */
    public function setInvalidRules($invalidRules)
    {
        $this->invalidRules = $invalidRules;

        return $this;
    }

    /**
     * @param string $invalidRules
     *
     * @return $this
     */
    public function addInvalidRule($invalidRules)
    {
        $invalidRules = strtoupper($invalidRules);
        if (!in_array($invalidRules, $this->invalidRules, true)) {
            $this->invalidRules[] = $invalidRules;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param string $notes
     *
     * @return $this
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getImprovedAt()
    {
        return $this->improvedAt;
    }

    /**
     * @param \DateTime $improvedAt
     * @return Job
     */
    public function setImprovedAt($improvedAt = null)
    {
        $this->improvedAt = $improvedAt;

        return $this;
    }

    /**
     * @param boolean $isDeleted
     * @return Job
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
        return $this;
    }

    public function getJobHref()
    {
        if ($this->isDeleted()) {
            return $this->name;
        } else {

        }
    }

    /**
     * @return boolean
     */
    public function isDeleted()
    {
        return $this->isDeleted;
    }


    /**
     * @return mixed
     */
    public function getOrganizationType()
    {
        return $this->organizationType;
    }

    /**
     * @param mixed $organizationType
     */
    public function setOrganizationType($organizationType)
    {
        $this->organizationType = $organizationType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * @param mixed $organizationId
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;
    }

    /**
     * @return mixed
     */
    public function getPhoneMobile()
    {
        return $this->phoneMobile;
    }

    /**
     * @param mixed $phoneMobile
     */
    public function setPhoneMobile($phoneMobile)
    {
        $this->phoneMobile = $phoneMobile;
        return $this;
    }
}