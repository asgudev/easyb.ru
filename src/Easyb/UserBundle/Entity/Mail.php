<?php

namespace Easyb\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mail
 *
 * @ORM\Table(name="mails")
 * @ORM\Entity(repositoryClass="Easyb\UserBundle\Entity\MailRepository")
 */
class Mail
{
    /**
     * Письмо при регистрации
     */
    const TYPE_REGISTRATION = 1;
    /**
     * при сбросе пароля
     */
    const TYPE_RESETTING    = 2;
    /**
     * предложение одобрено
     */
    const TYPE_DEMAND_APPROVE  = 3;

    /**
     * предложение отклонено
     */
    const TYPE_DEMAND_DECLINE  = 11;

    /**
     * отклик на спрос
     */
    const TYPE_DEMAND_REACTION = 4;
    /**
     * автопоиск
     */
    const TYPE_AUTOSEARCH = 5;
    /**
     * созданные админом объявления
     */
    const TYPE_ADMIN_CREATED = 6;
    /**
     * отправка админом предложения на доработку
     */
    const TYPE_ADMIN_OFFER_IMPROVE = 7;
    /**
     * отправка админом тендера на доработку
     */
    const TYPE_ADMIN_DEMAND_IMPROVE = 8;
    /**
     * одобрение админом предложения
     */
    const TYPE_ADMIN_OFFER = 9;
    /**
     * одобрение админом тендера
     */
    const TYPE_ADMIN_DEMAND = 10;
    /**
     * одобрение админом тендера
     */
    const TYPE_JOB_APPROVE = 12;

    /**
     * Дисквалификация из тендера
     */
    const TYPE_PROPOSAL_DISQUALIFICATION = 13;

    /**
     * Отправка КП на тендер
     */
    const TYPE_PROPOSAL_SEND = 14;

    /**
     * Получение КП на тендер
     */
    const TYPE_PROPOSAL_RECIEVE = 15;

    /**
     * Отправка компании на доработку
     */
    const TYPE_ADMIN_JOB_IMPROVE = 22;

    public static $types = [
        self::TYPE_REGISTRATION => 'Подтверждение регистрации',
        self::TYPE_RESETTING    => 'Сброс пароля'
    ];

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="smallint")
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=255)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     */
    private $message;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Mail
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return Mail
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Mail
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->subject?:'n/a';
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Mail
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}