<?php
/**
 * Created by IntelliJ IDEA.
 * User: User
 * Date: 22.01.2016
 * Time: 19:07
 */

namespace Easyb\UserBundle\Events;

use Symfony\Component\EventDispatcher\Event;

use Easyb\UserBundle\Entity\Job;

class JobEvent extends Event
{
    /**
     * Компания была одобрена
     */
    const ADMIN_JOB_APPROVE = 'job.approve';

    /**
     * Компания была одобрена
     */
    const ADMIN_JOB_IMPROVE = 'job.improve';

    /**
     * @var Job
     */
    private $job;

    public function __construct(Job $job = null)
    {
        $this->job = $job;
    }


    /**
     * @return Job
     */
    public function getJob()
    {
        return $this->job;
    }

}