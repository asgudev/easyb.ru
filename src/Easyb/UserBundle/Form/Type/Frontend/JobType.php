<?php
namespace Easyb\UserBundle\Form\Type\Frontend;

use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use Doctrine\ORM\EntityRepository;
use Easyb\AdvertBundle\Form\Type\FrontendFileType;
use Easyb\MainBundle\Entity\City;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class JobType
 *
 * @package Easyb\UserBundle\Form\Type\Frontend
 */
class JobType extends AbstractType
{
    private $em;

    public function __construct(Doctrine $doctrine)
    {
        $this->em = $doctrine->getManager();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', null, array('label' => 'Наименование юридического лица',
            'required' => true,
            'attr' => array(
                'placeholder' => "Наименование"
            ),
        ));

        $builder->add('position', null, array(
            'label' => 'Должность',
            'required' => true,
            'attr' => array(
                'placeholder' => "Менеджер"
            ),
        ));

        $builder->add('description', null, array('label' => 'Метка в шаблоне, т.к. HTML!',
            'required' => false,
            'attr' => array('class' => 'ckeditor')));

        $builder->add('site', null, array(
            'label' => 'Сайт',
            'required' => true,
            'attr' => array(
                "placeholder" => "http://example.com/"
            ),
        ));

        $builder->add('email', null, array('label' => 'Общий почтовый ящик компании',
            'required' => true,
            'attr' => array(
                "placeholder" => "info@example.ru"
            ),
        ));
        $builder->add('work_email', null, array('label' => 'Электронный адрес Вашей рабочей почты',
            'required' => true,
            'attr' => array(
                'placeholder' => "Sidorov.Mikhail@example.ru"
            )
        ));
        $builder->add('phoneMobile', null, array(
            'label' => 'Рабочий телефон',
            'required' => true,
            'attr' => array(
                "class" => "phone",
                "placeholder" => "+7 (999) 999-99-99"
            ),
        ));

        $builder->add('phone', null, array(
            'label' => 'Мобильный телефон',
            'required' => true,
            'attr' => array(
                "class" => "phone",
                "placeholder" => "+7 (999) 999-99-99"
            ),
        ));

        $builder->add('linkedin', null, array('label' => 'Ссылка на страницу компании в LinkedIn',
            "required" => false,
            'attr' => array(
                'placeholder' => 'http://linkedin.com/company/...'
            ),
        ));
        $builder->add('skype', null,
            array(
                'label' => 'Skype',
                'attr' => array(
                    'placeholder' => 'skype'
                )
            ));

        $builder->add("organizationType", "choice", array(
            'label' => 'ОПФ',
            'empty_value' => 'Нет',
            'choices' => [
                "ООО" => "ООО",
                "ОАО" => "ОАО",
                "ЗАО" => "ЗАО",
                "АО" => "АО",
                "ПАО" => "ПАО",
                "ИП" => "ИП"
            ],
            'required' => true,
        ));

        $builder->add('organizationId', 'text',
            array(
                'label' => 'ИНН', 'attr' =>
                [
                    'placeholder' => 1234567890,
                    'size' => 12,
                ]
            ));


        $builder->add('categories', 'entity',
            array(
                'label' => 'Укажите сферу деятельности компании (не более трех)',
                'class' => 'Easyb\MainBundle\Entity\Category',
                'required' => true,
                'multiple' => true,
                'expanded' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->where("c.id = c.parent")
                        ->orderBy('c.name', 'ASC');
                }
            ));
        /*
                $builder->add('parent', 'entity', array('label' => 'Регион',
                    'class' => 'Easyb\MainBundle\Entity\City',
                    'data_class' => 'Easyb\MainBundle\Entity\City',
                    'required' => false,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('c')
                            ->where("c.id = c.parent")
                            ->orderBy('c.id', 'ASC');
                    },
                ));*/

        $builder->add('city', 'hidden', array('label' => 'Город',
            'required' => true,
        ));

        $builder->add('logo', null, array('required' => false,
            'attr' => array('hidden' => 'true')
        ));

        $builder->add('files', 'file_collection', array(
            'type' => new FrontendFileType(),
            'required' => false,
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false
        ));

        $builder->add('file', 'uploadify', array('uploaderPath' => 'save_photo',
            'required' => false,
            'swfPath' => 'bundles/easybmain/js/uploadify/uploadify.swf',
            'success' => 'addPhoto(JSON.parse(data));'
        ));

        $builder
            ->get('city')->addModelTransformer(new CallbackTransformer(
                function ($output) {
                    if ($output != null)
                        return $output->getId();
                    else return 0;
                },
                function ($input) {
                    $city = $this->em->getRepository("EasybMainBundle:City")->find($input);
                    return $city;
                }));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array('class' => 'Easyb\\UserBundle\\Entity\\Job'));
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'job';
    }
}