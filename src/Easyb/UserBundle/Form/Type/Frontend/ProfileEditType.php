<?php
/**
 * Created by JetBrains PhpStorm.
 * User: shustrik
 * Date: 5/21/13
 * Time: 2:52 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Easyb\UserBundle\Form\Type\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class ProfileEditType
 *
 * @package Easyb\UserBundle\Form\Type\Frontend
 */
class ProfileEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('fio', 'hidden', array('label' => 'Имя и Фамилия'));
        $builder->add('linkedin', null, array(
            'label' => 'Ссылка на Ваш профиль в LinkedIn',
            "required" => false,
            'attr' => array(
                'placeholder' => 'http://linkedin.com/profile/...'
            ),
        ));
        $builder->add('email', null, array('label' => "Ваш электронный ящик, на который будут поступать уведомления"));
        $builder->add('reviewNotification', 'hidden', array(
            'required' => false,
            'data' => '1'
        ));

        $builder->add('plainPassword', 'repeated', array(
            'type' => 'password',
            'required' => false,
            'options' => array('translation_domain' => 'FOSUserBundle'),
            'first_options' => array('label' => 'form.new_password'),
            'second_options' => array('label' => 'form.new_password_confirmation'),
            'invalid_message' => 'fos_user.password.mismatch',
        ));

//        $constraint = new UserPassword();
//        $builder->add('current_password', 'password', array (
//                                                            'label'              => 'form.current_password',
//                                                            'translation_domain' => 'FOSUserBundle',
//                                                            'mapped'             => false,
//                                                            'constraints'        => $constraint,
//                                                            'required'           => false
//                                                      ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Easyb\UserBundle\Entity\User',
            'intention' => 'profile',
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'profile_edit';
    }
}