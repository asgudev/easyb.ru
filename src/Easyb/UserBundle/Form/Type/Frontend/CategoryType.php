<?php
/**
 * Created by IntelliJ IDEA.
 * User: User
 * Date: 13.01.2016
 * Time: 17:19
 */

namespace Easyb\UserBundle\Form\Type\Frontend;

use Doctrine\ORM\EntityRepository;

use Easyb\MainBundle\Entity\Category;

use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\ChoiceList\View\ChoiceView;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CategoryType extends AbstractType
{

    private $category;

    public function getParent()
    {
        return 'entity';
    }

    public function getName()
    {
        return 'category_entity_type';
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('category', 'entity', array(
            'label' => 'Раздел каталога',
            'class' => 'Easyb\MainBundle\Entity\Category',
            'required' => true,
            /*'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('c')
                    ->where("c.parent = c.id")
                    ->orderBy('c.name', 'ASC');
            }*/
        ));

        $formModifier = function (FormInterface $form, Category $category = null) {

            $this->category = $category;
            $form->add('subcategory', 'entity', array(
                'class' => 'Easyb\MainBundle\Entity\Category',
                'label' => 'Подкатегория',
                'multiple' => true,
                'expanded' => true,
                'required' => false,
                'query_builder' => function (EntityRepository $cr) {
                    $query =  $cr->createQueryBuilder('c')
                        ->where("c.parent = :category")
                        ->andWhere("c.id != :category")
                        ->setParameter("category", $this->category)
                        ->orderBy('c.id', 'ASC')
                    ;
                    return $query;
                }
            ));
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                // this would be your entity, i.e. SportMeetup
                $data = $event->getData();

                $formModifier($event->getForm(), $data->getCategory());
            }
        );

        $builder->get('category')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                // It's important here to fetch $event->getForm()->getData(), as
                // $event->getData() will get you the client data (that is, the ID)
                $category = $event->getForm()->getData();


                // since we've added the listener to the child, we'll have to pass on
                // the parent to the callback functions!
                $formModifier($event->getForm()->getParent(), $category);
            }
        );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver ) {
        $resolver->setDefaults(array(
            'query_builder' => function ( EntityRepository $repo ) {
                return $repo->createQueryBuilder('e')->where('e.parent = e.id');
            }
        ));
    }

}