<?php
/**
 * Created by JetBrains PhpStorm.
 * User: scorp
 * Date: 21.05.13
 * Time: 14:16
 * To change this template use File | Settings | File Templates.
 */
namespace Easyb\UserBundle\Form\Type\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CompleteRegistrationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('fio', 'text', array(
            'required' => true,
            'label'    => 'Имя и Фамилия'
        ));
        $builder->add('plainPassword', 'repeated', array(
            'type'            => 'password',
            'required'        => true,
            'options'         => array ('translation_domain' => 'FOSUserBundle'),
            'first_options'   => array ('label' => 'Пароль'),
            'second_options'  => array ('label' => 'Повторите пароль'),
            'invalid_message' => 'Пароли не совпадают',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Easyb\\UserBundle\\Entity\\User',
            'validation_groups' => array('complete_registration')
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return "complete_registration_type";
    }
}