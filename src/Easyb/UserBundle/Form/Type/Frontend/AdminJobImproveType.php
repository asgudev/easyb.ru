<?php

namespace Easyb\UserBundle\Form\Type\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class AdminJobImproveType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rule1', 'checkbox', [
                'label' => 'Правило № 1',
                'label_attr' => ['class' => 'note-label'],
                'required' => false,
                'mapped' => false
            ])
            ->add('rule4', 'checkbox', [
                'label' => 'Правило № 4',
                'label_attr' => ['class' => 'note-label'],
                'required' => false,
                'mapped' => false
            ])
            ->add('rule5', 'checkbox', [
                'label' => 'Правило № 5',
                'label_attr' => ['class' => 'note-label'],
                'required' => false,
                'mapped' => false
            ])
            ->add('notes', 'textarea', [
                    'label' => 'Замечания',
                    'label_attr' => ['class' => 'note-label'],
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'Введите Ваши замечания для указанного объявления ...'
                    ]]
            );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Easyb\\UserBundle\\Entity\\Job',
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'admin_job_improve_type';
    }
}