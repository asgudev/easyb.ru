<?php
/**
 * Created by JetBrains PhpStorm.
 * User: scorp
 * Date: 21.05.13
 * Time: 20:55
 * To change this template use File | Settings | File Templates.
 */

namespace Easyb\UserBundle\Form\EventListener;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class InitialUserDataListener implements EventSubscriberInterface
{
    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(FormEvents::BIND => 'onBind');
    }

    /**
     * @param FormEvent $event
     */
    public function onBind(FormEvent $event)
    {
        $data = $event->getData();
        $data->setUsername($data->getEmail());
        $data->setPlainPassword(rand());
        $event->setData($data);
    }
}
