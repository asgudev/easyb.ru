<?php
/**
 * Created by JetBrains PhpStorm.
 * User: shustrik
 * Date: 5/22/13
 * Time: 2:17 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Easyb\UserBundle\Manager;


use Doctrine\ORM\EntityManager;
use Easyb\AdvertBundle\Events\DemandEvent;
use Easyb\AdvertBundle\Events\OfferEvent;
use Easyb\UserBundle\Entity\Job;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Validator\ValidatorInterface;


/**
 * Class JobManager
 *
 * @package Easyb\UserBundle\Manager
 */
class JobManager
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    /**
     * @var \Symfony\Component\EventDispatcher\EventDispatcher
     */
    private $dispatcher;
    /**
     * @var \Symfony\Component\Validator\Validator
     */
    private $validator;
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repository;

    /**
     * @param \Doctrine\ORM\EntityManager $em
     * @param \Symfony\Component\EventDispatcher\EventDispatcher $dispatcher
     * @param \Symfony\Component\Validator\Validator $validator
     */
    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->validator = $validator;
        $this->repository = $em->getRepository('EasybUserBundle:Job');
    }

    public function createInstance()
    {
        return new Job();
    }

    /**
     * @param Job $job
     */
    public function create(Job $job)
    {
        $this->save($job);
    }

    /**
     * @param Job $job
     */
    public function update(Job $job)
    {
        $this->save($job);
    }

    public function remove(Job $job)
    {
        //$this->em->remove($job);
        $dispatcher = $this->dispatcher;

        $demands = $this->em->getRepository("EasybAdvertBundle:Demand")->findByJob($job);
        foreach ($demands as $demand) {
            $event = new DemandEvent($demand);
            $dispatcher->dispatch(DemandEvent::DELETE, $event);
        }

        $offers = $this->em->getRepository("EasybAdvertBundle:Offer")->findByJob($job);
        foreach ($offers as $offer) {
            $event = new OfferEvent($offer);
            $dispatcher->dispatch(OfferEvent::DELETE, $event);
        }


        $job->setIsDeleted(true);
        $this->em->flush();
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @param Job $object
     *
     * @throws \Exception
     */
    private function save(Job $object)
    {
        $object->setFile(null);
        try {
            $errors = $this->validator->validate($object);
            if (count($errors) > 0) {
                throw new \Exception($errors);
            }
            $this->em->persist($object);
            $this->em->flush();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function adminApprove(Job $job)
    {
        $job->setIsApproved(true);
        $job->setNotes(null);
        $job->setInvalidRules(array());
        $job->setIsImproved(false);
        $job->setImprovedAt(null);

        $this->save($job);
    }

    public function adminImprove(Job $job)
    {
        $job->setIsApproved(false);
        $job->setIsImproved(true);
        $job->setImprovedAt(new \DateTime());
        $this->save($job);

    }

    public function adminSetNotesWithRules(Job $job, $notes, $rule1 = false, $rule4 = false, $rule5 = false)
    {
        $job->setInvalidRules(array());

        if ((bool)$rule1) {
            $job->addInvalidRule('Запрещено размещать copy-past. Вся информация проверяется на уникальность администратором. Причина: Поисковые машины индексируют только уникальный текст. Если Вы хотите, чтобы Вас нашли и в поисковых системах, то необходимо следовать требованиями поисковиков. Уникальность текста рекомендуется проверять на сайте Text.ru.');
        }

        if ((bool)$rule4) {
            $job->addInvalidRule('Запрещено рекламировать сторонние ресурсы.');
        }

        if ((bool)$rule5) {
            $job->addInvalidRule('Запрещено дублировать информацию на сайте.');
        }

        $job->setNotes((string)$notes);

        $this->save($job);
    }

    public function adminSetNotes(Job $job, $notes)
    {
        $job->setInvalidRules(array());
        $job->setNotes((string)$notes);
        $this->save($job);
    }

    public function adminClearNotes(Job $job)
    {

        $job->setNotes(null);
        $job->setInvalidRules(array());
        $this->save($job);
    }

    public function adminClearImproved(Job $job)
    {
        $job->setIsImproved(false);
        $job->setImprovedAt(null);
        $this->save($job);
    }

    public function getSimilarJobs(Job $job)
    {
        return $this->em->getRepository("EasybUserBundle:Job")->findSimilarJobs($job);
    }
}