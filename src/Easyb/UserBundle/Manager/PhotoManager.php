<?php
/**
 * Created by JetBrains PhpStorm.
 * User: scorp
 * Date: 6/12/13
 * Time: 11:57 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Easyb\UserBundle\Manager;

use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Validator\Validator;
use Imagecow\Image;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Easyb\UserBundle\Entity\Job;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Easyb\UserBundle\Entity\User;

/**
 * Class PhotoManager
 *
 * @package Easyb\UserBundle\Manager
 */
class PhotoManager
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;
    /**
     * @var \Easyb\AdvertBundle\Entity\DemandRepository
     */
    protected $repository;

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository('EasybUserBundle:Job');
    }

    public function upload(UploadedFile $file)
    {
        if (null === $file) {
            return;
        }
        $name = uniqid() . '.' . $file->guessExtension();
        $this->checkDirectory($this->getUploadRootDir() . '/original');
        $file->move($this->getUploadRootDir() . '/original', $name);
        foreach (Job::$sizes as $key => $size) {
            $this->resizeImage($name, '/' . $key . '/', $size['width'], $size['height']);
        }
        $file = null;

        return [
            'name'  => $name,
            'photo' => $this->getUploadDir() . '/' . Job::UPLOAD_DIR_SHOW . '/' . $name
        ];
    }

    private function resizeImage($fileName, $dir, $weight, $height)
    {
        $filePath = $this->getUploadRootDir() . $dir . $fileName;
        $this->checkDirectory($this->getUploadRootDir() . $dir);
        $image = Image::create();
        $image->load($this->getUploadRootDir() . '/original/' . $fileName);
        $image->resize($weight, $height);
        $image->save($filePath);
    }

    /**
     * @return string
     */
    protected function getUploadDir()
    {
        return '/upload/job/logo';
    }

    /**
     * @return string
     */
    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../../web' . $this->getUploadDir();
    }

    /**
     * @param $directory
     *
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     */
    private function checkDirectory($directory)
    {
        if (!is_dir($directory)) {
            if (false === @mkdir($directory, 0777, true)) {
                throw new FileException(sprintf('Unable to create the "%s" directory', $directory));
            }
        } elseif (!is_writable($directory)) {
            throw new FileException(sprintf('Unable to write in the "%s" directory', $directory));
        }
    }

    /**
     * @param string $logoName
     * @param User   $user
     *
     * @return bool
     */
    public function remove($logoName, User $user)
    {
        $job = $this->repository->findOneByLogo($logoName);
        if ($job && $job->getUser() == $user) {
            $job->removeUpload();
            $this->saveJob($job);
        }

        return true;
    }

    /**
     * @param Job $job
     *
     * @throws \Exception
     */
    private function saveJob(Job $job)
    {
        try {
            $this->em->persist($job);
            $this->em->flush();
        } catch (\Exception $e) {
            throw $e;
        }
    }
}