<?php
/**
 * Created by JetBrains PhpStorm.
 * User: scorp
 * Date: 5/28/13
 * Time: 17:45 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Easyb\UserBundle\Manager;


use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Validator\Validator;

use Easyb\UserBundle\Entity\Report;
use Easyb\UserBundle\Entity\User;

/**
 * Class ReportManager
 *
 * @package Easyb\UserBundle\Manager
 */
class ReportManager implements ReportManagerInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    /**
     * @var \Symfony\Component\EventDispatcher\EventDispatcher
     */
    private $dispatcher;
    /**
     * @var \Symfony\Component\Validator\Validator
     */
    private $validator;
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repository;

    /**
     * @param \Doctrine\ORM\EntityManager                        $em
     * @param \Symfony\Component\EventDispatcher\EventDispatcher $dispatcher
     * @param \Symfony\Component\Validator\Validator             $validator
     */
    public function __construct(EntityManager $em, EventDispatcher $dispatcher, Validator $validator)
    {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->validator = $validator;
        $this->repository = $em->getRepository('EasybUserBundle:Report');
    }

    /**
     * @param Report $report
     *
     * @return mixed|void
     */
    public function create(Report $report)
    {
        $this->save($report);
    }

    /**
     * @param Report $report
     *
     * @return mixed|void
     */
    public function delete(Report $report)
    {
        $this->em->remove($report);
        $this->em->flush();
    }

    /**
     * @param Report $object
     *
     * @throws \Exception
     */
    private function save(Report $object)
    {
        try {
            $errors = $this->validator->validate($object);
            if (count($errors) > 0) {
                throw new \Exception($errors);
            }
            $this->em->persist($object);
            $this->em->flush();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param User $user
     * @param User $author
     *
     * @return Report|mixed
     */
    public function createInstance(User $user, User $author)
    {
        $report = new Report();
        $report->setUser($user);
        $report->setAuthor($author);

        return $report;
    }

}