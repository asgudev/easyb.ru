<?php
/**
 * Created by JetBrains PhpStorm.
 * User: scorp
 * Date: 5/28/13
 * Time: 17:44 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Easyb\UserBundle\Manager;

use Easyb\UserBundle\Entity\Report;
use Easyb\UserBundle\Entity\User;

/**
 * Class ReportManagerInterface
 *
 * @package Easyb\UserBundle\Manager
 */
interface ReportManagerInterface
{
    /**
     * @param Report $report
     *
     * @return mixed
     */
    public function create(Report $report);

    /**
     * @param Report $report
     *
     * @return mixed
     */
    public function delete(Report $report);

    /**
     * @param User $user
     * @param User $author
     *
     * @return mixed
     */
    public function createInstance(User $user, User $author);
}