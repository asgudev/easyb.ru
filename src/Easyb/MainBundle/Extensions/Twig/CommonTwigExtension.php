<?php
namespace Easyb\MainBundle\Extensions\Twig;

use EM\SphinxSearchBundle\Services\Search\Sphinxsearch;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CommonTwigExtension extends \Twig_Extension
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return array('highlight'    => new \Twig_Filter_Method($this, 'highlight', array('is_safe' => array('html'))));
    }

    /**
     * @param $string
     * @param $index
     * @param $highlightWord
     *
     * @return mixed
     */
    public function highlight($string, $index, $highlightWord)
    {
        $sphinx = new Sphinxsearch('localhost',  $this->container->getParameter('sphinxsearch_port'));
        $result = $sphinx->buildExcerpts(array(strip_tags($string)), $index, $highlightWord, array('limit' => '50'));

        return $result[0];
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'common_extension';
    }
}

