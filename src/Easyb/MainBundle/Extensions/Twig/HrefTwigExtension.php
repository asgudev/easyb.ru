<?php
/**
 * Created by IntelliJ IDEA.
 * User: Pc
 * Date: 26.01.2016
 * Time: 0:29
 */

namespace Easyb\MainBundle\Extensions\Twig;


class HrefTwigExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('link', array($this, 'linkFilter')),
        );
    }

    public function linkFilter($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
    {
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        $price = '$'.$price;

        return $price;
    }

    public function getName()
    {
        return 'app_extension';
    }
}