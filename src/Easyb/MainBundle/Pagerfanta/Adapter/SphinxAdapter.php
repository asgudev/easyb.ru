<?php

/*
 * This file is part of the Pagerfanta package.
 *
 * (c) Pablo Díez <pablodip@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Easyb\MainBundle\Pagerfanta\Adapter;

use Pagerfanta\Adapter\AdapterInterface;
use \Doctrine\Common\Collections\ArrayCollection;

use EM\SphinxSearchBundle\Services\Search\Sphinxsearch;

/**
 * SolariumAdapter.
 *
 * @author Igor Wiedler <igor@wiedler.ch>
 *
 * @api
 */
class SphinxAdapter implements AdapterInterface
{
    private $client;
    private $query;
    private $indexes;
    private $defaultIndex;
    private $doctrine;

    /**
     * Constructor.
     *
     * @param \EM\SphinxSearchBundle\Services\Search\Sphinxsearch $client
     * @param Solarium_Query_Select                               $query           A Solarium select query.
     *
     * @param                                                     $indexes
     * @param bool                                                $defaultIndex
     * @param                                                     $doctrine
     *
     * @api
     */
    public function __construct(Sphinxsearch $client, $query, $indexes, $defaultIndex = false, $doctrine)
    {
        $this->client = $client;
        $this->query = $query;
        $this->indexes = $indexes;
        $this->defaultIndex = $defaultIndex;
        $this->doctrine = $doctrine;
    }

    /**
     * {@inheritdoc}
     */
    public function getNbResults()
    {
        return $this->getIndexNbResults($this->defaultIndex ? $this->defaultIndex : $this->indexes[0]);
    }


    /**
     * @param $index
     *
     * @return int
     */
    public function getIndexNbResults($index)
    {
        $options = $this->buildIndexes();
        $searchResults = $this->client->search($this->query, $this->indexes, $options);

        return $searchResults['total_found'];
    }

    /**
     * {@inheritdoc}
     */
    public function getSlice($offset, $length)
    {
        $options = $this->buildIndexes($offset, $length);
        return $this->getResultSet($this->indexes, $options);
    }

    /**
     * @param $indexes
     * @param $options
     *
     * @return ArrayCollection
     */
    private function getResultSet($indexes, $options)
    {
        $material = new ArrayCollection();
        $searchResults = $this->client->search($this->query, $indexes, $options);
        if (isset($searchResults['matches'])) {
            $keys = array_keys($searchResults['matches']);
            $material = $this->doctrine->getRepository('EasybAdvertBundle:AbstractAdvert')->findAllMaterial($keys);
        }
        return $material;
    }

    /**
     * @param int $offset
     * @param int $length
     *
     * @return array
     */
    private function buildIndexes($offset = 0, $length = 1)
    {
        return [
            'result_offset' => $offset,
            'result_limit' => $length
        ];
    }
}
