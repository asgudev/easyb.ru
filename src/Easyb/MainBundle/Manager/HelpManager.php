<?php
/**
 * Created by JetBrains PhpStorm.
 * User: scorp
 * Date: 6/11/13
 * Time: 14:45 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Easyb\MainBundle\Manager;

use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Validator\Validator;
use Easyb\MainBundle\Entity\Help;

/**
 * Class HelpManager
 *
 * @package Easyb\MainBundle\Manager
 */
class HelpManager
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    /**
     * @var \Symfony\Component\EventDispatcher\EventDispatcher
     */
    private $dispatcher;
    /**
     * @var \Symfony\Component\Validator\Validator
     */
    private $validator;
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repository;

    /**
     * @param \Doctrine\ORM\EntityManager                        $em
     * @param \Symfony\Component\EventDispatcher\EventDispatcher $dispatcher
     * @param \Symfony\Component\Validator\Validator             $validator
     */
    public function __construct(EntityManager $em, EventDispatcher $dispatcher, Validator $validator)
    {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->validator = $validator;
        $this->repository = $em->getRepository('EasybMainBundle:Help');
    }

    /**
     * @param Help $help
     *
     * @return mixed|void
     */
    public function create(Help $help)
    {
        $this->save($help);
    }

    /**
     * @param Help $help
     *
     * @return mixed|void
     */
    public function delete(Help $help)
    {
        $this->em->remove($help);
        $this->em->flush();
    }

    /**
     * @param Help $help
     *
     * @throws \Exception
     */
    private function save(Help $help)
    {
        try {
            $errors = $this->validator->validate($help);
            if (count($errors) > 0) {
                throw new \Exception($errors);
            }
            $this->em->persist($help);
            $this->em->flush();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param string $email
     * @param string $text
     *
     * @return Help|mixed
     */
    public function createInstance($email, $text)
    {
        $help = new Help();
        $help->setEmail($email);
        $help->setText($text);

        return $help;
    }

}