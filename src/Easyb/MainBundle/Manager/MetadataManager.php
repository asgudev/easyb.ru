<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 03.10.14
 * Time: 11:27
 */

namespace Easyb\MainBundle\Manager;

use Doctrine\ORM\EntityManager;
use Easyb\AdvertBundle\Entity\AbstractAdvert;
use Easyb\AdvertBundle\Entity\Demand;
use Easyb\AdvertBundle\Entity\Offer;
use Easyb\MainBundle\Entity\Article;
use Easyb\MainBundle\Entity\SiteSettings;
use Easyb\UserBundle\Entity\Job;

/**
 * Class MetadataManager
 * @package Easyb\MainBundle\Manager
 */
class MetadataManager
{
    const TITLE = 'Easyb - реклама услуг бесплатно, коммерческие тендеры, сайт услуг.';
    const DESCRIPTION = 'Размещайте рекламу предоставляемых услуг бесплатно, рекламируйте компанию в каталоге услуг для бизнеса, объявляйте тендеры на оказание услуг, объявляйте конкурс на оказание услуг, добавляйте деловые услуги.';
    const KEYWORDS = 'коммерческие тендеры, тендеры, площадки тендеров, закупки тендеры, тендеры России, оказание услуг, предоставление услуг, портал услуг, сайт услуг, реклама бесплатно, размещение рекламы, моя реклама объявления, разместить рекламу, дать рекламу';

    const  SERVICE_NAME = '[service_name]';
    const  COMPANY_NAME = '[company_name]';
    const  SERVICE_SECTION = '[service_section]';
    private static $methods = array(Article::CLASS =>'articleMetadata',
                                    Offer::CLASS => 'advertMetadata',
                                    Demand::CLASS => 'advertMetadata',
                                    Job::CLASS => 'jobMetadata',
                                    SiteSettings::CLASS => 'siteSettingsMetadata'
    );
    private static $routes = array('catalog_offer' => 'catalogOfferMetadata',
                                   'catalog_demand' => 'catalogDemandMetadata'
    );
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }



    /**
     * @param string $route
     * @param object null $object
     * @param object null $customMetadata
     * @param int null $categoryId
     * @return array
     */
    public function getMetadata($route, $object = null, $customMetadata = null, $categoryId = null)
    {
        $metadata = array();
        if ( is_object($object) and (isset (self::$methods[get_class($object)]))) {
             $metadata = $this->{self::$methods[get_class($object)]}($object, $customMetadata);
        }

        if (isset (self::$routes[$route]) and $categoryId) {
            $metadata = $this->{self::$routes[$route]} ($categoryId);
        }
        $metadata = $this->getLayoutMetadata($metadata);

        return $metadata;
    }

    private function getLayoutMetadata($metadata)
    {
        $metadata['title'] = $metadata['title'] ?: self::TITLE;
        $metadata['description'] = $metadata['description'] ?: self::DESCRIPTION;
        $metadata['keywords'] = $metadata['keywords'] ?: self::KEYWORDS;
        //$metadata['footer_text'] = $metadata['footer_text'] ?: '';
        //$metadata['head_text'] = $metadata['head_text'] ?: '';

        return $metadata;
    }

    public function articleMetadata($object)
    {
        $metadata = array();
        $metadata['title'] = $object->getTitle();
        $metadata['description'] = $object->getMetaDescription();
        $metadata['keywords'] = $object->getMetaKeywords();

        return $metadata;
    }

    public function advertMetadata($object, $customMetadata)
    {
        $metadata = array();
        $metadata['title'] = $this->replaceAdvert($customMetadata->getTitle(), $object);
        $metadata['description'] = $this->replaceAdvert($customMetadata->getDescription(), $object);
        $metadata['keywords'] = $object->getMetaDescription() ? : $this->replaceAdvert($customMetadata->getKeywords(), $object);

        return $metadata;
    }

    /**
     * @param string $text
     * @param object $advert
     * @return mixed
     */
    private function replaceAdvert($text, $advert)
    {
        $text = str_replace(array(self::SERVICE_NAME, self::COMPANY_NAME, self::SERVICE_SECTION),
            array($advert->getName(), $advert->getJob()->getName(), $advert->getCategory()->getName()), $text);

        return $text;
    }

    public function jobMetadata($object, $customMetadata)
    {
        $metadata = array();
        $metadata['title'] = $this->replaceJob($customMetadata->getTitle(), $object);
        $metadata['description'] = $this->replaceJob($customMetadata->getDescription(), $object);
        $metadata['keywords'] = $this->replaceJob($customMetadata->getKeywords(), $object);

        return $metadata;
    }

    /**
     * @param string $text
     * @param object $job
     * @return mixed
     */
    private function replaceJob($text, $job)
    {
        $advert = $this->em->getRepository('EasybAdvertBundle:AbstractAdvert')->findOneByJob($job->getId());


        if ($advert != null) {
            $text = str_replace(array(self::SERVICE_NAME, self::COMPANY_NAME, self::SERVICE_SECTION),
                array($advert->getName(), $job->getName(), $advert->getCategory()->getName()), $text);
        }

        return $text;
    }

    public function siteSettingsMetadata($object)
    {
        $metadata = array();
        if (!empty($object)) {
            $metadata['title'] = $object->getTitle();
            $metadata['description'] = $object->getDescription();
            $metadata['keywords'] = $object->getKeywords();
            $metadata['footer_text'] = $object->getFooterText();
        }

        return $metadata;
    }

    public function catalogOfferMetadata($categoryId)
    {
        $metadata = array();
        $categoryMetadata = $this->em->getRepository('EasybMainBundle:Category')->findOneById($categoryId);
        $metadata = array(
            'title' => $categoryMetadata->getTitleOffer(),
            'description' => $categoryMetadata->getDescriptionOffer(),
            'keywords' => $categoryMetadata->getKeywordsOffer(),
            'footer_text' => $categoryMetadata->getFooterTextOffer(),
            'head_text' => $categoryMetadata->getHeadTextOffer()
        );

        return $metadata;
    }

    public function catalogDemandMetadata($categoryId)
    {
        $categoryMetadata = $this->em->getRepository('EasybMainBundle:Category')->findOneById($categoryId);
        $metadata = array(
            'title' => $categoryMetadata->getTitleDemand(),
            'description' => $categoryMetadata->getDescriptionDemand(),
            'keywords' => $categoryMetadata->getKeywordsDemand(),
            'footer_text' => $categoryMetadata->getFooterTextDemand(),
            'head_text' => $categoryMetadata->getHeadTextDemand()
        );

        return $metadata;
    }
}