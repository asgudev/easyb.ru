<?php
/**
 * User: scorp
 * Date: 7/14/13
 * Time: 23:27 PM
 */

namespace Easyb\MainBundle\Manager;

use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Validator\Validator;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Easyb\MainBundle\Entity\Banner;

/**
 * Class BannerManager
 *
 * @package Easyb\MainBundle\Manager
 */
class BannerManager
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;
    /**
     * @var \Easyb\AdvertBundle\Entity\DemandRepository
     */
    protected $repository;

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository('EasybMainBundle:Banner');
    }

    /**
     * @param UploadedFile $file
     *
     * @return array
     */
    public function upload(UploadedFile $file)
    {
        if (null === $file) {
            return;
        }
        $name = uniqid() . '.' . $file->guessExtension();
        $this->checkDirectory($this->getUploadRootDir());
        $file->move($this->getUploadRootDir(), $name);
        $file = null;

        return [
            'name'  => $name,
            'banner' => $this->getUploadDir() . '/' . $name
        ];
    }

    /**
     * @return string
     */
    protected function getUploadDir()
    {
        return '/upload/banners';
    }

    /**
     * @return string
     */
    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../../web' . $this->getUploadDir();
    }

    /**
     * @param $directory
     *
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     */
    private function checkDirectory($directory)
    {
        if (!is_dir($directory)) {
            if (false === @mkdir($directory, 0777, true)) {
                throw new FileException(sprintf('Unable to create the "%s" directory', $directory));
            }
        } elseif (!is_writable($directory)) {
            throw new FileException(sprintf('Unable to write in the "%s" directory', $directory));
        }
    }

    /**
     * @param string $attachName
     *
     * @return bool
     */
    public function remove($attachName)
    {
        $banner = $this->repository->findOneByAttach($attachName);
        if ($banner) {
            $banner->removeUpload();
            $this->saveBanner($banner);
        }

        return true;
    }

    /**
     * @param Banner $banner
     *
     * @throws \Exception
     */
    private function saveBanner(Banner $banner)
    {
        try {
            $this->em->persist($banner);
            $this->em->flush();
        } catch (\Exception $e) {
            throw $e;
        }
    }
}