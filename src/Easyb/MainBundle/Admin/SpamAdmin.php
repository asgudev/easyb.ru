<?php
namespace Easyb\MainBundle\Admin;

use Easyb\AdminBundle\Admin\Admin as BaseAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;

use Easyb\MainBundle\Entity\Banner;

class SpamAdmin extends BaseAdmin
{
    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('contents', null, [
                'required' => true,
                'label'    => 'Текст рассылки',
                'attr'     => array('class' => 'ckeditor')
            ]);
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('createdAt', null, [
                'label' => 'Дата'
            ])
            ->add('type', null, [
                'label' => 'Статус',
                'template' => 'EasybMainBundle:SpamAdmin:list_type.html.twig'
            ])
            ->add('contents', null, [
                'label' => 'Содержание'
            ]);

        $listMapper->add('_action', 'actions', [
            'actions' => [
                'edit'   => [],
                'delete' => []
            ]]);
    }

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        if (!$this->hasRequest()) {
            $this->datagridValues = [
                '_page'       => 1,
                '_per_page'   => $this->getMaxPerPage(),
                '_sort_order' => 'ASC',
                '_sort_by'    => 'id'
            ];
        }
    }


    public function configure()
    {
        $this->setTemplate('edit', 'EasybMainBundle:SpamAdmin:edit.html.twig');
    }
}