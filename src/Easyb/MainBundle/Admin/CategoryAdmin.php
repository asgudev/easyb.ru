<?php
namespace Easyb\MainBundle\Admin;

use Doctrine\ORM\EntityRepository;
use Easyb\AdminBundle\Admin\Admin as BaseAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Validator\ErrorElement;

class CategoryAdmin extends BaseAdmin
{
    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, array('label' => 'Категория',
                'required' => true,
                'help' => 'Название категории'))
            ->add('titleOffer', null, array('label' => 'Title', 'help' => 'Предложение'))
            ->add('parent', 'entity', array(
                'class' => 'Easyb\MainBundle\Entity\Category',
                'label' => 'Родительская категория',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->where("c.parent = c.id")
                        ->orderBy('c.name', 'ASC');
                }
            ))
            ->add('descriptionOffer', null, array('label' => 'Description', 'help' => 'Предложение'))
            ->add('keywordsOffer', null, array('label' => 'Keywords', 'help' => 'Предложение'))
            ->add('headTextOffer', null, array('label' => 'Текст',
                'help' => 'Предложение - верхний',
                'attr' => array('class' => 'ckeditor ckone')))
            ->add('footerTextOffer', null, array('label' => 'Текст',
                'help' => 'Предложение - нижний',
                'attr' => array('class' => 'ckeditor ckthree')))
            ->add('titleDemand', null, array('label' => 'Title', 'help' => 'Тендер'))
            ->add('descriptionDemand', null, array('label' => 'Description', 'help' => 'Тендер'))
            ->add('keywordsDemand', null, array('label' => 'Keywords', 'help' => 'Тендер'))
            ->add('headTextDemand', null, array('label' => 'Текст',
                'help' => 'Тендер - верхний',
                'attr' => array('class' => 'ckeditor cktwo')))
            ->add('footerTextDemand', null, array('label' => 'Текст',
                'help' => 'Тендер - нижний',
                'attr' => array('class' => 'ckeditor ckfour')));
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->addIdentifier('parent', null, array('label' => 'Родительская категория'))
            ->addIdentifier('name', null, array('label' => 'Категория'));

        $listMapper->add('_action', 'actions', array('actions' => array('delete' => array('label' => 'delete'),
            'edit' => array(),
            'show' => array(),
        )));
    }

    /**
     * Configuration show view
     *
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('name', null, array('label' => 'Категория'))
            ->add('parent', null, array('label' => 'Родительская категория'));
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('name', null, array('label' => 'Категория'))
            ->add('parent', null, array('label' => 'Родительская категория'));
    }

    public function configure()
    {
        $this->setTemplate('edit', 'EasybMainBundle:CategoryAdmin:edit.html.twig');
    }
}
