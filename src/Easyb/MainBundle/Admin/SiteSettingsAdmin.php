<?php
namespace Easyb\MainBundle\Admin;

use Easyb\AdminBundle\Admin\Admin as BaseAdmin;
use Easyb\MainBundle\Entity\SiteSettings;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;

use Easyb\MainBundle\Entity\Banner;

class SiteSettingsAdmin extends BaseAdmin
{
    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        if (in_array($this->getRoot()->getSubject()->getRoute(), SiteSettings::$SHOW_TEXT))
        {
            $formMapper = $this->getMetaFormFields($formMapper);
            $formMapper->add('footerText', null, [
                'required' => false,
                'label'    => 'Текст в футере',
                'attr'     => array('class' => 'ckeditor')
        ]);
        }
        else {
            $this->getMetaFormFields($formMapper);
        }

    }


    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
          ->addIdentifier('id')
          ->add('name', 'url', [
                'label' => 'Cтраница',
                'route' => array(
                    'name' => 'admin_easyb_main_sitesettings_edit',
                    'absolute' => true,
                    'identifier_parameter_name' => 'id')
          ])
        ;


        $listMapper->add('_action', 'actions', [
            'actions' => [
                'delete'   => array('label' => 'delete'),
                'edit'     => [],
                'show'     => [],
            ]]);
    }

    /**
     * Configuration show view
     *
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper)
    {
        if (in_array($this->getRoot()->getSubject()->getRoute(), SiteSettings::$SHOW_TEXT))
        {
            $formMapper = $this->getMetaShowFields($showMapper);
            $formMapper->add('footerText', null, [
                'label'    => 'Текст в футере',
            ]);
        }
        else {
            $this->getMetaShowFields($showMapper);
        }
    }

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        if (!$this->hasRequest()) {
            $this->datagridValues = [
                '_page'       => 1,
                '_per_page'   => $this->getMaxPerPage(),
                '_sort_order' => 'ASC',
                '_sort_by'    => 'id'
            ];
        }
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            ->remove('delete')
        ;
    }

    public function configure()
    {
        $this->setTemplate('edit', 'EasybMainBundle:SiteSettingsAdmin:edit.html.twig');
    }

    private function getMetaFormFields(FormMapper $formMapper)
    {
        return $formMapper
            ->add('name',null, [
                'required' => true,
                'label' => 'Название'
            ])
            ->add('title', null, [
                'required' => true,
                'label'    => 'Title'
            ])
            ->add('description', null, [
                'required' => true,
                'label'    => 'Description'
            ])
            ->add('keywords', null, [
                'required' => true,
                'label'    => 'Keywords'
            ])
            ;
    }

    private function getMetaShowFields(ShowMapper $showMapper)
    {
        return $showMapper
            ->add('name',null, [
                'label' => 'Название'
            ])
            ->add('title', null, [
                'label'    => 'Title'
            ])
            ->add('description', null, [
                'label'    => 'Description'
            ])
            ->add('keywords', null, [
                'label'    => 'Keywords'
            ])
            ;
    }
}