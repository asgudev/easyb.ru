<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 15.08.14
 * Time: 12:31
 */

namespace Easyb\MainBundle\Admin;

use Easyb\AdminBundle\Admin\Admin as BaseAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\RouterInterface;

use Easyb\MainBundle\Form\Type\ArticleFileType;
use Easyb\MainBundle\Entity\ArticleFile;

class ArticleAdmin extends BaseAdmin
{
    protected $em;

    /**
     * @param string          $code
     * @param string          $class
     * @param string          $baseControllerName
     * @param EntityManager   $entityManager
     */
    public function __construct($code, $class, $baseControllerName, EntityManager $entityManager)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->em     = $entityManager;

        if (!$this->hasRequest()) {
            $this->datagridValues = array(
                '_page'       => 1,
                '_per_page'   => $this->getMaxPerPage(),
                '_sort_order' => 'DESC',
                '_sort_by'    => 'createdAt'
            );
        }
    }

    public function configure()
    {
        $this->setTemplate('edit', 'EasybMainBundle:Admin:edit.html.twig');
    }

    public function getModelInstance($class)
    {
        return new $class;
    }

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, [
                'required' => true,
                'label'    => 'Заголовок статьи'
            ])
            ->add('title', null, [
                'required' => true,
                'label' => 'Тайтл'
            ])
            ->add('text', null, [
                'required' => true,
                'label'    => 'Текст статьи',
                'attr'     => array('class' => 'ckeditor')
            ])
            ->add('metaDescription', null, [
                'required' => false,
                'label'    => 'Мета Описание'
            ])
            ->add('metaKeywords', null, [
                'required' => false,
                'label'    => 'Мета keywords'
            ])
            ->add('files', 'collection', [
                'type'         => new ArticleFileType(),
                'allow_add'    => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label'        => 'Выберите файл и дайте ему имя'
            ])
        ;

        $formMapper->getFormBuilder()->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'preSubmit'));
        $formMapper->getFormBuilder()->addEventListener(FormEvents::SUBMIT, array($this, 'submit'));
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->addIdentifier('name', null, array('label' => 'Заголовок статьи'))
            //->add('text', null, array('label' => 'Текст статьи'))
            ->add('publicationAt', null, array('label' => 'Дата'));

        $listMapper->add('_action', 'actions', array('actions' => array(
            'delete'   => array('label' => 'delete'),
            'edit'     => array(),
            'show'     => array(),
        )));
    }

    /**
     * Configuration show view
     *
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name', null, array('label' => 'Название'))
            ->add('text', 'text', array('label' => 'Текст'))
            ->add('createdAt', null, array('label' => 'Дата'));
    }

    /**
     * @param DatagridMapper $filter
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {

    }

    /**
     * @param FormEvent $event
     */
    public function preSubmit(FormEvent $event)
    {
        $data = $event->getData();
        if (array_key_exists('files', $data)) {
            foreach ($data['files'] as $file) {
                $this->validateFiles($file['file'], $event->getForm());
            }
        }
    }

    /**
     * @param $file
     * @param $form
     *
     * @return bool
     */
    private function validateFiles($file, $form)
    {
        if ($file == null) {
            return false;
        }
        if (!array_key_exists(mb_convert_case($file->getClientOriginalExtension(), MB_CASE_LOWER), ArticleFile::$filesTypes)) {
            $form->addError(new FormError('Прикрепляемым Вами файл не соответствует формату или превышает установленный размер 10 Mb.'));
        }
    }

    /**
     * @param FormEvent $event
     */
    public function submit(FormEvent $event)
    {
        $data = $event->getData();
    }

    /**
     * Добавляем свой баш - обновить дату публикации
     *
     * @return array
     */
    public function getBatchActions()
    {
        // retrieve the default (currently only the delete action) actions
        $actions = parent::getBatchActions();

        // check user permissions
        if($this->hasRoute('edit') && $this->isGranted('EDIT') && $this->hasRoute('delete') && $this->isGranted('DELETE')){
            $actions['refresh_publication_date']=[
                'label'            => 'Обновить дату публикации',
                'ask_confirmation' => false // If true, a confirmation will be asked before performing the action
            ];

        }

        return $actions;
    }
}


