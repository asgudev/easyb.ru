<?php
namespace Easyb\MainBundle\Admin;

use Easyb\AdminBundle\Admin\Admin as BaseAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;

use Easyb\MainBundle\Entity\Banner;

class BannerAdmin extends BaseAdmin
{
    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, [
                'required' => true,
                'label'    => 'Расположение баннера',
                'disabled' => true
            ])
            ->add('type', 'choice', [
                'choices'  => Banner::$typeText,
                'required' => true,
                'label'    => 'Тип баннера'
            ])
            ->add('showAt', 'date', [
                'required' => false,
                'widget' => 'single_text',
                'format'  => 'dd-MM-yyyy',
                'label' => 'Показывать баннер c'
            ])
            ->add('showBy', 'date', [
                'required' => false,
                'widget' => 'single_text',
                'format'  => 'dd-MM-yyyy',
                'label' => 'Показывать баннер по'
            ])
            ->add('isActive', null, [
                'label'    => 'Активен?',
                'required' => false,
            ])
            ->add('contents', null, [
                'label'    => 'Контент',
                'required' => false,
                'attr'  => [
                    'class' => 'ckeditor'
                ]
            ])
            ->add('link', null, [
                'label'    => 'Ссылка на изображение',
                'required' => false,
                'help'     => 'Сначала введите URL ссылки, потом загрузите изображение.'
            ])
            ->add('attach', null, [
                'required' => false,
                'attr'     => ['hidden' => 'true']
            ])
            ->add('file', 'uploadbanner', [
                'label'        => 'Файл',
                'uploaderPath' => 'save_banner',
                'required'     => false,
                'swfPath'      => 'bundles/easybmain/js/uploadify/uploadify.swf',
                'success'      => 'addBanner(JSON.parse(data));'
            ]);
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
          ->add('id')
          ->addIdentifier('name', null, [
                'label'    => 'Расположение баннера',
          ])
          ->add('type', null, [
                'label' => 'Тип',
                'template' => 'EasybMainBundle:BannerAdmin:list_type.html.twig'
          ])
          ->add('isActive', null, [
                'label' => 'Активен?'
          ]);

        $listMapper->add('_action', 'actions', [
            'actions' => [
                'edit'     => [],
                'show'     => [],
            ]]);
    }

    /**
     * Configuration show view
     *
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
          ->add('name', null, [
                'label'  => 'Расположение баннера'
          ])
          ->add('contents', 'text', [
                'label' => 'Контент'
          ])
          ->add('link', null, [
              'label' => 'Ссылка'
          ])
          ->add('attach', null, [
              'label' => 'Файл'
          ])
          ->add('isActive', null, [
                'label' => 'Активен?'
          ]);
    }

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        if (!$this->hasRequest()) {
            $this->datagridValues = [
                '_page'       => 1,
                '_per_page'   => $this->getMaxPerPage(),
                '_sort_order' => 'ASC',
                '_sort_by'    => 'id'
            ];
        }
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            ->remove('delete')
        ;
    }

    public function configure()
    {
        $this->setTemplate('edit', 'EasybMainBundle:BannerAdmin:edit.html.twig');
    }
}