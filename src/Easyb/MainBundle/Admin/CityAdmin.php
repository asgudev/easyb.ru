<?php
namespace Easyb\MainBundle\Admin;

use Easyb\AdminBundle\Admin\Admin as BaseAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;

use Knp\Menu\ItemInterface as MenuItemInterface;

class CityAdmin extends BaseAdmin
{
    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
          ->add('name', null, [
                    'label'    => 'Город',
                    'required' => true,
                    'help'     => 'Название города'
          ])
          ->add('sort', null, [
                    'label'    => 'Номер сортировки',
                    'required' => true,
                    'help'     => 'Чем больше номер, тем ниже в списке'
          ])
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
          ->add('id')
          ->addIdentifier('name', null, array('label' => 'Город'))
          ->add('sort', null, array('label' => 'Номер сортировки'));

        $listMapper->add('_action', 'actions', array('actions' => array('delete'     => array('label' => 'delete'),
                                                                        'edit'       => array(),
                                                                        'show'       => array(),
                                                                        )));
    }

    /**
     * Configuration show view
     *
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
          ->add('id')
            ->add('name', null, array('label' => 'Город'))
            ->add('sort', null, array('label' => 'Номер сортировки'));
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
          ->add('name', null, array('label' => 'Город'));
    }

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        if (!$this->hasRequest()) {
            $this->datagridValues = array(
                '_page'       => 1,
                '_per_page'   => $this->getMaxPerPage(),
                '_sort_order' => 'ASC',
                '_sort_by'    => 'sort'
            );
        }
    }
}
