<?php

namespace Easyb\MainBundle\Controller;

use Easyb\MainBundle\Entity\City;
use Easyb\MainBundle\Form\Type\GetContatType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AjaxController extends Controller
{
    /**
     * @param $object
     *
     * @return array
     */
    private function buildResult($object)
    {
        $result = array();
        foreach ($object as $one) {
            $result[] = array('data' => array($one->getName(), $one->getId()),
                'value' => $one->getName(),
                'result' => $one->getId());
        }

        return $result;
    }

    /**
     * @return bool|string
     */
    private function buildQueryString()
    {
        $str = $this
            ->get('request')
            ->get('q');
        if ($str) {
            return '%' . mb_strtolower($str, mb_detect_encoding($str)) . '%';
        }

        return false;
    }

    /**
     * @Route("/search_job", name="search_job")
     */
    public function ajaxJobAction()
    {
        $result = array();
        $findString = $this->buildQueryString();
        if (!$findString) {
            return new JsonResponse($result);
        }
        $repository = $this->getDoctrine()->getRepository("EasybUserBundle:Job");
        $users = $repository->findAnyByName($findString);
        $result = $this->buildResult($users);

        return new JsonResponse($result);
    }

    /**
     * @Route("/search_city", name="search_city")
     */
    public function ajaxCityAction()
    {
        $result = array();
        $findString = $this->buildQueryString();
        if (!$findString) {
            return new JsonResponse($result);
        }
        $repository = $this->getDoctrine()->getRepository("EasybMainBundle:City");
        $users = $repository->findAnyByName($findString);
        $result = $this->buildResult($users);

        return new JsonResponse($result);
    }

    /**
     * @param City $city
     * @Route("/get_cities_ajax", name="get_cities_ajax")
     * @Method("POST")
     */
    public function getCitiesInRegion(Request $request)
    {
        $regionId = $request->request->get('regionId');
        $cities = $this->getDoctrine()->getRepository('EasybMainBundle:City')->findCitiesInRegion($regionId);


        $citiesArrray = [];

        foreach ($cities as $city) {
            $citiesArrray[$city->getId()] = $city->getName();
        }

        return new JsonResponse([
            "cities" => $citiesArrray,
        ]);
    }

    /**
     * @Route("/search_category", name="search_category")
     */
    public function ajaxCategoryAction()
    {
        $result = array();
        $findString = $this->buildQueryString();
        if (!$findString) {
            return new JsonResponse($result);
        }
        $repository = $this->getDoctrine()->getRepository("EasybMainBundle:Category");
        $users = $repository->findAnyByName($findString);
        $result = $this->buildResult($users);

        return new JsonResponse($result);
    }

    /**
     * @param City $city
     * @Route("/get_categories_ajax", name="get_categories_ajax")
     * @Method("POST")
     */
    public function getSubInCategory(Request $request)
    {
        $categoryId = $request->request->get('categoryId');

        $categories = $this->getDoctrine()->getRepository('EasybMainBundle:Category')->findSubcategoryAndSort($categoryId);
        $catArray = [];
        foreach ($categories as $cat) {
            $catArray[$cat->getId()] = $cat->getName();
        }
        return new JsonResponse([
            "categories" => $catArray,
        ]);
    }

    /**
     * @Route("/get_job_categories", name="get_job_categories")
     * @Method("POST")
     */
    public function getJobCategories(Request $request)
    {
        $jobId = $request->request->get("jobId");

        $categories = $this->getDoctrine()->getRepository('EasybUserBundle:Job')->find($jobId)->getCategories();
        $catArray = [];
        foreach ($categories as $cat) {
            $catArray[$cat->getId()] = $cat->getName();
        }
        return new JsonResponse([
            "categories" => $catArray,
        ]);
    }

    protected function getMetaData($object = null, $customMetadata = null)
    {
        return $this->get('easyb.manager.metadata')->getMetadata($this->getRequest()->get('_route'), $object, $customMetadata);
    }


    /**
     * @Route("/get_offer_contacts", name="get_contacts")
     */
    public function getOfferContactsAction(Request $request)
    {
        $form = $this->createForm(new GetContatType());

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $data = explode("/", $request->headers->get('referer'));
                $router = $this->get('router');

                $url = str_replace("http://easyb.ru", "", $request->headers->get('referer'));

                $params = $router->match($url);

                $type = $params["_route"];
                $slug = $params["slug"];

                if ($type == "show_offer") {
                    $offer = $this->getDoctrine()->getRepository('EasybAdvertBundle:Offer')->findOneBySlug($slug);
                    if (!$offer) return new Response(null);
                    return $this->render('EasybAdvertBundle:Offer:show.html.twig', [
                        'offer' => $offer,
                        'showContact' => true,
                        'similarAds' => null,
                        'metadata' => $this->getMetadata($offer, $this->getDoctrine()->getRepository('EasybAdvertBundle:AdvertMetaData')->findAll()[0]),
                        'files' => null,
                        'isOffer' => true,
                        'canAnswer' => null,
                        'hasReactions' => false,
                        'hasCategories' => false,
                        'canAskQuestion' => false,
                        'offers' => null,
                        'demands' =>  null,
                    ]);
                } elseif ($type == "show_demand") {
                    $demand = $this->getDoctrine()->getRepository('EasybAdvertBundle:Demand')->findOneBySlug($slug);
                    if (!$demand) return new Response(null);
                    return $this->render('EasybAdvertBundle:Demand:show.html.twig', [
                        'demand' => $demand,
                        'showContact' => true,
                        'similarAds' => null,
                        'metadata' => $this->getMetadata($demand, $this->getDoctrine()->getRepository('EasybAdvertBundle:AdvertMetaData')->findAll()[0]),
                        'files' => null,
                        'isOffer' => false,
                        'canAnswer' => null,
                        'hasReactions' => false,
                        'hasCategories' => false,
                        'canAskQuestion' => false,
                        'offers' => null,
                        'demands' =>  null,
                    ]);
                } elseif ($type == "show_job") {
                    $job = $this->getDoctrine()->getRepository('EasybUserBundle:Job')->findOneBySlug($slug);
                    if (!$job) return new Response(null);

                    return $this->render('EasybUserBundle:Job:show.html.twig', [
                        'metadata' => $this->getMetadata($job, $this->getDoctrine()->getRepository('EasybUserBundle:JobMetadata')->findAll()[0]),
                        'job' => $job,
                        'offers' => null,
                        'demands' =>  null,
                        'similarJobs' => null,
                        'showContact' => true,
                    ]);
                } else {
                    return new Response(null);
                }
            } else {
                return new Response(null);
            }
        }


        return $this->render('EasybMainBundle:Frontend:getContact.html.twig', [
            'form' => $form->createView()
        ]);
    }
}