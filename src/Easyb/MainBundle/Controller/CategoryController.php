<?php
/**
 * Created by IntelliJ IDEA.
 * User: User
 * Date: 06.03.2016
 * Time: 14:46
 */

namespace Easyb\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Easyb\UserBundle\Form\Type\Frontend\CompleteRegistrationType;
use Easyb\UserBundle\Form\Type\Frontend\ReportType;
use Easyb\UserBundle\Entity\Job;
use Easyb\UserBundle\Entity\Report;
use Easyb\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;

class CategoryController extends Controller
{

    /**
     * @Route("/test_cat", name="test_category_chain")
     */
    public function categoryChainAction(){

    }
}