<?php

namespace Easyb\MainBundle\Controller;

trait ControllerHelperTrait
{
    /**
     * @return \Easyb\MainBundle\BreadCrumbs\BreadCrumbsManager
     */
    public function getBreadCrumbsManager()
    {
        return $this->container->get('easyb.breadcrumbs.manager');
    }

    /**
     * @param \Symfony\Component\Form\AbstractType     $type
     * @param object                                   $object
     * @param array                                    $options
     *
     * @return \Symfony\Component\Form\Form
     */
    protected function getForm($type, $object, array $options = array ())
    {
        $form = $this->createForm($type, $object, $options);

        return $form;
    }
}