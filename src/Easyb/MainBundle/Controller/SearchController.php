<?php

namespace Easyb\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter as PagerAdapter;
use Symfony\Component\HttpFoundation\Request;

use Easyb\AdvertBundle\Entity\AbstractAdvert;
use Easyb\MainBundle\Pagerfanta\Adapter\SphinxAdapter;
use Easyb\MainBundle\Controller\ControllerHelperTrait;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/")
 */
class SearchController extends Controller
{
    use ControllerHelperTrait;

    const COUNT_ON_LIST_PAGE = 20;

    private static $emptyAdvert = array();

    private function getMetaData($object = null, $customMetadata = null)
    {
        return $this->get('easyb.manager.metadata')->getMetadata($this->getRequest()->get('_route'), $object, $customMetadata, $this->getFilter('categoryId'));
    }

    /**
     * @Route("/search_ajax", name="search_ajax")
     *
     * @return array
     */
    public function searchAjaxAction()
    {
        $form = $this->createForm($this->get('easyb.form.type.search_type'));
        $formData = $form->submit($this->getRequest())->getData();
        $this->setSearchFilters($formData);

        $indexToSearch = $this->getIndexToSearch($formData['advertTypeId']);
        $options = array (
            'result_offset' => 0,
            'result_limit'  => 10
        );
        $searchResults = $this->getSphinxSearch()->search('@name ' . $formData['text'], $indexToSearch, $options, false);
        if (!isset($searchResults['matches'])) {
            return new JsonResponse([]);
        }
        $resultsCollection = $this->getResults($searchResults);
        $result = array (
            'html' => $this->renderView('EasybMainBundle:Search:searchResults.html.twig',
                array (
                      'results'    => $resultsCollection,
                      'findString' => $this->getFilter('search_text')
                ))
        );

        return new JsonResponse($result);
    }

    /**
     * @Template()
     * @Route("/catalog", name="catalog")
     *
     * @return array
     */
    public function catalogAction()
    {
        // new redirect to new url
        // url see at - /catalog/offer_or_demand/maybe_city/maybe_category
        $request = $this->getRequest();
        $request->getSession()->remove('search_text');
        if ($categorySlug = $request->get('categorySlug')) {
            return $this->redirect($this->generateUrl($request->get('advertTypeId') == AbstractAdvert::DEMAND ? 'catalog_demand' : 'catalog_offer', array('url' => $categorySlug)));
        }

        $formData = $request->request->get('search_type');
        if (!empty($formData)) {
            if ($formData['text']) {
                $request->getSession()->set('search_text', $formData['text']);
            }
            if ($formData['advertTypeId'] == AbstractAdvert::DEMAND) {
                return $this->getUrl($formData, 'catalog_only_demand', 'catalog_demand');
            }
            if ($formData['advertTypeId'] == AbstractAdvert::OFFER) {
                return $this->getUrl($formData, 'catalog_only_offer', 'catalog_offer');
            }
        }

        return $this->searchData();
    }

    /**
     * @param array  $formData
     * @param string $url1
     * @param string $url2
     *
     * @return string
     */
    private function getUrl($formData, $url1, $url2)
    {
        if ($formData['categoryId'] || $formData['cityId'] || $formData['subcategoryId']) {
            $redirect = $this->redirect($this->generateUrl($url2, $this->setUrlParam($formData)));
        } else {
            $redirect = $this->redirect($this->generateUrl($url1, array()));
        }

        return $redirect;
    }

    /**
     * @param array $formData
     *
     * @return array
     */
    private function setUrlParam($formData)
    {
        $param = array();
        $city = $category = null;

        if ($formData['cityId']) {
            $city = $this->getDoctrine()->getRepository('EasybMainBundle:City')->findOneById($formData['cityId']);
        }
        if ($formData['categoryId']) {
            $category = $this->getDoctrine()->getRepository('EasybMainBundle:Category')->findOneById($formData['categoryId']);
        }
        if ($formData['subcategoryId']) {
            $category = $this->getDoctrine()->getRepository('EasybMainBundle:Category')->findOneById($formData['subcategoryId']);
        }

        if ($city) {
            if ($category) {
                $param['url'] = $category->getSlug() .'/'. $city->getSlug();
            } else {
                $param['url'] = $city->getSlug();
            }
        } elseif ($category) {
            $param['url'] = $category->getSlug();
        }

        return $param;
    }

    private function searchData($isBreadcrumb = null)
    {
        $siteSettings = $this->getDoctrine()->getRepository('EasybMainBundle:SiteSettings')->findBy(array('route' => $this->getRequest()->get('_route')));

        if ($this->getRequest()->query->get('search_text')) {
            $findString = $this->getRequest()->query->get('search_text');
            $this->getRequest()->getSession()->set('search_text', $findString);
        } else {
            $findString = $this->getFilter('search_text') ? $this->getFilter('search_text') : null;
        }

        $this->checkForRequestFilter($this->getRequest()->query);
        $form = $this->createForm($this->get('easyb.form.type.search_type'));
        if (!$isBreadcrumb) {
            $this->getBreadCrumbsManager()->create(['EasyB' => 'homepage', 'Каталог' => null]);
        }
        $this->setFiltersForCatalog($form, $this->getRequest());
        $indexToSearch = $this->getIndexToSearch($this->getFilter('advertTypeId'), true);
        $sphinxSearch = $this->getSphinxSearch($this->getRequest()->query->get('newResults') ? $this->getRequest()->query->get('newResults') : null);
        $this->resetViews($this->getRequest()->query->get('newResults'), $this->getRequest()->query->get('autoId'));
        $adverts = $this->getSphinxPager($sphinxSearch, $findString, $indexToSearch, $this->getRequest()->query->get('count') ? $this->getRequest()->query->get('count') : self::COUNT_ON_LIST_PAGE);
        //$cities = $this->getDoctrine()->getRepository('EasybMainBundle:City')->findAllAndSort();
        $regions = $this->getDoctrine()->getRepository('EasybMainBundle:City')->findRegionsAndSort();
        $categories = $this->getDoctrine()->getRepository('EasybMainBundle:Category')->getAllSortByName();
        $category = $this->getFilter('categoryId') ? $this->getDoctrine()->getRepository('EasybMainBundle:Category')->findOneById($this->getFilter('categoryId')) : false;

        return [
            'metadata' => array_key_exists("0", $siteSettings) ? $this->getMetadata($siteSettings[0]) : null,
            'adverts'    => $adverts,
            'isOffer' => $this->getFilter('advertTypeId') == AbstractAdvert::OFFER ? true : false,
            //'cities'     => $cities,
            'regions'     => $regions,
            'categories' => $categories,
            'form'       => $form->createView(),
            'city'       => $this->getFilter('cityId') ? $this->getDoctrine()->getRepository('EasybMainBundle:City')->findOneById($this->getFilter('cityId')) : null,
            'category'   => $category,
            'pageMessages' => $this->getPageMessages($category),
            'advertTypeMessage' => $this->getFilter('advertTypeId') == AbstractAdvert::OFFER ? 'Предложение' : 'Тендер'
        ];
    }

    /**
     * @Template("EasybMainBundle:Search:catalog.html.twig")
     * @Route("/demand/{url}", name="catalog_demand", requirements={"url" = ".+"})
     *
     * @return array
     */
    public function catalogDemandAction(Request $request)
    {
        $this->customFilter($request);

        $breadcrumbSlug = $this->getDoctrine()->getRepository('EasybMainBundle:Category')->findOneBySlug($request->attributes->get('_route_params')['url']);
        if ($breadcrumbSlug) {
            $this->getBreadCrumbsManager()->create(['EasyB' => 'homepage', 'Каталог' => 'catalog_only_demand', $breadcrumbSlug->getName() =>null]);
        } else {
            $this->getBreadCrumbsManager()->create(['EasyB' => 'homepage', 'Каталог' => 'catalog_only_demand']);
        }
        $request = $this->getRequest();
        $request->getSession()->set('advertTypeId', AbstractAdvert::DEMAND);
        if ($text = $request->getSession()->get('search_text')) {
            $request->query->set('search_text', $text);
        }

        return $this->searchData(true);
    }

    /**
     * @Template("EasybMainBundle:Search:onlySubjectCatalog.html.twig")
     * @Route("/demand/", name="catalog_only_demand")
     *
     * @return array
     */
    public function catalogOnlyDemandAction()
    {
        $request = $this->getRequest();
        $request->getSession()->remove('cityId');
        $request->getSession()->remove('categoryId');
        $request->getSession()->set('advertTypeId', AbstractAdvert::DEMAND);
        if ($text = $request->getSession()->get('search_text')) {
            $request->query->set('search_text', $text);
        }

        return $this->searchData();
    }

    /**
     * @Template("EasybMainBundle:Search:catalog.html.twig")
     * @Route("/offer/{url}", name="catalog_offer", requirements={"url" = ".+"})
     *
     * @return array
     */
    public function catalogOfferAction(Request $request)
    {
        $this->customFilter($request);

        $breadcrumbSlug = $this->getDoctrine()->getRepository('EasybMainBundle:Category')->findOneBySlug($request->attributes->get('_route_params')['url']);
        if ($breadcrumbSlug) {
            $this->getBreadCrumbsManager()->create(['EasyB' => 'homepage', 'Каталог' => 'catalog_only_offer', $breadcrumbSlug->getName() =>null]);
        } else {
            $this->getBreadCrumbsManager()->create(['EasyB' => 'homepage', 'Каталог' => 'catalog_only_offer']);
        }
        $request = $this->getRequest();
        $request->getSession()->set('advertTypeId', AbstractAdvert::OFFER);
        if ($text = $request->getSession()->get('search_text')) {
            $request->query->set('search_text', $text);
        }

        return $this->searchData(true);
    }

    /**
     * @Template("EasybMainBundle:Search:onlySubjectCatalog.html.twig")
     * @Route("offer/", name="catalog_only_offer")
     *
     * @return array
     */
    public function catalogOnlyOfferAction()
    {
        $request = $this->getRequest();
        $request->getSession()->remove('cityId');
        $request->getSession()->remove('categoryId');
        $request->getSession()->set('advertTypeId', AbstractAdvert::OFFER);
        if ($text = $request->getSession()->get('search_text')) {
            $request->query->set('search_text', $text);
        }

        return $this->searchData();
    }

    private function customFilter(Request $request)
    {
        $url = $request->get('url');
        $request->getSession()->remove('cityId');
        $request->getSession()->remove('categoryId');

        if ($url) {
            $urlParam = explode('/', $url);
            if (count($urlParam) > 3) {
                throw new NotFoundHttpException('404');
            }

                if (array_key_exists(0, $urlParam) && $urlParam[0]) {
                    $cityId = $this->getDoctrine()->getRepository('EasybMainBundle:City')->findOneBySlug($urlParam[0]);
                    if (!$cityId) {
                        $categoryId = $this->getDoctrine()->getRepository('EasybMainBundle:Category')->findOneBySlug($urlParam[0]);
                        if (!$categoryId) {
                            throw new NotFoundHttpException('Такой категории или города не существует.');
                        }
                        $request->query->set('categoryId', $categoryId->getId());
                        $request->getSession()->set('categoryId', $categoryId->getId());
                    } else {
                        $request->query->set('cityId', $cityId->getId());
                        $request->getSession()->set('cityId', $cityId->getId());
                    }
                }
            if (array_key_exists(1, $urlParam) && $urlParam[1]) {
                $cityId = $this->getDoctrine()->getRepository('EasybMainBundle:City')->findOneBySlug($urlParam[1]);
                if (!$cityId) {
                    throw new NotFoundHttpException('Такого города не существет');
                }
                $request->query->set('cityId', $cityId->getId());
                $request->getSession()->set('cityId', $cityId->getId());
            }
        }
    }

    /**
     * @param                                           $form
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function setFiltersForCatalog($form, Request $request)
    {
        if (!$request->headers->get('referer')) {
            $this->setFilters([
                              'categoryId'   => $request->query->get('categoryId') ? $request->query->get('categoryId') : null,
                              'cityId'       => $request->query->get('cityId') ? $request->query->get('cityId') : null,
                              'jobId'        => $request->query->get('jobId') ? $request->query->get('jobId') : null,
                              'advertTypeId' => $request->query->get('advertTypeId') ? $request->query->get('advertTypeId') : AbstractAdvert::OFFER,
                              'search_text'  => $request->query->get('search_text') ? $request->query->get('search_text') : null
                              ]);
        }
        if ($request->headers->get('referer') && !$request->request->get('search_type') && !$request->query->get('page')) {
            $this->setFilters([
                              'categoryId'   => $request->query->get('categoryId') ? $request->query->get('categoryId') : null,
                              'advertTypeId' => $request->query->get('advertTypeId'),
                              'cityId'       => $request->query->get('cityId') ? $request->query->get('cityId') : null,
                              'jobId'        => null,
                              'search_text'  => $request->query->get('search_text') ? $request->query->get('search_text') : null
                              ]);
        }
        if ($request->request->get('search_type')) {
            $formData = $form->submit($request)->getData();
            if ($request->query->get('advertTypeId')) {
                $formData['advertTypeId'] = $request->query->get('advertTypeId');
            }
            $this->setSearchFilters($formData);
        }
    }

    /**
     * @param $requestQuery
     */
    public function checkForRequestFilter($requestQuery)
    {
        if ($requestQuery->get('categoryId')) {
            $this->setFilters(['categoryId' => $requestQuery->get('categoryId')]);
            $this->setFilters(['jobId' => null]);
        }
        if ($requestQuery->get('jobId')) {
            $this->setFilters(['jobId' => $requestQuery->get('jobId')]);
            $this->setFilters(['categoryId' => null]);
        }
    }

    /**
     * @param array $filters
     */
    public function setFilters($filters = [])
    {

    }

    /**
     * @param $filter
     *
     * @return mixed
     */
    public function getFilter($filter)
    {
        return $this->getRequest()->getSession()->get($filter);
    }

    /**
     * @param $data
     */
    public function setSearchFilters($data)
    {
        $this->setFilters([
                          'categoryId'   => $data['categoryId'],
                          'cityId'       => $data['cityId'],
                          'advertTypeId' => $data['advertTypeId'],
                          'search_text'  => $data['text'],
                          'jobId'        => null
                          ]);
    }

    /**
     * @param $sphinxSearch
     * @param $query
     * @param $indexesToSearch
     *
     * @param $count
     *
     * @return \Pagerfanta\Pagerfanta
     */
    public function getSphinxPager($sphinxSearch, $query, $indexesToSearch, $count)
    {
        $paginator = new Pagerfanta(new SphinxAdapter($sphinxSearch, $query, $indexesToSearch, false, $this->getDoctrine()));
        $paginator->setMaxPerPage($count);
        $paginator->setCurrentPage($this->getPage(), false, true);

        return $paginator;
    }

    /**
     * @return int|mixed|null
     */
    public function getPage()
    {
        return $this
               ->getRequest()
               ->get('page') ? $this
                               ->getRequest()
                               ->get('page') : 1;
    }

    /**
     * @param bool $newResults
     *
     * @return \EM\SphinxSearchBundle\Services\Search\Sphinxsearch
     */
    public function getSphinxSearch($newResults = false)
    {
        /** @var \EM\SphinxSearchBundle\Services\Search\Sphinxsearch $sphinxSearch */
        $sphinxSearch = $this->get('em.search.sphinxsearch.search');
        $sphinxSearch->resetFilters();
        $sphinxSearch->setMatchMode(SPH_MATCH_EXTENDED);
        $sphinxSearch->SetSortMode(SPH_SORT_EXTENDED, 'date_public' . " DESC");
        if ($this->getFilter('categoryId')) {
            $sphinxSearch->setFilter('category', [$this->getFilter('categoryId')]);
        }
        if ($this->getFilter('cityId')) {
            $sphinxSearch->setFilter('city', [$this->getFilter('cityId')]);
        }
        if ($this->getFilter('jobId')) {
            $sphinxSearch->setFilter('job', [$this->getFilter('jobId')]);
        }
        if ($newResults) {
            $sphinxSearch->setFilterRange('date_public', $newResults, time());
        }

        return $sphinxSearch;
    }

    /**
     * @param $searchResults
     *
     * @return array
     */
    public function getResults($searchResults)
    {
        $resultsCollection = [];
        foreach ($searchResults['matches'] as $key => $options) {
            $material = $this->getDoctrine()->getRepository($options['attrs']['model_name'])->findOneById($key);
            $resultsCollection[$key]['object'] = $material;
            $resultsCollection[$key]['index'] = $options['attrs']['index'];
        }

        return $resultsCollection;
    }

    /**
     * @param      $advertTypeId
     *
     * @return string
     */
    public function getIndexToSearch($advertTypeId)
    {
        if ($advertTypeId == AbstractAdvert::DEMAND) {
            return ['Demand'];
        }

        return ['Offer'];
    }

    /**
     * @param $result
     * @param $id
     */
    private function resetViews($result, $id)
    {
        if ($result) {
            $this->get('easyb.autosearch.manager')->resetViews($id);
        }
    }

    private function getPageMessages($category)
    {
        $messages = array('catalog_offer' => sprintf("В данный момент в разделе \"%s\" размещенных коммерческих предложений нет.", $category),
                          'catalog_demand' => sprintf("В данный момент в разделе \"%s\" размещенных тендеров нет.", $category),
                          'catalog_only_offer' => "В данный момент в разделе \"Мое Предложение\" размещенных коммерческих предложений нет.",
                          'catalog_only_demand' => "В данный момент в разделе \"Мой Тендер\" размещенных тендеров нет."

        );

        return $messages;
    }
}
