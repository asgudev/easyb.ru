<?php
namespace Easyb\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Easyb\MainBundle\Entity\Banner;

class BannersController extends Controller
{
    /**
     * @Template()
     * @return array
     */
    public function upBannerAction()
    {
        $banner = $this->getDoctrine()->getRepository('EasybMainBundle:Banner')->getBanner(Banner::POSITION_UP);

        return $this->getBanner($banner);
    }

    /**
     * @Template("EasybMainBundle:Banners:upBanner.html.twig")
     * @return array
     */
    public function downBannerAction()
    {
        $banner = $this->getDoctrine()->getRepository('EasybMainBundle:Banner')->getBanner(Banner::POSITION_DOWN);

        return $this->getBanner($banner);
    }

    /**
     * @Template()
     * @return array
     */
    public function sideBannerAction()
    {
        $banner = $this->getDoctrine()->getRepository('EasybMainBundle:Banner')->getBanner(Banner::POSITION_RIGHT_FIRST);
        $bannerSecond = $this->getDoctrine()->getRepository('EasybMainBundle:Banner')->getBanner(Banner::POSITION_RIGHT_SECOND);
        $bannerThird = $this->getDoctrine()->getRepository('EasybMainBundle:Banner')->getBanner(Banner::POSITION_RIGHT_THIRD);
        $bannerGoogle = $this->getDoctrine()->getRepository('EasybMainBundle:Banner')->getBanner(Banner::POSITION_GOOGLE);

        $banners = [];
        if ($banner) $banners[$banner->getId()] = $this->getBanner($banner);
        if ($bannerSecond) $banners[$bannerSecond->getId()] = $this->getBanner($bannerSecond);
        if ($bannerThird) $banners[$bannerThird->getId()] = $this->getBanner($bannerThird);
        if ($bannerGoogle) $banners[$bannerGoogle->getId()] = $this->getBanner($bannerGoogle);

        return [
            'banners' => $banners
        ];
    }

    /**
     * @param Banner|null $banner
     *
     * @return array
     */
    private function getBanner($banner)
    {
        if ($banner) {
            if ($banner->getType() == Banner::TYPE_IMAGE) {
                return [
                    'image' => $banner->getWebAttach(),
                    'link'  => $banner->getLink()
                ];
            }
            if ($banner->getType() == Banner::TYPE_HTML_JAVA) {
                return [
                    'html' => $banner->getContents()
                ];
            }
            if ($banner->getType() == Banner::TYPE_FLASH) {
                return [
                    'flash' => $banner->getWebAttach(),
                    'id'    => $banner->getId()
                ];
            }
        }

        return [];
    }
}
