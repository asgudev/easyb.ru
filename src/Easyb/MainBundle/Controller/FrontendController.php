<?php

namespace Easyb\MainBundle\Controller;

use Easyb\MainBundle\Entity\Help;
use Easyb\MainBundle\Entity\SiteSettings;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Easyb\MainBundle\Form\Type\HelpType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/")
 */
class FrontendController extends Controller
{

    private function getMetaData($object = null, $customMetadata = null)
    {
        return $this->get('easyb.manager.metadata')->getMetadata($this->getRequest()->get('_route'), $object, $customMetadata);
    }

    /**
     * @Template()
     *
     * @Route("/", name="homepage")
     * @return array
     */
    public function indexAction()
    {
        $siteSettings = $this->getDoctrine()->getRepository('EasybMainBundle:SiteSettings')->findBy(array('route' => SiteSettings::MAIN_ROUTE));

        return [
            'metadata' => $this->getMetaData($siteSettings[0] ? : null)
        ];
    }

    /**
     * @Template()
     * @Route("/help", name="help")
     *
     * @return array
     */
    public function helpAction()
    {
        $siteSettings = $this->getDoctrine()->getRepository('EasybMainBundle:SiteSettings')->findBy(array('route' => SiteSettings::HELP_ROUTE));

        /** @var $helpManager \Easyb\MainBundle\Manager\HelpManager */
        $helpManager = $this->container->get('easyb.manager.help');
        $help = new Help();
        $form = $this->createForm(new HelpType(), $help);

        if ($this->getRequest()->getMethod() == 'POST') {
            $form->submit($this->getRequest());
            if ($form->isValid()) {
                $helpManager->create($help);

                return new Response($this->renderView('EasybMainBundle:Frontend:helpSuccess.html.twig'));
            }
        }

        return [
            'metadata' => $this->getMetaData($siteSettings[0] ? : null),
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/save_banner", name="save_banner")
     *
     * @return array
     */
    public function saveBannerAction()
    {
        $file = $this->getRequest()->files->get('filedata');
        $data = $this->get('easyb.manager.banner')->upload($file);

        return new JsonResponse([
            'banner' => $data['banner'],
            'name'   => $data['name']
        ]);
    }
}
