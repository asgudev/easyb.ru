<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 15.08.14
 * Time: 15:27
 */

namespace Easyb\MainBundle\Controller;

use Easyb\MainBundle\Entity\AbstractArticle;
use Easyb\MainBundle\Entity\ArticleFile;
use Easyb\MainBundle\Entity\SiteSettings;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Easyb\AdvertBundle\Entity\Reaction;
use Easyb\MainBundle\Entity\Article;
use Easyb\MainBundle\Entity\AbstractArticleRepository;

/**
 * @Route("/")
 */
class ArticleController extends AbstractArticleController
{

    public function getManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getMetaData($object = null, $customMetadata = null)
    {
        return $this->get('easyb.manager.metadata')->getMetadata($this->getRequest()->get('_route'), $object, $customMetadata);
    }

    /**
     * @Template()
     * @Route("/useful", name="useful")
     */
    public function indexAction()
    {
        $siteSettings = $this->getDoctrine()->getRepository('EasybMainBundle:SiteSettings')->findBy(array('route' => SiteSettings::USEFUL_ROUTE));

        $count = $this->getRequest()->query->get('count');
        $this->getBreadCrumbsManager()->create(['EasyB' => 'homepage', 'Полезная информация' => null]);
        $articles = $this->getPager($this->getManager()->getRepository('EasybMainBundle:Article')->createQueryBuilder('u')->add('select','u')->add('orderBy', 'u.publicationAt DESC')->getQuery(), $count ? $count : AbstractArticle::COUNT_ON_LIST_PAGE);
        return [
            'metadata' => $this->getMetadata($siteSettings[0] ? : null),
            'articles' => $articles
        ];
    }

    /**
     * @Template()
     *
     * @Route("/useful/{slug}", name="show_useful")
     */
    public function showAction($slug)
    {
        $article = $this->getManager()->getRepository('EasybMainBundle:Article')->findOneBy(array('slug' => $slug));
        if (!$article) {
            throw $this->createNotFoundException('404');
        }
        $this->getBreadCrumbsManager()->create(['EasyB' => 'homepage', 'Полезная информация' => 'useful', $article->getName()=> null],['Статья' => ['slug' => $article->getSlug()]]);
        $files = $article->getFiles();
        return [
            'metadata' => $this->getMetadata($article),
            'article' => $article,
            'files' => $files
        ];
    }

    /**
     * @param \Easyb\MainBundle\Entity\ArticleFile $file
     *
     * @Template()
     * @Route("/save_article_file/{id}#attachments", name="save_article_file", requirements={"id" = "\d+"})
     *
     * @return array
     */
    public function saveFileAction(ArticleFile $file)
    {
        $response = new Response();
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $file->getName() . '"');
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->setContent(file_get_contents($file->getAbsolutePath()));

        return $response;
    }
}


