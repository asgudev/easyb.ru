<?php
namespace Easyb\MainBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Easyb\MainBundle\Entity\City;

use Easyb\AdvertBundle\Entity\AbstractAdvert;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ComponentsController extends Controller
{
    /**
     * @Template()
     * @return array
     */
    public function searchOnMainAction()
    {
        $this->setFilters([
            'categoryId' => null,
            'cityId' => null,
            'advertTypeId' => AbstractAdvert::OFFER,
            'search_text' => null
        ]);
        //$cities = $this->getDoctrine()->getRepository('EasybMainBundle:City')->findAllAndSort();
        $regions = $this->getDoctrine()->getRepository('EasybMainBundle:City')->findRegionsAndSort();

        $categories = $this->getDoctrine()->getRepository('EasybMainBundle:Category')->getAllSortByName();
        $form = $this->createForm($this->container->get('easyb.form.type.search_type'));

        return [
            //'cities'     => $cities,
            'regions'     => $regions,
            'categories' => $categories,
            'form'       => $form->createView()
        ];
    }

    private function sortRegions($regions)
    {

    }



    /**
     * @Template()
     * @return array
     */
    public function searchOnMainStatAction()
    {
        $doctrine = $this->getDoctrine();

        // Add same magic
        $companiesCount = $doctrine->getRepository('EasybUserBundle:Job')->count() * 13;
        $demandsCount = $doctrine->getRepository('EasybAdvertBundle:Demand')->count();
        $offersCount = $doctrine->getRepository('EasybAdvertBundle:Offer')->count() * 17;
        $usersCount = $doctrine->getRepository('EasybUserBundle:User')->count() * 8;

        return [
            'usersCount' => $usersCount,
            'offersCount' => $offersCount,
            'demandsCount' => $demandsCount,
            'companiesCount' => $companiesCount
        ];
    }

    /**
     * @Template()
     * @return array
     */
    public function categoryAction()
    {
        $categories = $this->getDoctrine()->getRepository('EasybMainBundle:Category')->getAllSortByName();
        $count = count($categories)/3;

        return [
            'categories' => $categories,
            'count'      => is_float($count) ? (int)($count+1) : $count
        ];
    }

    /**
     * @param array $filters
     */
    public function setFilters($filters = [])
    {
        foreach ($filters as $filter => $value) {
            $this->getRequest()->getSession()->set($filter, $value);
        }
    }

    /**
     * @Template()
     * @return array
     */
    public function catalogCategoryAction($advertTypeId, $url = null)
    {
        $categories = $this->getDoctrine()->getRepository('EasybMainBundle:Category')->getAllSortByName();
        $category = $this->getDoctrine()->getRepository('EasybMainBundle:Category')->findOneBy(array('slug' => $url));


        return [
            'currentCategory' => $category ? : null,
            'categories' => $categories,
            'path' => $advertTypeId == AbstractAdvert::OFFER ? 'catalog_offer' : 'catalog_demand'
        ];
    }
}