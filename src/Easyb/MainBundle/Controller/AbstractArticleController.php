<?php

namespace Easyb\MainBundle\Controller;

use Easyb\AdvertBundle\Entity\Demand;
use Easyb\AdvertBundle\Entity\Offer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter as PagerAdapter;

use Easyb\MainBundle\Entity\AbstractArticle;
use Easyb\MainBundle\Entity\ArticleFile;
use Easyb\MainBundle\Controller\ControllerHelperTrait;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/")
 */
class AbstractArticleController extends Controller
{
    use ControllerHelperTrait;

    /**
     * @param     $query
     * @param int $maxPerPage
     *
     * @return \Pagerfanta\Pagerfanta
     */
    public function getPager($query, $maxPerPage = AbstractArticle::COUNT_ON_LIST_PAGE)
    {
        $paginator = new Pagerfanta(new PagerAdapter($query, false));
        $paginator->setMaxPerPage($maxPerPage);
        $paginator->setCurrentPage($this->getPage(), false, true);

        return $paginator;
    }

    /**
     * @return int|mixed|null
     */
    public function getPage()
    {
        return $this
            ->getRequest()
            ->get('page') ? $this
            ->getRequest()
            ->get('page') : 1;
    }
}
