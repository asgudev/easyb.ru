<?php

namespace Easyb\MainBundle\Resources\PagerView;

use Pagerfanta\PagerfantaInterface;

/**
 * PagerInterface.
 *
 * @author qwert
 *
 * @api
 */
class PagerView implements \Pagerfanta\View\ViewInterface
{
    /**
     * {@inheritdoc}
     */
    public function render(PagerfantaInterface $pagerfanta, $routeGenerator, array $options = array())
    {
        $options = array_merge(array('proximity' => 2,
                                     'css_current_class' => 'active',
                                     'css_class_prev' => 'b-pagination__previous-arrow active',
                                     'css_class_next' => 'b-pagination__next-arrow active'), $options);


        $currentPage = $pagerfanta->getCurrentPage();

        $pages = '<ul class="pagination">';
        $anchor = "";
        if (isset($options['anchor'])) {
            $anchor = '#' . $options['anchor'];
        }
        //first

        $firstPage = $currentPage - 2;
        if ($firstPage <= 0) {
            $firstPage = 1;
        }

        $endPage = $firstPage + 4;
        if ($endPage > $pagerfanta->getNbPages()) {
            $endPage = $pagerfanta->getNbPages();
            $firstPage = $pagerfanta->getNbPages() - 4;
            if ($firstPage <= 0) {
                $firstPage = 1;
            }
        }
        if ($pagerfanta->hasPreviousPage()) {
            $pages .= '<li><a class="grey" href="'. urldecode($routeGenerator(1)) . $anchor .'">&laquo;</a></li>';
            $pages .= '<li><a class="grey" href="' .  urldecode($routeGenerator($pagerfanta->getPreviousPage())) . $anchor . '">&lt;</a></li>';
        }

        for ($page = $firstPage; $page <= $endPage; $page++) {
            if ($page == $currentPage) {
                $pages .= sprintf('<li>' . $page . '</li>');

            } else {
                $pages .= sprintf('<li><a href="' . urldecode($routeGenerator($page)) . $anchor .  '">' . $page . '</a></li>');
            }
        }

        //last
        if ($pagerfanta->getNbPages() - $currentPage > $options['proximity']) {
            $pages .= sprintf('<li><a href="' . urldecode($routeGenerator($pagerfanta->getNextPage())) . $anchor . '" class="%s"></a></li>', $options['css_class_next']);
        }


        if ($pagerfanta->hasNextPage()) {
            $pages .= '<li><a class="grey" href="' . urldecode($routeGenerator($pagerfanta->getNextPage())) . $anchor . '">&gt;</a></li>';
            $pages .= '<li><a class="grey" href="' . urldecode($routeGenerator($pagerfanta->getNbPages())) . $anchor . '">&raquo;</a></li>';
        }


        $pages .= '</ul>';
        return $pages;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'pager';
    }
}
