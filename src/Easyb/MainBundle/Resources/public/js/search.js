(function (window, $, easyXDM) {
    "use strict";
    var TT_Search = {
        url: false,
        tabs: false,
        searchInputId: false,
        categoryId: false,
        subcategoryId: false,
        regionId: false,
        subcatId: false,
        advertTypeId: false,
        categoryBlock: false,
        subcategoryBlock: false,
        regionBlock: false,
        cityBlock: false,
        citySelector: false,
        categorySelector: false,
        subcategorySelector: false,
        searchResultLi: false,


        initialize: function (url, searchInputId, categoryId, subcatId, advertTypeId, subcategoryId) {
            this.url = url;
            this.tabs = $('.tabs');
            this.searchInputId = $('#' + searchInputId);
            this.categoryId = $('#' + categoryId);
            this.subcategoryId = $('#' + subcategoryId);
            this.subcatId = $('#' + subcatId);
            this.advertTypeId = $('#' + advertTypeId);
            this.categoryBlock = $('.category-block');
            this.subcategoryBlock = $('.subcategory-block');
            this.cityBlock = $('.city-block');
            this.regionBlock = $('.region-block');
            this.searchResultLi = $('.search-result > li');
            this.regionSelector = this.regionBlock.parent().parent().find(".opt-link");
            this.citySelector = this.cityBlock.parent().parent().find(".opt-link");
            this.categorySelector = this.categoryBlock.parent().parent().find(".opt-link");
            this.subcategorySelector = this.subcategoryBlock.parent().parent().find(".opt-link");


//            TT_Search.banEnterKeyDown();
            TT_Search.citySelector.removeClass("opt-link");
            TT_Search.subcategorySelector.removeClass("opt-link");

            if (this.citySelector.data("region") != '') {
                this.regionId = this.citySelector.data("region");
                this.getCitiesInRegion();
            }

            if (this.subcategorySelector.data("category") != '') {
                this.categoryId.val(this.subcategorySelector.data("category"));
                this.getSubInCategory();
            }
            /*
             if (this.citySelector.data("region") != ''){
             this.regionId = this.citySelector.data("region");
             this.getCitiesInRegion();
             }*/
        },

        getCitiesInRegion: function (change) {
            $.ajax({
                url: "/get_cities_ajax",
                type: "POST",
                data: {regionId: TT_Search.regionId},
                success: function (response) {
                    var cities = response.cities;
                    TT_Search.cityBlock.empty();

                    for (var city in cities) {
                        var $li = $("<li/>").attr('value', city).append($("<a/>").attr("href", "#").text(cities[city]));
                        TT_Search.cityBlock.append($li);
                    }
                    if (change) {
                        var first_city = TT_Search.cityBlock.children(':first-child');
                        first_city.click();
                        TT_Search.subcatId.val(first_city.val());
                    }
                    if (city != undefined) TT_Search.citySelector.addClass("opt-link");
                }
            });
        },

        getSubInCategory: function (change) {
            $.ajax({
                url: "/get_categories_ajax",
                type: "POST",
                data: {categoryId: TT_Search.categoryId.val()},
                success: function (response) {
                    var categories = response.categories;
                    TT_Search.subcategoryBlock.empty();

                    TT_Search.subcategoryBlock.append('<li value=""><a href="#">Все подразделы</a></li>');

                    for (var category in categories) {
                        var $li = $("<li/>").attr('value', category).append($("<a/>").attr("href", "#").text(categories[category]));
                        TT_Search.subcategoryBlock.append($li);
                    }
                    if (change) {
                        var first_item = TT_Search.subcategoryBlock.children(':first-child');
                        first_item.click();
                        TT_Search.subcategoryId.val($(first_item).val());
                    }
                    if (category != undefined) TT_Search.subcategorySelector.addClass("opt-link");
                }
            });
        },

        initializeListeners: function () {
            TT_Search.searchInputId.on('keyup', function (e) {
                e.preventDefault();
                var keyCodes = new Array(20, 27);
                if (!(e.keyCode <= 40 && e.keyCode >= 33) && !(e.keyCode <= 18 && e.keyCode >= 16) && $.inArray(e.keyCode, keyCodes) != 0 && e.keyCode != 13) {
                    TT_Search.searchAjax(this);
                }

                if (e.keyCode == 40) {
                    TT_Search.hightLightNextResult();
                } else if (e.keyCode == 38) {
                    TT_Search.hightlightPrevResult();
                }

                if (e.keyCode == 13) {
                    TT_Search.selectResult();
                }

                //кнопка автопоиска активна/неактивна
                if (TT_Search.searchInputId.val()) {
                    $('#autos-but').removeClass('active');
                    $('#autos-but').removeAttr('disabled');
                } else {
                    $('#autos-but').addClass('active');
                    $('#autos-but').attr('disabled', true);
                }
            });
            /*
             TT_Search.categoryBlock.on('click', 'li', function() {
             TT_Search.categoryId.val($(this).val());
             });*/

            TT_Search.regionBlock.on('click', 'li', function () {
                TT_Search.citySelector.removeClass("opt-link");
                TT_Search.regionId = $(this).val();
                if ($(this).val() != "") {
                    //TT_Search.regionId.val($(this).val());
                    TT_Search.getCitiesInRegion(true);
                } else {
                    TT_Search.regionId = '';
                    TT_Search.cityBlock.empty();

                    TT_Search.cityBlock.append('<li value=""><a href="#">Все города</a></li>');

                    TT_Search.cityBlock.children(':first-child').click();
                }
            });

            TT_Search.cityBlock.on('click', 'li', function () {
                TT_Search.subcatId.val($(this).val());
            });

            TT_Search.categoryBlock.on('click', 'li', function () {
                TT_Search.subcategorySelector.removeClass("opt-link");
                TT_Search.categoryId.val($(this).val());

                if ($(this).val() != "") {
                    TT_Search.getSubInCategory(true);
                } else {
                    TT_Search.categoryId.val("");
                    TT_Search.subcategoryBlock.empty();
                    TT_Search.subcategoryBlock.append('<li value=""><a href="#">Все подразделы</a></li>');
                    TT_Search.subcategoryBlock.children(':first-child').click();
                }
            });

            TT_Search.subcategoryBlock.on('click', 'li', function () {
                TT_Search.subcategoryId.val($(this).val());
            });

            TT_Search.tabs.on('click', 'a', function (e) {
                e.preventDefault();
                $('.tabs a').each(function () {
                    $(this).attr('class', 'opt-link');
                    $(this).removeAttr('selected');
                });
                $(this).attr('class', 'top-link active');
                TT_Search.advertTypeId.val($(this).attr('value'));
            });

            TT_Search.searchInputId.on('focusout', function () {
                setTimeout(function () {
                    $('.search').removeClass('open');
                }, 200);
            });

            TT_Search.searchInputId.on('focusin', function () {
                if ($.trim($('.search').find('.sub').html()) != '') {
                    TT_Search.searchAjax(this);
                }
            });

            TT_Search.searchResultLi.on('click', function () {
                console.log(1);
            });
        },

        selectResult: function () {
            if ($('.search-result > li').hasClass('active_li')) {
                TT_Search.setTextAndSendForm($('.active_li > a').text());
            }
        },

//        banEnterKeyDown: function()
//        {
//            $(window).keydown(function(event){
//                if(event.keyCode == 13) {
//                    event.preventDefault();
//                    return false;
//                }
//            });
//        },

        hightLightOnHover: function () {
            $('.search-result > li').on('click', function () {
                TT_Search.setTextAndSendForm($(this).find('a').text());
            });

            $('.search-result > li').hover(
                function () {
                    $('.active_li').removeClass('active_li');
                    $(this).addClass('active_li');
                },
                function () {
                    $(this).removeClass('active_li');
                }
            );
        },

        setTextAndSendForm: function (text) {
            TT_Search.searchInputId.val(text);
            $('#search-form').submit();
        },

        hightLightNextResult: function () {
            if ($('.search-result > li').hasClass('active_li')) {
                var position = $('.search-result > li').index($('.active_li'));
                $('.active_li').removeClass('active_li');
            } else {
                var position = -1;
            }
            $('.search-result > li').eq(position + 1).addClass('active_li');
        },

        hightlightPrevResult: function () {
            if ($('.search-result > li').hasClass('active_li')) {
                var position = $('.search-result > li').index($('.active_li'));
                $('.active_li').removeClass('active_li');
            } else {
                var position = 0;
            }
            $('.search-result > li').eq(position - 1).addClass('active_li');
        },

        initResultBlock: function () {
            $('.search-result').on('click', 'li a', function (e) {
                e.preventDefault();
                TT_Search.searchInputId.val($(this).text());
            });
        },

        searchAjax: function (ws) {
            var form = $(ws).closest('form').serialize();
            $.ajax({
                url: TT_Search.url,
                dataType: 'json',
                data: form,
                success: function (data) {
                    $('.search').addClass('open');
                    if (data.html) {
                        $('.search .sub').html(data.html);
                    } else {
                        if (TT_Search.searchInputId.val() == '') {
                            $('.search .sub').html('');
                        } else {
                            $('.search .sub').html('<ul><li>Ничего не найдено</li></ul>');
                        }
                    }
                    if (TT_Search.searchInputId.val() == '') {
                        $('.search').removeClass('open');
                    } else {
                        $('.search').addClass('open');
                    }
                    TT_Search.initResultBlock();
                    TT_Search.hightLightOnHover();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }
    };
    window.search = TT_Search || {};
})(window, window.$, window.easyXDM);