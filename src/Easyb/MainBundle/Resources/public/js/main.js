jQuery(document).delegate('#close_login', 'click', function () {
    tb_remove();
});

jQuery(document).delegate('#close_modal', 'click', function () {
    tb_remove();
});

jQuery(document).delegate('.ajax', 'submit', function () {
    var form = $(this);
    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: form.serializeArray(),
        error: function (jqXHR, textStatus, errorThrown) {
            location.href = location.href;
        },
        success: function (data, textStatus, jqXHR) {
            block = form.parents('.ajax-content').last().parent();
            if (block) {
                $(block).html(data);
            }
        },
        statusCode: {
            302: function (data) {
                location.href = data.responseText;
            }
        }
    });
    return false;
});


jQuery(document).delegate('input[type="reset"]', 'click', function () {
    location.href = history.go(-1).href;
});

jQuery(document).click(function(e) {
    if (!jQuery(e.target).is(".opt-link")) {
        jQuery('.select-block').each(function() {
            jQuery(this).removeClass('open');
        });
        jQuery(this).parent().addClass('open');
    }
});


jQuery(document).on('click', '.select-block .opt-link', function(e) {
    e.preventDefault();
    jQuery('.select-block').each(function() {
        jQuery(this).removeClass('open');
    });
    jQuery(this).parent().addClass('open');
});

jQuery(document).on('click', '.select-block .sub ul li', function(e) {
    e.preventDefault();
    jQuery(this).closest('.select-block').find('.opt-text').text(jQuery(this).children().text());
});

jQuery(document).ready(function () {
    jQuery('#count_on_page').on('change', function(e) {
        e.preventDefault();
        location.href = jQuery('#count_on_page').attr("selected", "selected").val();
    });
});

//стрелка вверх
jQuery(function() {
    jQuery.fn.scrollToTop = function() {
        jQuery(this).hide().removeAttr("href");
        if (jQuery(window).scrollTop() >= "450") jQuery(this).fadeIn("slow")
        var scrollDiv = jQuery(this);
        jQuery(window).scroll(function() {
            if (jQuery(window).scrollTop() <= "450") jQuery(scrollDiv).fadeOut("slow")
            else jQuery(scrollDiv).fadeIn("slow")
        });
        jQuery(this).click(function() {
            jQuery("html, body").animate({scrollTop: 0}, "slow")
        })
    }
});

jQuery(function() {
    jQuery("#Go_Top").scrollToTop();
});

//мигание уведомления
jQuery(document).ready(function(){
    jQuery.fn.wait = function(time, type) {
        time = time || 1000;
        type = type || "fx";
        return this.queue(type, function() {
            var self = this;
            setTimeout(function() {
                jQuery(self).dequeue();
            }, time);
        });
    };
    function runIt() {
        jQuery(".exclam").wait()
            .animate({"opacity": 0.1},100)
            .wait()
            .animate({"opacity": 1},50,runIt);
    }
    runIt();
});