<?php
namespace Easyb\MainBundle\BreadCrumbs;

use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;
use Symfony\Component\Routing\Router;

/**
 * class BreadCrumbsManager
 */
class BreadCrumbsManager
{
    /**
     * @var \WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs
     */
    protected $breadcrumbs;

    /**
     * @var \Symfony\Component\Routing\Router
     */
    private $router;

    /**
     * @param \WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs $breadcrumbs
     * @param \Symfony\Component\Routing\Router                 $router
     */
    public function __construct(Breadcrumbs $breadcrumbs, Router $router)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->router = $router;
    }

    /**
     * @param array $options
     * @param array $parameters
     */
    public function create($options = [], $parameters = [])
    {
        foreach ($options as $name => $link) {
            if ($link) {
                $this->breadcrumbs->addItem($name, $this->router->generate($link, array_key_exists($name, $parameters) ? $parameters[$name] : []));
            } else {
                $this->breadcrumbs->addItem($name);
            }
        }
    }
}
