<?php
/**
 * User: scorp
 * Date: 01.07.13
 * Time: 17:31
 */
namespace Easyb\MainBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Easyb\AdvertBundle\Entity\AbstractAdvert;
use Easyb\AdvertBundle\Entity\Demand;
use Easyb\AdvertBundle\Entity\Proposal;
use Easyb\AdvertBundle\Entity\Question;
use Easyb\AdvertBundle\Entity\Reaction;
use Easyb\AdvertBundle\Events\DemandEvent;
use Easyb\AdvertBundle\Events\OfferEvent;
use Easyb\AdvertBundle\Events\ProposalEvent;
use Easyb\AdvertBundle\Events\QuestionEvent;
use Easyb\AdvertBundle\Events\ReactionEvent;
use Easyb\MainBundle\Mailing\MailingInterface;
use Easyb\UserBundle\Entity\Mail;
use Easyb\UserBundle\Events\JobEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Router;

class MailerListener implements EventSubscriberInterface
{
    /**
     * @var MailingInterface
     */
    protected $mailing;
    /**
     * @var \Symfony\Component\Routing\Router
     */
    protected $router;
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @param MailingInterface $mailing
     * @param \Doctrine\ORM\EntityManager $em
     * @param \Symfony\Component\Routing\Router $router
     */
    public function __construct(MailingInterface $mailing, EntityManager $em, Router $router)
    {
        $this->mailing = $mailing;
        $this->em = $em;
        $this->repository = $em->getRepository('EasybUserBundle:Mail');
        $this->router = $router;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            ReactionEvent::APPROVE_OR_DECLINE => 'sendReactionMail',
            ReactionEvent::CREATE => 'sendWhenCreate',
            ReactionEvent::SUBMIT_ADMIN_ADVERT => 'sendForAdminAdvert',
            ReactionEvent::SUBMIT_ADMIN_OFFER => 'sendForAdminOffer',
            DemandEvent::ADMIN_DEMAND_APPROVE => 'sendForAdminDemand',
            ReactionEvent::SUBMIT_ADMIN_OFFER_IMPROVE => 'sendForAdminOfferImprove',
            ReactionEvent::SUBMIT_ADMIN_DEMAND_IMPROVE => 'sendForAdminDemandImprove',
            ReactionEvent::NO_PROPOSAL => 'sendProposalDisqual',


            ReactionEvent::NO_WINNER => 'sendNoWinner',
            DemandEvent::NO_WINNER => 'sendNoWinner',

            ProposalEvent::PROPOSAL_SEND => 'sendProposalRecieved',

            ProposalEvent::PROPOSAL_WINNER => 'sendProposalWinner',
            ProposalEvent::PROPOSAL_LOSER => 'sendProposalLoser',

            DemandEvent::DELETE => 'deleteDemand',
            OfferEvent::DELETE => 'deleteOffer',

            DemandEvent::EXTEND_OFFER => 'sendExtendOffer',
            ReactionEvent::EXTEND_OFFER => 'sendExtendOffer',

            DemandEvent::EXTEND_WINNER => 'sendExtendWinner',
            ReactionEvent::EXTEND_WINNER => 'sendExtendWinner',

            JobEvent::ADMIN_JOB_APPROVE => 'sendForAdminJobApprove',
            JobEvent::ADMIN_JOB_IMPROVE => 'sendForAdminJobImprove',

            QuestionEvent::QUESTION_CREATED => 'sendQuestionCreated',
            QuestionEvent::QUESTION_ANSWERED => 'sendQuestionAnswered',
        );
    }

    public function sendQuestionAnswered(QuestionEvent $event)
    {
        $question = $event->getQuestion();
        $template = 'EasybMainBundle:Mail:mail.html.twig';

        $mail_type = Question::MAIL_QUESTION_ANSWERED;
        $mail = $this->repository->findOneByType($mail_type);
        $email = $question->getUser()->getEmail();

        $parameters = [
            'subject' => $mail->getSubject(),
            'text' => $mail->getMessage()
        ];
        $this->sendEmail($template, $parameters, $email);
    }

    public function sendQuestionCreated(QuestionEvent $event)
    {
        $question = $event->getQuestion();
        $template = 'EasybMainBundle:Mail:mail.html.twig';

        $mail_type = Question::MAIL_QUESTION_CREATED;
        $mail = $this->repository->findOneByType($mail_type);
        $email = $question->getDemand()->getUser()->getEmail();

        $parameters = [
            'subject' => $mail->getSubject(),
            'text' => $mail->getMessage()
        ];
        $this->sendEmail($template, $parameters, $email);

        $this->sendToCurators($question->getDemand()->getCurators(), $template, $parameters);
    }

    private function sendToCurators($curators, $template, $parameters)
    {
        if ($curators) {
            foreach ($curators as $email) {
                if ($email != '') $this->sendEmail($template, $parameters, $email);
            }
        }
    }


    public function sendExtendWinner($event)
    {
        $template = 'EasybMainBundle:Mail:mail.html.twig';
        if ($event instanceof DemandEvent) {
            $email = $event->getDemand()->getUser()->getEmail();
        } elseif ($event instanceof ReactionEvent) {
            $email = $event->getReaction()->getUser()->getEmail();
        }

        $mail_type = Demand::MAIL_DEMAND_EXTEND_WINNER;
        $mail = $this->repository->findOneByType($mail_type);


        $parameters = [
            'subject' => $mail->getSubject(),
            'text' => $mail->getMessage()
        ];
        $this->sendEmail($template, $parameters, $email);

        if ($event instanceof DemandEvent) {
            $this->sendToCurators($event->getDemand()->getCurators(), $template, $parameters);
        }
    }

    public function sendExtendOffer($event)
    {
        $template = 'EasybMainBundle:Mail:mail.html.twig';
        if ($event instanceof DemandEvent) {
            $email = $event->getDemand()->getUser()->getEmail();
        } elseif ($event instanceof ReactionEvent) {
            $email = $event->getReaction()->getUser()->getEmail();
        }

        $mail_type = Demand::MAIL_DEMAND_EXTEND_OFFER;
        $mail = $this->repository->findOneByType($mail_type);
        $parameters = [
            'subject' => $mail->getSubject(),
            'text' => $mail->getMessage()
        ];
        $this->sendEmail($template, $parameters, $email);

        if ($event instanceof DemandEvent) {
            $this->sendToCurators($event->getDemand()->getCurators(), $template, $parameters);
        }
    }

    public function sendNoWinner($event)
    {

        $template = 'EasybMainBundle:Mail:mail.html.twig';
        if ($event instanceof DemandEvent) {
            $email = $event->getDemand()->getUser()->getEmail();
        } elseif ($event instanceof ReactionEvent) {
            $email = $event->getReaction()->getUser()->getEmail();
        }

        //print $email;

        $mail_type = Demand::MAIL_DEMAND_NO_WINNER;
        $mail = $this->repository->findOneByType($mail_type);
        $parameters = [
            'subject' => $mail->getSubject(),
            'text' => $mail->getMessage()
        ];
        $this->sendEmail($template, $parameters, $email);

        if ($event instanceof DemandEvent) {
            $this->sendToCurators($event->getDemand()->getCurators(), $template, $parameters);
        }
    }

    public function sendProposalWinner(ProposalEvent $event)
    {
        $proposal = $event->getProposal();
        $template = 'EasybMainBundle:Mail:mail.html.twig';

        //to winner
        $mail_type = $event->isAuto() ? Proposal::MAIL_STATUS_AUTO_WINNER : Proposal::MAIL_STATUS_WINNER;
        $email = $proposal->getReaction()->getUser()->getEmail();
        $mail = $this->repository->findOneByType($mail_type);
        $parameters = [
            'subject' => $mail->getSubject(),
            'text' => $mail->getMessage()
        ];
        $this->sendEmail($template, $parameters, $email);

        //to owner
        $mail_type = $event->isAuto() ? Proposal::MAIL_STATUS_AUTO_WINNER : Proposal::MAIL_STATUS_CHOOSE;
        $email = $proposal->getReaction()->getDemand()->getUser()->getEmail();
        $mail = $this->repository->findOneByType($mail_type);
        $parameters = [
            'subject' => $mail->getSubject(),
            'text' => $mail->getMessage()
        ];
        $this->sendEmail($template, $parameters, $email);
        $this->sendToCurators($proposal->getReaction()->getDemand()->getCurators(), $template, $parameters);
    }

    public function sendProposalLoser(ProposalEvent $event)
    {
        $proposal = $event->getProposal();

        $mail_type = Proposal::MAIL_STATUS_LOSER;
        $email = $proposal->getReaction()->getUser()->getEmail();

        $template = 'EasybMainBundle:Mail:mail.html.twig';
        $mail = $this->repository->findOneByType($mail_type);

        $parameters = [
            'subject' => $mail->getSubject(),
            'text' => $mail->getMessage()
        ];

        $this->sendEmail($template, $parameters, $email);
    }

    public function sendProposalDisqual(ReactionEvent $event)
    {
        $reaction = $event->getReaction();

        $mail_type = Mail::TYPE_PROPOSAL_DISQUALIFICATION;
        $email = $reaction->getUser()->getEmail();

        $template = 'EasybMainBundle:Mail:mail.html.twig';
        $mail = $this->repository->findOneByType($mail_type);

        $parameters = [
            'subject' => $mail->getSubject(),
            'text' => $mail->getMessage()
        ];

        $this->sendEmail($template, $parameters, $email);
    }

    public function sendProposalRecieved(ProposalEvent $event)
    {
        $template = 'EasybMainBundle:Mail:mail.html.twig';
        $proposal = $event->getProposal();

        //send to sender

        $mail_type = Mail::TYPE_PROPOSAL_SEND;
        $email = $proposal->getReaction()->getUser()->getEmail();
        $mail = $this->repository->findOneByType($mail_type);
        $message = $mail->getMessage();
        $message = str_replace('[username]', $proposal->getReaction()->getUser()->getFio(), $message);
        $message = str_replace('[company_author]', $proposal->getReaction()->getJob()->getName(), $message);
        $parameters = [
            'subject' => $mail->getSubject(),
            'text' => $message
        ];
        $this->sendEmail($template, $parameters, $email);

        //send to tender
        $mail_type = Mail::TYPE_PROPOSAL_RECIEVE;
        $email = $proposal->getReaction()->getDemand()->getUser()->getEmail();
        $mail = $this->repository->findOneByType($mail_type);

        $message = $mail->getMessage();
        $message = str_replace('[username]', $proposal->getReaction()->getDemand()->getUser()->getFio(), $message);
        $message = str_replace('[company_user]', $proposal->getReaction()->getJob()->getName(), $message);

        $parameters = [
            'subject' => $mail->getSubject(),
            'text' => $message
        ];
        $this->sendEmail($template, $parameters, $email);
        $this->sendToCurators($proposal->getReaction()->getDemand()->getCurators(), $template, $parameters);

    }

    public function deleteOffer(OfferEvent $event)
    {
        $offer = $event->getOffer();
        $offer->setIsDeleted(true);
        $this->em->flush();
    }

    public function deleteDemand(DemandEvent $event)
    {
        $demand = $event->getDemand();
        $reactions = $demand->getReactions()->toArray();
        $mail_type = Demand::MAIL_DEMAND_NO_WINNER;
        $template = 'EasybMainBundle:Mail:mail.html.twig';
        $mail = $this->repository->findOneByType($mail_type);

        /**
         * @var Reaction $reaction
         */
        foreach ($reactions as $reaction) {
            $reaction->setStatus(Reaction::STATUS_DECLINE);
            $email = $reaction->getJob()->getUser()->getEmail();

            $parameters = [
                'subject' => $this->getOfferReactionSubject($mail, $reaction),
                'text' => $this->getOfferReactionText($mail, $reaction)
            ];

            $this->sendEmail($template, $parameters, $email);
        }


        $parameters = [
            'subject' => $mail->getSubject(),
            'text' => $mail->getMessage()
        ];

        $email = $demand->getUser()->getEmail();
        $this->sendEmail($template, $parameters, $email);


        if ($event->isDelete()) {
            $demand->setIsDeleted(true);
            $demand->setIsActive(false);
        }
        $demand->setIsFinished(true);

        $this->em->flush();

    }

    /**
     * @param Mail $mail
     * @param Reaction $reaction
     *
     * @return string
     */
    public function getOfferReactionSubject(Mail $mail, Reaction $reaction)
    {
        $subject = $mail->getSubject();
        $subject = str_replace("[company]", $reaction->getDemand()->getjob()->getName(), $subject);

        return $subject;
    }

    /**
     * @param Mail $mail
     * @param Reaction $reaction
     *
     * @return string
     */
    public function getOfferReactionText(Mail $mail, Reaction $reaction)
    {
        $demand = $reaction->getDemand();

        $company = $demand->getJob();
        $offer = $reaction;

        $text = $mail->getMessage();

        $text = str_replace("[username]", $offer->getUser()->getFio(), $text);

        //заказчик
        $text = str_replace("[company]", $company->getName(), $text);
        $text = str_replace("[company_author]", $company->getName(), $text);

        //исполнитель
        $text = str_replace("[company_user]", $offer->getJob()->getName(), $text);


        //ссылка на тендер
        $route = $this->router->generate('show_demand', [
            'category' => $demand->getCategory()->getSlug(),
            'city' => $demand->getJob()->getCity()->getSlug(),
            'slug' => $demand->getSlug(),
        ], true);

        //файлы в предложении $demand->getFiles()
        $text = str_replace("[reaction_files]", $this->filesToString($reaction->getFiles()), $text);
        $text = str_replace("[advert_files]", $this->filesToString($demand->getFiles()), $text);


        $text = str_replace("[advert_name]", $demand->getName(), $text);
        $text = str_replace("[advert_link]", $route, $text);
        $text = str_replace("[demand_link]", $route, $text);
        $text = str_replace("[advert_username]", $demand->getUser()->getFio(), $text);
        $text = str_replace("[advert_phone]", $company->getPhone(), $text);
        $text = str_replace("[advert_email]", $company->getEmail(), $text);

        $text = str_replace("[advert_expireAt]", $demand->getExpireAt()->format("d.m.Y, H:i"), $text);
        $text = str_replace("[demand_expireProposalAt]", $demand->getExpireProposalAt()->format("d.m.Y, H:i"), $text);
        $text = str_replace("[demand_expireDecisionAt]", $demand->getExpireDecisionAt()->format("d.m.Y, H:i"), $text);
        $text = str_replace("[advert_createAt]", $demand->getCreatedAt()->format("d.m.Y, H:i"), $text);

        if ($company->getSkype()) {
            $text = str_replace("[advert_skype]", ', Skype: ' . $company->getSkype(), $text);
        } else {
            $text = str_replace("[advert_skype]", '', $text);
        }
        /*$text = str_replace("[offer_link]", $this->router->generate('show_offer', [
            'slug'     => $offer->getSlug(),
            'company'  => $offer->getJob()->getSlug(),
            'city'     => $offer->getJob()->getCity()->getSlug(),
            'category' => $offer->getCategory()->getSlug(),
            ], true), $text);*/

        $text = preg_replace("#\[href.*(target=\"(.*)\").*(title=\"(.*)\")\]#", "<a href=\"$2\" target=\"blank\">$4</a>", $text);
        $text = str_replace("[unsubscribe_link]", $this->router->generate('fos_user_profile_edit', [], true), $text);

        return $text;
    }

    private function filesToString($files)
    {
        $text = "";
        foreach ($files as $file) {
            $text .= "<a href=\"" . $this->router->generate("save_file", array(
                    "id" => $file->getId()
                ), true) . "\">" . $file->getDisplayName() . "</a><br>";
        }
        return $text;
    }

    /**
     * @param string $template
     * @param array $parameters
     * @param string $email
     */
    private function sendEmail($template, $parameters, $email)
    {
        $this->mailing->sendEmail($template, $parameters, $email);
    }

    /**
     * Компания одобрена
     */
    public function sendForAdminJobApprove(JobEvent $event)
    {

        $mail_type = Mail::TYPE_JOB_APPROVE;
        $template = 'EasybMainBundle:Mail:mail.html.twig';
        $mail = $this->repository->findOneByType($mail_type);
        $text = $mail->getMessage();

        $email = $event->getJob()->getUser()->getEmail();
        $text = preg_replace("#\[href.*(target=\"(.*)\").*(title=\"(.*)\")\]#", "<a href=\"$2\" target=\"blank\">$4</a>", $text);

        $parameters = [
            'subject' => $mail->getSubject(),
            'text' => $text
        ];

        $this->sendEmail($template, $parameters, $email);
    }

    /**
     * Компания одобрена
     */
    public function sendForAdminJobImprove(JobEvent $event)
    {

        $mail_type = Mail::TYPE_ADMIN_JOB_IMPROVE;
        $template = 'EasybMainBundle:Mail:mail.html.twig';
        $mail = $this->repository->findOneByType($mail_type);
        $text = $mail->getMessage();

        $email = $event->getJob()->getUser()->getEmail();
        $text = preg_replace("#\[href.*(target=\"(.*)\").*(title=\"(.*)\")\]#", "<a href=\"$2\" target=\"blank\">$4</a>", $text);

        $parameters = [
            'subject' => $mail->getSubject(),
            'text' => $text
        ];

        $this->sendEmail($template, $parameters, $email);
    }

    /**
     * Предложение одобрено на странице объявлений
     *
     * @param ReactionEvent $event
     */
    public function sendWhenCreate(ReactionEvent $event)
    {
        $reaction = $event->getReaction();
        if ($event->getType() == ReactionEvent::FOR_OFFER && $reaction->getStatus() == Reaction::STATUS_APPROVE && $reaction->getOffer()->getUser()->getReviewNotification()) {
            $this->generateMail($reaction);
        }
    }

    /**
     * @param Reaction $reaction
     */
    private function generateMail(Reaction $reaction, $type)
    {
        switch ($type) {
            case Reaction::STATUS_APPROVE:
                $mail_type = Mail::TYPE_DEMAND_APPROVE;
                break;
            case Reaction::STATUS_DECLINE:
                $mail_type = Mail::TYPE_DEMAND_DECLINE;
                break;
        }

        $email = $reaction->getJob()->getUser()->getEmail();
        $template = 'EasybMainBundle:Mail:mail.html.twig';
        $mail = $this->repository->findOneByType($mail_type);

        $parameters = [
            'subject' => $this->getOfferReactionSubject($mail, $reaction),
            'text' => $this->getOfferReactionText($mail, $reaction)
        ];

        $this->sendEmail($template, $parameters, $email);
    }

    /**
     * Предложение одобрено
     *
     * @param ReactionEvent $event
     */
    public function sendReactionMail(ReactionEvent $event)
    {

        $reaction = $event->getReaction();

        if ($reaction->getStatus() == Reaction::STATUS_APPROVE && $reaction->getDemand()->getUser()->getReviewNotification()) {
            $this->generateMail($reaction, Reaction::STATUS_APPROVE);
        } elseif ($reaction->getStatus() == Reaction::STATUS_DECLINE && $reaction->getDemand()->getUser()->getReviewNotification()) {
            $this->generateMail($reaction, Reaction::STATUS_DECLINE);
        }
    }

    /**
     * @param ReactionEvent $event
     */
    public function sendForAdminAdvert(ReactionEvent $event)
    {
        $data = $event->getFormData();

        $email = $event->getUser()->getEmail();
        $template = 'EasybMainBundle:Mail:mail.html.twig';
        $mail = $this->repository->findOneByType(Mail::TYPE_ADMIN_CREATED);
        $text = $mail->getMessage();
        $text = str_replace("[fio]", $data['fio'], $text);
        $text = str_replace("[email]", $data['email'], $text);
        $text = str_replace("[phone]", $data['phone'], $text);
        $text = str_replace("[question]", $data['question'], $text);

        $text = preg_replace("#\[href.*(target=\"(.*)\").*(title=\"(.*)\")\]#", "<a href=\"$2\" target=\"blank\">$4</a>", $text);

        $parameters = [
            'subject' => $mail->getSubject(),
            'text' => $text
        ];


        $this->sendEmail($template, $parameters, $email);
    }

    /**
     * @param ReactionEvent $event
     */
    public function sendForAdminOffer(ReactionEvent $event)
    {
        /**
         * @var AbstractAdvert $data
         */
        $data = $event->getFormData();

        $email = $event->getUser()->getEmail();
        $template = 'EasybMainBundle:Mail:mail.html.twig';
        $mail = $this->repository->findOneByType(Mail::TYPE_ADMIN_OFFER);

        $subject = $mail->getSubject();

        $text = $mail->getMessage();
        $text = str_replace("[username]", $data->getUser()->getFio(), $text);
        $text = str_replace("[offer_link]", $this->router->generate('show_offer', [
            'slug' => $data->getSlug(),
            'company' => $data->getJob()->getSlug(),
            'city' => $data->getJob()->getCity()->getSlug(),
            'category' => $data->getCategory()->getSlug(),
        ], true), $text);
        $text = preg_replace("#\[href.*(target=\"(.*)\").*(title=\"(.*)\")\]#", "<a href=\"$2\" target=\"blank\">$4</a>", $text);


        $parameters = [
            'subject' => $subject,
            'text' => $text
        ];

        $this->sendEmail($template, $parameters, $email);

        // Add submitted email for advert by it's identifier
        $data->addSubmittedEmail($data->getId());

        $this->updateAdvert($data);
    }

    /**
     * @param AbstractAdvert $advert
     */
    private function updateAdvert(AbstractAdvert $advert)
    {
        $this->em->persist($advert);
        $this->em->flush();
    }

    /**
     * @param ReactionEvent $event
     */
    public function sendForAdminOfferImprove(ReactionEvent $event)
    {
        /**
         * @var AbstractAdvert $data
         */
        $data = $event->getFormData();

        $email = $event->getUser()->getEmail();
        $template = 'EasybMainBundle:Mail:mail.html.twig';
        $mail = $this->repository->findOneByType(Mail::TYPE_ADMIN_OFFER_IMPROVE);

        $subject = $mail->getSubject();

        $text = $mail->getMessage();
        $text = str_replace("[username]", $data->getUser()->getFio(), $text);
        $text = str_replace("[offer_link]", $this->router->generate('show_offer', [
            'slug' => $data->getSlug(),
            'company' => $data->getJob()->getSlug(),
            'city' => $data->getJob()->getCity()->getSlug(),
            'category' => $data->getCategory()->getSlug(),
        ], true), $text);

        $parameters = [
            'subject' => $subject,
            'text' => $text
        ];

        $this->sendEmail($template, $parameters, $email);

        // Add submitted email for advert by it's identifier
        $data->addSubmittedEmail($data->getId());

        $this->updateAdvert($data);
    }

    /**
     * @param ReactionEvent $event
     */
    public function sendForAdminDemand(DemandEvent $event)
    {

        $demand = $event->getDemand();

        $email = $demand->getUser()->getEmail();
        $template = 'EasybMainBundle:Mail:mail.html.twig';
        $mail = $this->repository->findOneByType(Mail::TYPE_ADMIN_DEMAND);

        $subject = $mail->getSubject();

        $text = $mail->getMessage();
        $text = str_replace("[username]", $demand->getUser()->getFio(), $text);
        $text = str_replace("[demand_link]", $this->router->generate('show_demand', [
            'slug' => $demand->getSlug(),
            'company' => $demand->getJob()->getSlug(),
            'city' => $demand->getJob()->getCity()->getSlug(),
            'category' => $demand->getCategory()->getSlug(),
        ], true), $text);
        $text = preg_replace("#\[href.*(target=\"(.*)\").*(title=\"(.*)\")\]#", "<a href=\"$2\" target=\"blank\">$4</a>", $text);


        $parameters = [
            'subject' => $subject,
            'text' => $text
        ];

        $this->sendEmail($template, $parameters, $email);

        // Add submitted email for advert by it's identifier
        $demand->addSubmittedEmail($demand->getId());

        $this->updateAdvert($demand);
        $this->sendToCurators($demand->getCurators(), $template, $parameters);

        if ($demand->getIsPublic() == false) {
            $invites = $demand->getInvites();
            foreach ($invites as $email) {
                $mail = $this->repository->findOneByType(Demand::MAIL_DEMAND_INVITE);

                $subject = $mail->getSubject();
                $text = $mail->getMessage();

                $link = $this->router->generate("hidden_demand", array(
                    'private_hash' => $demand->getPrivateHash()
                ), true);
                $text = str_replace("[invite_link]", $link, $text);

                $parameters = [
                    'subject' => $subject,
                    'text' => $text
                ];
                $this->sendEmail($template, $parameters, $email);

            }
        }
    }

    /**
     * @param ReactionEvent $event
     */
    public function sendForAdminDemandImprove(ReactionEvent $event)
    {
        /**
         * @var AbstractAdvert $data
         */
        $data = $event->getFormData();

        $email = $event->getUser()->getEmail();
        $template = 'EasybMainBundle:Mail:mail.html.twig';
        $mail = $this->repository->findOneByType(Mail::TYPE_ADMIN_DEMAND_IMPROVE);

        $subject = $mail->getSubject();

        $text = $mail->getMessage();
        $text = str_replace("[username]", $data->getUser()->getFio(), $text);
        $text = str_replace("[demand_link]", $this->router->generate('show_demand', [
            'slug' => $data->getSlug(),
            'company' => $data->getJob()->getSlug(),
            'city' => $data->getJob()->getCity()->getSlug(),
            'category' => $data->getCategory()->getSlug(),
        ], true), $text);

        $parameters = [
            'subject' => $subject,
            'text' => $text
        ];

        $this->sendEmail($template, $parameters, $email);

        // Add submitted email for advert by it's identifier
        $data->addSubmittedEmail($data->getId());

        $this->updateAdvert($data);
    }
}