<?php
namespace Easyb\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class HelpType
 *
 * @package Easyb\MainBundle\Form\Type
 */
class HelpType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', null, array ('label'    => 'Укажите Ваш Email для обратной связи',
                                            'required' => true

        ));
        $builder->add('text', null, array ('label'    => 'Если Вы хотите сообщить об ошибке - большая просьба: прикладывайте ссылки с багами, пожалуйста. Спасибо.',
                                           'required' => true
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array ('class' => 'Easyb\\MainBundle\\Entity\\Help'));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'help';
    }
}