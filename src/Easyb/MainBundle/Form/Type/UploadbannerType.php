<?php
/**
 * User: scorp
 * Date: 14.07.13
 * Time: 23:14
 */
namespace Easyb\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormError as FormError;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\Form\CallbackValidator as CallbackValidator;

/**
 * class Uploadify
 *
 * rendering file field type with uploadify functionality
 */
class UploadbannerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setAttribute('widget', $options['widget'])
            ->setAttribute('uploaderPath', $options['uploaderPath'])
            ->setAttribute('success', $options['success'])
            ->setAttribute('formData', $options['formData'])
            ->setAttribute('multi', $options['multi'])
            ->setAttribute('swfPath', $options['swfPath'])
            ->setAttribute('fileTypeExts', $options['fileTypeExts']);
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['uploaderPath'] = $options['uploaderPath'];
        $view->vars['success'] = $options['success'];
        $view->vars['swfPath'] = $options['swfPath'];
        $view->vars['fileTypeExts'] = $options['fileTypeExts'];
        $view->vars['multi'] = $options['multi'];
        $view->vars['formData'] = $options['formData'];
    }


    /**
     * @param \Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array (
            'widget'       => 'file',
            'swfPath'      => '',
            'multi'        => false,
            'fileTypeExts' => '*.jpg; *.png;*.jpeg; *.gif; *GIF; *.JPEG; *.JPG; *.PNG; *.swf; *.SWF;',
            'success'      => '',
            'formData'     => array (),
            'uploaderPath' => ''
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return "uploadbanner";
    }
}