<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 15.08.14
 * Time: 13:35
 */

namespace Easyb\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ArticleFileType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('displayName', null, [
                'required' => true,
                'label'    => 'Название'
            ])
            ->add('file', 'file', [
                'required' => true,
                'label'    => 'Файл'
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array('data_class' => 'Easyb\\MainBundle\\Entity\\ArticleFile'));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return "article_file_type";
    }
}