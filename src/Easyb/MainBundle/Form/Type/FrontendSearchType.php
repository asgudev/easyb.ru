<?php

namespace Easyb\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\HttpFoundation\Session\Session;

use Easyb\AdvertBundle\Entity\AbstractAdvert;

class FrontendSearchType extends AbstractType
{
    protected $session;

    /**
     * @param \Symfony\Component\HttpFoundation\Session\Session $session
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('text', null, array('required' => false, 'data' => $this->session->get('search_text') ? $this->session->get('search_text') : ''))
          ->add('categoryId', 'hidden', array('data' => $this->session->get('categoryId') ? $this->session->get('categoryId') : ''))
          ->add('subcategoryId', 'hidden', array('data' => $this->session->get('subcategoryId') ? $this->session->get('subcategoryId') : ''))
          ->add('advertTypeId', 'hidden', array('data' => $this->session->get('advertTypeId') ? $this->session->get('advertTypeId') : AbstractAdvert::OFFER))
          ->add('cityId', 'hidden', array('data' => $this->session->get('cityId') ? $this->session->get('cityId') : ''));
    }
    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return "search_type";
    }
}