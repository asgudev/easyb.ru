<?

namespace Easyb\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/*
use Test\TestBundle\Entity\City;
use Test\TestBundle\Entity\Country;
use Test\TestBundle\Entity\State;
*/


class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', 'entity', array(
                'class' => 'MainBundle:Category',
                'placeholder' => '',
            ));

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                $form = $event->getForm();

                // this would be your entity, i.e. SportMeetup
                $data = $event->getData();

                $category = $data->getSport();
                $subcategories = null === $category ? array() : $category->getAvailablePositions();

                $form->add('subcategory', 'entity', array(
                    'class' => 'MainBundle:Category',
                    'placeholder' => '',
                    'choices' => $subcategories,
                ));
            }
        );
    }


    public function getName()
    {
        return 'category';
    }

    public function getDefaultOptions(array $options)
    {
        return array('data_class' => 'Easyb\MainBundle\Entity\Category');
    }
}