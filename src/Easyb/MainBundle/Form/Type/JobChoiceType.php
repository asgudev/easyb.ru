<?php

namespace Easyb\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;

/**
 * class JobChoiceType
 */
class JobChoiceType extends AbstractType
{
    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return "job_choice";
    }

    public function getParent()
    {
        return 'entity';
    }
}