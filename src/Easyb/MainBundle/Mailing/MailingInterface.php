<?php
/**
 * Created by JetBrains PhpStorm.
 * User: scorp
 * Date: 19.06.13
 * Time: 13:51
 * To change this template use File | Settings | File Templates.
 */
namespace Easyb\MainBundle\Mailing;

/**
 * Class MailingInterface
 *
 * @package Easyb\MainBundle\Mailing
 */
interface MailingInterface
{
    /**
     * @param string $template
     * @param string $parameters
     * @param string $toEmail
     *
     * @return mixed
     */
    public function sendEmail($template, $parameters, $toEmail);
}
