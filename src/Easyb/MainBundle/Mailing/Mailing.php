<?php
/**
 * Created by JetBrains PhpStorm.
 * User: scorp
 * Date: 19.06.13
 * Time: 13:53
 * To change this template use File | Settings | File Templates.
 */
namespace Easyb\MainBundle\Mailing;

/**
 * Class Mailing
 *
 * @package Easyb\MainBundle\Mailing
 */
class Mailing implements MailingInterface
{
    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * @var string
     */
    protected $fromEmail;

    /**
     * @var string
     */
    protected $name;

    /**
     * @param \Swift_Mailer     $mailer
     * @param \Twig_Environment $twig
     * @param string            $fromEmail
     * @param string            $name
     */
    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig, $fromEmail, $name)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->fromEmail = $fromEmail;
        $this->name = $name;
    }

    /**
     * @param string $template
     * @param array  $parameters
     * @param string $toEmail
     *
     * @return mixed
     */
    public function sendEmail($template, $parameters, $toEmail)
    {
        $template = $this->twig->loadTemplate($template);
        $subject = $template->renderBlock('subject', $parameters);
        $textBody = $template->renderBlock('body_text', $parameters);
        $htmlBody = $template->renderBlock('body_html', $parameters);

        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($this->fromEmail, $this->name)
            ->setTo($toEmail)
            ->setBody($htmlBody, 'text/html');
        /*
                if (!empty($htmlBody)) {
                    $message->setBody($htmlBody, 'text/html')
                        ->addPart($textBody, 'text/plain');
                } else {
                    $message->setBody($textBody);
                }
        */
        $this->mailer->send($message);
    }
}
