<?php

namespace Easyb\MainBundle\Entity\Slugable;

/**
 * Interface SlugableInterface
 *
 * @package Easyb\MainBundle\Entity\Slugable
 */
interface SlugableInterface
{
    /**
     * Set Slug
     *
     * @param string $slug
     */
    public function setSlug($slug);

    /**
     * Get Slug
     *
     * @return string
     */
    public function getSlug();
}