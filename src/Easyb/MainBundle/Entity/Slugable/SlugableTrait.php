<?php

namespace Easyb\MainBundle\Entity\Slugable;

/**
 * trait SlugableTrait
 *
 * @package Easyb\MainBundle\Entity\Slugable
 */
trait SlugableTrait
{
    /**
     * @Gedmo\Translatable
     * @Gedmo\Slug(fields={"name"}, separator="_")
     * @ORM\Column(length=300, unique=true, nullable=true)
     */
    private $slug;

    public function getSlug()
    {
        return $this->slug;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
    }
}
