<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 14.08.14
 * Time: 18:01
 */

namespace Easyb\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Imagecow\Image;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

use Easyb\MainBundle\Entity\AbstractArticle;


/**
 * ArticleFile
 *
 * @ORM\Entity
 * @ORM\Table(name="article_file")
 * @ORM\HasLifecycleCallbacks
 */
class ArticleFile
{
    public static $filesTypes = [
        'doc'  => 'doc',
        'docx' => 'docx',
        'docm' => 'docm',
        'txt'  => 'txt',
        'xlsx' => 'xlsx',
        'xlsb' => 'xlsb',
        'xls'  => 'xls',
        'pptx' => 'pptx',
        'pptm' => 'pptm',
        'ppt'  => 'ppt',
        'pdf'  => 'pdf',
        'gif'  => 'gif',
        'jpeg' => 'jpeg',
        'jpg'  => 'jpg',
        'png'  => 'png',
        'bmp'  => 'bmp',
        'pps'  => 'pps'
    ];

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="display_name", type="string", length=255)
     * @Assert\NotBlank(message="Введите имя файла")
     */
    private $displayName;

    /**
     * @var AbstractArticle $article
     *
     * @ORM\ManyToOne(targetEntity="Easyb\MainBundle\Entity\AbstractArticle", inversedBy="files")
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $article;

    /**
     * @Assert\File(
     *     maxSize = "10240k",
     *     maxSizeMessage = "Максимальный размер 10Мб"
     * )
     *
     * @var string
     */
    private $file;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ArticleFile
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set displayName
     *
     * @param string $displayName
     * @return ArticleFile
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get displayName
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * @param string $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set advert
     *
     * @param \Easyb\MainBundle\Entity\AbstractArticle $article
     *
     * @return ArticleFile
     */
    public function setArticle(AbstractArticle $article = null)
    {
        $this->article = $article;

        return $this;
    }

    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->displayName;
    }

    /**
     * @ORM\PreFlush()
     */
    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->file) {
            return;
        }
        if ($this->name) {
            $this->removeUpload();
        }

        $ext = $this->file->guessExtension();
        $name = uniqid() . '.' . $ext;
        $this->checkDirectory($this->getUploadRootDir());
        $this->file->move($this->getUploadRootDir() , $name);
        $this->setName($name);

        $this->file = null;
    }

    private function checkDirectory($directory)
    {
        if (!is_dir($directory)) {
            if (false === @mkdir($directory, 0777, true)) {
                throw new FileException(sprintf('Unable to create the "%s" directory', $directory));
            }
        } elseif (!is_writable($directory)) {
            throw new FileException(sprintf('Unable to write in the "%s" directory', $directory));
        }
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
        $this->name = null;
    }

    /**
     * @return null|string
     */
    public function getAbsolutePath()
    {
        return null === $this->name ? null : $this->getUploadRootDir() . '/' . $this->name;
    }

    /**
     * @return string
     */
    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../../web' . $this->getUploadDir();
    }

    /**
     * @return string
     */
    protected function getUploadDir()
    {
        return '/upload/article';
    }
}