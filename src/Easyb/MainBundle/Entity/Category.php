<?php

namespace Easyb\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Easyb\MainBundle\Entity\Slugable\SlugableInterface;
use Easyb\MainBundle\Entity\Slugable\SlugableTrait;
use Gedmo\Mapping\Annotation as Gedmo;

use Easyb\UserBundle\Entity\Job;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Category
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Easyb\MainBundle\Entity\CategoryRepository")
 */
class Category implements SlugableInterface
{
    use SlugableTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="title_offer", type="string", length=255, nullable=true)
     */
    private $titleOffer;

    /**
     * @var string
     *
     * @ORM\Column(name="description_offer", type="text", nullable=true)
     */
    private $descriptionOffer;

    /**
     * @var string
     *
     * @ORM\Column(name="keywords_offer", type="text", nullable=true)
     */
    private $keywordsOffer;

    /**
     * @var string
     *
     * @ORM\Column(name="head_text_offer", type="text", nullable=true)
     */
    private $headTextOffer;

    /**
     * @var string
     *
     * @ORM\Column(name="footer_text_offer", type="text", nullable=true)
     */
    private $footerTextOffer;

    /**
     * @var string
     *
     * @ORM\Column(name="title_demand", type="string", length=255, nullable=true)
     */
    private $titleDemand;

    /**
     * @var string
     *
     * @ORM\Column(name="description_demand", type="text", nullable=true)
     */
    private $descriptionDemand;

    /**
     * @var string
     *
     * @ORM\Column(name="keywords_demand", type="text", nullable=true)
     */
    private $keywordsDemand;

    /**
     * @var string
     *
     * @ORM\Column(name="footer_text_demand", type="text", nullable=true)
     */
    private $footerTextDemand;

    /**
     * @var string
     *
     * @ORM\Column(name="head_text_demand", type="text", nullable=true)
     */
    private $headTextDemand;

    /**
     * @var boolean
     *
     * @ORM\Column(name="no_follow", type="boolean", nullable=true)
     */
    private $noFollow;

    /**
     * @var boolean
     *
     * @ORM\ManyToMany(targetEntity="Easyb\UserBundle\Entity\Job", mappedBy="categories")
     */
    private $jobs;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Easyb\MainBundle\Entity\Category")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name?:'n/a';
    }

    /**
     * Set titleOffer
     *
     * @param string $titleOffer
     * @return Category
     */
    public function setTitleOffer($titleOffer)
    {
        $this->titleOffer = $titleOffer;

        return $this;
    }

    /**
     * Get titleOffer
     *
     * @return string
     */
    public function getTitleOffer()
    {
        return $this->titleOffer;
    }

    /**
     * Set descriptionOffer
     *
     * @param string $descriptionOffer
     * @return Category
     */
    public function setDescriptionOffer($descriptionOffer)
    {
        $this->descriptionOffer = $descriptionOffer;

        return $this;
    }

    /**
     * Get descriptionOffer
     *
     * @return string
     */
    public function getDescriptionOffer()
    {
        return $this->descriptionOffer;
    }

    /**
     * Set keywordsOffer
     *
     * @param string $keywordsOffer
     * @return Category
     */
    public function setKeywordsOffer($keywordsOffer)
    {
        $this->keywordsOffer = $keywordsOffer;

        return $this;
    }

    /**
     * Get keywordsOffer
     *
     * @return string
     */
    public function getKeywordsOffer()
    {
        return $this->keywordsOffer;
    }

    /**
     * Set footerTextOffer
     *
     * @param string $footerTextOffer
     * @return Category
     */
    public function setFooterTextOffer($footerTextOffer)
    {
        $this->footerTextOffer = $footerTextOffer;

        return $this;
    }

    /**
     * Get footerTextOffer
     *
     * @return string
     */
    public function getFooterTextOffer()
    {
        return $this->footerTextOffer;
    }

    /**
     * Set titleDemand
     *
     * @param string $titleDemand
     * @return Category
     */
    public function setTitleDemand($titleDemand)
    {
        $this->titleDemand = $titleDemand;

        return $this;
    }

    /**
     * Get titleDemand
     *
     * @return string
     */
    public function getTitleDemand()
    {
        return $this->titleDemand;
    }

    /**
     * Set descriptionDemand
     *
     * @param string $descriptionDemand
     * @return Category
     */
    public function setDescriptionDemand($descriptionDemand)
    {
        $this->descriptionDemand = $descriptionDemand;

        return $this;
    }

    /**
     * Get descriptionDemand
     *
     * @return string
     */
    public function getDescriptionDemand()
    {
        return $this->descriptionDemand;
    }

    /**
     * Set keywordsDemand
     *
     * @param string $keywordsDemand
     * @return Category
     */
    public function setKeywordsDemand($keywordsDemand)
    {
        $this->keywordsDemand = $keywordsDemand;

        return $this;
    }

    /**
     * Get keywordsDemand
     *
     * @return string
     */
    public function getKeywordsDemand()
    {
        return $this->keywordsDemand;
    }

    /**
     * Set footerTextDemand
     *
     * @param string $footerTextDemand
     * @return Category
     */
    public function setFooterTextDemand($footerTextDemand)
    {
        $this->footerTextDemand = $footerTextDemand;

        return $this;
    }

    /**
     * Get footerTextDemand
     *
     * @return string
     */
    public function getFooterTextDemand()
    {
        return $this->footerTextDemand;
    }

    public function getNoFollow()
    {
        return $this->noFollow;
    }

    public function setNoFollow($noFollow)
    {
        $this->noFollow = $noFollow;

        return $this;
    }

    public function getHeadTextOffer()
    {
        return $this->headTextOffer;
    }

    public function setHeadTextOffer($headTextOffer)
    {
        $this->headTextOffer = $headTextOffer;
    }

    public function getHeadTextDemand()
    {
        return $this->headTextDemand;
    }

    public function setHeadTextDemand($headTextDemand)
    {
        $this->headTextDemand = $headTextDemand;
    }

    /**
     * Add jobs
     *
     * @param \Easyb\MainBundle\Entity\CategoryCategory $category
     *
     * @return Category
     */
    public function addCategory(Job $job)
    {
        $this->jobs[] = $job;

        return $this;
    }

    /**
     * Remove jobs
     *
     * @param \Easyb\MainBundle\Entity\Category $category
     */
    public function removeCategory(Job $job)
    {
        $this->jobs->removeElement($job);
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->jobs;
    }
}