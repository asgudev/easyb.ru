<?php

namespace Easyb\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Easyb\MainBundle\Entity\Slugable\SlugableInterface;
use Easyb\MainBundle\Entity\Slugable\SlugableTrait;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * City
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Easyb\MainBundle\Entity\CityRepository")
 */
class City implements SlugableInterface
{
    use SlugableTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var integer $sort
     * @ORM\Column(name="sort", type="integer", nullable=true)
     */
    protected $sort;


    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="City")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * construct method
     */
    public function __construct()
    {
        $this->sort = 0;
        $this->child = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     * @return City
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
    
        return $this;
    }

    /**
     * Get sort
     *
     * @return integer 
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent[] = $parent;
        return $this;
    }

    public function getRegion()
    {
        if ($this->parent == $this) {
            return $this;
        } else {
            return $this->getParent()->getName().", ".$this->name;
        }
    }
}