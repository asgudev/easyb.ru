<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 14.08.14
 * Time: 18:01
 */

namespace Easyb\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\Common\Collections\ArrayCollection;
use Easyb\MainBundle\Entity\Slugable\SlugableInterface;
use Easyb\MainBundle\Entity\Slugable\SlugableTrait;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Article
 *
 * @ORM\Table(name="abstract_article")
 * @ORM\Entity
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 */
abstract class AbstractArticle implements SlugableInterface
{
    use SlugableTrait;

    const COUNT_ON_LIST_PAGE = 5;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(message="Введите заголовок статьи")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     * @Assert\NotBlank(message="Заполните текст сообщения")
     */
    private $text;

    /**
     * @var ArrayCollection $files
     * @ORM\OneToMany(targetEntity="Easyb\MainBundle\Entity\ArticleFile", mappedBy="article", cascade={"persist"}, orphanRemoval=true)
     */
    protected $files;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publication_at", type="datetime", nullable=true)
     */
    private $publicationAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=true)
     */
    private $isActive;

    /**
     * @var integer
     * @ORM\Column(name="views", type="integer", nullable=true)
     */
    private $views;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="text", nullable=true)
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="text", nullable=true)
     */
    private $metaKeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    private $title;


    public function __construct()
    {
        $this->files            = new ArrayCollection();
        $this->createdAt        = new \DateTime();
        $this->publicationAt    = new \DateTime();
        $this->isActive         = true;
        $this->views            = 0;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Article
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Article
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Add files
     *
     * @param \Easyb\MainBundle\Entity\ArticleFile $file
     *
     * @return Article
     */
    public function addFile(ArticleFile $file)
    {
        $file->setArticle($this);
        $this->files->add($file);

        return $this;
    }

    /**
     * Remove files
     *
     * @param \Easyb\MainBundle\Entity\ArticleFile $files
     */
    public function removeFile(ArticleFile $files)
    {
        $this->files->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Article
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name?:'n/a';
    }

    /**
     * Set publicationAt
     *
     * @param \DateTime $publicationAt
     * @return Article
     */
    public function setPublicationAt($publicationAt)
    {
        $this->publicationAt = $publicationAt;

        return $this;
    }

    /**
     * Get publicationAt
     *
     * @return \DateTime
     */
    public function getPublicationAt()
    {
        return $this->publicationAt;
    }

    /**
     * Можно ли апать объявления
     * @return mixed
     */
    public function checkUpTime()
    {
        $now = new \DateTime();

        return ($now > $this->getPublicationAt()->modify('+1 hour'));
    }

    /**
     * @return mixed
     */
    public function whenUpTime()
    {
        $start = new \DateTime();
        $end = $this->getPublicationAt()->modify('+1 hour');
        $diff = $start->diff($end);
        $result = $diff->format('%i минут %s секунд');

        return $result == '0 минут 0 секунд' ? '1 час' : $result;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Article
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set views
     *
     * @param integer $views
     * @return Article
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * Get views
     *
     * @return integer
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * @param string $metaDescription
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     * @return AbstractArticle
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
    
        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string 
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return AbstractArticle
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }
}