<?php

namespace Easyb\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

/**
 * Banner
 *
 * @ORM\Table(name="banners")
 * @ORM\Entity(repositoryClass="Easyb\MainBundle\Entity\BannerRepository")
 */
class Banner
{
    /**
     * Тип баннера картинка
     */
    const TYPE_IMAGE = 1;
    /**
     * Тип баннера html или java
     */
    const TYPE_HTML_JAVA = 2;
    /**
     * Тип баннера flash
     */
    const TYPE_FLASH = 3;

    /**
     * сверху страницы
     */
    const POSITION_UP = 1;
    /**
     * снизу страницы
     */
    const POSITION_DOWN = 2;
    /**
     * справа страницы, первый сверху
     */
    const POSITION_RIGHT_FIRST = 3;
    /**
     * справа страницы, второй сверху
     */
    const POSITION_RIGHT_SECOND = 4;
    /**
     * справа страницы, третий сверху
     */
    const POSITION_RIGHT_THIRD = 5;
    /**
     * справа страницы, снизу под боковыми баннерами
     */
    const POSITION_GOOGLE = 6;

    public static $positionText = [
        self::POSITION_UP           => 'сверху страницы',
        self::POSITION_DOWN         => 'снизу страницы',
        self::POSITION_RIGHT_FIRST  => 'справа страницы, первый сверху',
        self::POSITION_RIGHT_SECOND => 'справа страницы, второй сверху',
        self::POSITION_RIGHT_THIRD  => 'справа страницы, третий сверху',
        self::POSITION_GOOGLE       => 'справа страницы, снизу под боковыми баннерами'
    ];

    public static $typeText = [
        self::TYPE_IMAGE     => 'GIF баннеры, изображение',
        self::TYPE_HTML_JAVA => 'HTML-баннеры и Java баннеры',
        self::TYPE_FLASH     => 'Flash'
    ];

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="smallint")
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="smallint")
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="contents", type="text")
     */
    private $contents;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="show_at", type="datetime")
     */
    private $showAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="show_by", type="datetime")
     */
    private $showBy;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @var string
     *
     * @ORM\Column(name="attach", type="string", length=255, nullable=true)
     */
    private $attach;

    /**
     * @var string
     */
    private $file;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Banner
     */
    public function setType($type)
    {
        if (!$type) {
            $type = self::TYPE_HTML_JAVA;
        }
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set contents
     *
     * @param string $contents
     * @return Banner
     */
    public function setContents($contents)
    {
        $this->contents = $contents;
    
        return $this;
    }

    /**
     * Get contents
     *
     * @return string 
     */
    public function getContents()
    {
        return $this->contents;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Banner
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set showAt
     *
     * @param \DateTime $showAt
     * @return Banner
     */
    public function setShowAt($showAt)
    {
        $this->showAt = $showAt;
    
        return $this;
    }

    /**
     * Get showAt
     *
     * @return \DateTime 
     */
    public function getShowAt()
    {
        return $this->showAt;
    }

    /**
     * Set showBy
     *
     * @param \DateTime $showBy
     * @return Banner
     */
    public function setShowBy($showBy)
    {
        $this->showBy = $showBy;
    
        return $this;
    }

    /**
     * Get showBy
     *
     * @return \DateTime 
     */
    public function getShowBy()
    {
        return $this->showBy;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Banner
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->name?:'n/a';
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Banner
     */
    public function setPosition($position)
    {
        $this->position = $position;
    
        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return Banner
     */
    public function setLink($link)
    {
        $this->link = $link;
    
        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set attach
     *
     * @param string $attach
     * @return Banner
     */
    public function setAttach($attach)
    {
        $this->attach = $attach;
    
        return $this;
    }

    /**
     * Get attach
     *
     * @return string 
     */
    public function getAttach()
    {
        return $this->attach;
    }

    /**
     * @ORM\PreFlush()
     */
    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->file) {
            return;
        }
        if ($this->attach) {
            $this->removeUpload();
        }
        $name = uniqid() . '.' . $this->file->guessExtension();
        $this->checkDirectory($this->getUploadRootDir());
        $this->file->move($this->getUploadRootDir(), $name);
        $this->setAttach($name);
        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
        if ($this->attach) {
            $this->attach = null;
        }
    }

    /**
     * @param string $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return string
     */
    public function getWebAttach()
    {
        return $this->getUploadDir() . '/' . $this->attach;
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     *
     * @return null|string
     */
    private function getAbsolutePath()
    {
        return null === $this->attach ? null : $this->getUploadRootDir() . $this->attach;
    }

    /**
     * @return string
     */
    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../../web' . $this->getUploadDir();
    }

    /**
     * @return string
     */
    protected function getUploadDir()
    {
        return '/upload/banners';
    }

    /**
     * @param $directory
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     */
    private function checkDirectory($directory)
    {
        if (!is_dir($directory)) {
            if (false === @mkdir($directory, 0777, true)) {
                throw new FileException(sprintf('Unable to create the "%s" directory', $directory));
            }
        } elseif (!is_writable($directory)) {
            throw new FileException(sprintf('Unable to write in the "%s" directory', $directory));
        }
    }

    /**
     * @return string
     */
    public function getDefaultLogo()
    {
        return '/bundles/easybmain/img/no-img.png';
    }
}