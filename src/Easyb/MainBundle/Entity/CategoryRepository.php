<?php

namespace Easyb\MainBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * CategoryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CategoryRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function getAllSortByName()
    {
        return $this
            ->createQueryBuilder('c')
            ->where("c.id = c.parent")
            ->orderBy('c.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $name
     *
     * @return array
     */
    public function findAnyByName($name)
    {
        return $this
            ->createQueryBuilder('c')
            ->where('(lower(c.name) LIKE :name)')
            ->setParameter('name', $name)
            ->getQuery()
            ->getResult();
    }

    public function findSubcategoryAndSort($category)
    {
        $categories = $this
            ->createQueryBuilder('c')
            ->where("c.parent = :category")
            ->andWhere("c.parent != c.id")
            ->setParameter("category", $category)
            ->orderBy('c.name', 'ASC')
            ->getQuery()
            ->getResult();

        return $categories;
    }

    public function findCategoryByParent(Category $category)
    {
        $categories = $this
            ->createQueryBuilder('c')
            ->where("c.parent = :category")
            ->setParameter("category", $category)
            ->orderBy('c.name', 'ASC')
            ->getQuery()
            ->getResult();

        return $categories;
    }
}
