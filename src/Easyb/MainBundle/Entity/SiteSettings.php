<?php

namespace Easyb\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SiteSettings
 *
 * @ORM\Table(name="site_settings")
 * @ORM\Entity(repositoryClass="Easyb\MainBundle\Entity\SiteSettingsRepository")
 */
class SiteSettings
{
    const MAIN_ROUTE = 'homepage';
    const CATALOG_DEMAND_ROUTE = 'catalog_only_demand';
    const CATALOG_OFFER_ROUTE = 'catalog_only_offer';
    const DEMANDS_ROUTE = 'demands';
    const MY_DEMAND_ROUTE = 'my_demand';
    const ADD_DEMAND_ROUTE = 'add_demand';
    const OFFERS_ROUTE = 'offers';
    const MY_OFFER_ROUTE = 'my_offer';
    const ADD_OFFER_ROUTE = 'add_offer';
    const RULES_PAGE_ROUTE = 'rules';
    const ADVERTISER_PAGE_ROUTE = 'advertisers';
    const HELP_ROUTE = 'help';
    const USEFUL_ROUTE = 'useful';

    public static $SHOW_TEXT = array(self::MAIN_ROUTE,
                                     self::CATALOG_DEMAND_ROUTE,
                                     self::CATALOG_OFFER_ROUTE
    );

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="keywords", type="text", nullable=true)
     */
    private $keywords;

    /**
     * @var string
     *
     * @ORM\Column(name="footer_text", type="text", nullable=true)
     */
    private $footerText;

    /**
     * @var string
     *
     * @ORM\Column(name="route", type="string", length=255, nullable=true)
     */
    private $route;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getRoute()
    {
        return $this->route;
    }

    public function setRoute($route)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     *
     * @return $this
     */
        public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set footerText
     *
     * @param string $footerText
     *
     * @return $this
     */
    public function setFooterText($footerText)
    {
        $this->footerText = $footerText;

        return $this;
    }

    /**
     * Get footerText
     *
     * @return string
     */
    public function getFooterText()
    {
        return $this->footerText;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->id;
    }
}