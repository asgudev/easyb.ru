<?php
namespace Easyb\DocumentBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Easyb\MainBundle\Entity\Category;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadCategoryData implements FixtureInterface, OrderedFixtureInterface
{
    /**
    * {@inheritDoc}
    */
    public function load(ObjectManager $manager)
    {
        $data = [
            0 => 'Банковские услуги',
            1 => 'Бухгалтерские, аудиторские услуги',
            2 => 'Доставка питьевой воды',
            3 => 'Доставка продуктов питания, кейтеринг',
            4 => 'Доставка цветов',
            5 => 'Кадровые услуги',
            6 => 'Клининговые услуги',
            7 => 'Консалтинговые услуги',
            8 => 'Компьютерные и IT услуги',
            9 => 'Курьерские, почтовые услуги',
            10 => 'Лингвистический перевод',
            11 => 'Организация праздников',
            12 => 'Организация деловых поездок',
            13 => 'Образовательные услуги',
            14 => 'Полиграфические и дизайнерские услуги',
            15 => 'Реклама, маркетинг, PR',
            16 => 'Ремонт и облуживание офисной техники',
            17 => 'Риэлторские услуги',
            18 => 'Страховые услуги',
            19 => 'Транспортные услуги',
            20 => 'Туристические услуги',
            21 => 'Услуги в сфере телекоммуникаций',
            22 => 'Финансовые услуги',
            23 => 'Юридические услуги'
        ];

        foreach($data as $name) {
            $category = new Category();
            $category->setName($name);
            $manager->persist($category);
        }
        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}