<?php
namespace Easyb\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Easyb\MainBundle\Entity\Banner;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadBannerData implements FixtureInterface, OrderedFixtureInterface
{
    /**
    * {@inheritDoc}
    */
    public function load(ObjectManager $manager)
    {
        foreach(Banner::$positionText as $key => $position) {
            $banner = new Banner();
            if (Banner::$positionText[$key] == 'справа страницы, снизу под боковыми баннерами') {
                $banner->setName('Google');
                $banner->setIsActive(false);
                $banner->setType(Banner::TYPE_HTML_JAVA);
                $banner->setContents('');
            } else {
                $banner->setName(Banner::$positionText[$key]);
                $banner->setIsActive(false);
                $banner->setType(Banner::TYPE_IMAGE);
                if ($key == Banner::POSITION_DOWN || $key == Banner::POSITION_UP) {
                    $banner->setContents('/bundles/easybmain/img/temp/b1.jpg');
                } else {
                    $banner->setContents('/bundles/easybmain/img/temp/b2.jpg');
                }
            }
            $banner->setPosition($key);
            $banner->setShowAt(new \DateTime());
            $banner->setShowBy(new \DateTime('+60 day'));

            $manager->persist($banner);
        }
        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 8;
    }
}