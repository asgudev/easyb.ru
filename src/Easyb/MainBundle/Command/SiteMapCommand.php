<?php
/**
 * User: scorp
 * Date: 26.09.13
 * Time: 14:11
 */
namespace Easyb\MainBundle\Command;

use Doctrine\ORM\EntityManager;
use Easyb\AdvertBundle\Entity\Demand;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Easyb\AdvertBundle\Entity\AbstractAdvert;
use Easyb\UserBundle\Entity\Job;

/**
 * @author scorp
 */
class SiteMapCommand extends ContainerAwareCommand
{
    static $startTemplate  = "<?xml version='1.0' encoding='UTF-8'?>\n<urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>\n";
    static $finishTemplate = "</urlset>";

    /**
     * @see Command
     */
    protected function configure()
    {
        $this
            ->setName('easyb:generate:site_map')
            ->setDescription('Generate sitemap.xml');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $file = __DIR__ . '/../../../../web/sitemap.xml';
            if (!file_exists($file)) {
                $f_o = fopen($file, "w");
            } else {
                $f_o = fopen($file,"w+");
            }
            fwrite($f_o, self::$startTemplate);
            fwrite($f_o, $this->getContent());
            fwrite($f_o, self::$finishTemplate);
            fclose($f_o);
            $output->writeln(sprintf('<info>Generate sitemap.xml was successful </info>'));
        } catch (\Exception $e) {
            $output->writeln(sprintf('<error>Generate sitemap.xml was unsuccessful </error>'));
            $output->writeln(sprintf('<error>%s</error>', $e->getMessage()));
        }
    }

    /**
     * @return string
     */
    private function getContent()
    {
        $content = '';
        $content .= $this->getAdvertsContent();
        $content .= $this->getJobsContent();
        $content .= $this->getCatalogContent();
        $content = str_replace('&','&amp;',$content);

        return $content;
    }

    /**
     * @return string
     */
    private function getAdvertsContent()
    {
        $content = '';
        foreach ($this->getActiveAdverts() as $object) {
            $url = $object instanceof Demand ? 'show_'.AbstractAdvert::DEMAND_STRING : 'show_'.AbstractAdvert::OFFER_STRING;
            $url = $this->getRouter()->generate($url, [
                'category' => $object->getCategory()->getSlug(),
                'city'     => $object->getJob()->getCity()->getSlug(),
                'slug'     => $object->getSlug(),
            ], true);
//            $url = $this->getRouter()->generate('show_advert', ['id' => $object->getId()], true);
            $date = $object->getPublicationAt()->format('Y-m-d').'T'.$object->getPublicationAt()->format('H:m:s').'+00:00';
            $content .= "<url> <loc>" . $url . "</loc>\n<lastmod>" . $date . "</lastmod> <changefreq>daily</changefreq>\n<priority>0.8</priority> </url>\n";
        }

        return $content;
    }

    /**
     * @return string
     */
    private function getJobsContent()
    {
        $content = '';
        foreach ($this->getJobs() as $object) {
            $url = $this->getRouter()->generate('show_job', ['slug' => $object->getSlug()], true);
            $content .= "<url> <loc>" . $url . "</loc>\n<changefreq>daily</changefreq>\n<priority>0.8</priority> </url>\n";
        }

        return $content;
    }

    /**
     * @return string
     */
    private function getCatalogContent()
    {
        $content = '';
        foreach ($this->getCatalogs() as $object) {
            $url = $this->getRouter()->generate('catalog_demand', ['url' => $object->getSlug()], true);
            $content .= "<url> <loc>" . $url . "</loc>\n<changefreq>daily</changefreq>\n<priority>1.0</priority> </url>\n";
            $url = $this->getRouter()->generate('catalog_offer', ['url' => $object->getSlug()], true);
            $content .= "<url> <loc>" . $url . "</loc>\n<changefreq>daily</changefreq>\n<priority>1.0</priority> </url>\n";
        }

        return $content;
    }

    /**
     * @return EntityManager
     */
    private function getEm()
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @return \Symfony\Bundle\FrameworkBundle\Routing\Router
     */
    private function getRouter()
    {
        return $this->getContainer()->get('router');
    }

    /**
     * @return \Easyb\UserBundle\Entity\UserRepository
     */
    private function getActiveAdverts()
    {
        return $this->getEm()->getRepository('EasybAdvertBundle:AbstractAdvert')
            ->createQueryBuilder('a')
            ->where('a.isActive = true')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return \Easyb\UserBundle\Entity\UserRepository
     */
    private function getJobs()
    {
        return $this->getEm()->getRepository('EasybUserBundle:Job')->findAll();
    }

    /**
     * @return \Easyb\UserBundle\Entity\UserRepository
     */
    private function getCatalogs()
    {
        return $this->getEm()->getRepository('EasybMainBundle:Category')->findAll();
    }
}