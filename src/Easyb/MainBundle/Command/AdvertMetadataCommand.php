<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 29.08.14
 * Time: 18:09
 */

namespace Easyb\MainBundle\Command;

use Doctrine\ORM\EntityManager;
use Easyb\AdvertBundle\Entity\AdvertMetaData;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Easyb\MainBundle\Entity\SiteSettings;

class AdvertMetadataCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('easyb:init:advert_metadata')
            ->setDescription('Init AdvertMetadata');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $init = new AdvertMetaData();
        $this->getEm()->persist($init);

        $this->getEm()->flush();
    }

    /**
     * @return EntityManager
     */
    private function getEm()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }
} 