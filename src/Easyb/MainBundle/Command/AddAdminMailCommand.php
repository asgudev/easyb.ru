<?php
namespace Easyb\MainBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Easyb\UserBundle\Entity\Mail;

/**
 * @author scorp
 */
class AddAdminMailCommand extends ContainerAwareCommand
{
    /**
     * @see Command
     */
    protected function configure()
    {
        $this
            ->setName('easyb:add:admin_mail')
            ->setDescription('Adding new mail for task 3432');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $subject = 'Пользователя сайта EasyB.ru заинтересовали услуги Вашей компании. ';
            $text = 'Здравствуйте. Просьба переслать это письмо в отдел продаж.
На сайте Easyb.ru (бесплатный каталог услуг для бизнеса) размещено объявление вашей компании о услугах, которые она предоставляет.
Пользователя сайта EasyB.ru заинтересовали услуги Вашей организации (см.ниже).

ФИО: [fio]
Е-маил: [email]
Телефон: [phone]
Суть вопроса: [question]

Удачной продажи!
С Уважением, Команда EasyB';

            $mail = new Mail();
            $mail->setName('Coзданные админом');
            $mail->setType(Mail::TYPE_ADMIN_CREATED);
            $mail->setSubject($subject);
            $mail->setMessage($text);
            $this->getEm()->persist($mail);
            $this->getEm()->flush();

            $output->writeln(sprintf('<info>Adding new mail for task 3432 was successful </info>'));
        } catch (\Exception $e) {
            $output->writeln(sprintf('<error>Adding new mail for task 3432 was unsuccessful </error>'));
            $output->writeln(sprintf('<error>%s</error>', $e->getMessage()));
        }
    }

    /**
     * @return EntityManager
     */
    private function getEm()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }

}