<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 26.08.14
 * Time: 16:44
 */

namespace Easyb\MainBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Easyb\MainBundle\Entity\SiteSettings;

class SiteSettingsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('easyb:load:site_settings')
            ->setDescription('Load SiteSettings');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $main = $this->getEm()->getRepository('EasybMainBundle:SiteSettings')->findOneBy(array('id' => 1));
        $main->setRoute(SiteSettings::MAIN_ROUTE);
        $this->getEm()->persist($main);

        $page1 = new SiteSettings();
        $page1->setName('Объявления: Мой тендер');
        $page1->setRoute(SiteSettings::DEMANDS_ROUTE);
        $this->getEm()->persist($page1);

        $page2 = new SiteSettings();
        $page2->setName('Отклики: Мой тендер');
        $page2->setRoute(SiteSettings::MY_DEMAND_ROUTE);
        $this->getEm()->persist($page2);

        $page3 = new SiteSettings();
        $page3->setName('Размещение объявления demand');
        $page3->setRoute(SiteSettings::ADD_DEMAND_ROUTE);
        $this->getEm()->persist($page3);

        $page4 = new SiteSettings();
        $page4->setName('Объявления: Мое предложение');
        $page4->setRoute(SiteSettings::OFFERS_ROUTE);
        $this->getEm()->persist($page4);

        $page5 = new SiteSettings();
        $page5->setName('Отклики: Мое предложение');
        $page5->setRoute(SiteSettings::MY_OFFER_ROUTE);
        $this->getEm()->persist($page5);

        $page6 = new SiteSettings();
        $page6->setName('Размещение объявления offer');
        $page6->setRoute(SiteSettings::ADD_OFFER_ROUTE);
        $this->getEm()->persist($page6);

        $page7 = new SiteSettings();
        $page7->setName('Правила');
        $page7->setRoute(SiteSettings::RULES_PAGE_ROUTE);
        $this->getEm()->persist($page7);

        $page8 = new SiteSettings();
        $page8->setName('Рекламодателям');
        $page8->setRoute(SiteSettings::ADVERTISER_PAGE_ROUTE);
        $this->getEm()->persist($page8);

        $page9 = new SiteSettings();
        $page9->setName('Помощь');
        $page9->setRoute(SiteSettings::HELP_ROUTE);
        $this->getEm()->persist($page9);

        $page10 = new SiteSettings();
        $page10->setName('Каталог offer');
        $page10->setRoute(SiteSettings::CATALOG_OFFER_ROUTE);
        $this->getEm()->persist($page10);

        $page11 = new SiteSettings();
        $page11->setName('Каталог demand');
        $page11->setRoute(SiteSettings::CATALOG_DEMAND_ROUTE);
        $this->getEm()->persist($page11);

        $page12 = new SiteSettings();
        $page12->setName('Полезная информация');
        $page12->setRoute(SiteSettings::USEFUL_ROUTE);
        $this->getEm()->persist($page12);


        $this->getEm()->flush();
    }

    /**
     * @return EntityManager
     */
    private function getEm()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }
} 