<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 10.09.14
 * Time: 10:48
 */

namespace Easyb\MainBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Easyb\MainBundle\Entity\Category;

class CategoryNoFollowCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('easyb:set:category_nofollow')
            ->setDescription('Set Category Nofollow');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $category = $this->getEm()->getRepository('EasybMainBundle:Category')->findOneBy(array('slug' => 'bankovskiie_uslughi'));
        $category->setNoFollow(true);
        $this->getEm()->persist($category);

        $category = $this->getEm()->getRepository('EasybMainBundle:Category')->findOneBy(array('slug' => 'dostavka_pit_ievoi_vody'));
        $category->setNoFollow(true);
        $this->getEm()->persist($category);

        $category = $this->getEm()->getRepository('EasybMainBundle:Category')->findOneBy(array('slug' => 'dostavka_tsvietov'));
        $category->setNoFollow(true);
        $this->getEm()->persist($category);

        $category = $this->getEm()->getRepository('EasybMainBundle:Category')->findOneBy(array('slug' => 'kadrovyie_uslughi'));
        $category->setNoFollow(true);
        $this->getEm()->persist($category);

        $category = $this->getEm()->getRepository('EasybMainBundle:Category')->findOneBy(array('slug' => 'kur_ierskiie_pochtovyie_uslughi'));
        $category->setNoFollow(true);
        $this->getEm()->persist($category);

        $category = $this->getEm()->getRepository('EasybMainBundle:Category')->findOneBy(array('slug' => 'obrazovatiel_nyie_uslughi'));
        $category->setNoFollow(true);
        $this->getEm()->persist($category);

        $category = $this->getEm()->getRepository('EasybMainBundle:Category')->findOneBy(array('slug' => 'orghanizatsiia_dielovykh_poiezdok'));
        $category->setNoFollow(true);
        $this->getEm()->persist($category);

        $category = $this->getEm()->getRepository('EasybMainBundle:Category')->findOneBy(array('slug' => 'riemont_i_obluzhivaniie_ofisnoi_tiekhniki'));
        $category->setNoFollow(true);
        $this->getEm()->persist($category);

        $category = $this->getEm()->getRepository('EasybMainBundle:Category')->findOneBy(array('slug' => 'rieltorskiie_uslughi'));
        $category->setNoFollow(true);
        $this->getEm()->persist($category);

        $category = $this->getEm()->getRepository('EasybMainBundle:Category')->findOneBy(array('slug' => 'strakhovyie_uslughi'));
        $category->setNoFollow(true);
        $this->getEm()->persist($category);

        $category = $this->getEm()->getRepository('EasybMainBundle:Category')->findOneBy(array('slug' => 'finansovyie_uslughi'));
        $category->setNoFollow(true);
        $this->getEm()->persist($category);

        $this->getEm()->flush();
    }

    /**
     * @return EntityManager
     */
    private function getEm()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }
}