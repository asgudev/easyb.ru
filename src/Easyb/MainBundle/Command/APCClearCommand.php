<?php

namespace Easyb\MainBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Sylvain Lorinet <sylvain.lorinet@gmail.com>
 */
class APCClearCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
        ->setDefinition(array ())
        ->setName('apc:clear')
        ->addOption('opcode', null, InputOption::VALUE_NONE, 'Clear only the opcode cache')
        ->addOption('user', null, InputOption::VALUE_NONE, 'Clear only the user cache')
        ->setHelp('Note: without options, both caches will be deleted')
        ->setDescription('Delete APC opcode or user cache');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $clearOpcode = 'false';
        $clearUser = 'false';

        if (null != $input->getOption('opcode')) {
            $clearOpcode = 'true';
        } else if (null != $input->getOption('user')) {
            $clearUser = 'true';
        } else {
            $clearOpcode = 'true';
            $clearUser = 'true';
        }

        $commandFile = $this->getContainer()->get('templating')->render('EasybMainBundle:Command:clear.html.twig', array (
                                                                                                                           'opcode'         => $clearOpcode,
                                                                                                                           'user'           => $clearUser,
                                                                                                                     ));
        $webDir = $this->getContainer()->get('kernel')->getRootDir() . '/../web';
        $filename = 'cache.php';

        file_put_contents($webDir . '/' . $filename, $commandFile);

        $jSonReturn = file_get_contents('http://' . $this->getContainer()->getParameter('router.request_context.host') . '/' . $filename);
        if (!@unlink($webDir . '/' . $filename)) {
            $output->writeln('Not allowed to remove temporary file named : ' . $filename . ' ! Please remove it manually !');
        }

        $return = json_decode($jSonReturn, true);
        if (is_null($return)) {
            $output->writeln('The callback returned an invalid json, check your server/php settings.');

            return;
        }
        switch ($return['code']) {
            case -1:
                $output->writeln('The password does not match, please check the config.yml APC options !');

                return;
            case 0:
                $output->writeln('The APC cache has been deleted successfully.');

                return;
            case 1:
                $output->writeln('The APC opcode cache can not be deleted.');

                return;
            case 2:
                $output->writeln('The APC user cache can not be deleted.');

                return;
            case 3:
                $output->writeln('The APC user and opcode cache can not be deleted.');

                return;

            default:
                break;
        }
    }
}
