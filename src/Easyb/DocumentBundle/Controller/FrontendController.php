<?php

namespace Easyb\DocumentBundle\Controller;

use Easyb\MainBundle\Entity\SiteSettings;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Easyb\DocumentBundle\Entity\Document;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;


class FrontendController extends Controller
{

    private function getMetaData($object = null, $customMetadata = null)
    {
        return $this->get('easyb.manager.metadata')->getMetadata($this->getRequest()->get('_route'), $object, $customMetadata);
    }

    /**
     * @param Document $document
     * @Route("/page/{link}", name="show_page", requirements={"link" = ".+"})
     *
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function showAction(Document $document)
    {
        $siteSettings = $this->getDoctrine()->getRepository('EasybMainBundle:SiteSettings')->findBy(array('route' => $document->getLink()));

        if (!$document->getShow()) {
            throw $this->createNotFoundException(sprintf('Страница "%s" не подлежит публикации на сайте.', $document->getName()));
        }

        $view = $document->isModal() && $this->getRequest()->isXmlHttpRequest() ? 'EasybDocumentBundle:Frontend:showAsModal.html.twig' : 'EasybDocumentBundle:Frontend:show.html.twig';

        return new Response($this->renderView($view,
            array('document' => $document, 'metadata' => $this->getMetadata($siteSettings[0] ? : null))));
    }
}
