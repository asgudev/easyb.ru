<?php
/**
 * This file is part of easyb package.
 *
 * (c) Evgeniy Marchenkov <evgeniy.public@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Easyb\DocumentBundle\Twig\Extension;

use Twig_Environment;
use Doctrine\Common\Persistence\ObjectManager;

class DocumentExtension extends \Twig_Extension
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    protected $em;

    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository
     */
    protected $repository;

    /**
     * @var \Twig_Environment
     */
    protected $environment;

    /**
     * Constructor
     *
     * @param ObjectManager $em
     */
    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('EasybDocumentBundle:Document');
    }

    /**
     * Gets list of the twig functions
     *
     * @return array
     */
    public function getFunctions()
    {
        return [
            'render_new_job_added_modal' => new \Twig_Function_Method($this, 'renderNewJobAddedModal', ['is_safe' => ['html']]),
            'render_new_advert_added_modal' => new \Twig_Function_Method($this, 'renderNewAdvertAddedModal', ['is_safe' => ['html']])
        ];
    }

    /**
     * Init twig runtime
     *
     * @param Twig_Environment $environment
     */
    public function initRuntime(Twig_Environment $environment)
    {
        $this->environment = $environment;
    }

    /**
     * Renders new job document added modal
     *
     * @return string
     */
    public function renderNewJobAddedModal()
    {
        $document = $this->repository->findOneByLink('new_job_added');

        if (null !== $document) {
            $templateName = 'EasybDocumentBundle:Frontend:showAsModal.html.twig';
            $templateContext = ['document' => $document];

            return $this->output($templateName, $templateContext);
        }

        return '';
    }

    /**
     * Renders new advert document added modal
     *
     * @param string $advertType
     *
     * @return string
     */
    public function renderNewAdvertAddedModal($advertType = 'offer')
    {
        $link = $advertType === 'offer' ? 'new_offer_added' : 'new_demand_added';
        $document = $this->repository->findOneByLink($link);

        if (null !== $document) {
            $templateName = 'EasybDocumentBundle:Frontend:showAsModal.html.twig';
            $templateContext = ['document' => $document];

            return $this->output($templateName, $templateContext);
        }

        return '';
    }

    /**
     * @param string $templateName
     * @param array  $templateContext
     *
     * @return string
     */
    private function output($templateName, array $templateContext = array())
    {
        $output = '';

        try {
            $template = $this->environment->loadTemplate($templateName);

            $output = $template->render($templateContext);
        } catch (\Twig_Error_Loader $e) {
            // Do nothing
        } catch (\Twig_Error_Syntax $e) {
            // Do nothing
        }

        return $output;
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'document.extension';
    }
}
