<?php
namespace Easyb\DocumentBundle\Admin;

use Easyb\AdminBundle\Admin\Admin as BaseAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;

use Knp\Menu\ItemInterface as MenuItemInterface;

class DocumentAdmin extends BaseAdmin
{
    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
          ->add('name', null, array('label'    => 'Название страницы',
                                    'required' => true,
                                    'help'     => 'Название страницы'))
          ->add('sort', null, array('label'    => 'Сортировка "Номер по порядку"',
                                    'required' => false,
                                    'help'     => 'Порядок, по которому выстроятся страницы'))
          ->add('link', null, array('label'    => 'Слово для ссылки',
                                    'required' => true,
                                    'help'     => 'Слово - ключ по которому будет вызываться страница'))
          ->add('h1Title', null, array('label'    => 'H1 Заголовок',
                                       'required' => false,
                                       'help'     => 'Заголовок на странице, который может быть отличен от названия'))
          ->add('text', null, array('label'    => 'Текст страницы',
                                       'required' => false,
                                       'help'     => 'Текст страницы',
                                       'attr'     => array('class' => 'ckeditor')))
         ->add('modal', null, array('label'    => 'Модальное окно?',
                                    'required' => false,
                                    'help'     => 'Отображать ли данную страницу как модальное окно'))
          ->add('show', null, array('label'    => 'Отображать на сайте?',
                                    'required' => false,
                                    'help'     => 'Показывать ли на сайте данную страницу'))
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
                ->add('sort', null, array('label'      => 'Порядок'))
                ->addIdentifier('name', null, array('label'      => 'Название'))
                ->add('link', null, array('label'      => 'Слово для ссылки'))
                ->add('modal', null, array('label'     => 'Модальное окно?'))
                ->add('show', null, array('label'      => 'Отображать на сайте?'))
                ->add('views', null, array('label'     => 'Количество просмотров'));

        $listMapper->add('_action', 'actions', array('actions' => array('delete'     => array('label' => 'delete'),
                                                                        'edit'       => array(),
                                                                        'show'       => array(),
                                                                        'settings'   => array())));
    }

    /**
     * Configuration show view
     *
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
                ->add('sort', null, array('label'      => 'Порядок'))
                ->add('name', null, array('label'      => 'Название'))
                ->add('link', null, array('label'      => 'Слово для ссылки'))
                ->add('modal', null, array('label'     => 'Модальное окно?'))
                ->add('show', null, array('label'      => 'Отображать на сайте?'))
                ->add('views', null, array('label'     => 'Количество просмотров'))
                ->add('h1Title', null, array('label'   => 'H1 Заголовок'))
                ->add('text', null, array('label'      => 'Текст страницы', 'template'  => 'EasybDocumentBundle:DocumentAdmin:show_text.html.twig'))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
          ->add('name', null, array('label' => 'Название'))
          ->add('link', null, array('label' => 'Слово для ссылки'));
    }

    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        if (!$this->hasRequest()) {
            $this->datagridValues = array(
                '_page' => 1,
                '_per_page' => $this->getMaxPerPage(),
                '_sort_order' => 'DESC',
                '_sort_by' => 'id'
            );
        }
    }

    public function configure()
    {
        $this->setTemplate('edit', 'EasybDocumentBundle:DocumentAdmin:edit.html.twig');
    }
}
