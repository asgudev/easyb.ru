<?php

namespace Easyb\DocumentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Easyb\DocumentBundle\Entity\Document
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Easyb\DocumentBundle\Entity\DocumentRepository")
 */
class Document
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(message = "Введите название страницы")
     */
    private $name;

    /**
     * @var integer $sort
     *
     * @ORM\Column(name="sort", type="integer", nullable=true)
     */
    private $sort;

    /**
     * @var string $link
     *
     * @ORM\Column(name="link", type="string", length=255)
     * @Assert\NotBlank(message = "Введите ключ, по которому будет вызываться страница")
     */
    private $link;

    /**
     * @var string $h1Title
     *
     * @ORM\Column(name="h1_title", type="string", length=255, nullable=true)
     */
    private $h1Title;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string $text
     *
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;

    /**
     * @var boolean $show
     *
     * @ORM\Column(name="show", type="boolean", nullable=true)
     */
    private $show = false;

    /**
     * @var boolean $modal
     *
     * @ORM\Column(name="modal", type="boolean", nullable=true)
     */
    private $modal = false;

    /**
     * @var integer $views
     *
     * @ORM\Column(name="views", type="integer", nullable=true)
     */
    private $views;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param  string   $name
     * @return Document
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sort
     *
     * @param  integer  $sort
     * @return Document
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Set link
     *
     * @param  string   $link
     * @return Document
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set h1Title
     *
     * @param  string   $h1Title
     * @return Document
     */
    public function setH1Title($h1Title)
    {
        $this->h1Title = $h1Title;

        return $this;
    }

    /**
     * Get h1Title
     *
     * @return string
     */
    public function getH1Title()
    {
        return $this->h1Title;
    }

    /**
     * Set title
     *
     * @param  string   $title
     * @return Document
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set text
     *
     * @param  string   $text
     * @return Document
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set show
     *
     * @param  boolean  $show
     * @return Document
     */
    public function setShow($show)
    {
        $this->show = $show;

        return $this;
    }

    /**
     * Get show
     *
     * @return boolean
     */
    public function getShow()
    {
        return $this->show;
    }

    /**
     * @return boolean
     */
    public function isModal()
    {
        return $this->modal;
    }

    /**
     * @param boolean $modal
     *
     * @return Document
     */
    public function setModal($modal)
    {
        $this->modal = $modal;

        return $this;
    }

    /**
     * Set views
     *
     * @param  integer  $views
     * @return Document
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * Get views
     *
     * @return integer
     */
    public function getViews()
    {
        return $this->views;
    }

    public function __toString()
    {
        return $this->name ?: 'n/a';
    }
}
