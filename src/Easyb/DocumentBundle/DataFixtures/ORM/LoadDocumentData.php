<?php
namespace Easyb\DocumentBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Easyb\DocumentBundle\Entity\Document;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadDocumentData implements FixtureInterface, OrderedFixtureInterface
{
    private static $text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consectetur ultrices ante et convallis. Integer ut justo sem, ut interdum erat. Donec elementum, odio in sollicitudin placerat, justo felis tincidunt nisi, eget pellentesque turpis mi quis elit. Maecenas enim metus, ornare et ornare et, ultrices nec turpis. Maecenas dapibus sagittis metus. Nullam interdum dictum massa, volutpat luctus ligula viverra vitae. Maecenas ultrices consequat lorem, eu gravida ipsum lobortis id. Sed accumsan magna eu sapien dignissim non interdum augue consectetur.
Vestibulum varius laoreet eleifend. Sed lacinia vestibulum libero, ut egestas elit fringilla a. Suspendisse diam est, tincidunt ac luctus sit amet, lacinia nec elit. Phasellus eu nunc odio, quis eleifend velit. Cras ac arcu vitae ante sollicitudin consectetur in eu arcu. Fusce fringilla neque eget nunc sagittis eget iaculis risus lacinia. Phasellus sed nulla libero, ac consequat mauris. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
Praesent dictum mi sit amet lacus eleifend fermentum eget ac leo. Morbi vel metus ut purus accumsan lacinia in nec erat. In eu justo massa. In ac odio orci, at ultricies mauris. Praesent viverra volutpat volutpat. Donec felis nisi, accumsan sit amet sagittis eget, vulputate id nibh. Nam a tellus non risus rhoncus consectetur. Nunc at libero in leo fermentum semper nec vitae augue. Suspendisse dui neque, sollicitudin ut consequat vitae, lobortis sed dolor. Cras sed leo eget enim convallis euismod non scelerisque magna. Aenean sed sem nisi, et egestas lorem. Morbi orci dolor, rhoncus ut suscipit sit amet, consectetur non ante.
Nulla facilisi. Nullam sagittis urna sed mauris ultricies lacinia. Mauris adipiscing, dui nec dapibus imperdiet, risus ante interdum lorem, a sagittis justo nulla sed est. Duis leo risus, accumsan non tempor ac, scelerisque eget mi. Quisque et enim sit amet quam sodales tempus. Curabitur vitae erat vitae leo porta ullamcorper. Aliquam sed lectus est, in dictum leo. Proin quis urna id metus euismod ullamcorper eget a metus. In hac habitasse platea dictumst.
Donec rhoncus, ipsum et cursus tempor, dolor massa fermentum nisl, vel consequat sem nunc at quam. Pellentesque tortor urna, lacinia ac luctus eget, dignissim sed purus. Aliquam luctus, tortor et ullamcorper malesuada, massa leo iaculis justo, sed pretium nibh lorem non ligula. Donec porttitor leo eget lacus suscipit a luctus elit ullamcorper. Maecenas orci odio, pharetra in suscipit ut, tristique eu arcu. Integer vulputate fermentum neque, sit amet sagittis nulla tristique sit amet. Maecenas nibh odio, convallis ut ullamcorper a, luctus at metus. In hac habitasse platea dictumst. Aliquam erat volutpat. Cras id lacus vel erat condimentum sagittis. Praesent vestibulum leo quis arcu malesuada eu porta ligula vehicula. Aliquam ac massa malesuada mi lacinia facilisis. ";

    private static $title = "Lorem Ipsum";

    private static $announcement = "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...";

    /**
    * {@inheritDoc}
    */
    public function load(ObjectManager $manager)
    {
        $data = [
            '0' => 'about',
            '1' => 'help',
            '2' => 'advertisers',
            '3' => 'rules'
        ];
        $name = [
            '0' => 'О портале',
            '1' => 'Написать нам',
            '2' => 'Рекламодателям',
            '3' => 'Пользовательское соглашение'
        ];


        foreach ($data as $key => $link) {
            $document = new Document();
            $document->setName($name[$key]);
            $document->setSort($key);
            $document->setLink($link);
            $document->setH1Title(self::$title . $key);
            $document->setTitle(self::$announcement . $key);
            $document->setText(self::$text . $key);
            $document->setShow(true);
            $document->setViews($key+2);

            $manager->persist($document);
        }
        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }
}