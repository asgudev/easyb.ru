<?php

namespace Easyb\AdminBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class EasybAdminBundle extends Bundle
{
    public function getParent()
    {
        return 'SonataAdminBundle';
    }
}