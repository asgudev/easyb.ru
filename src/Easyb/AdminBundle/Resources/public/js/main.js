/**
 * Created by eu-g-en on 08.05.15.
 */

if (typeof jQuery === 'undefined') {
    throw new Error('Admins main javaScript file requires jQuery')
}

(function($) {
    "use strict";

    // Easyb plugin
    $.easyb = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.easyb');
            return false;
        }
    };

    // Default settings
    var defaults = {
        modalSelector: '[data-easyb="modal"]',
        modalWidth: 550,
        formSelector: '[data-easyb="form"]'
    };

    // Adds event handlers
    $(document).on('click', '[data-easyb="modal-open-link"]', function(event) {
        event.preventDefault();

        var data = $.data(document, 'easyb'), $this = $(this), $modal, $modalContentUrl;

        $modal = $(data.modalSelector);
        $modalContentUrl = $this.data('modal-content-url');

        $modal.dialog({
            autoOpen: false,
            width: data.modalWidth,
            title: 'Доработать',
            modal: true,
            buttons: {
                'Доработать': function () {
                    $.ajax({
                        url: $modalContentUrl,
                        type: 'POST',
                        cache: false,
                        data: $(data.formSelector).serializeArray(),
                        error: function (xhr, status, error) {
                            // Close modal dialog
                            $modal.dialog('close');
                        },
                        success: function (response, status, xhr) {
                            if (4 === xhr.readyState && (200 === xhr.status || 400 === xhr.status)) {
                                if ('ok' === response.status) {
                                    // Refresh modal content
                                    $modal.html(response.content);
                                } else {
                                    // Close modal dialog
                                    $modal.dialog('close');
                                }
                            }
                        }
                    });
                },
                'Отмена': function () {
                    $modal.dialog("close");
                }
            }
        });

        $.ajax({
            url: $modalContentUrl,
            type: 'GET',
            cache: false,
            error: function (xhr, status, error) {
                // Close modal dialog
                $modal.dialog('close');
            },
            success: function (response, status, xhr) {
                if (4 === xhr.readyState && (200 === xhr.status || 400 === xhr.status) && 'ok' === response.status) {
                    // Refresh modal content
                    $modal.html(response.content);
                    // Open modal dialog
                    $modal.dialog('open');
                }
            }
        });
    });

    // Methods
    var methods = {
        init: function (options) {
            if ($.data(document, 'easyb') !== undefined) {
                return;
            }

            // Set plugin data
            $.data(document, 'easyb', $.extend({}, defaults, options || {}));

            return this;
        },
        data: function () {
            return $.data(document, 'easyb');
        }
    };
})(window.jQuery);
