﻿/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	config.language = 'ru';
    config.enterMode = CKEDITOR.ENTER_BR;
	// config.uiColor = '#AADC6E';
    config.toolbar = [
        { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink', '-', 'RemoveFormat' ] }
    ];
    config.removePlugins = 'elementspath';
   // config.removePlugins = 'contextmenu';
    config.extraPlugins = 'wordcount,notification';
    CKEDITOR.config.forcePasteAsPlainText = true;
    
    config.wordcount = {
        showParagraphs: false,
        showWordCount: true,
        showCharCount: true,
        countSpacesAsChars: false,
        countHTML: false,
        minCharCount: 500,
        maxCharCount: 2000,
        filter: new CKEDITOR.htmlParser.filter({
            elements: {
                div: function( element ) {
                    if(element.attributes.class == 'mediaembed') {
                        return false;
                    }
                }
            }
        })
    };
};
