<?php

namespace Easyb\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Easyb\AdminBundle\Form\EventListener\FileFormListener;
use Symfony\Component\PropertyAccess\PropertyPath;
use Symfony\Component\PropertyAccess\PropertyAccess;

class EMFileType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('delete', 'checkbox', array('label' => 'Удалить файл?'));
        $builder->add('file', 'file', array('attr' => $options['attr']));
        $listener = new FileFormListener($options['absolute_path'], $options['field']);
        $builder->addEventSubscriber($listener);
    }

    /**
     * {@inheritDoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $object = $form
            ->getParent()
            ->getViewData();
        $view->vars['imagePath'] = false;
        if ($options['show_path']) {
            $propertyAccessor = PropertyAccess::getPropertyAccessor();
            $propertyPath = new PropertyPath($options['show_path']);

            $view->vars['imagePath'] = $propertyAccessor->getValue($object, $propertyPath);
//            $view->vars['imagePath'] = $propertyPath->getValue($object);
        }
        $view->vars['with_delete'] = $options['with_delete'];
        $view->vars['type'] = $options['type'];
    }

    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setRequired(array(
            'type',
            'absolute_path',
            'show_path',
            'field'
        ));
        $resolver->setDefaults(array(
            'with_delete' => true,
            'show_path' => false
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'form';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'em_file';
    }
}