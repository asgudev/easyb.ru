<?php

namespace Easyb\AdminBundle\Form\EventListener;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\PropertyAccess\PropertyPath;
use Symfony\Component\HttpFoundation\File\File;
/**
 * Resize a collection form element based on the data sent from the client.
 *
 * @author Bernhard Schussek <bernhard.schussek@symfony-project.com>
 */
class FileFormListener implements EventSubscriberInterface
{

    private $propertyPathFile;
    private $propertyPathValue;

    public function __construct($filePath, $value)
    {
        $this->propertyPathFile = new PropertyPath($filePath);
        $this->propertyPathValue = new PropertyPath($value);
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SET_DATA => 'preSetData',
                     FormEvents::SUBMIT       => 'onSubmit',);
    }

    /**
     * @param \Symfony\Component\Form\FormEvent $event
     *
     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
     */
    public function preSetData(FormEvent $event)
    {
        $data = array('file'=> null);
        $event->setData($data);
    }

    /**
     * @param \Symfony\Component\Form\FormEvent $event
     *
     * @return mixed
     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
     */
    public function onSubmit(FormEvent $event)
    {
        $object = $event->getForm()->getParent()->getData();
        $data = $event->getData();
        if ($data['delete']) {
            $files = $this->propertyPathFile->getValue($object);
            $this->deleteFiles($files);
            $this->propertyPathValue->setValue($object, null);
            $data = null;
        } else {
            $data = $data['file'];
        }
        $event->setData($data);
    }

    /**
     * delete uploaded files
     *
     * @param $files
     */
    private function deleteFiles($files)
    {
        if (is_array($files)) {
            foreach ($files as $file) {
                @unlink($file);
            }
        } elseif ($files) {
            @unlink($files);
        }
    }
}