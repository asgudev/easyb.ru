<?php

namespace Easyb\AdminBundle\Extensions\Twig;

use Symfony\Component\Locale\Locale;

/**
 * class EasybAdminTwigExtension
 */
class EasybDateTwigExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            'localeDate' => new \Twig_Filter_Method($this, 'localeDateFilter'),
        );
    }
    /**
     * @param        $date
     * @param bool   $pattern
     * @param string $dateType
     * @param string $timeType
     *
     * @return string
     */
    public function localeDateFilter($date, $pattern = false, $dateType = 'long', $timeType = 'none')
    {
        $values = array(
            'none'   => \IntlDateFormatter::NONE,
            'short'  => \IntlDateFormatter::SHORT,
            'medium' => \IntlDateFormatter::MEDIUM,
            'long'   => \IntlDateFormatter::LONG,
            'full'   => \IntlDateFormatter::FULL,
        );
        $dateFormater = \IntlDateFormatter::create(
            \Locale::getDefault(),
            $values[$dateType],
            $values[$timeType],
            date_default_timezone_get()
        );
        if ($pattern) {
            $dateFormater->setPattern($pattern);
        }

        return $dateFormater->format($date->getTimestamp());
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'my_locale';
    }
}
