<?php
/**
 * This file is part of PhpStorm package.
 *
 * (c) Evgeniy Marchenkov <evgeniy.public@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Easyb\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Sonata\AdminBundle\Controller\CoreController as BaseController;

class CoreController extends BaseController
{
    /**
     * @return Response
     */
    public function dashboardAction()
    {
        $doctrine = $this->getDoctrine();
        $demandsCount = $doctrine->getRepository('EasybAdvertBundle:Demand')->countNonApproved();
        $offersCount = $doctrine->getRepository('EasybAdvertBundle:Offer')->countNonApproved();
        $companyCount = $doctrine->getRepository('EasybUserBundle:Job')->countNonApproved();

        return $this->render($this->getAdminPool()->getTemplate('dashboard'), array(
            'base_template'   => $this->getBaseTemplate(),
            'admin_pool'      => $this->container->get('sonata.admin.pool'),
            'counters' => ['offers' => $offersCount, 'demands' => $demandsCount, 'company' => $companyCount],
            'blocks'          => $this->container->getParameter('sonata.admin.configuration.dashboard_blocks')
        ));
    }
}