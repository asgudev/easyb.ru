<?php

namespace Easyb\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin as BaseAdmin;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Validator\ValidatorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Model\DomainObjectInterface;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Admin\Pool;
use Sonata\AdminBundle\Validator\ErrorElement;

use Sonata\AdminBundle\Translator\LabelTranslatorStrategyInterface;
use Sonata\AdminBundle\Builder\FormContractorInterface;
use Sonata\AdminBundle\Builder\ListBuilderInterface;
use Sonata\AdminBundle\Builder\DatagridBuilderInterface;
use Sonata\AdminBundle\Builder\ShowBuilderInterface;
use Sonata\AdminBundle\Builder\RouteBuilderInterface;
use Sonata\AdminBundle\Route\RouteGeneratorInterface;

use Sonata\AdminBundle\Security\Handler\SecurityHandlerInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Model\ModelManagerInterface;

use Knp\Menu\FactoryInterface as MenuFactoryInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Knp\Menu\MenuItem;


abstract class Admin extends BaseAdmin
{
    /**
     * {@inheritdoc}
     */
    protected $datagridValues = array('_page'        => 1,
        '_per_page'    => 200,);

    /**
     * {@inheritdoc}
     */
    protected $maxPageLinks = 10;

    /**
     * {@inheritdoc}
     */
    protected $maxPerPage = 200;

    /**
     * Predefined per page options
     *
     * @var array
     */
    protected $perPageOptions = array(10,
        30,
        50,
        100,
        150,
        200);


    /**
     * @var array
     */
    protected $helperText = array('edit' => false,
        'show' => false,
        'list' => false);

    protected $usersCount;

    /**
     * {@inheritdoc}
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->predefinePerPageOptions();
        $this->configureHelperTexts();
    }

    /**
     * {@inheritdoc}
     */
    public function getFilterParameters()
    {
        $parameters = parent::getFilterParameters();
        if ($this->hasRequest()) {
            $this->maxPerPage = $parameters['_per_page'];
        }

        return $parameters;
    }

    /**
     * Set custom per page options
     *
     * @param array $options
     */
    public function setPerPageOptions(array $options)
    {
        $this->perPageOptions = $options;
    }

    /**
     * Returns predefined per page options
     *
     * @return array
     */
    public function getPerPageOptions()
    {
        return $this->perPageOptions;
    }

    /**
     * Returns true if the per page value is allowed, false otherwise
     *
     * @param int $perPage
     *
     * @return bool
     */
    public function determinedPerPageValue($perPage)
    {
        return in_array($perPage, $this->perPageOptions);
    }

    /**
     * @param int $perPage
     *
     * @return array
     */
    public function getPerPageParameters($perPage)
    {
        $values = $this->datagrid->getValues();
        $values['_per_page'] = $perPage;

        return array('filter' => $values);
    }

    /**
     * Predefine per page options
     */
    protected function predefinePerPageOptions()
    {
        $this->perPageOptions = array_unique($this->perPageOptions);
        sort($this->perPageOptions);
    }


    /**
     * @param $action
     * @param $text
     */
    public function setHelperText($action, $text)
    {
        $this->helperText[$action] = $text;
    }


    /**
     * @param array $texts
     */
    public function setHelperTexts(array $texts)
    {
        $this->helperText = $texts;
    }


    /**
     * @param $action
     *
     * @return string
     */
    public function getHelperText($action)
    {
        return isset($this->helperText[$action]) ? $this->helperText[$action] : false;
    }


    /**
     * @return array|string
     */
    public function getHelperTexts()
    {
        return $this->helperText;
    }

    /**
     * configure helper texts
     */
    public function configureHelperTexts()
    {
        $this->setHelperTexts($this->buildHelperText($this->helperText));
    }

    /**
     * @param array $helperText
     *
     * @return array
     */
    public function buildHelperText(array $helperText)
    {
        return $helperText;
    }

    /**
     * @return int
     */
    public function getCountUsers()
    {
        return count($this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository('EasybUserBundle:User')->findAll());
    }

    /**
     * @return int
     */
    public function getCountLastWeekUsers()
    {
        return count($this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository('EasybUserBundle:User')->findUsersByLastWeek());
    }
}