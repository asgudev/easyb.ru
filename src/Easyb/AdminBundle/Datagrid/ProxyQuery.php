<?php

namespace Easyb\AdminBundle\Datagrid;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery as ParentProxyQuery;

/**
 * This class try to unify the query usage with Doctrine
 */
class ProxyQuery extends ParentProxyQuery
{
    protected $additinalOrders = array();

    /**
     * {@inheritdoc}
     */
    public function execute(array $params = array(), $hydrationMode = null)
    {
        // always clone the original queryBuilder
        $queryBuilder = clone $this->queryBuilder;

        // todo : check how doctrine behave, potential SQL injection here ...
        if ($this->getSortBy()) {
            $sortBy = $this->getSortBy();
            if (strpos($sortBy, '.') === false) { // add the current alias
                $sortBy = $queryBuilder->getRootAlias() . '.' . $sortBy;
            }
            $queryBuilder->addOrderBy($sortBy, $this->getSortOrder());
        }
        foreach ($this->getAdditionalOrderBy() as $column) {
            if($column['field'] != $sortBy)
            {
                $queryBuilder->addOrderBy($column['field'], isset($column['order']) ? $column['order'] : $this->getSortOrder());
            }
        }
        return $this
          ->getFixedQueryBuilder($queryBuilder)
          ->getQuery()
          ->execute($params, $hydrationMode);
    }

    public function addAdditionalOrderBy($columns)
    {
        $this->additinalOrders = $columns;
    }

    public function getAdditionalOrderBy()
    {
        return $this->additinalOrders;
    }


    /**
     * This method alters the query to return a clean set of object with a working
     * set of Object
     *
     * @param \Doctrine\ORM\QueryBuilder $queryBuilder
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function getFixedQueryBuilder(QueryBuilder $queryBuilder)
    {
        $queryBuilderId = clone $queryBuilder;

        // step 1 : retrieve the targeted class
        $from = $queryBuilderId->getDQLPart('from');
        $class = $from[0]->getFrom();

        // step 2 : retrieve the column id
        $idName = current($queryBuilderId
          ->getEntityManager()
          ->getMetadataFactory()
          ->getMetadataFor($class)
          ->getIdentifierFieldNames());

        // step 3 : retrieve the different subjects id
        $select = sprintf('%s.%s', $queryBuilderId->getRootAlias(), $idName);
        $queryBuilderId->resetDQLPart('select');
        $queryBuilderId->add('select', 'DISTINCT ' . $select);

        //for SELECT DISTINCT, ORDER BY expressions must appear in select list
        /* Consider
            SELECT DISTINCT x FROM tab ORDER BY y;
        For any particular x-value in the table there might be many different y
        values.  Which one will you use to sort that x-value in the output?
        */
        // todo : check how doctrine behave, potential SQL injection here ...
        if ($this->getSortBy()) {
            $sortBy = $this->getSortBy();
            if (strpos($sortBy, '.') === false) { // add the current alias
                $sortBy = $queryBuilderId->getRootAlias() . '.' . $sortBy;
            }
            $sortBy .= ' AS __order_by';
            $queryBuilderId->addSelect($sortBy);

        }
        foreach ($this->getAdditionalOrderBy() as $column) {
            $queryBuilderId->addSelect($column['field']);
        }

        $results = $queryBuilderId
          ->getQuery()
          ->execute(array(), Query::HYDRATE_ARRAY);
        $idx = array();
        $connection = $queryBuilder
          ->getEntityManager()
          ->getConnection();
        foreach ($results as $id) {
            $idx[] = $connection->quote($id[$idName]);
        }

        // step 4 : alter the query to match the targeted ids
        if (count($idx) > 0) {
            $queryBuilder->andWhere(sprintf('%s IN (%s)', $select, implode(',', $idx)));
            $queryBuilder->setMaxResults(null);
            $queryBuilder->setFirstResult(null);
        }

        return $queryBuilder;
    }
}
