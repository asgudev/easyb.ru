<?php

namespace Easyb\SubscribeBundle\Controller;

use Easyb\SubscribeBundle\Entity\Subscriber;
use Easyb\SubscribeBundle\Form\Type\SubscribeType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/subscribe", name="subscribe")
     */
    public function subscribeAction(Request $request)
    {
        $subscriber = new Subscriber();

        $form = $this->createForm(new SubscribeType($this->getDoctrine()), $subscriber);
        $regions = $this->getDoctrine()->getRepository('EasybMainBundle:City')->findRegionsAndSort();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($subscriber);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'subscribe',
                'Вы подписались на рассылку уведомлений о проведении тендеров на Easyb.ru. Спасибо.'
            );
            return $this->redirect('/');
        }

        return $this->render("SubscribeBundle:Default:subscribe.html.twig", [
            'form' => $form->createView(),
            'regions' => $regions,
        ]);
    }
}
