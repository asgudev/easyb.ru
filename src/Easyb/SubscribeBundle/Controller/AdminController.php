<?php
namespace Easyb\SubscribeBundle\Controller;

use Easyb\SubscribeBundle\Entity\Subscriber;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\AdminBundle\Exception\ModelManagerException;

use JMS\SecurityExtraBundle\Annotation\Secure;

use Easyb\UserBundle\Entity\User;
use Easyb\UserBundle\Entity\Job;



class AdminController extends Controller
{
    /**
     * @Route("/admin/subscriber/delete/{id}", name="subscriber_delete")
     * @Secure(roles="ROLE_ADMIN")
     *
     * @return RedirectResponse
     */
    public function deleteSubscriberAction(Subscriber $subscriber)
    {
        $this->container->get('easyb.subscriber.manager')->deleteSubscriber($subscriber);
        return new RedirectResponse($this->generateUrl('admin_easyb_subscribers_list'));
    }

    /**
     * {@inheritdoc}
     */
    public function batchActionSubscriberDelete(ProxyQueryInterface $query)
    {
        $query->select('DISTINCT ' . $query->getRootAlias());
        $em = $this->getDoctrine()->getManager();

        foreach ($query->getQuery()->iterate() as $object) {
            //dump($object); die();
            $subscriber = $object[0];
            $em->remove($subscriber);
        }

        try {
            $em->flush();
        } catch (\Exception $e) {
            throw $e;
        }

        return new RedirectResponse(
            $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters())) . '#to-do'
        );
    }
}