<?php

namespace Easyb\SubscribeBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Easyb\MainBundle\Entity\Category;
use Symfony\Component\Validator\Constraints as Assert;
use Easyb\MainBundle\Entity\City;



/**
 * User
 *
 * @ORM\Table(name="subscribers")
 * @ORM\Entity()
 */
class Subscriber
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $fio
     * @ORM\Column(name="fio", type="string", length=255, nullable=true)
     */
    private $fio;

    /**
     * @var string $fio
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string $fio
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var Category $category
     *
     * @ORM\ManyToMany(targetEntity="Easyb\MainBundle\Entity\Category")
     */
    private $category;

    /**
     * @var Category $category
     *
     * @ORM\ManyToMany(targetEntity="Easyb\MainBundle\Entity\Category")
     * @ORM\JoinTable(name="subscriber_subcategory")
     */
    private $subcategory;

    /**
     * @var City
     *
     * @ORM\ManyToOne(targetEntity="Easyb\MainBundle\Entity\City")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $city;

    /**
     * construct method
     */
    public function __construct()
    {
        $this->subcategory = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set fio
     *
     * @param string $fio
     * @return Subscriber
     */
    public function setFio($fio)
    {
        $this->fio = $fio;

        return $this;
    }

    /**
     * Get fio
     *
     * @return string
     */
    public function getFio()
    {
        return $this->fio;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Subscriber
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Subscriber
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     *
     * @return Subscriber
     */
    public function setCategory(Category $category)
    {
        $this->category[] = $category;

        return $this;
    }

    /**
     *
     * @return Subscriber
     */
    public function addCategory(Category $category)
    {
        $this->category[] = $category;

        return $this;
    }

    /**
     *
     */
    public function removeCategory(Category $category)
    {
        $this->category->removeElement($category);
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     *
     * @return Subscriber
     */
    public function addSubcategory(Category $category)
    {
        $this->subcategory[] = $category;

        return $this;
    }

    /**
     *
     */
    public function removeSubcategory(Category $category)
    {
        $this->subcategory->removeElement($category);
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubcategory()
    {
        return $this->subcategory;
    }

    /**
     * Get city
     *
     * @return \Easyb\MainBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set city
     *
     * @param \Easyb\MainBundle\Entity\City $city
     *
     * @return Job
     */
    public function setCity(City $city = null)
    {
        $this->city = $city;

        return $this;
    }
}
