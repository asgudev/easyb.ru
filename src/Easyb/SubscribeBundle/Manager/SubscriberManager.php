<?php
namespace Easyb\SubscribeBundle\Manager;

use Doctrine\ORM\EntityManager;
use Easyb\SubscribeBundle\Entity\Subscriber;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Validator\Validator;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SubscriberManager
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    /**
     * @var \Symfony\Component\EventDispatcher\EventDispatcher
     */
    private $dispatcher;
    /**
     * @var \Symfony\Component\Validator\Validator
     */
    private $validator;
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repository;

    /**
     * @param \Doctrine\ORM\EntityManager                        $em
     * @param \Symfony\Component\EventDispatcher\EventDispatcher $dispatcher
     * @param \Symfony\Component\Validator\Validator             $validator
     */
    public function __construct(EntityManager $em, EventDispatcher $dispatcher, Validator $validator)
    {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->validator = $validator;
        $this->repository = $em->getRepository('SubscribeBundle:Subscriber');
    }

    public function deleteSubscriber(Subscriber $subscriber)
    {
        $this->em->remove($subscriber);
        $this->em->flush();
    }
}