<?php
namespace Easyb\SubscribeBundle\Admin;

use Easyb\AdminBundle\Admin\Admin as BaseAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;
use Easyb\UserBundle\Entity\Mail;


class SubscribeAdmin extends BaseAdmin
{

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('email', null, array('required' => true,
                'label'    => 'email',
                'disabled' => true
            ))
            ->add('fio', null, array('required' => true,
                'label'    => 'ФИО',
                'disabled' => true
            ))
            ->add('phone', null, array('required' => true,
                'label'    => 'Номер телефона',
                'disabled' => true
            ))
            ->add('category', null, array('required' => true,
                'label'    => 'Категории подписки',
                'disabled' => true,
                'expanded' => true,
                'multiple' => true,
            ))
            ->add('subcategory', null, array('required' => true,
                'label'    => 'Податегории',
                'disabled' => true,
                'expanded' => true,
                'multiple' => true,
            ))

           ;
    }

    public function getModelInstance($class)
    {
        return new $class();
    }


    /**
     * @return array
     */
    public function getBatchActions()
    {
        $actions = array();

        if ($this->hasRoute('delete') && $this->isGranted('DELETE')) {
            $actions['subscriber_delete'] = [
                'label' => 'Удалить из базы подписки',
                'ask_confirmation' => false // If true, a confirmation will be asked before performing the action
            ];
        }

        return $actions;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('email', null, array('label' => 'email'))
            ->add('fio', null, array('label'  => 'ФИО'))
            ->add('phone', null, array('label'  => 'Номер телефона'))
            ->add('city', null, array('label'  => 'Регион'))
            ->add('category', null, array('label' => 'Категории'))
            ->add('subcategory', 'entity', array('label' => 'Податегории'));


        $listMapper->add('_action', 'actions', array(
            'actions' => array(
                //'delete'     => array(),
                'show'     => array(),
            )));
    }

    /**
     * Configuration show view
     *
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
            ->add('email', null, array('label' => 'email'))
            ->add('phone', null, array('label'  => 'Номер телефона'))
            ->add('category', null, array('label' => 'Категории'))
            ->add('subcategory', null, array('label' => 'Податегории'));
    }
}