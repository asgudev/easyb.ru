<?php
namespace Easyb\SubscribeBundle\Form\Type;

use Doctrine\ORM\EntityRepository;

use Symfony\Component\Form\AbstractType;
use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Symfony\Component\Form\CallbackTransformer;

use Easyb\MainBundle\Entity\City;
use Easyb\MainBundle\Entity\Category;
use Easyb\MainBundle\Entity\CategoryRepository;

/**
 * Class JobType
 *
 * @package Easyb\UserBundle\Form\Type\Frontend
 */
class SubscribeType extends AbstractType
{

    private $em;

    public function __construct(Doctrine $doctrine)
    {
        $this->em = $doctrine->getManager();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fio', 'hidden', array('label' => 'Фамилия Имя Отчество',
                'required' => true,
                'attr' => array(
                    'placeholder' => "Фамилия Имя Отчество"
                ),
            ))
            ->add('email', null, array('label' => 'Email адрес',
                'required' => true,
                'attr' => array(
                    'placeholder' => "info@example.com",
                ),
            ))
            ->add('phone', null, array('label' => 'Мобильный телефон',
                'required' => true,
                'attr' => array(
                    'placeholder' => "+7 (999) 999-99-99"
                ),
            ))
            ->add('city', 'hidden', array('label' => 'Город',
                'required' => true,
            ))
            ->add('category', 'entity', array(
                'label' => 'Раздел каталога',
                'class' => 'Easyb\MainBundle\Entity\Category',
                'multiple' => false,
                /*'expanded' => true,*/
                'required' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->where("c.parent = c.id")
                        ->orderBy('c.name', 'ASC');
                }

            ))
        ;

        $formModifier = function (FormInterface $form, Category $category = null) {
            $form->add('subcategory', 'entity', array(
                'class' => 'Easyb\MainBundle\Entity\Category',
                'label' => 'Подкатегория',
                'multiple' => true,
                'expanded' => true,
                'required' => false,
                'query_builder' => function (CategoryRepository $cr) use ($category) {
                    return $cr->createQueryBuilder('c')
                        ->where("c.parent = :category")
                        ->andWhere("c.id != :category")
                        ->setParameter("category", $category)
                        ->orderBy('c.id', 'ASC');
                }
            ));
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                $data = $event->getData();

                $formModifier($event->getForm(), $data->getCategory());
            }
        );

        $builder->get('category')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                $category = $event->getForm()->getData();
                $formModifier($event->getForm()->getParent(), $category);
            }
        );

        $builder
            ->get('city')->addModelTransformer(new CallbackTransformer(
                function ($output) {
                    if ($output != null)
                        return $output->getId();
                    else return 0;
                },
                function ($input) {
                    if ($input != null) {
                        $city = $this->em->getRepository("EasybMainBundle:City")->find($input);
                        return $city;
                    } else return null;
                }));

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array('class' => 'Easyb\SubscribeBundle\Entity\Subscriber'));
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'subscribe';
    }
}