set :application, "Easyb"
set :domain,      "37.143.12.211"
set :deploy_to,   "/vhosts/easyb/www"
set :app_path,    "app"
set :repository,  "git@gitlab.com:eu-g-en/easyb.git"
set :scm,          :git
set :branch, "master"
set :shared_files,      ["app/config/parameters.yml","app/config/sphinx.conf", "app/config/parameters.yml.dist", "app/config/parameters_test.yml", "behat.yml", "web/cache.php", "web/robots.txt"]
set :shared_children,     [app_path + "/logs", app_path + "/spool", "vendor"]
set :use_composer, false
set :update_vendors, false
set :copy_vendor, false
set :dump_assetic_assets, true
set :maintenance_template_path, "app/Resources/maintenance.html.erb"
set :deploy_via,    :rsync_with_remote_cache
set :rsync_options, "-rlcv --delete-during --exclude=parameters.yml --exclude=build.xml --exclude=/.git --exclude=/app/cache --exclude=/app/build -exclude=/app/spool --exclude=/app/logs --exclude=/.gitignore --exclude=/web/upload --exclude=/web/cache.php --exclude=/web/error.log"
set :user, "easyb"
set :clear_controllers,     false
set :controllers_to_clear, ['app_test.php']
set :model_manager, "doctrine"
set :normalize_asset_timestamps, false
set :interactive_mode,      false
role :web,        domain                         # Your HTTP server, Apache/etc
role :app,        domain, :primary => true                          # This may be the same as your `Web` server
role :db,         domain, :primary => true       # This is where Symfony2 migrations will run

set  :keep_releases,  10
set  :use_sudo,      false
ssh_options[:forward_agent] = true
# Be more verbose by uncommenting the following line
# logger.level = Logger::MAX_LEVEL

namespace :symfony do
  desc "Runs apc clear"
  task :apc_clear, :roles => :app, :except => { :no_release => true } do
    capifony_pretty_print "--> Clearing apc cache"
    stream "#{try_sudo} sh -c 'cd #{latest_release} && #{php_bin} #{symfony_console} apc:clear'"
    capifony_puts_ok
  end
  namespace :composer do
    desc "Uses easyb composer version"
      task :get, :on_error => :continue do
           capifony_pretty_print "--> Uses easyb composer.phar version"
               run "cp /vhosts/easyb/www/composer.phar #{latest_release}/composer.phar"
           capifony_puts_ok
      end
  end
end

namespace :web_upload_dir do
  desc "web upload DIR"
  task :add_links do
    unless remote_file_exists?("#{latest_release}/web/upload")
      run "ln -s /vhosts/easyb/www/shared/web/upload/ #{latest_release}/web/upload"
    end
  end
end

#"symfony:apc_clear",
after "deploy", "symfony:doctrine:schema:update", "symfony:doctrine:cache:clear_result",  "deploy:cleanup", "deploy:web:enable"
after "deploy", "web_upload_dir:add_links"
